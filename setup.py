#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import sys
import os
from distutils.core import setup

if 'py2exe' in sys.argv:
    import py2exe
    import sysgrove.modules.masterdata.screens
    import sysgrove.modules.purchase.screens
    import sysgrove.modules.sales.screens
    import sysgrove.modules.help.screens
    import sysgrove.modules.reporting.screens

VERSION = __import__('sysgrove').__version__
LICENCE = __import__('sysgrove').__licence__


data = []
data.append(('imageformats', ['lib/imageformats/qsvg4.dll']))
data.append(('', ['config.dat',
                  'ReleaseNote.txt',
                  'icone.ico',
                  'SysGrove_PG.backup'
                  ]))


# def append_dir(dirname, targetdir):
#     for filename in os.listdir(dirname):
#         f1 = os.path.join(dirname, filename)
#         if os.path.isfile(f1):
#             data.append((targetdir, [f1]))


# for folder in os.listdir('sysgrove/modules/'):
#     f1 = os.path.join('sysgrove/modules/', folder)
#     if os.path.isdir(f1):
#         f2 = os.path.join(f1, 'resources/xml')
#         append_dir(f2, os.path.join(os.path.join(
#             'modules', folder), 'resources/xml'))
# print data

setup(
    name='SysGrove',
    version=VERSION,
    description="""
    SysGrove Open Source ERP
    """,
    author='SysGrove',
    author_email='contact@sysgrove.com',
    license=LICENCE,

    packages=['sysgrove',
              'sysgrove.core',
              'sysgrove.core.document',
              'sysgrove.core.document.ui',
              'sysgrove.ui',
              'sysgrove.ui.classes',
              'sysgrove.ui.classes.mixins',
              'sysgrove.ui.classes.widgets',
              'sysgrove.modules',
              'sysgrove.modules.help',
              'sysgrove.modules.masterdata',
              'sysgrove.modules.masterdata.models',
              'sysgrove.modules.masterdata.screens',
              'sysgrove.modules.masterdata.ui',
              'sysgrove.modules.sales',
              'sysgrove.modules.purchase',
              'sysgrove.modules.log',
              'sysgrove.modules.finance',
              'sysgrove.modules.finance.ui',
              'sysgrove.modules.finance.screens',
              'sysgrove.modules.reporting',
              ],

    package_dir={'': ''},
    package_data={'': ['sysgrove/resources/css/*',
                       'sysgrove/resouces/images/*.*',
                       # 'sysgrove/modules/*/resources/xml/*.xml'
                       ]},
    data_files=data,
    options={
        "py2exe": {"dll_excludes": ["MSVCP90.dll"],
                   "includes": ["sip",
                                "PyQt4.QtXml",
                                "PyQt4.QtSvg",
                                "PyQt4.QtNetwork"
                                ],
                   "packages": ["sqlalchemy",
                                "psycopg2",
                                "xlrd", "xlwt",
                                ],
                   }
    },
    windows=[{'script': 'sysgrove/main.py',
             'icon_resources': [(1, "icone.ico")]
              }]

)
