#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import importlib
from sysgrove.main import init_db
from sysgrove.settings import (
    DG_QUOTATION, DG_CONTRACT, DG_ORDER,
    DG_RORDER, DG_DELIVERY, DG_INVOICE,
    SOLDETO_CODE, SHIPTO_CODE,
    BILLTO_CODE, AGENT_CODE,
    SALES_ID, PURCHASE_ID, TRANSPORT_ID,
    PRODUCTION_ID, FINANCE_ID,
    REPORTING_ID, MDATA_ID,
)
from sysgrove.config import CONFIG
from sysgrove.modules.masterdata.models import DocGroup
from sysgrove.modules.masterdata.models import FlowID
from sysgrove.modules.masterdata.models import UserProfile
from sysgrove.modules.masterdata.models import Menu
from sysgrove.modules.masterdata.models import ThirdPartyType
from sysgrove.modules.masterdata.models import Status
from sysgrove.modules.masterdata.models import PriceCondition


def populate(datas, klass):
    print 'populate %s' % klass

    for values in datas:
        data = klass(**values)
        data.save()


def populate_userProfile():
    root = UserProfile(
        code=u'ROOT',
        description=u'root',
        firstName=u'root',
        password='pwd',
        rights=Menu.query.all()
    )
    root.save()
    guest = UserProfile(
        code=u'GUEST',
        description=u'guest',
        firstName=u'guest',
        # password='guest',
        rights=Menu.query.all()
    )
    guest.save()


def populate_menu():

    def addChild(parent, child):
        code, name, subentry = child[:3]
        model = None
        docGroup = -1
        if len(child) == 4:
            model = child[3]
        elif len(child) == 5:
            docGroup = child[4]
        desc = u"%s - %s" % (code, name)
        tree = Menu(code=unicode(code),
                    description=desc,
                    parentMenu_id=parent.id,
                    flowID=parent.flowID)
        if model:
            tree.model = model
        if docGroup > 0:
            tree.docGroup_id = docGroup
        else:
            tree.docGroup = parent.docGroup
        tree.save()  # to have a pk
        for entry in subentry:
            addChild(tree, entry)
        tree.save()  # to save children list

    for pack in CONFIG['modules'].values():
        mm = importlib.import_module(pack)
        data = mm.data
        if not data.submenu == []:
            flowid = FlowID.query.filter_by(code=data.flowId).one()
            menu = Menu(code=unicode(data.flowId),
                        description=data.verbose,
                        flowID=flowid)
            menu.save()  # to have a pk
            for entry in data.submenu:
                addChild(menu, entry)
            menu.save()


status = [{  # 1
    'code': u'OPEN',
    'description': u'Open Document',
    'internalFlag': False,
    'headerOnlyFlag': False,
    'cancelLikeFlag': False,
}, {  # 2
    'code': u'EXEC',
    'description': u'Executed',
    'internalFlag': True,
    'headerOnlyFlag': False,
    'cancelLikeFlag': False,
}, {  # 3
    'code': u'PARTEXEC',
    'description': u'Partially Executed',
    'internalFlag': True,
    'headerOnlyFlag': False,
    'cancelLikeFlag': False,
}, {  # 4
    'code': u'CANCEL',
    'description': u'Canceled',
    'internalFlag': False,
    'headerOnlyFlag': False,
    'cancelLikeFlag': True,
}, {  # 5
    'code': u'CLOSED',
    'description': u'Closed',
    'internalFlag': False,
    'headerOnlyFlag': True,
}, {
    'code': u'STRING',
    'description': u'String',
    'internalFlag': False,
    'headerOnlyFlag': False,
    'cancelLikeFlag': True,
}, {
    'code': u'CIRCLE',
    'description': u'Circle',
    'internalFlag': False,
    'headerOnlyFlag': False,
    'cancelLikeFlag': True,
}, {
    'code': u'WASHOUT',
    'description': u'Washed-Out Document',
    'internalFlag': False,
    'headerOnlyFlag': False,
    'cancelLikeFlag': True,
}]

docgroup = [{
    'code': unicode(DG_QUOTATION),
    'description': u'Quotation',
}, {
    'code': unicode(DG_CONTRACT),
    'description': u'Contract',
}, {
    'code': unicode(DG_ORDER),
    'description': u'Order',
}, {
    'code': unicode(DG_RORDER),
    'description': u'Release Order',
}, {
    'code': unicode(DG_DELIVERY),
    'description': u'Delivery',
}, {
    'code': unicode(DG_INVOICE),
    'description': u'Invoice',
}]

flowid = [{
    'code': unicode(SALES_ID),
    'description': u'Sales',
}, {
    'code': unicode(PURCHASE_ID),
    'description': u'Purchasing',
}, {
    'code': unicode(MDATA_ID),
    'description': u'Master Data',
}, {
    'code': unicode(FINANCE_ID),
    'description': u'Finance',
}, {
    'code': unicode(REPORTING_ID),
    'description': u'Reporting',
}]


thirdpartytype = [{
    'code': unicode(SOLDETO_CODE),
    'description': u'Sold-to Party',
}, {
    'code': unicode(SHIPTO_CODE),
    'description': u'Ship-to Party',
}, {
    'code': unicode(BILLTO_CODE),
    'description': u'Bill-to Party',
}, {
    'code': unicode(AGENT_CODE),
    'description': u'Agent',
}]

# some priceCondition are mandatory to be able to compute
# prices
pricecondition = [{  # 1
    'calculationType': u'Absolute',
    'code': u'PRB',
    'description': u'Basic Price',
    'typeCondition': u'Price',
    'serviceFlag': False,
}, {  # 2
    'calculationType': u'Percentage',
    'code': u'VAT',
    'description': u'VAT',
    'typeCondition': u'VAT',
    'serviceFlag': False,
}, {  # 3
    'calculationType': u'Absolute',
    'code': u'FTPR',
    'description': u'Future Price',
    'typeCondition': u'Price',
    'serviceFlag': False,
    'futureType': 'Future',
}, {  # 4
    'calculationType': u'Absolute',
    'code': u'PRM',
    'description': u'Premium',
    'typeCondition': u'Price',
    'serviceFlag': False,
    'futureType': 'Premium',
}, {  # 5
    'calculationType': u'Absolute',
    'code': u'FEX',
    'description': u'Forex (Foreign Exchange)',
    'typeCondition': u'Price',
    'serviceFlag': False,
    'futureType': 'Forex',
}, {  # 6
    'calculationType': u'Absolute',
    'code': u'SPD',
    'description': u'Spread',
    'typeCondition': u'Price',
    'serviceFlag': False,
    'futureType': 'Spread',

}]

init_db(True)
populate(status, Status)
populate(flowid, FlowID)
populate(docgroup, DocGroup)
populate_menu()
populate_userProfile()
populate(thirdpartytype, ThirdPartyType)
populate(pricecondition, PriceCondition)
