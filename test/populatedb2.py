#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import datetime
# Do NOT remove the followinf line, it will initiated
# the root user and the Menu
from test.initdb import populate
from sysgrove.settings import SOLDETO_CODE, BILLTO_CODE, SHIPTO_CODE, EVER
from sysgrove.modules.masterdata.models import TaxType, TransportType
from sysgrove.modules.masterdata.models import Address, Country, Currency
from sysgrove.modules.masterdata.models import Group, Site, Company
from sysgrove.modules.masterdata.models import Contact, ContactItem
from sysgrove.modules.masterdata.models import DocNumbering, DocType
from sysgrove.modules.masterdata.models import DocCopyRule, DateType, DocGroup
from sysgrove.modules.masterdata.models import ItemFamily, ItemGroup
from sysgrove.modules.masterdata.models import ThirdParty, PaymentTerm
from sysgrove.modules.masterdata.models import Incoterm, IncotermText, Lot
from sysgrove.modules.masterdata.models import PriceCondition
from sysgrove.modules.masterdata.models import PriceSchemaHeader
from sysgrove.modules.masterdata.models import PriceSchemaDetail
from sysgrove.modules.masterdata.models import ItemDetail
from sysgrove.modules.masterdata.models import ThirdPartyType
from sysgrove.modules.masterdata.models import UnitOfMeasure
from sysgrove.modules.masterdata.models import Warehouse


def populate_simple(klass, values):
    for v, desc in values.iteritems():
        try:
            value = klass(code=v, description=desc)
            value.save()
        except ValueError:
            pass


def populate_class(pref, klass, nb=20):
    values = {}
    for i in range(nb):
        code = "%s%s" % (pref, i)
        values[code] = u"description for %s" % code.decode('utf-8')
    populate_simple(klass, values)


# Populate LOT
populate_lot = lambda: populate_class('LOT', Lot)


country = [{
    'code': u'FR',
    'description': u'France',
}, {
    'code': u'IT',
    'description': u'Italy',
}]

address = [{
    'addressLine1': u'2 Bd Fayolle',
    'addressLine2': u'',
    'city': u'Le Puy en Velay',
    'country_id': 1,
    'zIPCode': u'43000'
}, {
    'addressLine1': u'2 Bd Fayolle',
    'addressLine2': u'',
    'city': u'Le Puy en Velay',
    'country_id': 1,
    'zIPCode': u'43000'
}, {
    'addressLine1': u'Via Garibaldi 129',
    'addressLine2': u'',
    'city': u'Usini (SS)',
    'country_id': 2,
    'zIPCode': u'07049'
}, {
    'addressLine1': u'2 Bd Fayolle',
    'addressLine2': u'',
    'city': u'Le Puy en Velay',
    'country_id': 1,
    'zIPCode': u'43000'
}, {
    'addressLine1': u'99 Rue de Rivoli',
    'addressLine2': u'',
    'city': u'Paris',
    'country_id': 1,
    'zIPCode': u'75001'
}, {
    'addressLine1': u'Via Garibaldi 129',
    'addressLine2': u'',
    'city': u'Usini (SS)',
    'country_id': 2,
    'zIPCode': u'07049'
}, {
    'addressLine1': u'Piazza Kennedy 3',
    'addressLine2': u'',
    'city': u'Ravenna',
    'country_id': 2,
    'zIPCode': u'48100'
}, {
    'addressLine1': u'Main Street',
    'addressLine2': u'',
    'city': u'Your city',
    'country_id': 1,
    'zIPCode': u'48100'
}, {
    'addressLine1': u'Main Street',
    'addressLine2': u'',
    'city': u'My city',
    'country_id': 1,
    'zIPCode': u'48100'
}, {
    'addressLine1': u'Main Street',
    'addressLine2': u'',
    'city': u'My city',
    'country_id': 1,
    'zIPCode': u'48100'
}, {
    'addressLine1': u'Main Street place',
    'addressLine2': u'',
    'city': u'My city',
    'country_id': 1,
    'zIPCode': u'48102'
}, {
    'addressLine1': u'Piazza Pietro Mennea',
    'addressLine2': u'',
    'city': u'Citta Bella',
    'country_id': 2,
    'zIPCode': u'01001',
}, ]

group = [{
    "code": u"SG01",
    "address_id": 1,
    "description": u"SysGrove International"
}]

company = [{
    'address_id': 2,
    'code': u'FR01',
    'description': u'SysGrove France',
    'currency_id': 1,
    'group_id': 1
}, {
    'address_id': 3,
    'code': u'IT01',
    'description': u'SysGrove Italia',
    'currency_id': 1,
    'group_id': 1
}]

currency = [{
    'code': u'EUR',
    'description': u'Euro',
}, {
    'code': u'USD',
    'description': u'US Dollar',
}]


taxtype = [{
    'code': u'FVAT',
    'description': u'Full VAT',
}, {
    'code': u'RVAT',
    'description': u'Reduced VAT',
}, {
    'code': u'EVAT',
    'description': u'VAT Exempt',
}, {
    'code': u'ZVAT',
    'description': u'0% VAT',
}]


site = [{
    'address_id': 4,
    'company_id': 1,
    'code': u'LPY',
    'description': u'Le Puy en Velay',
    'taxType_id': 1
}, {
    'address_id': 5,
    'company_id': 1,
    'code': u'PAR',
    'description': u'Paris',
    'taxType_id': 1
}, {
    'address_id': 6,
    'company_id': 2,
    'code': u'USI',
    'description': u'Usini',
    'taxType_id': 3
}, {
    'address_id': 7,
    'company_id': 2,
    'code': u'RAV',
    'description': u'Ravenna',
    'taxType_id': 3}]


docnumber = [{
    'code': u'A1',
    'description': u'Sales Quotation',
}, {
    'code': u'A2',
    'description': u'Sales Contract',
}, {
    'code': u'A3',
    'description': u'Sales Order',
}, {
    'code': u'A4',
    'description': u'Sales Release Order',
}, {
    'code': u'A5',
    'description': u'Sales Delivery',
}, {
    'code': u'A6',
    'description': u'Sales Invoice',
}]


doctype = [{
    'docGroup_id': 3,
    'docNumbering_id': 4,
    'code': u'SO',
    'description': u'Sales Order',
    'flowID_id': 1,
    'intraGroupFlag': True,
    'stockFlag': True,
    'priceHeader_id': 2,
    'priceDetail_id': 1,
}, {
    'docGroup_id': 4,
    'docNumbering_id': 5,
    'code': u'SRO',
    'description': u'Sales Release Order',
    'flowID_id': 1,
    'intraGroupFlag': True,
    'stockFlag': True,
    'priceHeader_id': 2,
    'priceDetail_id': 1,
}, {
    'docGroup_id': 5,
    'docNumbering_id': 6,
    'code': u'SDEL',
    'description': u'Sales Delivery',
    'flowID_id': 1,
    'intraGroupFlag': False,
    'stockFlag': True,
    'priceHeader_id': 2,
    'priceDetail_id': 1,
}, {
    'docGroup_id': 6,
    'docNumbering_id': 7,
    'code': u'SINV',
    'description': u'Sales Invoice',
    'flowID_id': 1,
    'intraGroupFlag': True,
    'stockFlag': True,
    'priceHeader_id': 2,
    'priceDetail_id': 1,
}]


doccopy = [{
    'docGroupOrigin_id': 3,
    'docGroupTarget_id': 3,
    'docTypeCodeOrigin_id': 1,
    'docTypeCodeTarget_id': 1,
    'flowIDOrigin_id': 1,
    'flowIDTarget_id': 1,
    'siteOrigin_id': 1,
    'siteTarget_id': 1
}, {
    'docGroupOrigin_id': 3,
    'docGroupTarget_id': 4,
    'docTypeCodeOrigin_id': 1,
    'docTypeCodeTarget_id': 3,
    'flowIDOrigin_id': 1,
    'flowIDTarget_id': 1,
    'siteOrigin_id': 1,
    'siteTarget_id': 1
}, {
    'docGroupOrigin_id': 3,
    'docGroupTarget_id': 5,
    'docTypeCodeOrigin_id': 3,
    'docTypeCodeTarget_id': 4,
    'flowIDOrigin_id': 1,
    'flowIDTarget_id': 1,
    'siteOrigin_id': 1,
    'siteTarget_id': 1
}, {
    'docGroupOrigin_id': 3,
    'docGroupTarget_id': 6,
    'docTypeCodeOrigin_id': 4,
    'docTypeCodeTarget_id': 5,
    'flowIDOrigin_id': 1,
    'flowIDTarget_id': 1,
    'siteOrigin_id': 1,
    'siteTarget_id': 1
}]


docGroup_Site_Alloc = [
    {'DocGroup_pk': 1, 'Site_pk': 1},
    {'DocGroup_pk': 1, 'Site_pk': 2},
    {'DocGroup_pk': 1, 'Site_pk': 3},
    {'DocGroup_pk': 1, 'Site_pk': 4},
    {'DocGroup_pk': 2, 'Site_pk': 1},
    {'DocGroup_pk': 2, 'Site_pk': 2},
    {'DocGroup_pk': 2, 'Site_pk': 3},
    {'DocGroup_pk': 2, 'Site_pk': 4},
    {'DocGroup_pk': 3, 'Site_pk': 1},
    {'DocGroup_pk': 3, 'Site_pk': 2},
    {'DocGroup_pk': 3, 'Site_pk': 3},
    {'DocGroup_pk': 3, 'Site_pk': 4},
    {'DocGroup_pk': 4, 'Site_pk': 1},
    {'DocGroup_pk': 4, 'Site_pk': 2},
    {'DocGroup_pk': 4, 'Site_pk': 3},
    {'DocGroup_pk': 4, 'Site_pk': 4},
    {'DocGroup_pk': 5, 'Site_pk': 1},
    {'DocGroup_pk': 5, 'Site_pk': 2},
    {'DocGroup_pk': 5, 'Site_pk': 3},
    {'DocGroup_pk': 5, 'Site_pk': 4},
    {'DocGroup_pk': 6, 'Site_pk': 1},
    {'DocGroup_pk': 6, 'Site_pk': 2},
    {'DocGroup_pk': 6, 'Site_pk': 3},
    {'DocGroup_pk': 6, 'Site_pk': 4}
]

docType_Site_Alloc = [
    {'DocType_pk': 1, 'Site_pk': 1},
    {'DocType_pk': 2, 'Site_pk': 1},
    {'DocType_pk': 3, 'Site_pk': 1},
    {'DocType_pk': 4, 'Site_pk': 1},
]


itemfamily = [{
    'analysisFlag': True,
    'code': u'FP',
    'description': u'Finished Product',
    'productionFlag': True,
    'stockFlag': True
}, {
    'analysisFlag': True,
    'code': u'RM',
    'description': u'Raw Material',
    'productionFlag': False,
    'stockFlag': True
}, {
    'analysisFlag': True,
    'code': u'INT',
    'description': u'Production Intermediate',
    'productionFlag': True,
    'stockFlag': True
}, {
    'analysisFlag': False,
    'code': u'SERV',
    'description': u'Service',
    'productionFlag': False,
    'stockFlag': False,
    'serviceFlag': True

}]

contactItem = [{
    'contact_id': 1,
    'email': u'john.doe@mail.com',
    'fax': u'',
    'name': u'John Doe',
    'phone': u'+33 6123456'
}, {
    'contact_id': None,
    'email': u'john@smith.com',
    'fax': u'',
    'name': u'John Smith',
    'phone': u'+44361564532'
}, {
    'contact_id': None,
    'email': u'mrsalicia@butterfly.com',
    'fax': u'',
    'name': u'Alicia Butterfly',
    'phone': u''
}, {
    'contact_id': 5,
    'email': u'John@junior.it',
    'fax': u'',
    'name': u'John Smith Jr',
    'phone': u''
}, {
    'contact_id': 5,
    'email': u'mary@doe.com',
    'fax': u'',
    'name': u'Mary Doe',
    'phone': u''
}]

datetype = [{
    'attributName': u'documentheader.documentdate',
    'code': u'CREATEDOC',
    'description': u'Document date creation',
    'docGroup_id': 3
}, {
    'attributName': u'documentheader.documentdate',
    'code': u'INVCRDATE',
    'description': u'DocumentCreation Date',
    'docGroup_id': 3
}]


itemgroup = [{
    'itemFamily_id': 1,
    'code': u'WH',
    'description': u'Wheat',
}, {
    'itemFamily_id': 1,
    'code': u'WSU',
    'description': u'White Sugar',
}, {
    'itemFamily_id': 1,
    'code': u'RSU',
    'description': u'Raw Sugar',
}, {
    'itemFamily_id': 1,
    'code': u'CO',
    'description': u'Corn',
}]

thirdpartytype = [{
    'code': u'1',
    'description': u'Sold-to Party',
}, {
    'code': u'2',
    'description': u'Ship-to Party',
}, {
    'code': u'3',
    'description': u'Bill-to Party',
}, {
    'code': u'4',
    'description': u'Agent',
}]


paymentterm = [{
    'dateType_id': 2,
    'numberDays': 30,
    'code': u'30ID',
    'description': u'30 Days Invoice Date',
    'selectDayCheckBox': False,
}, {
    'dateType_id': 1,
    'numberDays': 0,
    'code': u'CASH',
    'description': u'Cash in Advance',
    'selectDayCheckBox': False,
}]


incoterm = [{
    'code': u'FOB',
    'description': u'Free On Board',
}, {
    'code': u'CIF',
    'description': u'Cost, Insurance and Freight',
}, {
    'code': u'EXW',
    'description': u'Ex-Works',
}, {
    'code': u'FCA',
    'description': u'Free Carrier',
}, {
    'code': u'CPT',
    'description': u'Carriage Paid To',
}, {
    'code': u'CIP',
    'description': u'Carriage and Isurance Paid to',
}, {
    'code': u'DDP',
    'description': u'Delivered Duty Paid',
}, {
    'code': u'DDU',
    'description': u'Delivered Duty Unpaid',
}, {
    'code': u'CFR',
    'description': u'Cost and Freight',
}, {
    'code': u'FAS',
    'description': u'Free Alongside Ship',
}]


soldTo = ThirdPartyType.query.filter_by(id=SOLDETO_CODE).one()
shipTo = ThirdPartyType.query.filter_by(id=SHIPTO_CODE).one()
billTo = ThirdPartyType.query.filter_by(id=BILLTO_CODE).one()

thirdparty = [{
    'address_id': 8,
    'blockFlag': False,
    'contact_id': 1,
    'incoterm_id': 3,
    'intragroup': False,
    'paymentTerm_id': 1,
    'serviceProviderFlag': False,
    'taxType_id': 1,
    'code': u'1',
    'description': u'My Customer',
    'thirdPartyTypes': [soldTo, shipTo, billTo],
    'validTillDate': datetime.date.today(),
    'validFromDate': EVER,
}, {
    'address_id': 12,
    'blockFlag': False,
    'contact_id': 5,
    'incoterm_id': 7,
    'intragroup': False,
    'paymentTerm_id': 2,
    'taxType_id': 4,
    'code': u'4',
    'description': u'Your Customer',
    'thirdPartyTypes': [soldTo, shipTo, billTo],
    'validTillDate': datetime.date.today(),
    'validFromDate': EVER,
}]


incotermText = [{
    'code': u'EXW01',
    'description': u'Test text for Incoterm EXW',
    'incoterm_id': 3,
}]


pricecondition = [{  # 7 some PriceCondiotn are defined in initdb.py
    'calculationType': u'Sum',
    'code': u'FPR',
    'description': u'Final Price',
    'typeCondition': u'Price',
    'serviceFlag': False,
}, {  # 8
    'calculationType': u'Sum',
    'code': u'LINETOTAL',
    'description': u'Total from document line',
    'typeCondition': u'Price',
    'serviceFlag': False,
}, {  # 9
    'calculationType': u'Absolute',
    'code': u'COM',
    'description': u'Agent Commission',
    'typeCondition': u'Commission',
    'serviceFlag': True,
}, {  # 10
    'calculationType': u'Absolute',
    'code': u'INSURANCE',
    'description': u'Insurance',
    'typeCondition': u'Price',
    'serviceFlag': True,
}]


priceschemaheader = [{
    'code': u'FLPSFR',
    'description': u'Flat Price Schema for Document Line -  France',
}, {
    'code': u'PSHEADFLAT',
    'description': u'Flat Price Schema -  France',
}]


priceschemadetail = [{  # detail for FLPSFR
    'step': 10,
    'priceCondition_id': 1,
    'priceSchemaHeader_id': 1,
    'action': "",
}, {
    'step': 20,
    'priceCondition_id': 2,
    'conditionValue': 19.6,
    'action': "#10",
    'priceSchemaHeader_id': 1,
}, {
    'step': 30,
    'priceCondition_id': 8,
    'action': "#10 + #20",
    'priceSchemaHeader_id': 1,

}, {  # detail for PSHEADFLAT
    'step': 10,
    'priceCondition_id': 8,
    'priceSchemaHeader_id': 2,
    'action': "",
}, {
    'step': 20,
    'priceCondition_id': 10,
    'priceSchemaHeader_id': 2,
    'action': "",
}, {
    'step': 30,
    'priceCondition_id': 7,
    'action': "#10 + #20",
    'priceSchemaHeader_id': 2,
}]

transportype = [{
    'code': u'AIR',
    'description': u'Air Shipment',
}, {
    'code': u'SEA',
    'description': u'Sea Shipment',
}, {
    'code': u'ROAD',
    'description': u'Raod Shipment',
}, {
    'code': u'EXP',
    'description': u'Express Shipment',
}, {
    'code': u'RAIL',
    'description': u'Railway Shipment',
}]


items = [{
    'code': u'POP',
    'description': u'pop',
    'externalReference': u'pop external reference',
    'itemGroup_id': 4,   # corn
    'itemPrintedDescription': u'Pop printed description',
    'printedComment': u'Pop printed Comments',
    'country_id': 2,
    'productionPrice': 2.8
}, {
    'code': u'CANDY',
    'description': u'candy',
    'externalReference': u'candy external reference',
    'itemGroup_id': 3,   # raw sugar
    'itemPrintedDescription': u'candy printed description',
    'printedComment': u'candy printed Comments',
    'country_id': 2,
    'productionPrice': 67.8

}]


uom = [{
    'code': u'KG',
    'description': u'Kilo',
    'masterUoM': True
}]

warehouse = [{
    'code': 'MTP',
    'description': u'Montpellier',
}]


def populate_contact():
    for c in range(8):
        contact = Contact()
        contact.save()


def populate_site_alloc(data, klass):
    pk = klass.__tablename__ + '_pk'
    site_pk = 'Site_pk'
    for d in data:
        cur = klass.query.filter_by(id=d[pk]).one()
        site = Site.query.filter_by(id=d[site_pk]).one()
        cur.sites.append(site)
        cur.save()


populate(country, Country)
populate(address, Address)
populate(group, Group)
populate(currency, Currency)

populate(company, Company)
populate(taxtype, TaxType)
populate(site, Site)
populate(docnumber, DocNumbering)
populate(priceschemaheader, PriceSchemaHeader)
populate(priceschemadetail, PriceSchemaDetail)

populate(doctype, DocType)
populate(doccopy, DocCopyRule)
populate_site_alloc(docGroup_Site_Alloc, DocGroup)
populate_site_alloc(docType_Site_Alloc, DocType)

populate(datetype, DateType)
populate(itemfamily, ItemFamily)
populate(itemgroup, ItemGroup)

populate_contact()
populate(contactItem, ContactItem)

populate(paymentterm, PaymentTerm)
populate(incoterm, Incoterm)
populate(thirdparty, ThirdParty)
populate(incotermText, IncotermText)
populate(pricecondition, PriceCondition)

populate(transportype, TransportType)

populate_lot()
populate(items, ItemDetail)
populate(uom, UnitOfMeasure)
populate(warehouse, Warehouse)
