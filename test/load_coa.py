#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

import csv
from sysgrove.modules.masterdata.models import ChartOfAccount
from sysgrove.modules.masterdata.models import Ledger
import pdb

fr_coa = ChartOfAccount.query.filter_by(code=u"FRA01").one()
it_coa = ChartOfAccount.query.filter_by(code=u"ITA01").one()

italian_leger = [
    ("PURCHASE", "90"),
    ("SALES", "80"),
    ("THIRD PARTIES", "30"),
    ("TAXES", "76"),
]

for l in italian_leger:
    ledger = Ledger(description=l[0],
                    code1=l[1],
                    code=l[1],
                    chartOfAccount=it_coa)
    ledger.save()


# The database has to free frm French Ledger account
with open('test/fr_coa.csv', 'rb') as f:
    reader = csv.reader(f)
    for row in reader:
        print type(row), row
        if not row == []:
            # pdb.set_trace()
            code = row[0]

            desc = row[1]
            if desc[0] == ' ':
                desc = desc[1:]
            ledger = Ledger(description=desc,
                            lclass=code[0],
                            code1=code[1],
                            code=code,
                            chartOfAccount=fr_coa)
            i = 2
            for c in code[2:]:
                setattr(ledger, 'code%s' % i, code[i])
                i = i + 1
            # for c in range(i, 6):
            #     setattr(ledger, 'code%s' % c, '0')
            # ledger.code = ''.join([ledger.lclass, ledger.code1, ledger.code2,
            #                        ledger.code3, ledger.code4, ledger.code5])
            ledger.save()
