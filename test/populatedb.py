#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import random
import importlib
from sysgrove.settings import (
    SOLDETO_CODE,
    BILLTO_CODE,
    SHIPTO_CODE,
    AGENT_CODE
)
from sysgrove.main import db_session
from sysgrove.config import CONFIG
from sysgrove.main import init_db
from sysgrove.modules.masterdata.models import Language
from sysgrove.modules.masterdata.models import (
    Group,
    Company,
    Currency,
    Site,
    FlowID,
    DocGroup,
    DocType,
    TaxType,
    Status, WeekDays,
    Lot, Packing,
    ThirdPartyType,
    ThirdParty,
    Incoterm, IncotermText,
    ContactItem,
    Contact,
    Address,
    Country,
    PaymentTerm, PaymentTermText,
    ExchangeRate,
    UnitOfMeasure,
    DocNumbering,
    Menu,
    Market,
    ItemFamily, ItemGroup, ItemDetail,
    UserProfile, TransportType
)
from faker import Factory
fake = Factory.create()


def populate(klass, values):
    for v, desc in values.iteritems():
        try:
            value = klass(code=v, description=desc)
            value.save()
        except ValueError:
            pass


def populate_class(pref, klass, nb=20):
    values = {}
    for i in range(nb):
        code = "%s%s" % (pref, i)
        values[code] = u"description for %s" % code.decode('utf-8')
    populate(klass, values)


# Populate Group
populate_group = lambda: populate_class('GR', Group)

# Populate Language
populate_lang = lambda: populate_class('LANG', Language)

# Populate Compagnies
populate_comp = lambda: populate_class('COM', Company)

# Populate Sites
populate_site = lambda: populate_class('SI', Site)

# Populate Lot
populate_lot = lambda: populate_class('LOT', Lot)

# Packing
populate_packing = lambda: populate_class('PACK', Packing)


# Populate TaxType
vattype = {
    'ZVAT': u'0% VAT',
    'EVAT': u'VAT Exempted',
    'RVAT':  u'Reduced VAT',
    'FVAT': u'Full VAT',
}
populate_taxtype = lambda: populate(TaxType, vattype)

# Populate Currency
currency = {
    'EUR': u'Euro',
    'USD': u'US Dollar',
    'ZL': u'Zloty',
    'ZAR': u'South African Rand'
}
populate_currency = lambda: populate(Currency, currency)

 # Status
status = {
    'ACTIVE': u'Active',
    'CANCELLED': u'Cancelled',
    'USELESS': u'Useless'
}
populate_status = lambda: populate(Status, status)


# UnitOfMeasure
uom = {
    'KG': u'kilogram',
    'ST': u'ST descriptio,',
    'MT': u'MT description',
    'KS': u'KS description',
}
populate_unitOfMeasure = lambda: populate(UnitOfMeasure, uom)

# WeekDays
days = {
    'MO': u'Monday',
    'TU': u'Tuesday',
    'WE': u'Wednesday',
    'TH': u'Thursday',
    'FR': u'Friday',
    'SA': u'Saturday',
    'SU': u'Sunday'
}
populate_days = lambda: populate(WeekDays, days)

# Market
market = {
    'LD': u'London',
    'NY': u'New-York',
}
populate_market = lambda: populate(Market, market)


# Transport
trans = {
    'AIR': u'Air',
    'ROA': u'Road',
    'SEA': u'Sea',
    'RAI': u'Rail'
}
populate_transport = lambda: populate(TransportType, trans)


def populate_docNumbering():
    docNumbering = {
        'A1': u'Sales Order Type',
        'A2': u'Sales Delivery Type',
        'A3': u'Sales Invoicing',
        'P1': u'Internal Production'
    }
    for (k, v) in docNumbering.iteritems():
        dn = DocNumbering(code=k, description=v, documentNumber=0)
        dn.save()


def package_to_name(package):
    sp = package.split('.')
    return sp[len(sp) - 1]


def populate_flowId():
    flowId = {}
    for (code, pack) in CONFIG['modules'].iteritems():
        flowId[str(code)] = unicode(package_to_name(pack))
    populate(FlowID, flowId)


def populate_docGroup():
    groups = [
        u"Quotation",
        u"Contact",
        u"Order",
        u"Release Order",
        u"Delivery",
        u"Invoice",
        u"Production"
    ]
    docGroup = {}
    for i in range(7):
        docGroup[str(i + 1)] = groups[i]
    populate(DocGroup, docGroup)


tpt = {
    SOLDETO_CODE: u'Ordering Party',
    BILLTO_CODE: u'Invoicing Party',
    SHIPTO_CODE: u'Delivery Party',
    AGENT_CODE: u'agent'
}
populate_thirdPartyType = lambda: populate(ThirdPartyType, tpt)


# incoterm
incoterm = {
    'FOB': u'Freight On Board',
    'CIF': u'Cost, Insurance & Freight',
    'EXS': u'Ex-Silo',
}
populate_incoterm = lambda: populate(Incoterm, incoterm)


# PaymentTerm
paymentTerm = {
    '30ID': u'30 Days Invoicing Date',
    '15ODMD': u'15 from Ordering Date on Monday',
    '60EM': u'60 Days ending of the Month',
}
populate_paymentTerm = lambda: populate(PaymentTerm, paymentTerm)


def populate_company_finish():
    gr_all = Group.query.all()
    eu = Currency.query.filter_by(code='EUR').one()
    dl = Currency.query.filter_by(code='USD').one()
    query = Company.query.all()
    mid = len(query) / 2

    for comp in query[0:mid]:
        comp.group = gr_all[3]
        comp.currency = eu
    for comp in query[mid:len(query)]:
        comp.group = gr_all[4]
        comp.currency = dl
    db_session.commit()


def populate_site_finish():
    all_comp = Company.query.all()
    nb_comp = len(all_comp)
    i = 0
    for s in Site.query.all():
        s.company = all_comp[i % nb_comp]
        i = i + 1


def populate_docType():
    sale = FlowID.query.filter_by(code=unicode(1)).one()
    order = DocGroup.query.filter_by(code=unicode(3)).one()
    delivery = DocGroup.query.filter_by(code=unicode(5)).one()
    invoicing = DocGroup.query.filter_by(code=unicode(6)).one()
    rorder = DocGroup.query.filter_by(code=unicode(4)).one()
    group = [order, delivery, invoicing, rorder]
    for g in group:
        code = 'SO1_%s_%s' % (g.code, sale.code)
        dt1 = DocType(
            code=code,
            description=u'description for %s' % code.decode('utf-8'),
            flowID=sale,
            docGroup=g,
            stockFlag=True,
            intraGroupFlag=False,
            docNumbering=DocNumbering.query.filter_by(code='A1').one(),
        )
        dt1.save()
        dt2 = DocType(
            code='SS14_%s_%s' % (g.code, sale.code),
            description=u'description',
            flowID=sale,
            docGroup=g,
            stockFlag=True,
            docNumbering=DocNumbering.query.filter_by(code='A2').one(),
            intraGroupFlag=False,
        )
        dt2.save()


def populate_address():
    try:
        country = Country(code='FR', description=u'France')
        country.save()
    except ValueError:
        pass

    for i in range(20):
        fcountry = fake.country_from_continent()
        try:
            country = Country(
                code=fcountry['code'],
                description=u"%s (capital %s)" % (
                    fcountry['name'].decode('utf-8'),
                    fcountry['capital'].decode('utf-8'))
            )
            country.save()
        except ValueError:
            pass

    for i in range(5):
        fcountry = fake.country_from_continent(continent='Africa')
        try:
            country = Country(
                code=fcountry['code'],
                description=u"%s (capital %s)" % (
                    fcountry['name'].decode('utf-8'),
                    fcountry['capital'].decode('utf-8'))
            )
            country.save()
        except ValueError:
            pass

    nb_country = len(Country.query.all())
    for i in range(34):
        addr = Address(
            addressLine1=fake.streetAddress().decode('utf-8'),
            addressLine2=fake.secondaryAddress().decode('utf-8'),
            zIPCode=fake.postcode().decode('utf-8'),
            city=fake.city().decode('utf-8'),
            country=Country.query.all()[i % nb_country]

        )
        addr.save()


def populate_contact():
    for i in range(24):
        ci = ContactItem(
            name=fake.name().decode('utf-8'),
            email=fake.email().decode('utf-8'),
            phone=fake.phoneNumber(),
            fax=fake.phoneNumber()
        )
        ci.save()
        c = Contact(
            code="Contact %s" % i,
            description=u'description',
        )
        c.save()
        c.items.append(ci)
    db_session.commit()


def populate_thirdParty():
    ttype = ThirdPartyType.query.exclude_by(code="4").all()
    taxt = TaxType.query.filter_by(code='FVAT').one()
    i = 0
    for t in ttype:
        i = i + 1
        tp = ThirdParty(
            code='TP %s' % i,
            description=fake.company().decode('utf-8'),
            thirdPartyType=t,
            intragroup=False,
            taxType=taxt,
            serviceProviderFlag=True
        )
        tp.save()
        tp.contact = Contact.query.filter_by(code='CONTACT %s' % t.code).one()
        tp.address = Address.query.all()[int(t.code)]
    db_session.commit()


def populate_exchangeRate():
    currencies = Currency.query.all()
    for cc in currencies:
        for tc in currencies:
            fdate = fake.dateTime()
            er = ExchangeRate(
                exchangeRateDate=fdate,
                exchangeRateValue=round(random.uniform(1, 10), 5),
                companyCurrency=cc,
                targetCurrency=tc
            )
            er.save()


def populate_incotermText():
    for incoterm in Incoterm.query.all():
        for i in range(12):
            it = IncotermText(
                code="%s%s" % (incoterm.code, i),
                description=fake.paragraph().decode('utf-8'),
                incoterm=incoterm
            )
            it.save()


def populate_paymentText():
    for payment in PaymentTerm.query.all():
        for i in range(12):
            pt = PaymentTermText(
                code="%s%s" % (payment.code, i),
                description=fake.paragraph().decode('utf-8'),
                paymentTerm=payment
            )
            pt.save()

# itemFamily, ItemGroup, ItemDetail
itemFamily = {
    'FP': u'Finished Product',
    'PC': u'Production Component',
    'RM':  u'Raw Material',
    'SERV': u'Services',
}
populate_itemFamily = lambda: populate(ItemFamily, itemFamily)


def populate_itemGroup():
    fp = ItemFamily.query.filter_by(code='FP').one()
    serv = ItemFamily.query.filter_by(code='SERV').one()
    wheat = ItemGroup(
        code='FP_WH',
        description=u'Wheat',
        itemFamily=fp)
    sites = Site.query.all()
    nb_sites = len(sites)
    for s in sites[0:nb_sites / 3]:
        wheat.sites.append(s)
    wheat.save()

    sugar = ItemGroup(
        code='FP_SU',
        description=u'Sugar',
        itemFamily=fp)
    for s in sites[nb_sites / 3: nb_sites / 2]:
        sugar.sites.append(s)
    sugar.save()

    tr = ItemGroup(
        code='SERV_TP',
        description=u'Transport',
        itemFamily=serv)
    for s in sites:
        tr.sites.append(s)
    tr.save()

    ins = ItemGroup(
        code='SERV_IN',
        description=u'Insurance',
        itemFamily=serv)
    for s in sites[nb_sites / 2:nb_sites - 1]:
        ins.sites.append(s)
    ins.save()


def populate_ItemDetail():
    ld = Market.query.filter_by(code='LD').one()
    su = ItemGroup.query.filter_by(code='FP_SU').one()
    bsugar = ItemDetail(
        code='FP_SU_BS',
        description=u'Brown Sugar',
        itemPrintedDescription=u'Brown sugar',
        externalReference=u'b sugar',
        printedComment=u'best brown sugar',
        marketCode=ld,
        currency=Currency.query.filter_by(code='USD').one(),
        itemGroup=su,
        country=Country.query.filter_by(code='FR').one(),
        qtyProdMin=12.3,
        qtyProdMax=56.1,
    )
    for s in su.sites:
        bsugar.sites.append(s)
    bsugar.save()

    wsugar = ItemDetail(
        code='FP_SU_WS',
        description=u'White Sugar',
        itemPrintedDescription=u'White sugar',
        externalReference=u'w sugar',
        printedComment=u'best White sugar',
        marketCode=ld,
        currency=Currency.query.filter_by(code='USD').one(),
        itemGroup=su,
        country=Country.query.filter_by(code='FR').one(),
        qtyProdMin=11.3,
        qtyProdMax=46.1,
    )
    for s in su.sites:
        wsugar.sites.append(s)
    wsugar.save()


def populate_menu():

    def addChild(parent, child):
        code, name, subentry = child[:3]
        desc = u"%s - %s" % (code, name)
        tree = Menu(code=code, description=desc, parentMenu_id=parent.id)
        tree.save()  # to have a pk
        for entry in subentry:
            addChild(tree, entry)
        tree.save()

    for pack in CONFIG['modules'].values():
        mm = importlib.import_module(pack)
        data = mm.data
        if not data.submenu == []:
            menu = Menu(code=data.flowId, description=data.verbose)
            menu.save()  # to have a pk
            for entry in data.submenu:
                addChild(menu, entry)
            menu.save()


def populate_userProfile():

    root = UserProfile(
        code='ROOT',
        name=u'root',
        firstName=u'root',
        password='pwd',
        rights=Menu.query.all()
    )
    root.save()

    user = UserProfile(
        code='US0',
        name=u'Default user',
        firstName=u'default',
        password='1234',
        rights=Menu.query.all()
    )
    user.save()

init_db()
populate_taxtype()
populate_currency()
populate_lang()
populate_market()
populate_unitOfMeasure()
populate_group()
populate_comp()
populate_status()
populate_days()
populate_lot()
populate_packing()
populate_company_finish()
populate_site()
populate_site_finish()
populate_flowId()
populate_transport()
populate_docNumbering()
populate_docGroup()
populate_docType()
populate_thirdPartyType()
populate_incoterm()
populate_paymentTerm()
populate_contact()
populate_address()
populate_thirdParty()
populate_exchangeRate()
populate_incotermText()
populate_paymentText()
populate_itemFamily()
populate_itemGroup()
populate_ItemDetail()
populate_menu()
populate_userProfile()

# some allocation, just to fill the table
# don't worry about business rules
s = Site.query.all()[0]
for f in DocType.query.filter_by(flowID__code='1').all():
    s.docTypes.append(f)
db_session.commit()

for f in FlowID.query.all():
    f.sites.append(s)
db_session.commit()

for f in ThirdParty.query.all()[:10]:
    f.sites.append(s)
db_session.commit()

for i in Incoterm.query.all():
    i.sites.append(s)
db_session.commit()

for p in PaymentTerm.query.all():
    p.sites.append(s)
db_session.commit()
