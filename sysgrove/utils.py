#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
import os
import re
import unicodedata
import random
import string
from functools import reduce, wraps
import sqlalchemy
from sysgrove.settings import MODE_01, MODE_02, MODE_03, MODE_04, MODE_nn


log = logging.getLogger('sysgrove')

ctm = {
    '01': MODE_01,
    '02': MODE_02,
    '03': MODE_03,
    '04': MODE_04,
    '': MODE_nn
}


def debug_trace():
    '''Set a tracepoint in the Python debugger that works with Qt'''
    from PyQt4.QtCore import pyqtRemoveInputHook
    from pdb import set_trace
    pyqtRemoveInputHook()
    set_trace()


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def cc_prefix(a):
    """ Returns the numeric prefix of a context code """

    if re.match('^\d+', a):
        return re.findall(r'\d+', a)[0]
    if re.match('^[a-zA-Z][0-9]*', a):
        return re.findall(r'[a-zA-Z]', a)[0]


def cc_print(context_code):
    j = context_code.split('_', 1)
    if len(j) > 1:
        code = j[1][-2:]
        if code in ctm:
            return "%s%s" % (j[0], code)
        else:
            return j[0]
    else:
        return context_code


def cc_prev(context_code):
    if '_' in context_code:
        return cc_print(context_code)
    else:
        return context_code[:-2]


def code_to_mode(c, reverse=False):
    if reverse:
        my_dict = dict(zip(ctm.values(), ctm.keys()))
    else:
        my_dict = ctm
    if c in my_dict:
        return my_dict[c]
    if reverse:
        return ''
    else:
        return MODE_nn


def code2unicode(c):
    mode = code_to_mode(c)
    mode = mode.lower().capitalize()
    return mode


mode_to_code = lambda c: code_to_mode(c, True)


def code_to_screen(code):
    from sysgrove.core.screen import (
        Screen, Screen01,
        Screen02, Screen03,
        Screen04_site
    )
    if code == '01':
        return Screen01
    elif code == '02':
        return Screen02
    elif code == '03':
        return Screen03
    elif code == '04':
        return Screen04_site  # FIXME
    else:
        return Screen


# FIXME to be improved ... link could be compute only once!
def code_to_alchemyClass(code):
    # Hard coded : should be retreive some where in the module
    if code.startswith('1'):
        from sysgrove.modules.sales.models import SalesDocumentHeader
        return SalesDocumentHeader
    if code.startswith('2'):
        from sysgrove.modules.purcharse.models import PurchaseDocumentHeader
        return PurchaseDocumentHeader
    try:
        from sysgrove.modules.masterdata import _menu as m_menu
    except ImportError:
        m_menu = []
    try:
        from sysgrove.modules.finance import _menu as f_menu
    except ImportError:
        f_menu = []

    link = {}

    def add_link(menu):
        for entry in menu:
            if len(entry) == 4:
                c = entry[0]
                klass = entry[3]
                link[c] = klass
            for e in entry[2]:
                if len(e) == 4:
                    c = e[0]
                    klass = e[3]
                    link[c] = klass

    add_link(m_menu)
    add_link(f_menu)
    postfix = code[-2:]
    prefix = code[:-2]

    if postfix in ctm:
        if prefix in link:
            return retreive_class(link[prefix])
        elif code in link:
            return retreive_class(link[code])


def getattr_ex(instance, fields, choice=None):
    # TODO: choice is not yet handle.
    # How to deal with one2many relation in a proper way?
    def multi_getattr(inst, f):
        if not inst:
            return None
        elif isinstance(inst, list) and not choice:
            return getattr(inst[0], f)
        else:
            return getattr(inst, f)
    l = [instance]
    l.extend(fields.split('_'))
    return reduce(multi_getattr, l)


def setattr_ex(instance, fields, value, choice=None):
    # FIXME: same remark as for the 'getattr_ex' function
    # for the 'choice' argument
    sp = fields.split('_', 1)
    if len(sp) == 0:
        setattr(isinstance, fields, value)
    else:
        attr = getattr(instance, sp[0])
        if isinstance(attr, list) and not choice:
            if attr == []:
                attr = None
            else:
                attr = attr[0]
        setattr_ex(attr, sp[1], value, choice)


def getattr_info(fields, klass, choice=None):
    sp = fields.split('_')
    if len(sp) == 1:
        attr = getattr_ex(klass, fields)
        if hasattr(attr, 'info'):
            return attr.info
        else:
            return ""
    else:
        last = sp[len(sp) - 1]
        head = fields.split('_' + last)[0]
        return getattr(retreive_class_ex(head, klass), last).info


def getattr_verbose_name(fields, klass, choice=None):
    return getattr_info(fields, klass, choice)['verbose_name']
    # sp = fields.split('_')
    # if len(sp) == 1:
    #     return getattr_ex(klass, fields).info['verbose_name']
    # else:
    #     last = sp[len(sp) - 1]
    #     head = fields.split('_' + last)[0]
    #     return getattr(retreive_class_ex(head, klass), last).info[
    #         'verbose_name']


def package_to_path(name):
    if isinstance(name, str):
        folder = name.split('.')
        from sysgrove.main import module_path
        if folder[0] == 'sysgrove':
            res = module_path()
        for i in folder[1:]:
            res = os.path.join(res, i)
        return res  # FIXME : FAILS IF SOMETHING DOES NOT EXIST


def my_import(name):
    mod = __import__(name)
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


# from https://wiki.python.org/moin/PythonDecoratorLibrary
def memoize(obj):
    cache = obj.cache = {}

    @wraps(obj)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = obj(*args, **kwargs)
        return cache[key]
    return memoizer


@memoize
def get_class(klass):
    """Return a Class from its name"""
    try:
        parts = klass.split('.')
        module = ".".join(parts[:-1])
        m = __import__(module)
        for comp in parts[1:]:
            m = getattr(m, comp)
        setattr(get_class, klass, m)
        return m
    except ValueError:
        return None


def get_BaseClass(module, mixin=None):
    """Build the list of classes (in the module), which extend Base
    and are not Base itself neither some special classes which a prefix by
    "SG_"
    It mixin is set, filter for subclasses of mixin only

    """
    exported = dir(module)
    exported = filter(
        lambda x: x != 'Base' and not x.startswith('SG_')
        and not x.startswith('_'), exported)
    exported = map(lambda x: getattr(module, x), exported)
    exported = filter(
        lambda x:
        isinstance(x,
                   sqlalchemy.ext.declarative.api.DeclarativeMeta),
        exported)
    if mixin:
        exported = filter(lambda x: mixin in x.__mro__, exported)
    return exported


def find_key(value, dictionary):
    """ Code found in
    http://stackoverflow.com/questions/8023306/get-key-by-value-in-dictionary
    """
    return reduce(lambda x, y: x if x is not None else y,
                  map(lambda x: x[0] if x[1] == value else None,
                      dictionary.iteritems()))


# FIXME : store results in the the function, to avoid many
# queries to the database
@memoize
def retreive_class(name, origin=None):
    if not isinstance(name, str):
        return name
    if name == 'code':
        return origin

    from sysgrove.models import SG_Classes
    for c in SG_Classes.query.all():
        if c.name.lower() == name.lower():
            res = get_class("%s.%s" % (c.package, c.name))
            return res
    # If the name of the field is not a name of a class
    mapper = sqlalchemy.orm.class_mapper(origin)
    p = mapper.get_property(name)
    targetClass = SG_Classes.query.filter_by(name=p.target.name).one()
    res = get_class("%s.%s" % (targetClass.package, targetClass.name))
    return res


def retreive_class_ex(name, origin):
    lname = [origin]
    sp = name.split('_')
    if '' in sp:
        sp.pop(sp.index(''))
    lname.extend(sp)
    return reduce(lambda x, y: retreive_class(y, x), lname)


def get_attr_and_class(name, origin=None):
    if '_' in name:
        sp = name.split('_')
        attr = sp[len(sp) - 1]
        if name.startswith('_'):
            lklass = name.split('_')[1]
            klass = retreive_class(lklass)
        else:
            lklass = name.split('_' + attr)[0]
            klass = retreive_class_ex(lklass, origin)
        return (klass, attr)
    else:
        return (origin, name)


# from
# http://stackoverflow.com/questions/128573/using-property-on-classmethods
class classproperty(object):

    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)


def minimalize(name):
    return name[0].lower() + name[1:]


def capitalize(name):
    return name[0].upper() + name[1:]


def upper_split(name):
    new = ''
    for x in name:
        if x.isupper():
            new = new + " " + x
        else:
            new = new + x

    re = new.replace('  ', ' ')
    if re[0] is ' ':
        result = re[1:]
    return result


# FIXME: very similar to UIForm.form2attr_list
def prefix(klass, pref=''):
    display = klass.list_display
    if pref == '':
        pref = klass.get_ui_prefix()

    def iter(form, res):
        if isinstance(form, str):
            res.append(pref + form)
        elif isinstance(form, list):
            res1 = []
            for f in form:
                iter(f, res1)
            res.append(res1)
        elif isinstance(form, tuple):
            for f in form[1:]:
                iter(f, res)
    res = []
    iter(display, res)
    return res[0]


# adaptation from https://github.com/mozilla/unicode-slugify
def slugify(s, lower=True, spaces=False):
    rv = []
    for c in unicodedata.normalize('NFKD', s):
        cat = unicodedata.category(c)[0]
        # L and N signify letter/number.
        # http://www.unicode.org/reports/tr44/tr44-4.html#GC_Values_Table
        if cat in 'LN' or c in '-_~':
            rv.append(c)
        if cat == 'Z':  # space
            rv.append(' ')
    new = ''.join(rv).strip()
    if not spaces:
        new = re.sub('[-\s]+', '_', new)
        new = str(new)
    return new.lower() if lower else new


def have_same_value(l, value):
    for v in l:
        if not v == value:
            return False
    return True


class UserMessage(Exception):

        """Stop process to give to warn the user"""
        pass


def map_ex(func, arg):
    if isinstance(arg, list):
        return map(func, arg)
    else:
        return func(arg)


def compute_all_unique_combinaison(items):
    def unique_comninaison(items, n):
        if n == 0:
            yield []
        else:
            for i in xrange(len(items) - n + 1):
                for j in unique_comninaison(items[i + 1:], n - 1):
                    yield [items[i]] + j

    res = []
    for n in xrange(1, len(items) + 1):
        res += unique_comninaison(items, n)
    return res


def submenu_to_dict(submenu):
    def to_submenu_dict(submenu, res):
        if isinstance(submenu, list):
            if len(submenu) > 0:
                map(lambda x: to_submenu_dict(x, res), submenu)
        else:
            sub = {}
            sub['description'] = submenu[1]
            to_submenu_dict(submenu[2], sub)
            if len(submenu) == 4:
                sub['model'] = submenu[3]
            else:
                sub['model'] = None
            if len(submenu) == 5:
                sub['docGroup'] = submenu[4]
            else:
                sub['docGroup'] = None
            res[submenu[0]] = sub
    res = {}
    to_submenu_dict(submenu, res)
    return res
