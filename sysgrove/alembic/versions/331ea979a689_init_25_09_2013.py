"""init

Revision ID: 331ea979a689
Revises: None
Create Date: 2013-09-25 08:50:55.757502

"""

# revision identifiers, used by Alembic.
revision = '331ea979a689'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()





def upgrade_engine1():
    ### commands auto generated by Alembic - please adjust! ###
    pass
    ### end Alembic commands ###


def downgrade_engine1():
    ### commands auto generated by Alembic - please adjust! ###
    pass
    ### end Alembic commands ###

