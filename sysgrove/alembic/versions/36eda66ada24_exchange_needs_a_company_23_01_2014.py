"""exchange needs a company

Revision ID: 36eda66ada24
Revises: 2927352bbb1
Create Date: 2014-01-23 16:55:55.118000

"""

# revision identifiers, used by Alembic.
revision = '36eda66ada24'
down_revision = '2927352bbb1'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()


def upgrade_engine1():
    # commands auto generated by Alembic - please adjust! ###
    op.add_column('ExchangeRate',
                  sa.Column('Company_pk', sa.Integer(),
                            sa.ForeignKey('Company.Company_pk'),
                            nullable=True))
    op.drop_column('ExchangeRate', u'CompanyCurrency')
    # end Alembic commands ###


def downgrade_engine1():
    # commands auto generated by Alembic - please adjust! ###
    op.add_column('ExchangeRate',
                  sa.Column(u'CompanyCurrency', sa.INTEGER(),
                            sa.ForeignKey('Currency.Currency_pk'),
                            nullable=True))
    op.drop_column('ExchangeRate', 'Company_pk')
    # end Alembic commands ###
