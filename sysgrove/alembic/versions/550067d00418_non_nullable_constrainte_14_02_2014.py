"""non nullable constrainte

Revision ID: 550067d00418
Revises: 1e5dd7d4e48b
Create Date: 2014-02-14 10:47:08.385000

"""

# revision identifiers, used by Alembic.
revision = '550067d00418'
down_revision = '1e5dd7d4e48b'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()





def upgrade_engine1():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('ExchangeRate', 'ExchangeRateDate',
               existing_type=sa.DATE(),
               nullable=False)
    op.alter_column('Status', 'StatusCode',
               existing_type=sa.VARCHAR(),
               nullable=False)
    ### end Alembic commands ###


def downgrade_engine1():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('Status', 'StatusCode',
               existing_type=sa.VARCHAR(),
               nullable=True)
    op.alter_column('ExchangeRate', 'ExchangeRateDate',
               existing_type=sa.DATE(),
               nullable=True)
    ### end Alembic commands ###

