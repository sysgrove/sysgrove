"""non nullable constrainte for site

Revision ID: 4f3d783f31da
Revises: 550067d00418
Create Date: 2014-02-14 16:14:24.874000

"""

# revision identifiers, used by Alembic.
revision = '4f3d783f31da'
down_revision = '550067d00418'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()





def upgrade_engine1():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('DocumentHeader', 'Site_pk',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('MarketValuation', 'Site_pk',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('Payment', 'Site_pk',
               existing_type=sa.INTEGER(),
               nullable=False)
    ### end Alembic commands ###


def downgrade_engine1():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('Payment', 'Site_pk',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.alter_column('MarketValuation', 'Site_pk',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.alter_column('DocumentHeader', 'Site_pk',
               existing_type=sa.INTEGER(),
               nullable=True)
    ### end Alembic commands ###

