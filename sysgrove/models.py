#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
# import pdb
# pdb.set_trace()
import logging
import datetime
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy import (
    Column, ColumnDefault,
    Integer, Float, Enum,
    String, Unicode,
    Boolean,
    Date, DateTime, inspect,
    orm)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import types
from sysgrove.main import db_session, Session
from sysgrove.utils import get_BaseClass, get_class, debug_trace
from sysgrove.utils import classproperty, retreive_class_ex
from sysgrove import i18n
_ = i18n.language.ugettext

log = logging.getLogger('sysgrove')
Base = declarative_base()
Base.query = db_session.query_property()


# and some methodes to handle them
def add_pkg_classes(module):
    classes = get_BaseClass(module)
    pkg = module.__name__
    for c in classes:
        name = c.__name__
        first = SG_Classes.query.filter_by(
            name=name).filter_by(package=pkg).first()
        if not first:
            klass = SG_Classes(name=name, package=pkg)
            db_session.add(klass)
    try:
        db_session.commit()
    except Exception as e:
        db_session.rollback()
        log.debug(str(e).decode('utf-8'))
        raise Exception(u'Can not add pkg Class')
    db_session.flush()


def find_targetClass(klass, fieldname):
    # I guess we can use recursion here, without any problem
    mapper = orm.class_mapper(klass)
    sp = fieldname.split('_', 1)
    fname = sp[0]
    if klass.is_fk(fname):
        p = mapper.get_property(fname)
        targetClass = SG_Classes.query.filter_by(name=p.target.name).one()
        if fname == fieldname:
            return targetClass
        else:
            nClass = get_class("%s.%s" % (
                targetClass.package, targetClass.name))
            return find_targetClass(nClass, sp[1])


from collections import namedtuple
from sqlalchemy.orm.attributes import get_history
from sqlalchemy.orm import class_mapper, relationship
from sqlalchemy import event, ForeignKeyConstraint
from sysgrove.user import User

# here I've used a namedtuple to have a lightweight object with fields
# to access it's data; lighter then dict more readable then siple tuples
FieldChange = namedtuple('FieldChange', 'name old_value new_value')


def get_value_or_reference(values):

    try:
        val = values[0]
    except:
        val = u''

    if isinstance(val, InitMixin):
        return val.id

    return val


def get_modified_fields(target):
    """
    returns a list of FieldChange objects, each of this objects will have the
    field name, the old value and the new one
    """
    fields = list()
    for f in target.field_names():
        h = get_history(target, f)
        if h.has_changes():
            # FIXME: if it is possible to have h.deleted==[]
            # FIXED: get_value_or_reference tries to handle corner cases like
            # empty changed values and InitMixin subclasses

            old = get_value_or_reference(h.deleted)
            new = get_value_or_reference(h.added)

            fields.append(FieldChange(name=f,
                                      old_value=old,
                                      new_value=new))

    return fields


def get_doc_info(doc):

    name = u"%s" % (doc.verbose_name,)
    try:
        head = doc.documentHeader
        name_tpl = u"%s doc number %s doc type %s, line number %s"
        return (name_tpl % (name,
                            head.documentNumber,
                            head.docType.docNumbering.description,
                            doc.lineNumber),
                head.id,
                head.docType.id)
    except:
        try:
            name_tpl = u"%s doc number %s doc type %s"
            return (name_tpl % (name,
                                doc.documentNumber,
                                doc.docType.docNumbering.description),
                    doc.id,
                    doc.docType.id)
        except:
            return (name, doc.id, None)


class LoggableModel(object):

    """
    This mixin should be able to track creation, update and removal
    of models that extends it
    NOTES:
    right now the CDMixin.verbose_name property will be used to identify
    a model
    """
    @staticmethod
    def log_create(mapper, connection, target):

        try:
            user_id = User().get_current.id
            doc_name, doc_id, doc_type = get_doc_info(target)
            log = DocumentLog(doc_name=doc_name,
                              created_by_id=user_id,
                              doc_type=doc_type,
                              doc_ref=doc_id,
                              description=u"document created")
            log.save_to_new_session()
        except Exception as e:
            print e

    @staticmethod
    def log_update(mapper, connection, target):

        try:
            fields = get_modified_fields(target)

            user_id = User().get_current.id
            doc_name, doc_id, doc_type = get_doc_info(target)
            log = DocumentLog(doc_name=doc_name,
                              created_by_id=user_id,
                              doc_type=doc_type,
                              doc_ref=doc_id,
                              description=u"document updated")
            log.save_to_new_session()

            for f in fields:
                line = DocumentLogDetail(log_created_at=log.created_at,
                                         log_created_by_id=log.created_by_id,
                                         log_doc_name=log.doc_name,
                                         log_doc_ref=log.doc_ref,
                                         field_name=f.name,
                                         old_value=f.old_value,
                                         new_value=f.new_value)

                line.save_to_new_session()
        except Exception as e:
            print e

    @staticmethod
    def log_delete(mapper, connection, target):

        try:
            user_id = User().get_current.id
            doc_name, doc_id, doc_type = get_doc_info(target)
            log = DocumentLog(doc_name=doc_name,
                              created_by_id=user_id,
                              doc_type=doc_type,
                              doc_ref=doc_id,
                              description=u"document delete")
            log.save_to_new_session()
        except Exception as e:
            print e

    @classmethod
    def __declare_last__(cls):
        event.listen(cls, 'after_insert', cls.log_create)
        event.listen(cls, 'after_update', cls.log_update)
        event.listen(cls, 'after_delete', cls.log_delete)


# Some mixin to ease classes creation
class CDMixin(object):

    @declared_attr
    def __tablename__(cls):
        return cls.__name__

    """
    FIXME: reread
http://docs.sqlalchemy.org/en/rel_0_7/orm/mapper_config.html#class-mapping-api
    always_refresh – If True, all query operations for this mapped class will
    overwrite all data within object instances that already exist within
    the session, erasing any in-memory changes with whatever information was
    loaded from the database.
    Usage of this flag is highly discouraged; as an alternative, see the method
    Query.populate_existing().
    """
    # __mapper_args__ = {'always_refresh': True}

    @declared_attr
    def cancelData(cls):
        return Column("CancelData", Boolean, ColumnDefault(False))

    # overwrite if needed.
    def pre_save(self):
        pass

    # overwrite if needed.
    def post_save(self):
        pass

    # FIXME : cacading must be handled
    def delete(self, cascading=True):
        self.cancel()
        self.save()

    def cancel(self):
        self.cancelData = True

    def add(self):
        if not self in db_session:
            db_session.add(self)

    def save(self):
        """ simple version. Sophistication is needed"""
        self.pre_save()
        self.add()
        try:
            db_session.commit()
        except Exception as e:
            db_session.rollback()
            # PostGreSQL error message are encode...
            # I did not find a better way to make it runs on Windows
            log.debug(str(e).decode('utf-8'))
            raise Exception('Can not save due to database error.')
            db_session.expunge(self)

        db_session.flush()
        self.post_save()

    def save_to_new_session(self):
        """ simple version. Sophistication is needed"""
        session = Session()
        session.add(self)
        try:
            session.commit()
        except Exception as e:
            session.rollback()
            print e
            # PostGreSQL error message are encode...
            # I did not find a better way to make it runs on Windows
            raise Exception('Can not save due to database error.')

        session.flush()

    @classmethod
    def get_ui_prefix(cls):
        # returns class name with first character in lowercase. returned name
        # ends with undersdcore.
        return cls.__name__[0].lower() + cls.__name__[1:] + "_"

    @classmethod
    def is_direct(cls, field_name):
        mapper = inspect(cls)
        try:
            p = mapper.get_property(field_name)
        except InvalidRequestError:
            return False
        return isinstance(p, orm.ColumnProperty)

    @classmethod
    def is_fk(cls, field_name):
        mapper = inspect(cls)
        try:
            p = mapper.get_property(field_name)
        except InvalidRequestError:
            return False
        return isinstance(p, orm.RelationshipProperty) \
            and p.direction == orm.interfaces.MANYTOONE

    @classmethod
    def is_o2m(cls, field_name):
        mapper = inspect(cls)
        try:
            p = mapper.get_property(field_name)
        except InvalidRequestError:
            return False
        return isinstance(p, orm.RelationshipProperty) \
            and p.direction == orm.interfaces.ONETOMANY

    @classmethod
    def is_m2m(cls, field_name):
        mapper = inspect(cls)
        try:
            p = mapper.get_property(field_name)
        except InvalidRequestError:
            return False
        return isinstance(p, orm.RelationshipProperty) \
            and p.direction == orm.interfaces.MANYTOMANY

    @classmethod
    def toPython_type(cls, field):
        if not '_' in field:
            if cls.is_direct(field):
                mapper = inspect(cls)
                prop = mapper.get_property(field)
                bd_type = type(prop.columns[0].type)
                if bd_type == Float:
                    return float
                elif bd_type == Integer:
                    return int
                elif bd_type == Boolean:
                    return bool
                elif bd_type == Unicode:
                    return unicode
                elif bd_type == Enum or bd_type == String:
                    return str
                elif bd_type == Date:
                    return datetime.date
                elif bd_type == DateTime:
                    return datetime.datetime
                else:
                    return str
        else:
            sp = field.split('_')
            last = sp[len(sp) - 1]
            head = field.split('_' + last)[0]
            subclass = retreive_class_ex(head, cls)
            return subclass.toPython_type(last)

    @classmethod
    def _fields(cls):
        mapper = inspect(cls)
        fields = {}
        for (attr, col) in mapper.class_manager.viewitems():
            if attr != 'id' and attr != 'cancelData' \
                    and not attr.endswith('_id'):
                fields[attr] = col
        return fields

    @classmethod
    def field_names(cls):
        return set([f.key for f in class_mapper(cls).iterate_properties])

    @staticmethod
    def associated_relationship(attr, default_postfix='_id'):
        if attr.endswith(default_postfix):
            return attr.split(default_postfix)[0]
        else:
            return attr

    @classmethod
    def pk_attrs(klass):
        mapper = inspect(klass)
        tmp = [k for (k, v) in mapper.column_attrs.items()
               if v.expression.primary_key]
        return [CDMixin.associated_relationship(k) for k in tmp]

    @classmethod
    def mandatory(cls):
        mapper = inspect(cls)
        res = []
        for (attr, col) in mapper.columns.items():
            if not col.nullable and not col.default:
                res.append(attr)

        res = [CDMixin.associated_relationship(k) for k in res]
        for pk in cls.pk_attrs():
            res.remove(pk)
        return res

    def validate(self):
        errors = []
        for m in self.mandatory():
            if not getattr(self, m):
                errors.append((m, u'%s: is a mandatory field\n' % m))
        # same thing for all fk fields
        for field in self._fields().keys():
            if self.is_fk(field) and getattr(self, field):
                try:
                    getattr(self, field).validate()
                except ValueError as e:
                    errors = errors + [('%s_%s' % (field, x), y)
                                       for (x, y) in e.args]

        if not errors == []:
            raise ValueError(*errors)

    @classproperty
    def verbose_name(cls):
        return unicode(cls.__tablename__)

    @classproperty
    def list_columns(cls):
        list_display = getattr(cls, 'list_display', None)
        if list_display:
            from sysgrove.ui.classes import UIForm
            return UIForm.form2attr_list(list_display)
        else:
            return []

    @classproperty
    def list_columns_editable(cls):
        return []


# class InitMixin(CDMixin):
class InitMixin(LoggableModel, CDMixin):

    @declared_attr
    def id(cls):
        return Column(
            cls.__tablename__ + "_pk",
            Integer,
            info={'verbose_name': u'id'},
            primary_key=True
        )

    @classproperty
    def list_columns(cls):
        columns = super(InitMixin, cls).list_columns
        if not 'id' in columns:
            columns.append('id')
        return columns


class SimplMixin(InitMixin):

    @declared_attr
    def code(cls):
        return Column(
            cls.__tablename__ + "Code",
            Unicode(),
            # FIXME : it has to done but, some data have update
            nullable=False,
            info={"length": 10, 'verbose_name': u'%s Code' % cls.verbose_name}
        )

    def __repr__(self):
        return u"<%s(%s)>" % (self.__tablename__, self.code)

    def __str__(self):
        return self.code

    def __unicode__(self):
        return unicode(self.code)

    @classmethod
    def validate_code(cls, target, value, oldvalue, initiator):
        if value == "":
            raise ValueError('code: "%s" invalid value' % value)
        value = value.upper()
        # debug_trace()
        first = cls.query.filter_by(code=value).first()

        # creation
        if not target.id:
            if first:
                raise ValueError('code: code "%s" already used' % value)
            else:
                return value

        # update
        if value != oldvalue:
            raise ValueError(
                'code: changing code "%s"  is forbidden' % oldvalue)
        return value


class SGMixin(SimplMixin):
    list_display = [
        [(('GroupBox', 0), ['code', 'description'])],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    @declared_attr
    def description(cls):
        return Column(
            cls.__tablename__ + "Description",
            Unicode(),
            ColumnDefault(u""),
            nullable=False,
            info={
                "length": 30,
                'verbose_name': u'%s Description' % cls.verbose_name})


# FIXME: TO be finished...
class ChoiceType(types.TypeDecorator):

    impl = types.String

    def __init__(self, choices, **kw):
        self.choices = choices
        super(ChoiceType, self).__init__(**kw)

    def process_bind_param(self, value, dialect):
        for v in self.choices:
            if bytearray(v) == bytearray(value):
                return v
        return [v for v in self.choices if v == value][0]

    def process_result_value(self, value, dialect):
        return self.choices[value]


# Some classes to handles the classes of the application
class SG_Classes(Base):
    __tablename__ = 'SG_classes'
    id = Column(Integer, primary_key=True)
    name = Column(String())
    package = Column(String())

    def __repr__(self):
        return "<%s(%s, %s)>" % (self.__tablename__, self.name, self.package)


class DocumentLog(CDMixin, Base):

    created_at = Column(DateTime(), primary_key=True,
                        default=datetime.datetime.utcnow)
    created_by_id = Column(Integer, primary_key=True)
    created_by = relationship("UserProfile", backref="user_logs")
    doc_name = Column(Unicode,
                      primary_key=True)

    doc_type = Column(Integer, nullable=True)
    doc_ref = Column(Integer, primary_key=True)
    description = Column(Unicode)
    details = relationship("DocumentLogDetail", backref="document_log")

    __table_args__ = (ForeignKeyConstraint([created_by_id],
                                           ["UserProfile.UserProfile_pk"]),
                      {})


class DocumentLogDetail(CDMixin, Base):

    log_created_at = Column(DateTime(), primary_key=True)
    log_created_by_id = Column(Integer, primary_key=True)
    log_doc_name = Column(Unicode,
                          primary_key=True)
    log_doc_ref = Column(Integer, primary_key=True)
    field_name = Column(Unicode(), primary_key=True)
    old_value = Column(Unicode())
    new_value = Column(Unicode())

    __table_args__ = (ForeignKeyConstraint([log_created_at,
                                            log_created_by_id,
                                            log_doc_name,
                                            log_doc_ref],
                                           [DocumentLog.created_at,
                                            DocumentLog.created_by_id,
                                            DocumentLog.doc_name,
                                            DocumentLog.doc_ref]),
                      {})
