#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton
15/02/2013
Version 1.0

Kernel context class

    Role :

        - Provides a class for topbar (customised titlebar)

    Todo :

        - Make the window movable by dragging the topbar
        - Display buttons and title for real

revision #
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

"""

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


def toggleFS(win):
    if (win . isMaximized()):  # isFullScreen() ):
        win . showNormal()
    else:
        win . showMaximized()  # showFullScreen ( )


class Topbar_titlebar(QtGui.QWidget):

    def __init__(self, cwidget, win):

        super(Topbar_titlebar, self).__init__(cwidget)
        self.win = win

        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QtCore.QSize(0, 20))
        self.setObjectName(_fromUtf8("topbar"))

    def mousePressEvent(self, event):
        self.offset = event.pos()

    def mouseMoveEvent(self, event):
        x, y = event.globalX(), event.globalY()
        x_w, y_w = self.offset.x(), self.offset.y()
        self.win.move(x - x_w, y - y_w)


class Topbar (QtGui.QWidget):

    def __init__(self, cwidget, SysGroveMain):

        self.titlebar = Topbar_titlebar(cwidget, SysGroveMain)

        self.topbar = QtGui.QWidget(cwidget)
        self.topbar.setObjectName(_fromUtf8("topbar"))
        self.topbar.setStyleSheet(_fromUtf8("""
            QWidget#topbar{
            background-image: url(':barre-horizontale.png') no-repeat;
            opacity: 0.5;
            border: none;
            }"""))
        self.tblayout = QtGui . QHBoxLayout(self.topbar)
        self . tblayout . setObjectName("tbLayout")
        self . tblayout . setMargin(0)
        self . tblayout . setSpacing(0)

        self.tblayout . addWidget(self.titlebar)
        self.tbtitle_layout = QtGui.QHBoxLayout(self.titlebar)

        spacerItem = QtGui.QSpacerItem(
            0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self . tbtitle_layout . addItem(spacerItem)

        # Topbar . buttonarea

        self.TopbarBA = QtGui.QWidget(cwidget)
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.TopbarBA.sizePolicy().hasHeightForWidth())
        self.TopbarBA.setSizePolicy(sizePolicy)
        self.TopbarBA.setMinimumSize(QtCore.QSize(20, 20))
        self.TopbarBA.setObjectName(_fromUtf8("TopbarBA"))
        self.TopbarBA.setStyleSheet(_fromUtf8("""
            QWidget#TopbarBA{
            background-color: transparent;
            border: none;
            }
            """))
        self . tblayout . addWidget(self . TopbarBA)

        # Layout that fixes the 3 buttons

        self . tb_buttonLayout = QtGui.QHBoxLayout(self.TopbarBA)
        self . tb_buttonLayout . setObjectName(
            _fromUtf8("ToolBarButtonLayout"))
        self . tb_buttonLayout . setMargin(0)
        self . tb_buttonLayout . setContentsMargins(0, 0, 4, 0)
        self . tb_buttonLayout . setSpacing(2)

        tbb_sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        tbb_sizePolicy.setHorizontalStretch(0)
        tbb_sizePolicy.setVerticalStretch(0)

        # Topbar . minimize
        self.pb_min = QtGui.QPushButton(self.TopbarBA)
        self.pb_min.setObjectName(_fromUtf8("min"))
        self.pb_min.setSizePolicy(tbb_sizePolicy)

        self.pb_min.resize(28, 16)
        self.pb_min.setFlat(True)
        self.pb_min.setStyleSheet(_fromUtf8("""
            QPushButton {
                border: none;
                outline: none;
                background-repeat: no-repeat;
                background-image: url(':minimize.png');
            }
            QPushButton:hover{
                background-image: url(':minimize-hover.png');
            }"""))
        QtCore.QObject.connect(self.pb_min, QtCore.SIGNAL(
            _fromUtf8("clicked()")), SysGroveMain.showMinimized)
        QtCore.QMetaObject.connectSlotsByName(SysGroveMain)

        # Topbar . fullscreen
        self.pb = QtGui.QPushButton(self.TopbarBA)
        self.pb.setObjectName(_fromUtf8("fs"))
        self.pb.setSizePolicy(tbb_sizePolicy)

        self.pb.resize(27, 16)
        self.pb.setFlat(True)
        self.pb.setStyleSheet(_fromUtf8("""
            QPushButton{
                border: none;
                outline: none;
                background-repeat: no-repeat;
                background-image: url(':fullscreen.png');
            }
            QPushButton:hover{
                background-image: url(':fullscreen-hover.png');
            }"""))
        QtCore.QObject.connect(
            self.pb,
            QtCore.SIGNAL(_fromUtf8("clicked()")),
            lambda: toggleFS(SysGroveMain)
        )
        QtCore.QMetaObject.connectSlotsByName(SysGroveMain)

        # Topbar . close
        self.pb_close = QtGui.QPushButton(self.TopbarBA)
        self.pb_close.setObjectName(_fromUtf8("pb_close"))

        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.pb_close.sizePolicy().hasHeightForWidth())
        self.pb_close.setSizePolicy(tbb_sizePolicy)

        self.pb_close.resize(28, 16)
        self.pb_close.setFlat(True)
        self.pb_close.setStyleSheet(_fromUtf8("""
            QPushButton{
                border: none;
                outline: none;
                background-repeat: no-repeat;
                background-image: url(':close.png');
            }
            QPushButton:hover{
                background-image: url(':close-hover.png');
            }"""))

        QtCore.QObject.connect(
            self.pb_close,
            QtCore.SIGNAL(_fromUtf8("clicked()")),
            SysGroveMain.close
        )
        QtCore.QMetaObject.connectSlotsByName(SysGroveMain)

        self . tb_buttonLayout . addWidget(self . pb_min)
        self . tb_buttonLayout . addWidget(self . pb)
        self . tb_buttonLayout . addWidget(self . pb_close)
