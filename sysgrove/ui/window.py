#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton
11/02/2013
Version 1.0

    Role :

        - Provides the static layout of the MainWindow and global names
             for MainWindow's areas and layouts
        - Defines the splashscreen
        - Defines the launch() function that starts the main loop

revision #
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

"""
import logging
import sys
import locale
from PyQt4 import QtCore, QtGui
from PyQt4 import QtWebKit
from sysgrove.config import CONFIG
from sysgrove.ui.topbar import Topbar, Topbar_titlebar
from sysgrove.user import User
from sysgrove.start import buttonsSetup, menuSetup
#from sysgrove import __version__

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

log = logging.getLogger('sysgrove')

# app creation
app = QtGui.QApplication(sys.argv)
locale.setlocale(locale.LC_ALL, '')


class SysGroveMainWindow (QtGui.QMainWindow):

    """ Overload of the QMainWindow class that allows to modify
    events (such as resize ...)
    """

    def __init__(self):
        super(SysGroveMainWindow, self).__init__()
        self.resizeEvents = []

    def resizeEvent(self, event):
        for e in self.resizeEvents:
            e()


class UI_SysGroveMain(object):

    """ Defines the static content of the MainWindow """

    def setupUi(self, SysGroveMain):

        # This is the main window
        SysGroveMain.setObjectName(_fromUtf8(
            "SysGroveMain"))  # Should be in config
        SysGroveMain.resize(1024, 698)
        SysGroveMain.setMinimumSize(QtCore.QSize(
            1024, 600))  # Should be in config
        SysGroveMain.setStyleSheet(_fromUtf8(""))
        SysGroveMain.setUnifiedTitleAndToolBarOnMac(False)

        # ... and this is the centralWidget
        self.centralwidget = QtGui.QWidget(SysGroveMain)

        # Setting up theme ...
        SysGroveMain.setStyleSheet(CONFIG['css'])

        # Let's go !
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.centralwidget.setStyleSheet(_fromUtf8(
            "border: 1px solid black; border-radius: 6px;"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))

        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setMargin(0)

        # Intermediate layout
        self.verticalLayout_3 = QtGui.QVBoxLayout()

        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))

        # Topbar
        self. topbar = Topbar(self.centralwidget, SysGroveMain)
        self.verticalLayout_3.addWidget(self.topbar.topbar)

        # Intermediate layout
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))

        # Menubar
        self.widget_4 = QtGui.QWidget(self.centralwidget)
        # self.widget_4.setStyleSheet(_fromUtf8(
        # "border-radius: 0px; background-color: #f6f6f6;"))
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget_4.sizePolicy().hasHeightForWidth())
        self.widget_4.setSizePolicy(sizePolicy)
        self.widget_4.setMinimumSize(QtCore.QSize(600, 90))
        self.widget_4.setObjectName(_fromUtf8("GfxMenuBar"))

        # Menubar layout
        self.menuBarLayout = QtGui.QHBoxLayout(self.widget_4)

        self.menuBarLayout.setSpacing(2)
        self.menuBarLayout.setMargin(2)

        self.menuBarLayout.setDirection(QtGui.QBoxLayout.RightToLeft)

        self.horizontalLayout_2.addWidget(self.widget_4)

        # Tabbar
        self.widget_11 = QtGui.QWidget(self.centralwidget)
        self.widget_11.setStyleSheet(_fromUtf8("border-radius: 0px;"))

        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget_11.sizePolicy().hasHeightForWidth())
        self.widget_11.setSizePolicy(sizePolicy)
        self.widget_11.setMinimumSize(QtCore.QSize(404, 90))
        self.widget_11.setObjectName(_fromUtf8("widget_11"))

        # Tabbar layout
        self.tabbar_layout = QtGui.QHBoxLayout(self.widget_11)

        self.tabbar_layout.setDirection(QtGui.QBoxLayout.RightToLeft)

        # Adding tabbar to intermediate layout
        self.horizontalLayout_2.addWidget(self.widget_11)

        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setSizeConstraint(
            QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))

        # Accordion area
        self.widget_9 = QtGui.QWidget(self.centralwidget)
        self.centralwidget.setStyleSheet(_fromUtf8(
            "border-radius: 0px; border: 1px solid black"))

        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget_9.sizePolicy().hasHeightForWidth())
        self.widget_9.setSizePolicy(sizePolicy)
        self.widget_9.setMinimumSize(QtCore.QSize(202, 202))
        self.widget_9.setObjectName(_fromUtf8("GfxAccordion"))

        # Accordion area layout
        self.accordionLayout = QtGui.QHBoxLayout(self.widget_9)

        self.accordionLayout.setSpacing(2)
        self.accordionLayout.setMargin(2)

        self.verticalLayout_2.addWidget(self.widget_9)

        # Search bar area
        self.widget_12 = QtGui.QWidget(self.centralwidget)
        self.widget_12.setStyleSheet(_fromUtf8(
            "border-radius: 0px; border: 1px solid black"))

        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget_12.sizePolicy().hasHeightForWidth())
        self.widget_12.setSizePolicy(sizePolicy)
        self.widget_12.setMinimumSize(QtCore.QSize(202, 24))
        self.widget_12.setMaximumSize(QtCore.QSize(202, 24))
        self.widget_12.setObjectName(_fromUtf8("UISearchBarArea"))
        self.widget_12.setStyleSheet(_fromUtf8("background-color: #ffffff"))

        self.searchLayout = QtGui.QHBoxLayout(self.widget_12)
        self.searchLayout.setMargin(2)
        self.searchLayout.setSpacing(0)

        #
        self.verticalLayout_2.addWidget(self.widget_12)

        # Dynamic button zone
        self.widget_13 = QtGui.QWidget(self.centralwidget)
        self.widget_13.setStyleSheet(_fromUtf8(
            "border-radius: 0px; border: 1px solid black"))

        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget_13.sizePolicy().hasHeightForWidth())
        self.widget_13.setSizePolicy(sizePolicy)
        self.widget_13.setMinimumSize(QtCore.QSize(202, 140))
        self.widget_13.setObjectName(_fromUtf8("widget_13"))
        self.verticalLayout_2.addWidget(self.widget_13)

        self.gdynamicLayout = QtGui.QHBoxLayout(self.widget_13)
        self.gdynamicLayout.setMargin(0)
        self.gdynamicLayout.setSpacing(0)

        # Static button zone
        self.widget_14 = QtGui.QWidget(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget_14.sizePolicy().hasHeightForWidth())
        self.widget_14.setSizePolicy(sizePolicy)
        self.widget_14.setMinimumSize(QtCore.QSize(202, 68))
        self.widget_14.setObjectName(_fromUtf8("widget_14"))
        self.verticalLayout_2.addWidget(self.widget_14)

        self.staticLayout = QtGui.QGridLayout(self.widget_14)
        self.staticLayout.setMargin(3)
        self.staticLayout.setSpacing(2)

        # Status bar
        self.statusBar = QtGui.QWidget(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.statusBar.sizePolicy().hasHeightForWidth())
        self.statusBar.setSizePolicy(sizePolicy)
        self.statusBar.setMinimumSize(QtCore.QSize(202, 20))
        self.statusBar.setMaximumSize(QtCore.QSize(202, 20))
        self.statusBar.setObjectName(_fromUtf8("statusBar"))

        # Status bar layout
        self.status_bar_layout = QtGui.QHBoxLayout(self.statusBar)
        self.status_bar_layout.setMargin(0)
        self.status_bar_layout.setSpacing(0)

        # Linking layouts
        self.verticalLayout_2.addWidget(self.statusBar)
        self.horizontalLayout_3.addLayout(self.verticalLayout_2)
        self.horizontalLayout_3.setObjectName("HBOX3DEWIDGET16")

        self.horizontalLayout_3.setSpacing(0)  # FIXME: useful?
        self.horizontalLayout_3.setMargin(0)  # FIXME: useful ?

        # Content widget (widget_16)
        # For historical reasons, please keep the name widget_16

        self.widget_16 = QtGui.QWidget(self.centralwidget)
        # self.widget_16.setStyleSheet("background-color: #ff00ff")
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.widget_16.sizePolicy().hasHeightForWidth())
        #self.widget_16.setMinimumSize(QtCore.QSize(400, 400))
        self.widget_16.setSizePolicy(sizePolicy)
        self.widget_16.setObjectName(_fromUtf8("UIContent"))
        self.widget_16.setStyleSheet("padding: 0px")
        # self.widget_16.setStyleSheet("background: qlineargradient(x1: 0, y1:
        # 0, x2: 0, y2: 1, stop 0 rgb(170,209,193) , stop 1.0 rgb(240,240,240)
        # );")

        # Adding Content widget in the intermediate layout
        self.horizontalLayout_3.addWidget(self.widget_16)

        # centralLayout creation
        self.centralLayout = QtGui.QHBoxLayout(self.widget_16)
        self.centralLayout.setSpacing(0)
        self.centralLayout.setMargin(0)

        # Linking all intermediate layouts
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        SysGroveMain.setCentralWidget(self.centralwidget)

        # Qt Stuff
        self.retranslateUi(SysGroveMain)
        QtCore.QMetaObject.connectSlotsByName(SysGroveMain)

        self.lodep = 0

    # Maybe do something better later ...
    def retranslateUi(self, SysGroveMain):
        SysGroveMain.setWindowTitle(QtGui.QApplication.translate(
            "SysGroveMain", "SysGrove", None, QtGui.QApplication.UnicodeUTF8))

    def nofs(self):
        self.lodep += 1
        self.topbar.pb.hide()

    def refs(self):
        self.lodep -= 1
        if self.lodep == 0:
            self.topbar.pb.show()

# Good idea, but the code doesn't work ...
"""
    # If unsaved data is present, don't close the window
    def closeEvent(self, event):
        # do stuff
        print "me is clozed"
        if self.canExit():
            event.accept() # let the window close
        else:
            event.ignore()
"""


# Creates the splashscreen ...
pic = QtGui.QPixmap(":sysgrove.png")
splash = QtGui.QSplashScreen(pic)

# MainWindow creation
SysGroveMain = SysGroveMainWindow()
SysGroveMain.setWindowFlags(QtCore.Qt.FramelessWindowHint)
# SysGroveMain.setWindowFlags(QtCore.Qt.CustomizeWindowHint)

# Linking our window with what we created previously
ui = UI_SysGroveMain()
ui.setupUi(SysGroveMain)


# Defining global aliases for areas and layouts
GfxContent = ui.widget_16
GfxContentLayout = ui.centralLayout
GfxMenuBar = ui.widget_4
GfxAccordion = ui.widget_9
GfxAccordionLayout = ui.accordionLayout
GfxMenuBarLayout = ui.menuBarLayout
GfxStatusBar = ui.statusBar
GfxStatusBarLayout = ui.status_bar_layout
GfxTabBar = ui.widget_11
GfxTabBarLayout = ui.tabbar_layout
GfxDynamicButtonZone = ui.widget_13
GfxStaticButtonZone = ui.widget_14
GfxStaticButtonZoneGridLayout = ui.staticLayout
GfxWindow = ui.centralwidget
GfxSearch = ui.widget_12
GfxSearchLayout = ui.searchLayout
GfxDynamicButtonZoneGlobalLayout = ui.gdynamicLayout


# Application = app


def launch():
    buttonsSetup()
    menuSetup()
    # Need to be imported after MenuSetup since it defines
    # behaviour for LogButton
    # NOTE: this kind of "import with side effects" should be avoided using
    # something more explicit; I see this way of importing everywhere
    import sysgrove.core.log

    SysGroveMain.show()


def run_app():
    app.exec_()
    sys.exit()


class LauncherStatusBar(QtWebKit.QWebView):
    pass


from PyQt4.QtCore import pyqtSignal


class EnterPressLineEdit(QtGui.QLineEdit):

    enterPressed = pyqtSignal()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Enter or \
           event.key() == QtCore.Qt.Key_Return:
            self.enterPressed.emit()

        super(EnterPressLineEdit, self).keyPressEvent(event)


class GfxLauncher(object):

    def __init__(self):
        self.form = QtGui.QWidget()
        self.form.setWindowFlags(
            QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        sp = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        self.form.setSizePolicy(sp)

    def setupUi(self, config):

        self.titlebar = Topbar_titlebar(self.form, self.form)

        self.form.setObjectName(_fromUtf8("Form"))
        self.form.resize(506, 367)

        self.form.setStyleSheet("background-color: #cbcbce; color: #000000; ")
        self.titlebar.setGeometry(0, 0, 506, 20)
        # self.titlebar.show()

        self.closeButton = QtGui.QToolButton(self.form)
        self.closeButton.setGeometry(QtCore.QRect(486, 0, 20, 20))
        self.closeButton.setObjectName(_fromUtf8("closeButton"))

        self.closeButton.connect(
            self.closeButton, QtCore.SIGNAL("clicked()"), self.stop)

        self.toolButton = QtGui.QToolButton(self.form)
        self.toolButton.setGeometry(QtCore.QRect(400, 50, 81, 81))
        self.toolButton.setObjectName(_fromUtf8("toolButton"))

        self.toolButton.connect(self.toolButton, QtCore.SIGNAL(
            "clicked()"), self.start_app)

        self.toolButton_2 = QtGui.QToolButton(self.form)
        self.toolButton_2.setGeometry(QtCore.QRect(400, 150, 81, 81))
        self.toolButton_2.setObjectName(_fromUtf8("toolButton_2"))
        self.toolButton_3 = QtGui.QToolButton(self.form)
        self.toolButton_3.setGeometry(QtCore.QRect(400, 250, 81, 81))
        self.toolButton_3.setObjectName(_fromUtf8("toolButton_3"))
        self.lineEdit = QtGui.QLineEdit(self.form)
        self.lineEdit.setGeometry(QtCore.QRect(50, 190, 191, 20))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.lineEdit_2 = EnterPressLineEdit(self.form)
        self.lineEdit_2.setGeometry(QtCore.QRect(50, 250, 191, 20))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lineEdit_2.setEchoMode(2)
        self.lineEdit_2.enterPressed.connect(self.start_app)

        self.commandLinkButton = QtGui.QLabel(self.form)
        self.commandLinkButton.setGeometry(QtCore.QRect(130, 280, 191, 31))
        self.commandLinkButton.setObjectName(_fromUtf8("wrongPwd"))

        self.label = QtGui.QLabel(self.form)
        self.label.setGeometry(QtCore.QRect(50, 170, 101, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.form)
        self.label_2.setGeometry(QtCore.QRect(50, 220, 91, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.webView = LauncherStatusBar(self.form)
        self.webView.setGeometry(QtCore.QRect(0, 349, 511, 17))
        # http://ego-world.org/~will/index.html")))
        self.webView.setUrl(QtCore.QUrl(_fromUtf8("about:blank")))
        self.frame = QtGui.QLabel(self.form)
        self.frame.setGeometry(QtCore.QRect(0, 30, 381, 121))
        self.frame.setFrameShape(QtGui.QFrame.NoFrame)
        self.frame.setFrameShadow(QtGui.QFrame.Plain)
        self.frame.setObjectName(_fromUtf8("frame"))

        self.fl = QtGui.QHBoxLayout(self.frame)
        self.fl.setMargin(0)
        self.fl.setSpacing(0)

        self.ll = QtGui.QLabel(self.frame)
        self.pl = QtGui.QPixmap(':title.png')
        self.ll.setPixmap(self.pl)
        self.fl.addWidget(self.ll)

        versionLabel = QtGui.QLabel(self.form)
        if config["version_extended"] is not None:
            version = "%s - build %s " % (config["version"],
                                          config["version_extended"])
        else:
            version = config["version"]

        versionLabel.setText(version)

        self.retranslateUi()
        self.commandLinkButton.hide()
        QtCore.QMetaObject.connectSlotsByName(self.form)

    def retranslateUi(self):
        self.form.setWindowTitle(QtGui.QApplication.translate(
            "self.form",
            "Sysgrove Launcher",
            None,
            QtGui.QApplication.UnicodeUTF8))
        self.toolButton.setText(QtGui.QApplication.translate(
            "self.form", "dev", None, QtGui.QApplication.UnicodeUTF8))
        self.toolButton_2.setText(QtGui.QApplication.translate(
            "self.form", "test", None, QtGui.QApplication.UnicodeUTF8))
        self.toolButton_3.setText(QtGui.QApplication.translate(
            "self.form", "prod", None, QtGui.QApplication.UnicodeUTF8))
        self.commandLinkButton.setText(QtGui.QApplication.translate(
            "self.form",
            "Wrong Password",
            None,
            QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate(
            "self.form", "Username :", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate(
            "self.form", "Password :", None, QtGui.QApplication.UnicodeUTF8))

    def start(self, config):
        self.setupUi(config)
        self.form.show()

    def stop(self):
        self.form.hide()
        sys.exit()

    def start_app(self):
        u = str(self.lineEdit.text())
        p = str(self.lineEdit_2.text())
        try:
            User(code=u, pwd=p)
        except:
            self.commandLinkButton.show()
            log.error('Wrong user code or password')
            # self.stop()
            return

        # Hides the launcher
        self.form.hide()

        # FIXME : as sysgrove.updates import sysgrove.user
        # it has some problem maybe because of the singleton
        # maybe because the application is in other thread
        # maybe because of what we are doing with the root user
        # session.... rather dificult to check.
        # from sysgrove.updates import main
        # main()

        splash.show()
        splash.destroy()
        launch()


launcher = GfxLauncher()
