#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from functools import wraps
from PyQt4 import QtCore, QtGui
from sysgrove.config import CONFIG
from sysgrove.utils import slugify, UserMessage
from sysgrove.ui.window import (
    GfxContent,
    GfxTabBar,
    GfxTabBarLayout,
    GfxStaticButtonZoneGridLayout,
    GfxStaticButtonZone,
    GfxContentLayout,
    GfxMenuBar,
    GfxAccordion,
    GfxDynamicButtonZone,
    GfxAccordionLayout,
    GfxMenuBarLayout,
    GfxStatusBar,
    GfxStatusBarLayout,
    GfxDynamicButtonZoneGlobalLayout,
    GfxWindow,
    SysGroveMain,
    GfxSearch,
    GfxSearchLayout
)
from .uiobject import UIObject
from .widget import UIWidget
from .menu import UIMenuBarClass, UIMenuButton
from .statusbar import UIStatusBarClass
from .tab import UITabBarClass, UITabPanel, UITabWidget
from .spacer import UIVSpacer, UIHSpacer, UIVSplitter, UIHSplitter, UISpacer
from .dynamic_button import (
    UIDynamicButtonZoneInst,
    UIDynamicButton,
    UIStaticButtonZoneClass,
    UIStaticButtonClass
)
from .layout import UILayout, UIVLayout, UIGridLayout, UIHLayout
from .dynamic_button import UIDynamicButtonZoneClass
from .search import UISearchClass
from .button import UIButton
from .radiobutton import UIRadioButtonContainer, UIRadioButton
from .checkbox import UICheckBox, UICheckBoxActivable
from .frame import UIFrame
from .groupbox import UIGroupBox, UIGroupBoxWM
from .label import UILabel
from .date import UIMonth, UIDate
from .scrollarea import UIScrollArea
from .filedialog import UIFileDialog
from .table import LinkedTable
from .table import CheckedTable, SelectQtyTable, ManyKeyTable
from .lineedit import UILineEdit, FocusStyledLineEdit
from .textarea import UITextArea
from .web import UIWeb
from .datalookup import (
    SDataLookup,
    UIDataLookup, UIDataLookupPopup,
    # FIXME: should more in sysgrove.modules.xxx.ui.customs.py
    UIDataLookupExchangRate,
    UIDataLookupDocument,
    UIDataLookupDocument_O_PE,
    # UIDataLookupThirdParty,
    UIDataLookupSiteWithDCR
)
from .form import UIForm
from .buttonbar import UIButtonBar, UIButtonBarContent, UIButtonBarZone
from .tree import UITree, TreeWidgetItem
from .popup import UIPopup
from .inline import UIInline
from .popped import UIPoppedWidget
from .message import WarningPopup, show_info
from sysgrove.ui.classes.widgets.message_popup import MessagePopup


log = logging.getLogger('sysgrove')


class UIContentClass(UIObject):

    """ Class that is used to create UIContent (from GfxContent) """

    def __init__(self, area, layout, name):
        self.qtObject = area
        self.layout = layout
        self.name = name
        self.__items__ = {}
        # Note, we don't use UIObject constructor because parent is not an
        # UIObject, so we need to define a __items__ here


class UISpace (UIWidget):

    def __init__(self, parent="", name=""):
        super(UISpace, self).__init__(UIContent, name)
        self.qtObject.setStyleSheet("border: None;")
        self.layout = UIVLayout(self, "zigjs")
        UIContent.luisbi = self  # lastUISpaceBeingInstanciated
        #
        # FIXME FIXME : Add UISPACE instance (self) in UIContent (in a dico
        # maybe)  -->> necessary to access the UISpace during it's own setup.


UISearch = UISearchClass(GfxSearch, GfxSearchLayout)
UISearch.addSearchbarToLayout(GfxSearchLayout)
UIMenuBar = UIMenuBarClass(GfxMenuBar, GfxMenuBarLayout)
UIStatusBar = UIStatusBarClass(GfxStatusBar, GfxStatusBarLayout, "StatusBar")
UITabBar = UITabBarClass(
    GfxTabBar,
    GfxTabBarLayout,
    "tabBar",
    CONFIG['context_limit']
)
UIStaticButtonZone = UIStaticButtonZoneClass(
    GfxStaticButtonZone,
    GfxStaticButtonZoneGridLayout,
    "UIStaticButtonZone"
)
UIDynamicButtonZone = UIDynamicButtonZoneClass(
    GfxDynamicButtonZone,
    GfxDynamicButtonZoneGlobalLayout,
    "UIDynamicButtonZone"
)
UIContent = UIContentClass(GfxContent, GfxContentLayout, "UIContent")


class UIMenus(object):

    def __init__(self, image, text, staticZone, bar, code, static=False):
        self.image = image
        self.text = text
        self.static = static
        self.bar = bar
        self.code = code
        self.staticZone = staticZone

    def setupUi(self):
        slug = slugify(self.text)
        if self.static:
            item = self.staticZone.addStaticButton(slug, self.code)
        else:
            item = self.bar.addMenuButton(slug, self.code)
            item.addLink(self.code)

        item.addImage(self.image)
        item.setText(self.text)
        self.retranslateUi()

    def retranslateUi(self):
        pass


class UIAccordeon(UIObject):

    def __init__(self, parent, name, menu):
        self.menu = menu
        super(UIAccordeon, self).__init__(parent, name)

    def setupUi(self):
        self.accordion = UITree(self.parent, "accordion")
        header = self.accordion.headerItem()
        header.setHidden(True)

        def addChild(parent, child):
            code, name, subentry = child[:3]
            tree = TreeWidgetItem(parent, name, code)
            for entry in subentry:
                addChild(tree, entry)

        for entry in self.menu:
            addChild(self.accordion, entry)

        def action(tree, i):
            from sysgrove.context import kernel
            if tree.childs == []:
                kernel.set_current_context(tree.code)

        self.accordion.setAction(
            "itemClicked (QTreeWidgetItem *,int)",
            action
        )
        return self.accordion


class ExceptionMessagePopup(MessagePopup):

    def __init__(self, parent, exception, screen, name="iValid"):
        log.error(exception)
        logging.exception(exception)
        message = u"An exception has occurred %s" % exception
        super(ExceptionMessagePopup, self).__init__(None, None,
                                                    [message], parent)
        # the title is inherited, we just need to set the text
        self.setWindowTitle('Error!!!')
        self.closed.connect(self.hide)
