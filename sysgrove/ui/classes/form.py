#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : contact@sysgrove.com

20/02/2013
Version 1.0

revision # 1
Date of revision: 25/08/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  cou can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore, QtGui
import logging
import datetime
from sysgrove.settings import MODE_nn
from sysgrove.utils import (
    get_class, debug_trace,
    get_attr_and_class, code2unicode,
    slugify, retreive_class, getattr_ex,
    retreive_class_ex, getattr_info,
    getattr_verbose_name
)
from .widget import UIWidget
from .layout import UIHLayout, UIVLayout, UILayout, UIGridLayout
from .textarea import UITextArea
from .label import UILabel
from .lineedit import UILineEdit, UINumericLineEdit
from .datalookup import UIDataLookup
from .checkbox import UICheckBox
from .radiobutton import UIRadioButton, UIRadioButtonContainer
from .tab import UITabPanel, UITabWidget
from .date import UIDate, UIMonth
from .spacer import UIHSpacer, UIVSpacer, UISpacer
from .groupbox import UIGroupBox
from .inline import UIInline
from .table import LinkedTable, DataTable, ManyKeyTable, build_columns
from .button import UIButton
from .colordialog import UIColorDialog
from .uiobject import UIObject
from sqlalchemy import Enum, String, Date, Boolean, Text, Integer, Float

log = logging.getLogger('sysgrove')


class UIForm(UIWidget):

    def __init__(self, parent, name):
        super(UIForm, self).__init__(parent, name)
        if isinstance(parent, UITabWidget):
            self.layout = UIVLayout(parent, 'TabWidgetFormLayout')
        elif isinstance(parent, UIGroupBox):
            self.layout = UIVLayout(parent, 'GroupBoxLayout')
        elif isinstance(parent, UIForm):
            self.layout = UIVLayout(None, 'FormLayout')
        else:
            self.layout = UIVLayout(self, 'Layout')
        self.datamodel = None
        self.form = None
        self.parent = parent

    def setAlchemyClass(self, klass, prefix=None):
        if isinstance(klass, str):
            self.datamodel = get_class(klass)
        else:
            self.datamodel = klass
        if prefix:
            self.prefix = prefix
        else:
            self.prefix = self.datamodel.get_ui_prefix()

    def setDisplay(self, display):
        if isinstance(display, list):
            self.form = display
        elif isinstance(display, str):
            if self.datamodel:
                self.form = getattr(self.datamodel, display, None)

    def attributes(self, code=MODE_nn, space=True):
        if not self.form:
            self.form = self.datamodel.list_display
        maxLabelWidth = 0
        for f in self.form:
            (width, res) = self.build_form(f, code, self.datamodel)
            if res:
                if isinstance(res, UILayout):
                    self.layout.addLayout(res)
                else:
                    self.layout.addWidget(res)
            maxLabelWidth = max(maxLabelWidth, width)

        if space:
            spacer = UIVSpacer(self, "spacer")
            self.layout.addSpacerItem(spacer)

        label_align = []
        for f in self.form:
            if isinstance(f, str):
                label_align.append(f)
            elif isinstance(f, list):
                label_align.append(f[0])
            elif isinstance(f, tuple):
                label_align.append(f[1])
        # resize the label
        for f in label_align:
            if isinstance(f, str):
                lname = 'label' + self.prefix + f
                if lname in self.__items__:
                    label = self.__items__[lname]
                    label.setFixedWidth(maxLabelWidth)
        return (maxLabelWidth, self.layout)

    def build_form(self, form, code, klass):
        if isinstance(form, list):
            return self.list_attr(form, code, klass)
        elif isinstance(form, str):
            (subklass, attr) = get_attr_and_class(form, klass)
            return self.get_type(
                attr, code, subklass, self.prefix + form)
        elif isinstance(form, tuple):
            return self.tuple_attr(form[0], form[1:][0], code, klass)

    def list_attr(self, form, code, klass):
        hlayout = UIHLayout(None, 'Hlayout')
        if len(form) == 1 or klass.verbose_name == u'Document Copy Rule':
            spacer = UIHSpacer(self, "spacer")
            hlayout.addSpacerItem(spacer)

        res_width = None
        for attr in form:
            (width, res) = self.build_form(attr, code, klass)
            # let's keep the first
            if not res_width:
                res_width = width
            if res:
                if isinstance(res, UILayout):
                    hlayout.addLayout(res)
                elif isinstance(res, UISpacer):
                    hlayout.addStretch(1)
                else:
                    hlayout.addWidget(res)
        spacer = UIHSpacer(self, "spacer")
        hlayout.addSpacerItem(spacer)
        return (res_width, hlayout)

    def tab_attr(self, form, code, klass):
        hlayout = UIHLayout(None, 'TabLayout')
        tab = UITabPanel(self, 'TabPanel')
        for f in form:
            title = f[0]
            tab_widget = UITabWidget(tab, 'TabWidget' + title)
            tab_widget.setTitle(title)
            subform = UIForm(tab_widget, 'TabWidgetForm' + title)
            subform.setDisplay(f[1])
            subform.setAlchemyClass(klass, self.prefix)
            subform.attributes(code)
        hlayout.addWidget(tab)
        return (0, hlayout)

    def vlist_attr(self, form, code, klass):
        # subform must have a unique name
        fields = self.form2attr_list(form)
        subform = UIForm(self, 'VListForm%s' % fields[0])
        subform.setDisplay(form)
        subform.setAlchemyClass(klass, self.prefix)
        return (subform.attributes(code))

    def groupbox_attr(self, gbname, form, code, klass):
        if isinstance(gbname, int):
            name = str(gbname)
        elif isinstance(gbname, str):
            name = gbname.replace(' ', '_')
        elif isinstance(gbname, unicode):
            name = slugify(gbname)
        gb = UIGroupBox(self, 'GroupBox' + klass.__name__ + name)

        if isinstance(gbname, int):
            gb.setTitle(u"%s %s" % (code2unicode(code), klass.verbose_name))
        else:
            gb.setTitle(gbname)
        layout = UIVLayout(None, 'GroupBoxLayout%s' % name)

        if isinstance(form, list):
            subform = UIForm(gb, 'UIFormGB%s' % name)
            subform.setDisplay(form)
            subform.setAlchemyClass(klass, self.prefix)
            subform.attributes(code)
        elif isinstance(form, tuple):
            (res, subform) = self.tuple_attr(form[0], form[1:][0], code, klass)
            gb.setLayout(subform.qtObject)
        else:
            (res, subform) = self.build_form(form, code, klass)
            gb.setLayout(subform.qtObject)

        layout.addWidget(gb)
        return (0, layout)
        # return (0,gb)

    # Very special case for TP in Document header
    # IncoterTermText and PaymentText
    def groupBoxRes_attr(self, gbname, form, code, klass):
        gb = UIGroupBox(self, 'GroupBox' + form[0])
        gb.setTitle(gbname)
        layout = UIVLayout(gb, "GBResLayout" + form[0])
        # first of form must be represent by a DataLoopUp, other are just label
        if 'datalookup' in getattr(klass, form[0]).info:
            datalookup = get_class(getattr(klass, form[0]).info['datalookup'])(
                gb, self.prefix + form[0])
        else:
            datalookup = UIDataLookup(gb, self.prefix + form[0])
        datalookup.setCharSize(10)

        layout.addWidget(datalookup)

        subklass = retreive_class(form[0], klass)

        for field in form[1:]:
            if isinstance(field, str):
                if '_' in field:
                    sp = field.split('_')
                    if len(sp) == 2:
                        mapper = subklass.__mapper__
                        ctype = mapper.get_property(sp[1]).expression.type
                        if isinstance(ctype, Text):
                            ui = UITextArea(gb, self.prefix + field)
                            ui.setMinimumWidth(480)
                        else:
                            ui = UILabel(gb, self.prefix + field)
                    else:
                        ui = UILabel(gb, self.prefix + field)
                else:
                    label = UILabel(gb, self.prefix + field)
                layout.addWidget(ui)
            elif isinstance(field, list):
                hlayout = UIHLayout(None, 'GBHlayout')
                for f in field:
                    label = UILabel(gb, self.prefix + f)
                    hlayout.addWidget(label)
                spacer = UIHSpacer(gb, 'spacer')
                hlayout.addSpacerItem(spacer)
                layout.addLayout(hlayout)

        return (0, gb)

    def inline_attr(self, subklass, form, code, klass):
        lklass = retreive_class(subklass, klass)
        layout = UIHLayout(None, 'UIFormIL%s' % form)
        inline = UIInline(self, self.prefix + form, lklass)
        layout.addWidget(inline)
        return (0, layout)

    def table_attr(self, name, columns, code, klass, extra_args={}):
        layout = UIHLayout(None, 'TableLayout%s' % name)
        table = LinkedTable(self,
                            "table_%s" % name,
                            columns=build_columns(klass, columns),
                            klass=klass)

        # deal with extra_args
        for (f, v) in extra_args.iteritems():
            getattr(table, f)(v)

        layout.addWidget(table)
        # spacer = UIHSpacer(self, "spacer")
        # layout.addSpacerItem(spacer)
        return (0, layout)

    def linked_table_attr(self, name, code, klass, noPrefix=False):
        layout = UIHLayout(None, 'TableLayout%s' % name)
        if not noPrefix:
            name = self.prefix + name
        pk = klass.pk_attrs()
        assert(len(pk) >= 1)
        if len(pk) == 1:
            table = LinkedTable(self, name, klass=klass)
        else:
            table = ManyKeyTable(self, name,
                                 pk,
                                 klass=klass)
        layout.addWidget(table)
        return (0, layout)

    def dataGb_attr(self, attr, code, klass, length=10):
        gb = UIGroupBox(self, 'GroupBox%s' % attr)
        info = getattr_info(attr, klass)
        gb.setTitle(info['verbose_name'])
        hlayout = UIHLayout(gb, 'Hlayout' + attr)
        if 'datalookup' in info:
            datalookup = get_class(info['datalookup'])(
                gb, self.prefix + attr)
        else:
            datalookup = UIDataLookup(gb, self.prefix + attr)
        datalookup.setCharSize(length)
        hlayout.addWidget(datalookup)

        if 'description' in info:
            for d in info['description']:
                dlabel = UILabel(gb, self.prefix + attr + "_" + d)
                dlabel.setCharSize(10)
                hlayout.addWidget(dlabel)
        else:
            descriptionlabel = UILabel(gb, self.prefix + attr + "_description")
            descriptionlabel.setCharSize(40)
            hlayout.addWidget(descriptionlabel)

        spacer = UIHSpacer(gb, 'spacer')
        hlayout.addSpacerItem(spacer)

        spacer = UIHSpacer(self, "spacer")
        outerlayout = UIHLayout(None, 'GroupBoxLayout%s' % attr)
        outerlayout.addWidget(gb)
        outerlayout.addSpacerItem(spacer)
        return (0, outerlayout)

    def dateGb_attr(self, attr, code, klass, uiKlass=UIDate):
        if isinstance(attr, tuple):
            maxDate = attr[1]
            attr = attr[0]
        else:
            maxDate = None
        gb = UIGroupBox(self, 'GroupBox%s' % attr)
        info = getattr_info(attr, klass)

        gb.setTitle(info['verbose_name'])
        gb.setMaximumWidth(200)
        hlayout = UIHLayout(gb, 'Hlayout' + self.prefix + attr)
        name = self.prefix + attr

        if (name == "thirdParty_validFromDate"):
            date = self.dateFromTP = UIDate(
                gb, self.prefix + attr, typeDouble="From")
        elif (name == "_reqDeliveryStart"):
            date = self.dateFromRD = UIDate(
                gb, self.prefix + attr, typeDouble="From")
        elif (name == "_calcDeliveryStart"):
            date = self.dateFromCD = UIDate(
                gb, self.prefix + attr, typeDouble="From")
        elif (name == "thirdParty_validTillDate"):
            date = self.dateTillTP = UIDate(
                gb, self.prefix + attr, typeDouble="Till")
            self.dateFromTP.addLink(self.dateTillTP)
            self.dateTillTP.addLink(self.dateFromTP)
        elif (name == "_reqDeliveryEnd"):
            date = self.dateTillRD = UIDate(
                gb, self.prefix + attr, typeDouble="Till")
            self.dateFromRD.addLink(self.dateTillRD)
            self.dateTillRD.addLink(self.dateFromRD)
        elif (name == "_calcDeliveryEnd"):
            date = self.dateTillCD = UIDate(
                gb, self.prefix + attr, typeDouble="Till")
            self.dateFromCD.addLink(self.dateTillCD)
            self.dateTillCD.addLink(self.dateFromCD)
        else:
            date = uiKlass(gb, self.prefix + attr)

        if maxDate:
            if maxDate == 'today':
                date.setMaxDate(datetime.date.today())

        hlayout.addWidget(date)
        outerlayout = UIHLayout(None, 'GroupBoxLayout%s' % attr)
        outerlayout.addWidget(gb)
        return(0, outerlayout)

    # Something like a single ligne table but
    # built with label and UIlineEdit in order to access
    # to element
    def hbox_attr(self, name, form, code, klass):
        outerlayout = UIHLayout(None, "OuterLayout%s" % name)
        layout = UIGridLayout(None, 'Layout%s' % name)
        for i in range(len(form)):
            field = form[i]

            label = UILabel(self, 'Label%s' % field)
            label.setText(getattr(klass, field).info['verbose_name'])
            lineEditClass = UILineEdit
            if klass.is_direct(field):
                mapper = klass.__mapper__
                ctype = mapper.get_property(field).expression.type
                if isinstance(ctype, Float):
                    lineEditClass = UINumericLineEdit

            lineedit = lineEditClass(self, self.prefix + field)
            layout.addWidget(label, 0, i)
            layout.addWidget(lineedit, 1, i)
        spacer = UIHSpacer(self, 'spacer')
        outerlayout.addLayout(layout)
        outerlayout.addSpacerItem(spacer)
        return(0, outerlayout)

    def button_attr(self, text, name, code, klass):
        layout = UIHLayout(None, 'ButtonLayout%s' % name)
        button = UIButton(self, self.prefix + name)
        button.setText(text)
        button.setStyleSheet("""
            border: 2px solid #8f8f91;
            border-radius: 4px;
        """)
        layout.addWidget(button)
        layout.addStretch(1)
        return (0, layout)

    def class_attr(self, form, code, klass, uiKlass, args):
        info = getattr_info(form, klass)
        layout = UIHLayout(None, '%s%s' % (uiKlass.__name__, form))
        # label = self.create_label(info['verbose_name'], self.prefix + form)
        # layout.addWidget(label)
        ui = uiKlass(self, self.prefix + form, *args)
        width = 0
        if hasattr(ui, 'setTitle'):
            ui.setTitle(info['verbose_name'])
        else:
            label = self.create_label(info['verbose_name'], self.prefix + form)
            layout.addWidget(label)
            width = label.sizeHint().width()

        spacer = UIHSpacer(self, 'spacer')
        layout.addWidget(ui)
        layout.addSpacerItem(spacer)
        return(width, layout)

    def amount_attr(self, form, code, klass):
        from sysgrove.core.document.ui.customs import UIAmount
        name = self.prefix + form
        text = getattr(klass, form).info['verbose_name']
        hlayout = UIHLayout(None, 'Hlayout' + name)
        label = self.create_label(text, name)
        amount = UIAmount(self, name)
        amount.setDocument()
        hlayout.addWidget(label)
        hlayout.addWidget(amount)
        return (label.sizeHint().width(), hlayout)

    def spacer_attr(self, form):
        klass = get_class('sysgrove.ui.classes.UI%sSpacer' % form)
        spacer = klass(self, 'spacer')
        return(0, spacer)

    def create_clickShow(self, name, text, form, code, klass):
        click = UICheckBox(self, name)
        click.setText(text)
        layout = UIVLayout(None, 'clickShowLayout%s' % name)
        layout.addWidget(click)

        if isinstance(form, list):
            subform = UIForm(self, 'UIFormGB%s' % name)
            subform.setDisplay(form)
            subform.setAlchemyClass(klass, self.prefix)
            subform.attributes(code)
            # if getattr(subform, "docType_increaseStockFlag"):
            #     self.checkBox_atLeastOne(
            #         subform.docType_increaseStockFlag,
            #         subform.docType_decreaseStockFlag)
            layout.addLayout(subform.layout)
        elif isinstance(form, tuple):
            (res, subform) = self.tuple_attr(form[0], form[1:][0], code, klass)
            layout.addWidget(subform)
        else:
            (res, subform) = self.build_form(form, code, klass)
            layout.addWidget(subform)

        subform.setVisible(False)
        click.stateChanged.connect(subform.setVisible)

        if hasattr(subform, "docType_increaseStockFlag"):
            self.checkBox_atLeastOne(
                subform.docType_increaseStockFlag,
                subform.docType_decreaseStockFlag)
            click.stateChanged.connect(
                subform.docType_increaseStockFlag.setChecked)
            click.stateChanged.connect(
                subform.docType_decreaseStockFlag.setChecked)
        layout.addWidget(subform)
        return(0, layout)

    def checkBox_atLeastOne(self, one, two):
        def connectCheckBoxOne():
            if (one.isChecked() is False
                and two.isChecked() is False
                    and one.isVisible() is True):
                two.setChecked(True)

        def connectCheckBoxTwo():
            if (one.isChecked() is False
                and two.isChecked()is False
                    and one.isVisible() is True):
                one.setChecked(True)

        one.stateChanged.connect(connectCheckBoxOne)
        two.stateChanged.connect(connectCheckBoxTwo)
        one.isVisible

    def create_dtable(self, dim, form, code, klass):
        layout = UIHLayout(None, 'DataTableLayout%s' % dim['cell'])
        table = DataTable(self, self.prefix + dim['cell'], klass, dim)
        layout.addWidget(table)
        return (0, layout)

    def tuple_attr(self, disc, form, code, klass):
        if disc == 'TabPanel':
            return self.tab_attr(form, code, klass)
        elif disc == 'Vlist':
            return self.vlist_attr(form, code, klass)
        elif disc == 'DataLookUpGB':
            return self.dataGb_attr(form, code, klass)
        elif disc == 'DateGB':
            return self.dateGb_attr(form, code, klass)
        elif disc == 'MonthGB':
            return self.dateGb_attr(form, code, klass, uiKlass=UIMonth)
        elif disc == 'UIAmount':
            return self.amount_attr(form, code, klass)
        elif disc == 'UIMonth':
            text = getattr_verbose_name(form, klass)
            return self.create_date_frame(text, self.prefix + form, UIMonth)
        elif disc == 'UIColor':
            return self.color_attr(form, code, klass)
        elif disc == 'Spacer':
            return self.spacer_attr(form)
        elif disc == 'DefaultTable':
            return self.linked_table_attr("%s_%s" % (form, klass.__name__),
                                          code,
                                          klass,
                                          noPrefix=True)
            #text = getattr_verbose_name(form, klass)
            # return self.create_date_frame(text, self.prefix + form, UIMonth)
        # FIXME : most of the previous case (UIMonth, ComboBox,..)
        # could be handled in the following elif
        elif isinstance(disc, str):
            return self.class_attr(form, code, klass, get_class(disc), [])
        elif isinstance(disc, tuple):
            if disc[0] == 'GroupBox':
                return self.groupbox_attr(disc[1], form, code, klass)
            elif disc[0] == 'GroupBoxRes':
                return self.groupBoxRes_attr(disc[1], form, code, klass)
            elif disc[0] == 'Inline':
                return self.inline_attr(disc[1], form, code, klass)
            # FIXME Not yet used, and wrong call of linked_table_attr
            # elif disc[0] == 'Table':
            #     if disc[2]:
            #         return self.linked_table_attr(
            #             disc[1], form, code,
            #             retreive_class(disc[2]), disc[3])
            #     else:
            # return self.table_attr(disc[1], form, code, klass, disc[3])
            elif disc[0] == 'LinkedTable':
                return self.linked_table_attr(form,
                                              code,
                                              retreive_class(disc[1]))
            elif disc[0] == 'STable':
                    return self.hbox_attr(disc[1], form, code, klass)
            elif disc[0] == 'Button':
                return self.button_attr(disc[1], form, code, klass)
            elif disc[0] == 'Label':
                return (0, self.create_label(disc[1], form))
            elif disc[0] == 'ClickShow':
                return self.create_clickShow(disc[1], disc[2],
                                             form, code, klass)
            elif disc[0] == 'ClickShowS':
                # a different case where the field corresponding to
                # the checkBox correspond to a column.
                return self.create_clickShow(self.prefix + disc[1], disc[2],
                                             form, code, klass)
            elif disc[0] == 'DTable':
                return self.create_dtable(disc[1], form, code, klass)
            elif isinstance(disc[0], str):
                return self.class_attr(form, code, klass,
                                       get_class(disc[0]), disc[1:])
        else:
            raise Exception('NYI')

    def get_type(self, c, code, klass, name):
        # info = getattr(getattr(klass, c), 'info', {})
        info = getattr_info(c, klass)
        if 'verbose_name' in info:
            text = info["verbose_name"]
        else:
            text = name
        if 'length' in info:
            length = info["length"]
        else:
            length = 10
        if 'toolTip' in info:
            toolTip = info['toolTip']
        else:
            toolTip = None

        if not klass.is_direct(c):
            return self.create_fk_frame(c, text, name, klass)
        else:
            mapper = klass.__mapper__
            ctype = mapper.get_property(c).expression.type
            if isinstance(ctype, Enum):
                return self.create_enum(text, name, ctype)
            elif isinstance(ctype, Text):
                return self.create_item('', name, UITextArea, toolTip)
            elif isinstance(ctype, String):
                return self.create_string_frame(
                    c, code, text, name, klass, length)
            elif isinstance(ctype, Boolean):
                return self.create_item(text, name, UICheckBox, toolTip)
            elif isinstance(ctype, Date):
                return self.create_date_frame(text, name)
            elif isinstance(ctype, Integer):
                return self.create_lineedit_frame(text,
                                                  name,
                                                  length)
            else:
                return self.create_lineedit_frame(text,
                                                  name,
                                                  length,
                                                  lineClass=UINumericLineEdit)

    def create_label(self, text, name):
        label = UILabel(self, "label" + name)
        label.setVal(text)
        width = label.sizeHint().width()
        label.setFixedWidth(width + 4)
        return label

    def create_lineedit_frame(self, text, name, length=10,
                              lineClass=UILineEdit):
        hlayout = UIHLayout(None, 'Hlayout' + name)
        label = self.create_label(text, name)
        lineedit = lineClass(self, name)
        lineedit.setCharSize(length)
        hlayout.addWidget(label)
        hlayout.addWidget(lineedit)
        spacer = UIHSpacer(self, "spacer")
        hlayout.addSpacerItem(spacer)
        return (label.sizeHint().width(), hlayout)

    def create_item(self, text, name, uiClass, toolTip=None):
        item = uiClass(self, name)
        item.setText(text)
        if toolTip:
            item.setToolTip(toolTip)
        return (0, item)

    # FIXME: no more call....
    # def create_text(self, name):
    #     layout = UIVLayout(None, 'Hlayout' + name)
    #     item = UITextArea(self, name)
    #     layout.addWidget(item)
    #     spacer = UIVSpacer(self, "spacer")
    #     layout.addSpacerItem(spacer)
    #     return (0, layout)

    def color_attr(self, form, code, klass):
        name = self.prefix + form
        hlayout = UIHLayout(None, 'Hlayout' + name)
        label = self.create_label("Color", name)
        hlayout.addWidget(label)
        color = UIColorDialog(self, name)
        hlayout.addWidget(color)
        spacer = UIHSpacer(self, "spacer")
        hlayout.addSpacerItem(spacer)
        return(label.sizeHint().width(), hlayout)

    def create_date_frame(self, text, name, uiClass=UIDate):
        hlayout = UIHLayout(None, 'Hlayout' + name)
        label = self.create_label(text, name)
        hlayout.addWidget(label)
        if (name == "thirdParty_validFromDate"):
            date = self.dateFromTPV = UIDate(self, name, typeDouble="From")
        elif (name == "_reqDeliveryStart"):
            date = self.dateFromRDS = UIDate(self, name, typeDouble="From")
        elif(name == "thirdParty_validTillDate"):
            date = self.dateTillTPV = UIDate(self, name, typeDouble="Till")
            self.dateFromTPV.addLink(self.dateTillTPV)
            self.dateTillTPV.addLink(self.dateFromTPV)

        elif (name == "_reqDeliveryEnd"):
            date = self.dateTillRDE = UIDate(self, name, typeDouble="Till")
            self.dateFromRDS.addLink(self.dateTillRDE)
            self.dateTillRDE.addLink(self.dateFromRDS)
        else:
            date = uiClass(self, name)

        hlayout.addWidget(date)
        spacer = UIHSpacer(self, "spacer")
        hlayout.addSpacerItem(spacer)
        return(label.sizeHint().width(), hlayout)

    def create_enum(self, text, name, ctype):
        gb = UIGroupBox(self, 'GroupBoxEnum' + name)
        gb.setTitle(text)
        layout = UIHLayout(gb, 'LayoutEnum%s' % name)
        rcontainer = UIRadioButtonContainer(gb, name)
        for val in ctype.enums:
            radio = UIRadioButton(rcontainer, "radioButton%s" % val)
            radio.setText(val)
            radio.setValue(val)
        layout.addWidget(rcontainer)
        spacer = UIHSpacer(None, "spacer")
        layout.addSpacerItem(spacer)
        return (0, gb)

    def create_string_frame(self, c, code, text, name, klass, length=10):
        if c == 'code' and code in ["02", "03", "04"]:
            return self.create_fk_frame(c, text, name, klass, length)
        else:
            return self.create_lineedit_frame(text, name, length)

    def create_fk_frame(self, attr, text, name,  klass, length=10):
        hlayout = UIHLayout(None, 'Hlayout' + name)
        label = self.create_label(text, name)
        hlayout.addWidget(label)
        custom_datalookup = getattr(klass, 'datalookup', None)
        targetKlass = retreive_class_ex(name, klass)
        if attr == 'code':
            if custom_datalookup:
                datalookup = get_class(custom_datalookup)(self, name)
            else:
                datalookup = UIDataLookup(self, name)
        else:
            if 'datalookup' in getattr(klass, attr).info:
                datalookup = get_class(
                    getattr(klass, attr).info['datalookup'])(self, name)
            else:
                datalookup = UIDataLookup(self, name)

        datalookup.setCharSize(length)
        hlayout.addWidget(datalookup)
        descriptionlabel = UILabel(self, name + "_description")
        if hasattr(targetKlass, 'description'):
            info = targetKlass.description.info
            descriptionlabel.setCharSize(info['length'])
        else:
            descriptionlabel.setCharSize(length)
        hlayout.addWidget(descriptionlabel)

        spacer = UIHSpacer(self, "spacer")
        hlayout.addSpacerItem(spacer)
        return (label.sizeHint().width(), hlayout)

    @classmethod
    def form2attr_list(cls, form):
        def iter(form, res):
            if isinstance(form, str):
                res.append(form)
            elif isinstance(form, list):
                for f in form:
                    iter(f, res)
            elif isinstance(form, tuple):
                disc = form[0]
                if disc == 'DefaultTable':
                    pass
                elif isinstance(disc, tuple) and disc[0] in ['Button', 'Label']:
                    pass
                elif isinstance(disc, tuple) and disc[0] in ['ClickShowS']:
                    res.append(disc[1])
                    for f in form[1:]:
                        iter(f, res)
                else:
                    for f in form[1:]:
                        iter(f, res)
        res = []
        iter(form, res)
        return res

    def setVisible(self, visible):
        # visible = not visible == 0
        # for child in self.qtObject.children():
        for child in self.__items__.values():
            if isinstance(child, UIObject):
                child.setVisible(visible)
