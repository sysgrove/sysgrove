#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from sysgrove.config import CONFIG
from sysgrove.utils import cc_prefix
from .spacer import UIHSpacer
from .window import UIWindow
from .uiobject import UIObject
from .widget import UIWidget


class UIMenuBarClass(UIWidget):

    def __init__(self, gfx, layout):

        self.gfxParent = gfx

        self.qtObject = QtGui.QWidget(gfx)
        self.qtObject.lower()
        self.qtObject.setObjectName("menubar")
        self.qtObject.setStyleSheet("border: none;")

        self.qtObjectLayout = QtGui.QHBoxLayout(self.qtObject)
        self.qtObjectLayout.setDirection(QtGui.QBoxLayout.LeftToRight)
        self.qtObjectLayout.setMargin(0)

        self.spacer = QtGui.QSpacerItem(
            1, 1, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        layout.addSpacerItem(self.spacer)
        layout.addWidget(self.qtObject)
        spacer = UIHSpacer(self, 'spacer')
        self.layout = layout

        self.menu_buttons = {}
        self.print_buttons = {}

        self.offset = 0
        self.old_offset = 0
        #
        # When uiwindow will be resized all functions added with
        # addResizeEvent will be called.
        # Here the self.resizeEv is used to autorescroll menubar
        # if enough space is available
        #
        UIWindow.addResizeEvent(self.resizeEv)

    def resizeEv(self):
        if self.offset != 0:
            self.old_offset = self.offset
            self.offset = 0
        else:
            self.offset = self.old_offset
            self.old_offset = 0
        #self.moverWidget.move(self.offset, 0)

    # def setGrid(self):

    #     self.offset = 0
    #     self.max_offset = self.gfxParent.width() - 42

    #     self.leftButton = QtGui.QToolButton(self.qtObject)
    #     self.rightButton = QtGui.QToolButton(self.qtObject)

    #     self.leftButton.setStyleSheet("""
    #         background-color: #ddd;
    #         background-image: url(':arrow-left.png');
    #         background-repeat: no-repeat;"""
    #                                   )
    #     self.rightButton.setStyleSheet("""
    #         background-color: #ddd;
    #         background-image: url(':arrow-right.png');
    #         background-repeat: no-repeat;"""
    #                                    )

    #     self.leftButton.setAutoRepeat(True)
    #     self.rightButton.setAutoRepeat(True)

    #     self.leftButton.setAutoRepeatInterval(1)
    #     self.rightButton.setAutoRepeatInterval(1)

    #     #self.qtObjectLayout.addWidget(self.leftButton)

    #     self.qtObjectLayout.setSpacing(3)
    #     self.qtObjectLayout.setMargin(0)

    #     self.leftButton.setMinimumSize(QtCore.QSize(21, 84))
    #     self.leftButton.setMaximumSize(QtCore.QSize(21, 84))
    #     self.rightButton.setMinimumSize(QtCore.QSize(21, 84))
    #     self.rightButton.setMaximumSize(QtCore.QSize(21, 84))

    #     #self.gridWidget = QtGui.QWidget(self.qtObject)

    #     #self.qtObjectLayout.addWidget(self.gridWidget)

    #     #self.qtObjectLayout.addWidget(self.rightButton)

    #     #self.moverWidget = QtGui.QWidget(self.gridWidget)
    #     #self.moverWidget.setStyleSheet(
    #     #    "margin-top: 2px;margin-left: 4px;  background-color: #f6f6f6;")
    #     #self.gridLayout = QtGui.QGridLayout(self.qtObject)

    #     #self.gridLayout.setMargin(0)
    #     #self.gridLayout.setSpacing(2)

    #     self.leftButton.connect(self.leftButton, QtCore.SIGNAL(
    #         _fromUtf8("pressed()")), lambda: self.moveLeft())
    #     self.rightButton.connect(self.rightButton, QtCore.SIGNAL(
    #         _fromUtf8("pressed()")), lambda: self.moveRight())

    # def moveLeft(self):
    #     if self.offset < 0:
    #         self.offset += 1
    #     # setStyleSheet("padding-left: "+str(self.offset)+"px;")
    #     #self.moverWidget.move(self.offset, 0)

    # def moveRight(self):
    #     self.max_offset = self.gfxParent.width() - 42 - len(
    #         self.menu_buttons) * 87

    #     if self.offset > self.max_offset:
    #         self.offset -= 1
        # self.moverWidget.setStyleSheet("padding-left:
        # "+str(self.offset)+"px;")
        #self.moverWidget.move(self.offset, 0)

    def addMenuButton(self, name, code):
        for b in self.menu_buttons:
            # there is already a button with this code
            if code == self.menu_buttons[b].code:
                return  # wimps out

        # FIXME : why this is set before setting profile?
        # Even before the launching screen?

        # from sysgrove.user import user
        # if ct_code not in user.profile.rights:
        #     return

        menu_button = UIMenuButton(self, name, code)
        self.menu_buttons[menu_button.name] = menu_button
        #self.max_offset -= 87

        return menu_button

    def highlightNone(self):
        for b in self.menu_buttons:
            self.menu_buttons[b].unmark()

    def highlightButton(self, code):
        # First: unmark all
        for b in self.menu_buttons:
            self.menu_buttons[b].unmark()

        # Then mark the button we want
        for b in self.menu_buttons:
            # if code.startswith(self.menu_buttons[b].code):
            if cc_prefix(code) == cc_prefix(self.menu_buttons[b].code):
                self.menu_buttons[b].marker()


class UIMenuButton(UIObject):

    def __init__(self, menu_bar, name, code):

        self.name = name
        self.code = code
        self.parent = menu_bar  # OK, maybe FIXME later
        self.__items__ = {}  # for integrity reasons
        # button's parent widget is MenuBar instance's qtObject
        self.qtObject = QtGui.QToolButton(menu_bar.qtObject)
        self.qtObject.setObjectName(name)
        self.qtObject.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.qtObject.sizePolicy().hasHeightForWidth())
        self.qtObject.setMinimumSize(QtCore.QSize(84, 84))
        self.qtObject.setMaximumSize(QtCore.QSize(84, 84))
        self.qtObject.resize(84, 84)
        menu_bar.qtObjectLayout.addWidget(self.qtObject,
                                          0, QtCore.Qt.AlignLeft)
        self.text = ""
        self.icon = None
        self.iconName = ""
        # force to put it here again because items are created after css file
        # is read.
        self.setStyleSheet(CONFIG['css'])

        self.addLink(code)

    def addLink(self, context_code):
        from sysgrove.context import kernel
        self.setAction(
            "clicked()",
            lambda: kernel.open_context_if_not_empty(context_code)
        )
        self.link = context_code

    def setText(self, text):
        self.text = text
        self.qtObject.setText(text)

    # FIXME duplicated code
    def marker(self):
        self.qtObject.setProperty("menuselected", "true")
        self.style()
        self.qtObject.update()

    def unmark(self):
        self.qtObject.setProperty("menuselected", "false")
        self.qtObject.update()
        self.style()

    def addImage(self, img):
        self.iconName = img
        self.icon = QtGui.QIcon()
        self.icon.addPixmap(
            QtGui.QPixmap(_fromUtf8(':' + img)),
            QtGui.QIcon.Normal,
            QtGui.QIcon.Off
        )
        self.qtObject.setIcon(self.icon)
        self.qtObject.setIconSize(QtCore.QSize(48, 48))
