# !/usr/bin/env python
# -*- coding: utf-8 -*-
# """
#   ____             ____
#  / ___| _   _ ___ / ___|_ __ _____   _____
#  \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
#   ___) | |_| \__ \ |_| | | | (_) \ V /  __/
#  |____/ \__, |___/\____|_|  \___/ \_/ \___|
#         |___/


# Copyright SARL SysGrove
# contributor(s) : S. Collins, A. Mokhtari, G. Souveton
# 20/02/2013
# Version 1.0
# This file regroups all important GUI classes (about one per type
# of widget we can find)
#     Role :
#         - Provides an overlay for Qt objects that can be handled
#           easily by Kernel and modules
#         - Uses a specific architecture based on UIWidget to explore
#           GUI items with an universal getter overload
# revision # 1
# Date of revision: 25/02/2013
# reason for revision: - This version can manage TabBar
# contact:
# contact@sysgrove.com
# This software is a computer program whose purpose is to manage the
# logistic execution, production, analysis, finance, transportation,
# master data, reporting and communication of end user companies.
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----
# """
import logging

from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove.ui.window import (GfxWindow, GfxMenuBar,
                                GfxAccordion, GfxStaticButtonZone,
                                GfxDynamicButtonZone, GfxContent,
                                GfxSearch, GfxStatusBar,
                                GfxTabBar)
from .uiobject import UIObject
from .widget import UIWidget
log = logging.getLogger('sysgrove')


class UIMask (UIWidget):

    def __init__(self, parent, name, blur=8):
        super(UIMask, self).__init__(parent, name)

        self.qtObject.setMinimumSize(GfxWindow.size())
        self.qtObject.setMaximumSize(GfxWindow.size())
        self.qtObject.resize(GfxWindow.size())
        self.qtObject.setStyleSheet("background-color: #ffffff")

        self.screenshot = QtGui.QPixmap.grabWidget(GfxWindow)
        self.ly = QtGui.QHBoxLayout(self.qtObject)
        self.lb = QtGui.QLabel(self.qtObject)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.resize(GfxWindow.size())

        self.lb.setPixmap(self.screenshot)
        self.ly.addWidget(self. lb)

        if blur:
            self.bl = QtGui.QGraphicsBlurEffect()
            self.bl.setBlurRadius(blur)
            self.lb.setGraphicsEffect(self.bl)


class UIMaskPartial (QtGui.QWidget):

    def __init__(self, parent, name, popup):
        from sysgrove.context import kernel
        # if second mask
        # applied on parent
        # else applied on GfxContext
        if name == "mask2":
            super(UIMaskPartial, self).__init__(parent)
            self.mask.GfxInline = UIMaskGfxInline(parent)
        else:
            super(UIMaskPartial, self).__init__(
                kernel.get_current_context().ui_space.qtObject)
        self.GfxContent = kernel.get_current_context().ui_space.qtObject
        self.contextAssociate = kernel.get_current_context().ui_space

        self.sizeCurrent = GfxContent.size()

        self.maskGfxMenuBar = UIMaskGfxMenuBar(popup)
        self.maskGfxAccordion = UIMaskGfxAccordion(popup)
        self.maskGfxDynamicButtonZone = UIMaskGfxDynamicButtonZone(
            popup)
        self.maskGfxStaticButtonZone = UIMaskGfxStaticButtonZone(
            popup)
        self.maskGfxContext = UIMaskGfxContext(popup)
        self.maskGfxSearch = UIMaskGfxSearch(popup)
        self.maskGfxStatusBar = UIMaskGfxStatusBar(popup)
        self.maskGfxTabBar = UIMaskGfxTabBar(popup)
        self.popup = popup
        self.parentUIObject = self.GfxContent
        self.name = name

    def show(self):
        if self.name == "mask2":
            self.mask.GfxInline.show()
        self.maskGfxContext.show()
        self.maskGfxAccordion.show()
        self.maskGfxDynamicButtonZone.show()
        self.maskGfxStaticButtonZone.show()
        self.maskGfxSearch.show()
        self.maskGfxStatusBar.show()
        self.maskGfxTabBar.show()
        self.maskGfxTabBar.lower()

        from sysgrove.ui.classes import UITabBar
        for t in UITabBar.tabs:
            if UITabBar.tabs[t].property("menuselected").toBool() is False:
                UITabBar.tabs[t].qtObject.raise_()
            else:
                UITabBar.tabs[t].qtObject.lower()
                UITabBar.tabs[t].qtObject.stackUnder(self.maskGfxTabBar)
                self.maskButton = UIMaskButton(
                    UITabBar.tabs[t].qtObject, self.popup)
                self.maskButton.show()

        from sysgrove.ui.classes import UIMenuBar
        self.maskGfxMenuBar.show()
        self.maskGfxMenuBar.lower()
        for b in UIMenuBar.menu_buttons:
            self.maskGfxMenuBar.stackUnder(UIMenuBar.menu_buttons[b].qtObject)
            UIMenuBar.menu_buttons[b].qtObject.raise_()

    def hide(self):
        if self.name == "mask2":
            self.mask.GfxInline.hide()
        self.maskGfxContext.hide()
        self.maskGfxMenuBar.hide()
        self.maskGfxAccordion.hide()
        self.maskGfxDynamicButtonZone.hide()
        self.maskGfxStaticButtonZone.hide()
        self.maskGfxSearch.hide()
        self.maskGfxStatusBar.hide()
        self.maskGfxTabBar.hide()
        try:
            self.maskButton.hide()
        except:
            pass

    def resize(self, w, h):
        self.setMinimumSize(w, h)
        self.setMaximumSize(w, h)
        self.show()


class UIMaskButton (QtGui.QWidget):

    def __init__(self, parent, popup):
        from sysgrove.context import kernel

        #parent.qtobject = parent
        super(UIMaskButton, self).__init__(parent)
        self.setMinimumSize(parent.size())
        self.setMaximumSize(parent.size())
        self.resize(parent.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = parent.size()
        self.parentUIObject = parent
        self.popup = popup

    def paintEvent(self, event):
        if (self.sizeCurrent != self.parentUIObject.size()):
            self.setMinimumSize(self.parentUIObject.size())
            self.setMaximumSize(self.parentUIObject.size())
            self.resize(self.parentUIObject.size())
            self.sizeCurrent = self.parentUIObject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxMenuBar (QtGui.QWidget):

    def __init__(self, parent):
        from sysgrove.context import kernel
        GfxMenuBar.qtobject = GfxMenuBar
        super(UIMaskGfxMenuBar, self).__init__(GfxMenuBar)
        self.setMinimumSize(GfxMenuBar.size())
        self.setMaximumSize(GfxMenuBar.size())
        self.resize(GfxMenuBar.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = GfxMenuBar.size()
        self.parentUIObject = GfxMenuBar
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != GfxMenuBar.qtobject.size()):
            self.setMinimumSize(GfxMenuBar.qtobject.size())
            self.setMaximumSize(GfxMenuBar.qtobject.size())
            self.resize(GfxMenuBar.size())
            self.sizeCurrent = GfxMenuBar.qtobject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxAccordion(QtGui.QWidget):

    def __init__(self, parent):

        from sysgrove.context import kernel
        GfxAccordion.qtobject = GfxAccordion
        super(UIMaskGfxAccordion, self).__init__(GfxAccordion)
        self.setMinimumSize(GfxAccordion.size())
        self.setMaximumSize(GfxAccordion.size())
        self.resize(GfxAccordion.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = GfxAccordion.size()
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != GfxAccordion.qtobject.size()):
            self.setMinimumSize(GfxAccordion.qtobject.size())
            self.setMaximumSize(GfxAccordion.qtobject.size())
            self.resize(GfxAccordion.size())
            self.sizeCurrent = GfxAccordion.qtobject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxDynamicButtonZone(QtGui.QWidget):

    def __init__(self, parent):

        from sysgrove.context import kernel
        GfxDynamicButtonZone.qtobject = GfxDynamicButtonZone
        super(UIMaskGfxDynamicButtonZone, self).__init__(GfxDynamicButtonZone)
        self.setMinimumSize(GfxDynamicButtonZone.size())
        self.setMaximumSize(GfxDynamicButtonZone.size())
        self.resize(GfxDynamicButtonZone.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = GfxDynamicButtonZone.size()
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != GfxDynamicButtonZone.qtobject.size()):
            self.setMinimumSize(GfxDynamicButtonZone.qtobject.size())
            self.setMaximumSize(GfxDynamicButtonZone.qtobject.size())
            self.resize(GfxDynamicButtonZone.size())
            self.sizeCurrent = GfxDynamicButtonZone.qtobject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxStaticButtonZone(QtGui.QWidget):

    def __init__(self, parent):

        from sysgrove.context import kernel
        GfxStaticButtonZone.qtobject = GfxStaticButtonZone
        super(UIMaskGfxStaticButtonZone, self).__init__(GfxStaticButtonZone)
        self.setMinimumSize(GfxStaticButtonZone.size())
        self.setMaximumSize(GfxStaticButtonZone.size())
        self.resize(GfxStaticButtonZone.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = GfxStaticButtonZone.size()
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != GfxStaticButtonZone.qtobject.size()):
            self.setMinimumSize(GfxStaticButtonZone.qtobject.size())
            self.setMaximumSize(GfxStaticButtonZone.qtobject.size())
            self.resize(GfxStaticButtonZone.size())
            self.sizeCurrent = GfxStaticButtonZone.qtobject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxSearch(QtGui.QWidget):

    def __init__(self, parent):

        from sysgrove.context import kernel
        GfxSearch.qtobject = GfxSearch
        super(UIMaskGfxSearch, self).__init__(GfxSearch)
        self.setMinimumSize(GfxSearch.size())
        self.setMaximumSize(GfxSearch.size())
        self.resize(GfxSearch.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = GfxSearch.size()
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != GfxSearch.qtobject.size()):
            self.setMinimumSize(GfxSearch.qtobject.size())
            self.setMaximumSize(GfxSearch.qtobject.size())
            self.resize(GfxSearch.size())
            self.sizeCurrent = GfxSearch.qtobject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxStatusBar(QtGui.QWidget):

    def __init__(self, parent):

        from sysgrove.context import kernel
        GfxStatusBar.qtobject = GfxStatusBar
        super(UIMaskGfxStatusBar, self).__init__(GfxStatusBar)
        self.setMinimumSize(GfxStatusBar.size())
        self.setMaximumSize(GfxStatusBar.size())
        self.resize(GfxStatusBar.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = GfxStatusBar.size()
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != GfxStatusBar.qtobject.size()):
            self.setMinimumSize(GfxStatusBar.qtobject.size())
            self.setMaximumSize(GfxStatusBar.qtobject.size())
            self.resize(GfxStatusBar.size())
            self.sizeCurrent = GfxStatusBar.qtobject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxTabBar(QtGui.QWidget):

    def __init__(self, parent):

        from sysgrove.context import kernel
        GfxTabBar.qtobject = GfxTabBar
        super(UIMaskGfxTabBar, self).__init__(GfxTabBar)
        self.setMinimumSize(GfxTabBar.size())
        self.setMaximumSize(GfxTabBar.size())
        self.resize(GfxTabBar.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = GfxTabBar.size()
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != GfxTabBar.qtobject.size()):
            self.setMinimumSize(GfxTabBar.qtobject.size())
            self.setMaximumSize(GfxTabBar.qtobject.size())
            self.resize(GfxTabBar.size())
            self.sizeCurrent = GfxTabBar.qtobject.size()
            self.show()

        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxInline(QtGui.QWidget):

    def __init__(self, parent):
        from sysgrove.context import kernel
        parent.qtobject = parent
        super(UIMaskGfxInline, self).__init__(parent)
        self.setMinimumSize(parent.size())
        self.setMaximumSize(parent.size())
        self.resize(parent.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = parent.size()
        self.popup = parent

    def paintEvent(self, event):
        if (self.sizeCurrent != self.popup.qtobject.size()):
            self.setMinimumSize(self.popup.qtobject.size())
            self.setMaximumSize(self.popup.qtobject.size())
            self.resize(self.popup.size())
            self.sizeCurrent = self.popup.qtobject.size()
            self.show()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIMaskGfxContext(QtGui.QWidget):

    def __init__(self, parent):

        from sysgrove.context import kernel
        super(UIMaskGfxContext, self).__init__(
            kernel.get_current_context().ui_space.qtObject)
        self.setMinimumSize(
            kernel.get_current_context().ui_space.qtObject.size())
        self.setMaximumSize(
            kernel.get_current_context().ui_space.qtObject.size())
        self.resize(kernel.get_current_context().ui_space.qtObject.size())
        self.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.8)")
        self.ly = QtGui.QHBoxLayout(self)
        self.lb = QtGui.QLabel(self)
        self.ly.setMargin(0)
        self.ly.setSpacing(0)
        self.lb.setStyleSheet("background-color: #333333;")
        self.ly.addWidget(self. lb)
        self.o = QtGui.QGraphicsOpacityEffect()
        self.o.setOpacity(0.3)
        self.lb.setGraphicsEffect(self.o)
        self.sizeCurrent = kernel.get_current_context(
        ).ui_space.qtObject.size()
        self.popup = parent

    def paintEvent(self, event):
        from sysgrove.context import kernel
        if (self.sizeCurrent != kernel.get_current_context().ui_space.qtObject.size()):
            self.setMinimumSize(
                kernel.get_current_context().ui_space.qtObject.size())
            self.setMaximumSize(
                kernel.get_current_context().ui_space.qtObject.size())
            self.resize(kernel.get_current_context().ui_space.qtObject.size())
            self.sizeCurrent = kernel.get_current_context(
            ).ui_space.qtObject.size()
            self.show()
            self.popup.raise_()
        self.update()

    def mousePressEvent(self, event):
        self.popup.hide()
        self.popup.show()
        self.popup.raise_()


class UIPopup(UIObject):

    def __init__(self, parent, name, content):

        # frameMask is the blur mask that shows up above
        # the screen, behind the popup. The popup is a child of frameMask
        super(UIPopup, self).__init__(parent, name)

        self.mask = UIMask(parent, "mask")
        self.box = QtGui.QWidget(parent.qtObject)
        self.layout = QtGui.QVBoxLayout(self.qtObject)

        from sysgrove.ui.window import GfxWindow

        w, h = 400, 272
        x = (GfxWindow.frameGeometry().width() - w) / 2
        y = (GfxWindow.frameGeometry().height() - h) / 2

        self.box.setMinimumSize(QtCore.QSize(w, h))
        self.box.setMaximumSize(QtCore.QSize(w, h))
        self.box.resize(w, h)
        self.box.move(x, y)

        self.op = QtGui.QGraphicsOpacityEffect()
        self.op .setOpacity(0.8)
        self.mask.setGraphicsEffect(self.op)

        self.boxLayout = QtGui.QVBoxLayout(self.box)

        self.boxContent = QtGui.QWidget(self.box)

        self.boxContentLayout = QtGui.QVBoxLayout(self.boxContent)

        self.boxLayout.addWidget(self.boxContent)

        self.buttonsWidget = QtGui.QWidget(self.box)
        self.buttonsWidget.setMaximumSize(8000, 50)

        self.buttonsLayout = QtGui.QHBoxLayout(self.buttonsWidget)

        # for c in content:

        #     if c == 'content':

        #         if content[c] != '':
        #             x = QtGui.QLabel(self.box)
        #             x.setGeometry(100, 100, 200, 100)
        #             x.setText(content[c])
        #             self.boxLayout.addWidget(x)

        #             elif c == "buttons":

        #                 for b in content[c]:
        #                     x = QtGui.QToolButton(self.buttonsWidget)
        #                     x.setMinimumSize(100, 40)
        #                     x.setText(b)
        #                     if content[c][b] == 'close':
        # if content[c][b] == 'close':  # don't ask why !
        #                             x.connect(x, QtCore.SIGNAL(_fromUtf8(
        #                "clicked()")), lambda: self.close())
        #                         else:
        #                             from sysgrove.context import kernel
        #                             x.connect(
        #                                 x, QtCore.SIGNAL(
        #                                     _fromUtf8("clicked()")),
        # lambda: kernel.lin_trap(content[c][b]))

        self.buttonsLayout.addWidget(x)

        self.boxLayout.addWidget(self.buttonsWidget)

    def open(self):
        self.mask.show()
        self.box.show()

    def close(self):
        self.mask.hide()
        self.box.hide()
        self.mask.deleteLater()
        self.box.deleteLater()
        deleteLater()
        self.box.deleteLater()
