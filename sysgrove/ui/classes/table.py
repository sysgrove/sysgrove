
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
import locale
import datetime
from functools import partial
from sysgrove.settings import READONLY_COLOR
from sysgrove.utils import getattr_ex
from sysgrove.utils import debug_trace
from sysgrove.utils import getattr_verbose_name
from sysgrove.utils import retreive_class


from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from sysgrove.ui.classes.uiobject import UIObject

log = logging.getLogger('sysgrove')


# Subclass to handle order correctly
class IntTableWidgetItem(QtGui.QTableWidgetItem):

    def __init__(self, text):
        # call custom constructor with UserType item type
        QtGui.QTableWidgetItem.__init__(self, text,
                                        QtGui.QTableWidgetItem.UserType)

    # Qt uses a simple < check for sorting items, override this to use the
    # sortKey
    def __lt__(self, other):
        value, ok = self.data(QtCore.Qt.UserRole).toInt()
        if ok:
            other_value, ok = other.data(QtCore.Qt.UserRole).toInt()
            if ok:
                return value < other_value
        return QtGui.QTableWidgetItem.__lt__(self, other)


# Enable sorting on column fill with checkBox
class CheckBoxTableWidgetItem(QtGui.QTableWidgetItem):

    def __init__(self, text):
        # call custom constructor with UserType item type
        QtGui.QTableWidgetItem.__init__(self, text,
                                        QtGui.QTableWidgetItem.UserType)

    def __lt__(self, other):
        return self.checkState() < other.checkState()


# FIXME : to be replace by a collections.namedtuple
class ColumnController(object):
    NAME = 0
    TYPE = 1
    EDITABLE = 2
    HIDDEN = 3
    ATTR = 4


def build_columns(klass, fields=None):
    res = []
    if fields:
        list_columns = fields[:]
    else:
        list_columns = klass.list_columns[:]
    for attr in list_columns:
        if attr in klass.list_columns_editable:
            editable = True
        else:
            editable = False
        if attr == 'id':
            hidden = True
        else:
            hidden = False
        res.append((getattr_verbose_name(attr, klass),
                    klass.toPython_type(attr),
                    editable,
                    hidden,
                    attr))
    # add also the the primaryKeys
    if not fields:
        pks = klass.pk_attrs()
        for pk in pks:
            res.append((u"", int, False, True, pk))
    return res


# FIXME : should move under the widgets package
class ErrorTableWidget(QtGui.QTableWidget):

    changeRed = QtCore.pyqtSignal(int, int, bool)

    def sizeHint(self):
        return QtCore.QSize(self.width(), self.height())

    def width(self):
        width = self.verticalHeader().sizeHint().width()
        for i in xrange(self.columnCount()):
            width += self.columnWidth(i)
        width += self.verticalScrollBar().sizeHint().width()
        return width

    def height(self):
        height = self.horizontalHeader().sizeHint().height()
        for i in xrange(self.rowCount()):
            height += self.rowHeight(i)
        height += self.horizontalScrollBar().sizeHint().height()
        return height


# FIXME : should move under the widgets package
class TableColumn(ColumnController, ErrorTableWidget):

    """
        A table widget to represent row of database table
        (sort of implemantation of QSqlQueryModel)
        selectAll may contain the column number with a
        selectAll/deselectAll property
        Be careful, is the selectAll/deselectAll is place 'by hand'
        and as columns are resize to theirs content. The selectAll/deselectAll
        need to be in the first column.
    """

    def __init__(self, columns=None,
                 showReadOnly=False,
                 selectAll=None, *args, **kwargs):
        super(TableColumn, self).__init__(*args, **kwargs)
        self.columns = columns
        self.showReadOnly = showReadOnly
        self.selectAll = selectAll

        self.setColumnCount(len(self.columns))
        self.setHorizontalHeaderLabels([x[self.NAME] for x in columns])
        for i in xrange(len(columns)):
            if columns[i][self.HIDDEN]:
                self.setColumnHidden(i, True)

        self.setSortingEnabled(True)

        # style
        self.horizontalHeader().setResizeMode(
            QtGui.QHeaderView.ResizeToContents)

        if isinstance(self.selectAll, int):
            hh = self.horizontalHeader()
            self.checkBox = QtGui.QCheckBox(hh)
            self.checkBox.stateChanged.connect(self.checkBox_stateChanged)
            self.checkBox_position()
            hh.sectionResized.connect(self.on_section_resize)
            hh.sectionAutoResize.connect(self.on_section_autoresize)
        else:
            self.checkBox = None

        self.verticalHeader().hide()
        self.setStyleSheet("""
            QTableWidget {
                /*background-color: #eee;*/
                border:1px solid #333;
                selection-background-color: #797992;
                outline:none;
            }""")

        self.horizontalHeader().setStyleSheet("""
            background: #eee;
            selection-background-color: #9393A4;
            border: 1px solid #dddddd;
            font-weight:bold
            """)

    def checkBox_position(self):
        x = self.columnViewportPosition(self.selectAll) + 2
        self.checkBox.setGeometry(x, 2, 16, 17)

    def resizeEvent(self, event=None):
        super(TableColumn, self).resizeEvent(event)
        if self.checkBox:
            self.checkBox_position()

    def isEditable(self, col):
        return self.columns[col][self.EDITABLE]

    def editableColumns(self):
        return [col
                for col in xrange(len(self.columns))
                if self.isEditable(col)]

    def clear(self):
        self.clearContents()
        self.setRowCount(0)
        self.clearSelection()

    def addRow(self, row=None, reset_sorting=True):
        sorting = self.isSortingEnabled()
        self.setSortingEnabled(False)
        next_row = self.rowCount()
        self.setRowCount(next_row + 1)
        if not row:
            return
        standart_flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        for col in range(len(self.columns)):
            col_type = self.columns[col][self.TYPE]

            if self.columns[col][self.TYPE] == bool:
                item = CheckBoxTableWidgetItem("")
                item.setFlags(standart_flags | QtCore.Qt.ItemIsUserCheckable)
                if row[col]:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)

            else:
                if col_type == int:
                    item = IntTableWidgetItem(unicode(row[col]))
                else:
                    item = QtGui.QTableWidgetItem(unicode(row[col]))
                item.setFlags(standart_flags)
                if col_type == datetime.date:
                    if row[col]:
                        item.setData(QtCore.Qt.UserRole,
                                     QtCore.QVariant(QtCore.QDate(row[col])))
                        item.setData(QtCore.Qt.DisplayRole,
                                     QtCore.QVariant(QtCore.QDate(row[col])
                                                     .toString("dd/MM/yyyy")))
                    else:
                        item.setData(QtCore.Qt.UserRole, QtCore.QVariant())
                elif col_type == str:
                    # force the string, it could be a fk
                    item.setData(QtCore.Qt.UserRole,
                                 QtCore.QVariant(unicode(row[col])))
                elif col_type == float and isinstance(row[col], float):
                    item.setData(QtCore.Qt.UserRole, QtCore.QVariant(row[col]))
                    item.setData(QtCore.Qt.DisplayRole,
                                 QtCore.QVariant(locale.format('%.3f',
                                                               row[col],
                                                               grouping=True)))
                else:
                    item.setData(QtCore.Qt.UserRole,
                                 QtCore.QVariant(row[col]))

            if col_type == float:
                item.setData(QtCore.Qt.TextAlignmentRole,
                             QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
            if self.isEditable(col):
                item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)
            elif self.showReadOnly:
                item.setBackgroundColor(QtGui.QColor(READONLY_COLOR))
            self.setItem(next_row, col, item)
        if reset_sorting:
            self.setSortingEnabled(sorting)

    def checkBox_stateChanged(self, state):
        assert(self.columns[self.selectAll][self.TYPE] == bool)
        # We need to remove sorting, otherwise items moved
        # and for does work
        sorting = self.isSortingEnabled()
        self.setSortingEnabled(False)
        for row in xrange(self.rowCount()):
            item = self.item(row, self.selectAll)
            item.setCheckState(state)
        self.setSortingEnabled(sorting)

    def on_section_resize(self, section, old, new):
        if self.checkBox:
            self.checkBox_position()

    def on_section_autoresize(self, section, old, new):
        if self.checkBox:
            self.checkBox_position()


class LinkedTable(UIObject):

    def __init__(self, parent, name,
                 klass=None, columns=None,
                 showReadOnly=False,
                 selectAll=None):
        super(LinkedTable, self).__init__(parent, name)
        self.klass = klass
        if columns:
            self.columns = columns
        else:
            self.columns = build_columns(klass)

        if parent and hasattr(parent, 'qtObject'):
            parent = parent.qtObject
        self.qtObject = TableColumn(self.columns, showReadOnly,
                                    selectAll, parent)
        self.qtObject.setObjectName(name)

        self.attr_columns = [x[self.ATTR] for x in self.columns]

    def setLink(self, klass, prefix, slave=None,
                masters=[], filters={}, order={}):
        if not self.klass:
            if slave:
                self.klass = slave[1]
            else:
                self.klass = klass
            self.clear()
        self.linkSlave = slave
        self.linkMasters = masters
        self.linkFilters = filters
        self.linkOrder = order

    def setVal(self, values):
        self.clear()
        if not values:
            return
        for value in values:
            row = []
            for col in self.attr_columns:
                if col:
                    cell = getattr_ex(value, col)
                else:
                    cell = None
                if cell:
                    row.append(cell)
                else:
                    if col == 'id':
                        # the case of the object have not yet been saved
                        # so we store the object itself
                        row.append(value)
                    else:
                        row.append('')
            self._addRow(row)

    def rowObject(self, row):
        if row is None:
            return None

        col_id = self.attr_columns.index('id')
        value_id, ok = self.item(row, col_id).data(QtCore.Qt.UserRole).toInt()
        if ok:
            value = self.klass.query.filter_by(id=value_id).one()
            # If some columns are editable, then the value of some columns
            # may have changed.... so the object has to be update...
            for col in xrange(len(self.columns)):
                if self.isEditable(col):
                    self._cell2obj(value, row, col)
        else:
            value = self._row2obj(row)

        return value

    def valueObject(self):
        res = []
        nb_rows = self.rowCount()
        for row in range(nb_rows):
            res.append(self.rowObject(row))
        return res

    def _cell2obj(self, obj, row, col):
        item = self.item(row, col)
        col_type = self.columns[col][self.TYPE]
        if self.klass.is_direct(self.attr_columns[col]):
            if col_type == bool:
                if item.checkState() == QtCore.Qt.QtCore.Qt.Checked:
                    setattr(obj, self.attr_columns[col], True)
                else:
                    setattr(obj, self.attr_columns[col], False)
            elif col_type == int:
                value, ok = item.data(QtCore.Qt.UserRole).toInt()
                if ok:
                    setattr(obj, self.attr_columns[col], value)
            elif col_type == float:
                value, ok = item.data(QtCore.Qt.UserRole).toFloat()
                if ok:
                    setattr(obj, self.attr_columns[col], value)
            elif col_type == datetime.date:
                setattr(obj, self.attr_columns[col],
                        item.data(QtCore.Qt.UserRole).toDate().toPyDate())
            elif col_type == str:
                setattr(obj, self.attr_columns[col],
                        str(item.data(QtCore.Qt.UserRole).toString()))
            else:
                setattr(obj, self.attr_columns[col],
                        unicode(item.data(QtCore.Qt.UserRole).toString()))
        elif self.klass.is_fk(self.attr_columns[col]):
            code = unicode(item.data(QtCore.Qt.UserRole).toString())
            subclass = retreive_class(self.attr_columns[col], self.klass)
            if code == 'None' or not code:
                value = None
            else:
                value = subclass.query.filter_by(code=code).one()
            setattr(obj, self.attr_columns[col], value)

    def _row2obj(self, row_index):
        col_id = self.attr_columns.index('id')
        cur = self.item(row_index, col_id).data(
            QtCore.Qt.UserRole).toPyObject()
        if not cur:
            # create the object and use the id cell to store it.
            cur = self.klass()
            for col in xrange(len(self.attr_columns)):
                self._cell2obj(cur, row_index, col)
            self.item(row_index, col_id).setData(QtCore.Qt.UserRole,
                                                 QtCore.QVariant(cur))
        return cur

    def _getRow(self, row):
        if row is None:
            return []
        res = []
        for col in range(len(self.attr_columns)):
            item = self.item(row, col)
            if item:
                col_type = self.columns[col][self.TYPE]
                if self.klass.is_direct(self.attr_columns[col]):
                    if col_type == bool:
                        if item.checkState() == QtCore.Qt.QtCore.Qt.Checked:
                            res.append(True)
                        else:
                            res.append(False)
                    elif col_type == int:
                        value, ok = item.data(QtCore.Qt.UserRole).toInt()
                        if ok:
                            res.append(value)
                        else:
                            res.append('')
                    elif col_type == float:
                        value, ok = item.data(QtCore.Qt.UserRole).toFloat()
                        if ok:
                            res.append(value)
                        else:
                            res.append('')
                    elif col_type == datetime.date:
                        res.append(
                            item.data(QtCore.Qt.UserRole).toDate().toPyDate())
                    elif col_type == str:
                        res.append(item.toString())
                elif self.klass.is_fk(self.attr_columns[col]):
                    code = unicode(item.data(QtCore.Qt.DisplayRole).toString())
                    subclass = retreive_class(self.attr_columns[col],
                                              self.klass)
                    if code:
                        value = subclass.query.filter_by(code=code).one()
                    else:
                        value = ''
                    res.append(value)
                else:
                    res.append(item.data(QtCore.Qt.UserRole).toString())
            else:
                res.append('')
        return res

    def _addRow(self, row=None, reset_sorting=True):
        sorting = self.isSortingEnabled()
        self.setSortingEnabled(False)
        self.qtObject.addRow(row, reset_sorting=False)
        if row:
            for col in xrange(len(self.attr_columns)):
                if self.isEditable(col):
                    # if it is an fk, please add a ComBoxWidget
                    if self.klass.is_fk(self.attr_columns[col]):
                        subclass = retreive_class(
                            self.attr_columns[col], self.klass)
                        if hasattr(subclass, 'code'):
                            query = subclass.query.all()
                            query = map(lambda x: x.code, query)
                            query.insert(0, '')
                            combobox = QtGui.QComboBox()
                            combobox.addItems(query)
                            item = self.item(self.rowCount() - 1, col)
                            value = item.data(QtCore.Qt.UserRole).toString()
                            try:
                                i = query.index(value)
                            except ValueError:
                                i = 0
                            combobox.setCurrentIndex(i)
                            self.setCellWidget(self.rowCount() - 1,
                                               col,
                                               combobox)
                            combobox.currentIndexChanged.connect(
                                partial(self.comboBoxSelectionChanged,
                                        item,
                                        combobox))
        if reset_sorting:
            self.setSortingEnabled(sorting)

    def comboBoxSelectionChanged(self, item, combobox, index):
        item.setData(QtCore.Qt.DisplayRole,
                     combobox.itemText(index))


class CheckedTable(LinkedTable):

    """ The check column is the first one
    """

    def valueObject(self):
        res = []
        nb_rows = self.rowCount()
        col_id = self.attr_columns.index('id')
        for row in range(nb_rows):
            item_check = self.item(row, 0)
            if item_check.checkState() == QtCore.Qt.Checked:
                value_id, ok = self.item(row, col_id).data(
                    QtCore.Qt.UserRole).toInt()
                if ok:
                    value = self.klass.query.filter_by(id=value_id).one()
                    # If some columns are editable, then the value of
                    # some columns
                    # may have changed.... so the object has to be update...
                    for col in xrange(len(self.columns)):
                        if self.isEditable(col):
                            self._cell2obj(value, row, col)
                else:
                    value = self._row2obj(row)
                res.append(value)
        return res


class SelectQtyTable(LinkedTable):

    """ The check column is the first one
    """

    def valueObject(self):
        res = []
        nb_rows = self.rowCount()
        col_id = self.attr_columns.index('id')
        editables = self.editableColumns()
        index_qty2link = editables[1]
        for row in range(nb_rows):
            state = self.item(row, 0).checkState()
            if state in [QtCore.Qt.Checked, QtCore.Qt.PartiallyChecked]:
                item_id, valid = self.item(row, col_id).data(
                    QtCore.Qt.DisplayRole).toInt()
                if valid:
                    line = self.klass.query.filter_by(id=item_id).one()
                else:
                    line = self._row2obj(row)
                nQty, ok = self.item(row, index_qty2link).data(
                    QtCore.Qt.DisplayRole).toFloat()
                res.append((line, nQty))
        return res


class ManyKeyTable(LinkedTable):

    def __init__(self, parent, name, keys,
                 klass=None, columns=None, showReadOnly=False,
                 selectAll=None):
        self.klass = klass
        if columns:
            self.columns = columns
        else:
            self.columns = build_columns(klass)
        self.keys = keys
        super(ManyKeyTable, self).__init__(parent, name,
                                           klass=self.klass,
                                           columns=self.columns,
                                           showReadOnly=showReadOnly,
                                           selectAll=selectAll)

    def rowObject(self, row):
        index_keys = {}
        for key in self.keys:
            index = self.attr_columns.index(key)
            index_keys[key] = index
        keys_value = {}

        for key in self.keys:
            index = index_keys[key]
            fkey = self.item(row, index).data(QtCore.Qt.UserRole).toPyObject()
            keys_value[key + '_id'] = fkey.id
        if len(keys_value) == len(self.keys):
            value = self.klass.query.filter_by(**keys_value).one()
            # If some columns are editable, then the value of some columns
            # may have changed.... so the object has to be update...
            for col in xrange(len(self.columns)):
                if self.isEditable(col):
                    self._cell2obj(value, row, col)
        else:
            value = self._row2obj(row)

        return value

    def _row2obj(self, row_index):
        # create the object and use the id cell to store it.
        # FIXME the object is created each time
        cur = self.klass()
        for col in xrange(len(self.attr_columns)):
            self._cell2obj(cur, row_index, col)
        return cur

    # def _addRow(self, row=None, reset_sorting=True):
    #     if row and not len(row) == len(self.attr_columns):
    #         for key in self.tobeHidden:
    #             attr = key.split('_id', 1)[0]
    #             index = self.attr_columns.index(attr)
    #             obj = row[index]
    #             row.append(obj.id)

    #     super(ManyKeyTable, self)._addRow(row, reset_sorting)


class DataTable(UIObject):

    """
        A table widget to set and update many objects in one shot.
        The objects are not totally represented,
        a max of 3 attributes could be represented this way with a
        table : one by the vertical header, one by the horizontalHeader
        and one by the cells
        This is store in the dim parameter that is a dict such as
        dim = {'h': attr_name,
               'v': attr_name
               'c': attr_name}
        klass is the class
    """

    def __init__(self, parent, name, klass, dim):
        super(DataTable, self).__init__(parent, name)
        if parent and hasattr(parent, 'qtObject'):
            parent = parent.qtObject
        self.qtObject = QtGui.QTableWidget(parent)

        self.klass = klass
        self.rows = None
        self.columns = None
        self.dim = dim
        self.cell_type = self.klass.toPython_type(dim['cell'])
        if 'horz' in dim:
            self.columns = retreive_class(dim['horz'], self.klass).query.all()
        if 'vert' in dim:
            self.rows = retreive_class(dim['vert'], self.klass).query.all()

        self.build_table()
        self.setSortingEnabled(False)

    def build_table(self):
        # FIXME
        # lets assume that that rows and columns ae always foreigh key
        # and code attribute is available
        # this part may have to be generalized
        if self.columns:
            self.setColumnCount(len(self.columns))
            self.setHorizontalHeaderLabels(map(lambda x: x.code, self.columns))
        if self.rows:
            self.setRowCount(len(self.rows))
            self.setVerticalHeaderLabels(map(lambda x: x.code, self.rows))
        else:
            self.setRowCount(1)
            self.verticalHeader().hide()

        # style
        self.horizontalHeader().setResizeMode(
            QtGui.QHeaderView.ResizeToContents)

    def clear(self):
        self.clearContents()

    def setVal(self, values):
        self.clearContents()

        for value in values:
            if 'vert' in self.dim:
                row_val = getattr(value, self.dim['vert'])
                row = self.rows.index(row_val)
            else:
                row = 0
            if 'horz' in self.dim:
                col_val = getattr(value, self.dim['horz'])
                col = self.columns.index(col_val)

            item_val = getattr(value, self.dim['cell'])
            item = QtGui.QTableWidgetItem(unicode(item_val))
            item.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
                                                  | QtCore.Qt.ItemIsEditable)

            # set Qt.DisplayRole and update Qt.TextAlignmentRole if needed
            if item_val is None or item_val == '':
                item.setData(QtCore.Qt.DisplayRole, QtCore.QVariant())
            elif isinstance(item_val, float):
                item.setData(QtCore.Qt.TextAlignmentRole,
                             QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
                item.setData(QtCore.Qt.DisplayRole,
                             QtCore.QVariant(locale.format('%.3f',
                                                           item_val,
                                                           grouping=True)))
            elif isinstance(item_val, datetime.datetime):
                item.setData(QtCore.Qt.DisplayRole,
                             QtCore.QVariant(QtCore.QDate(item_val)
                                             .toString("dd/MM/yyyy")))
            elif isinstance(item_val, bool):
                standart_flags = item.flags()
                item = CheckBoxTableWidgetItem("")
                item.setFlags(standart_flags | QtCore.Qt.ItemIsUserCheckable)
                if item_val:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
            else:
                item.setData(QtCore.Qt.DisplayRole,
                             QtCore.QVariant(item_val))

            item.setData(QtCore.Qt.UserRole, QtCore.QVariant(value))
            self.setItem(row, col, item)

    def objectValues(self):
        values = []
        for row in xrange(self.rowCount()):
            for col in xrange(self.columnCount()):
                if self.item(row, col):
                    item = self.item(row, col).data(
                        QtCore.Qt.UserRole).toPyObject()
                    val = self._cell2obj(row, col)
                    if item:
                        setattr(item, self.dim['cell'], val)
                    else:
                        item = {'horz': self.columns[col],
                                'cell': val}
                        if 'vert' in self.dim:
                            item['vert'] = self.rows[row]
                    values.append(item)
        return values

    # use only the value store as DisplayRole
    def _cell2obj(self,  row, col):
        item = self.item(row, col)
        if self.klass.is_direct(self.dim['cell']):
            if self.cell_type == bool:
                if item.checkState() == QtCore.Qt.QtCore.Qt.Checked:
                    return True
                else:
                    return False
            elif self.cell_type == int:
                value, ok = item.data(QtCore.Qt.DisplayRole).toInt()
                if ok:
                    return value
            elif self.cell_type == float:
                value, ok = item.data(QtCore.Qt.DisplayRole).toFloat()
                if ok:
                    return value
            elif self.cell_type == datetime.date:
                return item.data(QtCore.Qt.DisplayRole).toDate().toPyDate()
            elif self.cell_type == str:
                return str(item.data(QtCore.Qt.DisplayRole).toString())
            else:
                return unicode(item.data(QtCore.Qt.DisplayRole).toString())
        elif self.klass.is_fk(self.dim['cell']):
            code = unicode(item.data(QtCore.Qt.DisplayRole).toString())
            subclass = retreive_class(self.adim['cell'], self.klass)
            if code == 'None' or not code:
                value = None
            else:
                value = subclass.query.filter_by(code=code).one()
            return value
        return None


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    app = QApplication(sys.argv)

    w = QtGui.QWidget()
    l = QtGui.QVBoxLayout(w)

    columns = [
        (u"", bool, False, False),
        (u"Name", str, False, False),
        ('id', int, False, False)
    ]
    table = TableColumn(columns, False, 0, w)
    table.addRow([False, 'une', 1])
    table.addRow([True, 'deux', 2])
    table.addRow([False, 'trois', 3])
    table.addRow([False, 'quatre', 4])

    colBool = table.horizontalHeaderItem(1)
    print 'colBool ', colBool.text()

    item = table.item(1, 1)
    item.setBackground(QtGui.QBrush(QtCore.Qt.red))

    l.addWidget(table)
    w.resize(400, 300)
    w.show()
    sys.exit(app.exec_())
