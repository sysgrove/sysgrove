#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore, QtGui
from sysgrove.ui.classes.uiobject import UIObject


class UIWidget(UIObject):

    """ Represents a widget. This is not any widget, this creates a QWidget.
    If you don't want to do so, use an UIObject instead """

    def __init__(self, parent_ui_component, name):
        """ Constructor that calls UIObject's one and create a widget """
        super(UIWidget, self).__init__(parent_ui_component, name)
        # The difference between UIWidget and UIObject is that UIWidget creates
        # a Widget.
        # NOTE : will be overwritten in all classes that inherit UIWidget
        self.qtObject = QtGui.QWidget(parent_ui_component.qtObject)
        self.qtObject.setObjectName(name)
        self.qtObject.setStyleSheet("border: 0px;")

    def setGeometry(self, x, y, w, h):
        self.qtObject.setGeometry(QtCore.QRect(x, y, w, h))

    def setMinimumSize(self, x, y):
        self.qtObject.setMinimumSize(QtCore.QSize(x, y))

    def setMaximumSize(self, x, y):
        self.qtObject.setMaximumSize(QtCore.QSize(x, y))
