#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from datetime import datetime
from datetime import date
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from .uiobject import UIObject
from .calendar import UIPoppedCalendar, UIPoppedMonth

from .widgets.datelineedit import DateLineEdit, MonthLineEdit

from .widgets.date_range import DateRange

log = logging.getLogger('sysgrove')


INVALID_CSS = "background-color:#c95a5a; border: 1px solid #ff0000;"


class UIDate(UIObject):
    # FIXME : need to emulate a editingFinished() when
    # all fields are non-empty and we're leaving one of them.
    edited = QtCore.pyqtSignal(str)

    def __init__(self, parent_ui_component, name,
                 poppedClass=UIPoppedCalendar, lineEditClass=DateLineEdit,
                 typeDouble=None):
        # typedouble can have value : None, "From" or "Till"
        super(UIDate, self).__init__(parent_ui_component, name)
        self.qtObject = QtGui.QWidget(parent_ui_component.qtObject)
        self.qtObject.setStyleSheet("border: none;")
        self.qtObject.setMaximumSize(QtCore.QSize(144, 32))
        self.qtObjectLayout = QtGui.QHBoxLayout(self.qtObject)
        self.qtObjectLayout.setSpacing(0)
        self.qtObjectLayout.setMargin(0)

        self.dateFormat = lineEditClass(parent_ui_component.qtObject)
        self.dateFormat.setMinimumSize(98, 20)
        self.dateFormat.setMaximumSize(98, 20)

        self.qtObjectLayout.addWidget(self.dateFormat)

        # Button outside the dateFormat
        self.qtButton = QtGui.QToolButton(self.qtObject)
        self.qtButton.setStyleSheet("""
            QToolButton{
                background-color: transparent;
            }

            QToolButton:hover{
                background-color: transparent;
                border: 1px solid #F5BC40;
            }""")

        self.icon = QtGui.QIcon()
        self.icon.addPixmap(QtGui.QPixmap(_fromUtf8(':calendar.png')),
                            QtGui.QIcon.Normal,
                            QtGui.QIcon.Off)
        self.qtButton.setIcon(self.icon)
        self.qtButton.setIconSize(QtCore.QSize(24, 24))
        self.name = name
        self.parent_ui_component = parent_ui_component

        self.qtButton.connect(self.qtButton, QtCore.SIGNAL(
            _fromUtf8("clicked()")), self.select_twoDate)
        self.qtButton.setToolTip("'+Press here to show Calendar+'")
        self.qtObjectLayout.addWidget(self.qtButton)
        self.date = None

        if typeDouble:
            self.qtCalendar = DateRange(self)
            self.qtCalendar.confirmed.connect(self.valid_twoDate)

        # UIDateSimple
        else:
            self.qtButton.connect(self.qtButton, QtCore.SIGNAL(
                                  _fromUtf8("clicked()")), self.select_date)
            self.qtButton.setToolTip("'+Press here to show Calendar+'")
            self.qtObjectLayout.addWidget(self.qtButton)
            self.qtCalendar = poppedClass(self, "cal")
            self.qtCalendar.z.selection_finished.connect(self.valid_date)

        self.otherFeild = None
        self.typeDouble = typeDouble

    def setInvalidStyle(self):
        if isinstance(self.qtCalendar, DateRange):
            self.dateFormat.setStyleSheet(INVALID_CSS)
            self.otherFeild.dateFormat.setStyleSheet(INVALID_CSS)
        else:
            self.dateFormat.setStyleSheet(INVALID_CSS)
            self.qtCalendar.z.showToday()
            self.qtCalendar.valid.connect(self.valid, QtCore.SIGNAL(
                _fromUtf8("clicked()")), self.valid_date)

    def setReadOnly(self, ro):
        self.dateFormat.setReadOnly(ro)

    def select_date(self):
        if self.dateFormat.value:
            qd = self.dateFormat.value
            self.qtCalendar.z.setSelectedDate(qd)
        self.qtCalendar.show()

    def select_twoDate(self):
        self.qtCalendar.show()

    def valid_date(self, date):
        self.date = date
        if(self.date):
            self.dateFormat.setText(self.date.toPyDate().strftime("%d/%m/%Y"))
            self.qtCalendar.hide()

    def addLink(self, other):
        self.otherFeild = other

    def valid_twoDate(self, dateFrom, dateTill):
        self.dateFrom = dateFrom
        self.dateTill = dateTill
        if (self.dateFrom and self.dateTill):
            if self.dateFrom > self.dateTill:
                print "error dateFrom > dateTill"
            else:
                if (self.typeDouble == "From"):
                    self.date = dateFrom
                    self.dateFormat.setText(
                        self.date.toPyDate().strftime("%d/%m/%Y"))
                    self.otherFeild.dateFormat.setText(
                        self.dateTill.toPyDate().strftime("%d/%m/%Y"))
                else:
                    self.date = dateTill
                    self.dateFormat.setText(
                        self.date.toPyDate().strftime("%d/%m/%Y"))
                    self.otherFeild.dateFormat.setText(
                        self.dateFrom.toPyDate().strftime("%d/%m/%Y"))
                self.qtCalendar.hide()

    def value(self):
        try:
            return datetime.strptime(str(self.dateFormat.text()),
                                     '%d/%m/%Y').date()
        except ValueError:
            return None

    def setVal(self, val):
        if not val or val == unicode(None) or val == '':
            self.dateFormat.setText('//')
        elif isinstance(val, datetime) or isinstance(val, date):
            self.dateFormat.setText(val.strftime("%d/%m/%Y"))
        elif '/' in val:
            self.dateFormat.setText(val)
        elif '-' in val:
            # Let's try to reformat the date from database
            # rebuild the date object
            the_date = datetime.strptime(val, '%Y-%m-%d').date()
            # and reformat it
            self.dateFormat.setText(the_date.strftime("%d/%m/%Y"))
        else:  # unknown case, raise excpetio
            raise Exception('Unknown case with date format %s' % val)

    def clear(self):
        # self.dateFormat.val = '//'
        self.dateFormat.setText('//')

    def setMaxDate(self, date):
        self.dateFormat.setMaxDate(date)
        self.qtCalendar.setMaxDate(date)


class UIMonth(UIDate):

    def __init__(self, parent_ui_component, name):
        # FIXME we have to write a QMonthLineEdit class
        super(UIMonth, self).__init__(parent_ui_component, name,
                                      poppedClass=UIPoppedMonth,
                                      lineEditClass=MonthLineEdit)

    def valid_date(self, date):
        self.date = date
        if(self.date):
            self.dateFormat.setText(self.date.toPyDate().strftime("%m/%Y"))
            self.qtCalendar.hide()

    def setVal(self, val):
        if not val or val == unicode(None) or val == '':
            self.dateFormat.setText('mm/yyyy')
        elif isinstance(val, date) or isinstance(val, datetime):
            self.dateFormat.setText(val.strftime("%m/%Y"))
        elif '/' in val:
            self.dateFormat.setText(val)
        elif '-' in val:
            # Let's try to reformat the date from database
            # rebuild the date object
            the_date = datetime.strptime(val, '%Y-%m-%d').date()
            # and reformat it
            self.dateFormat.setText(the_date.strftime("%m/%Y"))
        else:  # unknown case, raise excpetio
            raise Exception('Unknown case with date format %s' % val)

    def clear(self):
        self.dateFormat.setText('mm/yyyy')

    def value(self):
        try:
            return datetime.strptime(str(self.dateFormat.text()),
                                     '%m/%Y').date()
        except ValueError:
            return None

    def setMonths(self, months):
        self.qtCalendar.setMonths(months)


class LineMonth(UIObject):

    def __init__(self, parent, name):
        super(LineMonth, self).__init__(parent, name)
        self.qtObject = MonthLineEdit(parent.qtObject)
        self.setReadOnly(True)
        self.setMinimumSize(98, 20)
        self.setMaximumSize(98, 20)

        self.format = "%m/%Y"

    def setVal(self, val):
        if not val or val == unicode(None) or val == '':
            self.setText('mm/yyyy')
        elif isinstance(val, date) or isinstance(val, datetime):
            self.setText(val.strftime(self.format))
        elif '/' in val:
            self.setText(val)
        elif '-' in val:
            # Let's try to reformat the date from database
            # rebuild the date object
            the_date = datetime.strptime(val, '%Y-%m-%d').date()
            # and reformat it
            self.setText(the_date.strftime(self.format))
        else:  # unknown case, raise excpetio
            raise Exception('Unknown case with date format %s' % val)

    def clear(self):
        self.setText('mm/yyyy')

    def value(self):
        try:
            return datetime.strptime(str(self.text()),
                                     self.format).date()
        except ValueError:
            return None
