#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

"""
import logging
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from .uiobject import UIObject
from .tree import TreeWidget
log = logging.getLogger('sysgrove')


class UILayout (UIObject):
    pass


class UIGridLayout (UILayout):

    def __init__(self, parent=None, name=""):
        super(UIGridLayout, self).__init__(parent, name)
        if parent:
            self.qtObject = QtGui.QGridLayout(parent . qtObject)
        else:
            self.qtObject = QtGui.QGridLayout()
        self.qtObject.setObjectName(name)

    def addWidget(self, widget_27, row, col, useless_1=1, useless_2=1):
        """ Overload that allows to use UIObjects """
        try:
            self.qtObject.addWidget(
                widget_27.qtObject, row, col)  # ,useless_1,useless_2)
        except:
            try:
                self.qtObject.addWidget(widget_27, row, col)
            except Exception as e:
                log.debug("unexpected exception %s" % e)
                raise Exception(e)

    # FIXME : Wrong : method doesn't exist for grid layout
    def addSpacerItem(self, spacer, row, col, useless_1=1, useless_2=1):
        self.qtObject.addSpacerItem(spacer.qtObject, row, col)

    def addLayout(self, layout, row, col, useless_1=1, useless_2=1):
        self.qtObject.addLayout(layout.qtObject, row, col)


class UIVLayout (UILayout):

    def __init__(self, parent=None, name=""):
        super(UIVLayout, self).__init__(parent, name)
        if parent:
            self.qtObject = QtGui.QVBoxLayout(parent . qtObject)
        else:
            self.qtObject = QtGui.QVBoxLayout()
        self.qtObject.setObjectName(name)

    def addWidget(self, widget_27):
        self.qtObject.addWidget(widget_27.qtObject)

    def addSpacerItem(self, spacer):
        self.qtObject.addSpacerItem(spacer.qtObject)

    def addLayout(self, layout):
        self.qtObject.addLayout(layout.qtObject)


class UIHLayout (UILayout):

    def __init__(self, parent=None, name=""):
        super(UIHLayout, self).__init__(parent, name)
        if parent:
            self.qtObject = QtGui.QHBoxLayout(parent . qtObject)
        else:
            self.qtObject = QtGui.QHBoxLayout()
        self.qtObject.setObjectName(name)

    def addWidget(self, widget_27):
        if isinstance(widget_27, TreeWidget):
            self.qtObject.addWidget(widget_27)
        else:
            self.qtObject.addWidget(widget_27.qtObject)

    def addSpacerItem(self, spacer):
        self.qtObject.addSpacerItem(spacer.qtObject)

    def addLayout(self, layout):
        self.qtObject.addLayout(layout.qtObject)

    def removeWidget(self, widget):
        self.qtObject.removeWidget(widget.qtObject)
