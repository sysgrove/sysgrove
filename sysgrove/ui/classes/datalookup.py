#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

----
"""
from functools import partial
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove.utils import (
    retreive_class, getattr_ex,
    debug_trace, getattr_verbose_name
)
from sysgrove.settings import READONLY_CSS
from sysgrove.settings import STATUS_OPEN, STATUS_PARTEXEC

from sysgrove.utils import UserMessage
from sysgrove.models import Base
from .uiobject import UIObject
from .lineedit import FocusStyledLineEdit
from .table import LinkedTable, build_columns, TableColumn
from .mixins.dialog_mixin import QDialogMixin
from .message import show_info
from sysgrove import i18n
_ = i18n.language.ugettext


focus_out = """
    background-color:#ffffff;
    color: #000000;
    border: 1px solid #aaaaaa;
"""


class UIDataLookupPopup(QtGui.QDialog, QDialogMixin):

    def __init__(self, parent, name="lookup", pk_field='code',
                 fields=['code', 'description'], *args, **kwargs):
        from sysgrove.context import kernel
        super(UIDataLookupPopup, self).__init__(None, QtCore.Qt.Dialog)
        self.contextAssociate = None
        self.possible_values = None
        if kernel.get_current_context().ui_space:
            self.contextAssociate = kernel\
                .get_current_context().ui_space.qtObject
        self.pk_field = pk_field
        self.fields = fields

        # the title is inherited, we just need to set the text
        self.setWindowTitle(parent.linkTitle)

        self.qtObject = self
        columns = build_columns(parent.klass, self.fields)
        self.table = LinkedTable(self,
                                 "TableDataLookup",
                                 columns=columns,
                                 klass=parent.klass)

        search_columns = []
        for col in columns:
            n, t, e, h, a = col
            if t in [unicode, str]:
                search_columns.append((n, t, True, h, a))

        search_table = TableColumn(columns=search_columns,
                                   showReadOnly=False,
                                   selectAll=None,
                                   parent=self)
        search_table.setObjectName('SearchTableDataLookup')
        search_table.setRowCount(1)
        height = search_table.horizontalHeader().sizeHint().height()
        height += search_table.rowHeight(0)
        search_table.setMinimumHeight(height)
        search_table.setMaximumHeight(height)

        # add a QLineEdit in each cell
        for i in xrange(len(search_columns)):
            tableItem = QtGui.QLineEdit()
            tableItem.textChanged.connect(
                partial(self.onTextChanged,
                        search_columns[i][self.table.ATTR]))
            search_table.setCellWidget(0, i, tableItem)

        # Setting buttons
        layout = QtGui.QVBoxLayout(self)
        self.buttons = QtGui.QWidget(self)
        layout.addWidget(search_table)
        layout.addWidget(self.table.qtObject)
        layout.addWidget(self.buttons)

        self.buttonsLayout = QtGui.QHBoxLayout(self.buttons)
        self.validButton = QtGui.QToolButton(self.buttons)
        self.validIcon = QtGui.QIcon()
        self.validIcon.addPixmap(
            QtGui.QPixmap(_fromUtf8(':document-save.svg')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off
        )
        self.validButton.setIcon(self.validIcon)
        self.validButton.setIconSize(QtCore.QSize(30, 30))
        self.validButton.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        self.validButton.setText(_(" Select "))

        self.cancelButton = QtGui.QToolButton(self.buttons)
        self.cancelIcon = QtGui.QIcon()
        self.cancelIcon.addPixmap(
            QtGui.QPixmap(_fromUtf8(':edit-clear.svg')),
            QtGui.QIcon.Normal, QtGui.QIcon.Off
        )
        self.cancelButton.setIcon(self.cancelIcon)
        self.cancelButton.setIconSize(QtCore.QSize(30, 30))
        self.cancelButton.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        self.cancelButton.setText(_("Cancel"))

        self.buttons.setStyleSheet("""
        QWidget{
            border: none;
        }
        QToolButton{
        border: 2px solid #8f8f91;
            border-radius: 6px;
            font-weight: bold;
            font-size: 10px;
            background-color: qlineargradient(
        x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde);
         }
        QToolButton:hover{
        /*border: 2px solid #8f8f91;*/
        border: 2px solid #F5BC40;
            border-radius: 6px;
            font-weight: bold;
            font-size: 10px;
            background-color: qlineargradient(
        x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dadbde, stop: 1 #f6f7fa);
        }""")

        self.setStyleSheet("""
        QWidget{
            border: 2px solid #F5BC40;
            border-radius: 6px;
            font-weight: bold;
        }
        QScrollBar:vertical {
          border: none;
          background: #ececec;
          width: 16px;
          margin: 20px 0 20px 0;
        }

        QScrollBar::handle:vertical {
          background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 #F3AA3F, stop: 1 #EE8639);
          width: 14px;
          min-height: 18px;
          background-image: url(:scrollhandlevertical.png);
          background-repeat: no-repeat;
          background-position: center center;
          border-radius: 4px;
          border: 1px solid #888888;
        }

        QScrollBar::handle:vertical:hover {
          background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 #EE8639, stop: 1 #F3AA3F);
          background-image: url(:scrollhandlevertical.png);
          background-repeat: no-repeat;
          background-position: center center;
          border: 1px solid #666666;
        }

        QScrollBar::add-line:vertical {
          border: 1px solid #999999;
          background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 #F5BC41, stop: 1 #F3AA3F);
          height: 20px;
          subcontrol-position: bottom;
          subcontrol-origin: margin;
        }

        QScrollBar::sub-line:vertical {
          border: 1px solid #999999;
          background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 #F5BC41, stop: 1 #F3AA3F);
          height: 20px;
          subcontrol-position: top;
          subcontrol-origin: margin;
        }
        QScrollBar::add-line:vertical:hover {
          border: 1px solid #999999;
          background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 #F3AA3F, stop: 1 #F5BC41);
          height: 20px;
          subcontrol-position: bottom;
          subcontrol-origin: margin;
        }

        QScrollBar::sub-line:vertical:hover {
          border: 1px solid #999999;
          background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 #F3AA3F, stop: 1 #F5BC41);
          height: 20px;
          subcontrol-position: top;
          subcontrol-origin: margin;
        }
        QScrollBar::up-arrow:vertical {
          width: 16px;
          height: 20px;
          background: url(:scroll-up.png); /* FIXME */
          background-repeat: no-repeat;
          background-position: center center;
        }

        QScrollBar::down-arrow:vertical {
          width: 16px;
          height: 20px;
          background: url(:scroll-down.png); /* FIXME */
          background-repeat: no-repeat;
          background-position: center center;
        } """)

        self.buttonsLayout.addWidget(self.validButton)
        self.buttonsLayout.addWidget(self.cancelButton)

        self.validButton.connect(
            self.validButton,
            QtCore.SIGNAL("clicked()"), self.valid)
        # FIXME a more uptodate way to write the previuous line.
        # which does not work????
        # self.validButton.clicked.connect(self.valid)

        self.cancelButton.clicked.connect(self.hide)
        self.table.clicked.connect(self.choose)
        self.table.doubleClicked.connect(self.choose_and_valid)

        self.selectedCode = None
        self.selectedObject = None
        self.codeText = None
        self.parentUIObject = parent

        width = 150
        compteur = 0
        for i in self.fields:
            width += self.table.columnWidth(compteur)
            compteur += 1
        self.table.horizontalHeader().setStretchLastSection(True)
        h = 295
        self.resize(width, h)
        self.actu = True

    def setTable(self):
        self.table.clear()
        self.possible_values = self.parentUIObject.values(self.parentUIObject,
                                                          self.pk_field)
        self.table.setVal(self.possible_values)

    def choose_and_valid(self, index):
        self.choose(index)
        self.valid()

    def choose(self, index):
        if not index.isValid():
            return
        # FIXME, should be compute only one time
        codeIndex = self.fields.index(self.pk_field)
        row = index.row()
        item = self.table.item(row, codeIndex)
        if item:
            self.selectedCode = str(
                item.data(QtCore.Qt.DisplayRole).toString())

    def valid(self, and_hide=True):
        # FIXME... code is not very nice. Especially discarding with
        # self.parent.name.startswith('_')
        # And special case for M2M
        # Please review it
        # A better solution could be to use screen.bdd_to_ui
        # for the selected fields

        if self.selectedCode:
            from sysgrove.context import kernel
            ui_content = kernel.get_current_context().screen.ui_space
            fb_kwargs = {self.pk_field: self.selectedCode}

            if self.parentUIObject.filter_with_masters and \
                    self.parentUIObject.linkMasters:
                for master in self.parentUIObject.linkMasters:
                    attr = master[0]
                    uiMaster = getattr(ui_content,
                                       self.parentUIObject.linkPrefix + attr,
                                       None)
                    if uiMaster:
                        value = str(uiMaster.value())
                        if '_' in attr:
                            attr = attr.split('_')[1]
                        fb_kwargs[attr + '__code'] = value

            self.selectedObject = self.parentUIObject.klass.query.filter_by(
                **fb_kwargs).one()

            self.parentUIObject.setVal(self.selectedCode)

            if '_' in self.parentUIObject.name:
                root_name = self.parentUIObject.name.split(
                    self.parentUIObject.linkPrefix, 1)[1]
            else:
                root_name = 'code'
            if self.parentUIObject.name.startswith('_'):
                inline = self.parentUIObject.getFirsInline()
                ui_space = inline.uiform
                fields = inline.fields
            else:
                # supress the self.parent.name prefix
                from sysgrove.context import kernel
                screen = kernel.get_current_context().screen
                multiple = getattr(screen, 'multiple', None)
                if multiple:
                    ui_space = self.parentUIObject.getMyParentFromThem(
                        multiple)
                else:
                    ui_space = screen.ui_space
                fields = screen.fields

            # We have to update all field
            if root_name == 'code':
                root_name = ''
            else:
                root_name = root_name + '_'

            # The fields have been forced, use the
            if self.parentUIObject.fields:
                fields = self.parentUIObject.fields
            for f in fields:
                if f.startswith(root_name):
                    if root_name == '':
                        attr = f
                    else:
                        attr = f.split(root_name)[1]
                    uiObject = getattr(
                        ui_space, self.parentUIObject.linkPrefix + f, None)
                    if attr and uiObject:
                        val = getattr_ex(self.selectedObject, attr)
                        if isinstance(val, list):
                            # FIXME: should be always true
                            uiObject.clear()
                            if attr.endswith('s'):
                                uiObject.setVal(val)
                            else:
                                if len(val) > 0:
                                    uiObject.setVal(unicode(val[0]))
                                else:
                                    uiObject.setVal('')
                        elif isinstance(val, Base):
                            if hasattr(val, self.pk_field):
                                uiObject.setVal(
                                    getattr(val, self.pk_field))
                        elif not val:
                            uiObject.setVal('')
                        else:
                            uiObject.setVal(unicode(val))
            # set defaults values
            self.handle_defaults(ui_space)

        if and_hide:
            self.hide()

    def handle_defaults(self, ui_space):
        for (attr, target) in self.parentUIObject.defaults:
            uiTarget = getattr(
                ui_space, self.parentUIObject.linkPrefix + target, None)
            if uiTarget:
                if attr == '':
                    selectedAttr = self.selectedObject
                else:
                    selectedAttr = getattr(self.selectedObject, attr, None)
                if selectedAttr:
                    uiTarget.setVal(selectedAttr.code)

    def onTextChanged(self, attr, text):
        text = unicode(text).upper()

        values = [v for v in self.possible_values
                  if text in getattr_ex(v, attr).upper()]
        self.table.clear()
        self.table.setVal(values)


# SDataLoopup and UIDataloopup are exactly the same
# We make distinction to be able to add 'description' field or not
class SDataLookup(UIObject):
    # FIXME : need to emulate a editingFinished() when all fields are
    # non-empty and we're leaving one of them
    # edited = QtCore.pyqtSignal(str)

    def __init__(self, parent_ui_component, name):
        super(SDataLookup, self).__init__(parent_ui_component, name)
        self.linkedUIObject = None
        self.popup = None
        self.setVal_update = False
        self.defaults = []
        self.filter_with_masters = False
        self.fields = None
        self.linkTitle = None

        self.qtObject = QtGui.QWidget(parent_ui_component.qtObject)
        self.qtObject.setStyleSheet("border:None;")
        self.qtObject.setMaximumHeight(32)
        self.qtObjectLayout = QtGui.QHBoxLayout(self.qtObject)
        self.qtObjectLayout.setSpacing(0)
        self.qtObjectLayout.setMargin(0)

        # self.lineEdit = QtGui.QLineEdit(parent_ui_component.qtObject)
        self.lineEdit = FocusStyledLineEdit("""
                                            background-color:#ddddff;
                                            color: #000000;
                                            border: 1px solid #aaaaaa;
                                            """,
                                            focus_out,
                                            parent_ui_component.qtObject)

        self.lineEdit.setMinimumHeight(20)
        self.lineEdit.setMaximumHeight(20)
        self.lineEdit.setStyleSheet("border: 1px solid #aaaaaa;")

        self.qtObjectLayout.addWidget(self.lineEdit)

        # Button outside the dateFormat
        self.qtButton = QtGui.QToolButton(self.qtObject)
        self.qtButton.setStyleSheet("""
            QToolButton{
                background-color: transparent;
            }

            QToolButton:hover{
                background-color: transparent;
                border: 1px solid #F5BC40;
            }""")

        self.qtButton.setText("")

        self.icon = QtGui.QIcon()
        self.icon.addPixmap(QtGui.QPixmap(_fromUtf8(':lookup.svg')),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.qtButton.setIcon(self.icon)
        self.qtButton.setIconSize(QtCore.QSize(20, 20))
        self.qtButton.setToolTip("'+Search in list+'")
        self.qtButton.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.qtObjectLayout.addWidget(self.qtButton)

        self.lineEdit.editingFinished.connect(self.returnPressed)
        self.lineEdit.textChanged.connect(self.returnPressed)
        self.qtButton.clicked.connect(self.open_popup)

        self.setCharSize(10)

    def addDefault(self, default):
        self.defaults.append(default)

    @show_info
    def returnPressed(self, text=None, *args, **kwargs):
        if self.popup:
            if text:
                if self.setVal_update:
                    self.setVal_update = False
                    self.popup.selectedCode = unicode(text).upper()
                else:
                    # case where the user had manually enter a value
                    # check that it is a posssible values
                    values = self.values(self, self.popup.pk_field)
                    values = map(lambda x: getattr(x, self.popup.pk_field),
                                 values)
                    if [a for a in values
                            if a.startswith(unicode(text).upper())]:
                        return
                    else:
                        self.clear()
                        raise UserMessage(
                            u"Values starting with %s are not allowed" % text)
            else:
                self.popup.selectedCode = unicode(self.lineEdit.text()).upper()
            self.popup.valid(False)

    def clear(self):
        if self.popup:
            self.popup.selectedObject = None
        self.lineEdit.clear()

    def setReadOnly(self, bool):
        # Take care, if you want to disconnect the signal : do NOT use lambda
        # http://stackoverflow.com/questions/15600014/pyqt-disconnect-slots-new-style
        self.lineEdit.setReadOnly(bool)
        self.lineEdit.setStyleSheet(READONLY_CSS)
        self.qtButton.clicked.disconnect(self.open_popup)

    def value(self):
        return unicode(self.lineEdit.text())

    def valueObject(self):
        if self.popup:
            return self.popup.selectedObject

    def setVal(self, value):
        if value:
            # FIXME : find a nicest way to do this
            self.setVal_update = True
            self.lineEdit.setText(value)
            self.setVal_update = False

        else:
            self.lineEdit.setText('')
        self.lineEdit.setCursorPosition(0)

    def setCharSize(self, charSize):
        self.qtObject.setMinimumWidth(charSize * 8 + 10 + 30)
        self.qtObject.setMaximumWidth(charSize * 8 + 10 + 30)

    def open_popup(self):
        self.resetStyle()
        self.popup.setTable()
        self.popup.show()

    def create_popup(self):
        self.popup = UIDataLookupPopup(self)

    def setLink(self, klass, prefix, slave=None, title=u"",
                masters=[], filters={}, order={}, join=(), ):
        self.linkTitle = title
        if slave:
            self.klass = retreive_class(slave[1], klass)
            if not self.linkTitle:
                if slave[0] == slave[1]:
                    self.linkTitle = self.klass.verbose_name
                else:
                    self.linkTitle = getattr_verbose_name(slave[0], klass)
        else:
            self.klass = klass
            if not self.linkTitle:
                self.linkTitle = self.klass.verbose_name

        self.linkPrefix = prefix
        self.linkSlave = slave
        self.linkMasters = masters
        self.linkFilters = filters
        self.linkOrder = order
        self.join = join
        self.create_popup()

    def setTitle(self, title):
        if not self.linkTitle:
            self.linkTitle = title

    # It exists some case, where filters are conditionnal
    # (for example, by the value of an other field)
    # so we must be able to change them.
    def resetFilter(self):
        self.linkFilters = []

    def setFilters(self, filters):
        self.linkFilters = filters

    def force_fields(self, fields):
        self.fields = fields

    # FIXME: do not overwritten setValidStyle, it is never used
    def setInvalidStyle(self):
        self.lineEdit.setStyleSheet("""
            background-color:#c95a5a;
            color: #000000;
            border: 1px solid #ff0000;""")

    def resetStyle(self):
        self.lineEdit.setStyleSheet(focus_out)

    def changeEnabled(self):
        if self.qtObject.isVisible():
            self.qtObject.setVisible(False)
        else:
            self.qtObject.setVisible(True)


class UIDataLookup(SDataLookup):
    pass


class UIDataLookupExchangRate(UIDataLookup):

    def create_popup(self):
        from sysgrove.modules.masterdata.models import ExchangeRate
        fields = ExchangeRate.list_columns[:]
        fields.append('id')
        self.popup = UIDataLookupPopup(
            self,
            pk_field='id',
            fields=fields)

    def setVal(self, exR):
        if isinstance(exR, str) or isinstance(exR, unicode):
            if self.popup.selectedObject:
                self.setVal(self.popup.selectedObject)
            else:
                super(UIDataLookupExchangRate, self).setVal(exR)
        else:
            from sysgrove.modules.masterdata.models import ExchangeRate
            assert(isinstance(exR, ExchangeRate))
            self.lineEdit.setText(str(exR.exchangeRateValue))
            self.lineEdit.setCursorPosition(0)

    def valueObject(self):
        if self.popup.selectedObject:
            return self.popup.selectedObject

    def values(self, linkedUI, pk_field='code'):
        # in case of document return only the exchangerate
        # correcponding to the company
        from sysgrove.context import kernel
        from sysgrove.modules.masterdata.models import ExchangeRate
        doc = kernel.get_current_context().screen.get_current_value()
        if hasattr(doc, 'site') and doc.site:
            return ExchangeRate.query\
                .filter_by(company_id=doc.site.company.id).all()


class UIDataLookupDocument(UIDataLookup):

    def __init__(self, parent, name):
        super(UIDataLookupDocument, self).__init__(parent, name)
        self.filter_with_masters = True

    def create_popup(self):
        self.popup = UIDataLookupPopup(
            self,
            pk_field='documentNumber',
            fields=['documentNumber',
                    'status',
                    'documentValueDate',
                    'soldToCode',
                    'soldToCode_description',
                    'soldToCode_address_addressLine1',
                    'soldToCode_address_zIPCode',
                    'soldToCode_address_city', 'soldToCode_address_country'
                    ]
        )


class UIDataLookupDocument_O_PE(UIDataLookupDocument):

    def values(self, linkedUI, pk_field):
        res = super(UIDataLookupDocument, self).values(linkedUI, pk_field)
        return filter(lambda x: x.status.id in [STATUS_OPEN, STATUS_PARTEXEC],
                      res)


# Special case to handle DocCopyRule filter
class UIDataLookupPopupWithDCR(UIDataLookupPopup):

    def setTable(self):
        from sysgrove.context import kernel
        self.table.clear()
        joinKlass = self.parentUIObject.join[0]
        joinFilter = self.parentUIObject.join[1]
        screen = kernel.get_current_context().screen
        multiple = getattr(screen, 'multiple', None)
        if multiple:
            ui_content = self.parentUIObject.getMyParentFromThem(multiple)
        else:
            ui_content = screen.ui_space

        subst_filter = UIObject.subst_filter(ui_content,
                                             joinFilter,
                                             self.parentUIObject.linkMasters,
                                             self.parentUIObject.linkPrefix)

        dcr = joinKlass.query.filter_by(**subst_filter).all()
        values = map(
            lambda x: getattr(x, self.parentUIObject.linkSlave[2] + 'Origin'),
            dcr)
        values = set(values)

        for val in values:
            row = []
            for f in self.fields:
                attr = getattr_ex(val, f, None)
                if attr:
                    if hasattr(attr, 'code'):
                        row.append(unicode(attr.code))
                    else:
                        row.append(attr)
                else:
                    row.append('')
            self.table.addRow(row)


class UIDataLookupSiteWithDCR(UIDataLookup):

    def create_popup(self):
        self.popup = UIDataLookupPopupWithDCR(
            self,
            pk_field='code',
        )
