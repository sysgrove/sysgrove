#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove.settings import READONLY_CSS
from sysgrove.config import CONFIG
from sysgrove.utils import retreive_class, getattr_ex, map_ex


def create_icon(name):
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(_fromUtf8(':%s' % name)),
                   QtGui.QIcon.Normal, QtGui.QIcon.Off)
    return icon


# FIXME: UIObject has to be reviewed... A mixin should be better
# as the usage of qtObject.
# The most important thing of UIOjbect is the possibility to access
# (thanks to getattr) to a child (child of child, child of child
# of child...) in the same way, without taking care of the level
# of inheritance
# But we cannot use new signal of QObject with such a class
# the class inheritance in  broken....
# read http://pyqt.sourceforge.net/Docs/PyQt4/new_style_signals_slots.html
class UIObject(object):

    """ This class only exists for inheritance purpose, and providing
    a recursive UIItem getter """

    def __init__(self, parent_ui_component, name):
        """ Constructor takes an UIObject and a name.
        If name is empty, takes the variable name (if possible) """
        self.__items__ = {}
        self.name = name
        self.parent = parent_ui_component

        # do not erased self.klass, please
        if not hasattr(self, 'klass'):
            self.klass = None
        self.linkPrefix = None
        self.linkSlave = None
        self.linkMasters = None
        self.linkFilters = {}
        self.linkOrder = {}

        # Add the newly creates UIObject to its parent's items.
        # Because parent can be None
        if parent_ui_component and hasattr(parent_ui_component, "__items__"):
            if name in parent_ui_component.__items__:
                if isinstance(parent_ui_component.__items__[name], list):
                    parent_ui_component.__items__[name].append(self)
                else:
                    parent_ui_component.__items__[name] = [
                        parent_ui_component.__items__[name],
                        self
                    ]
            else:
                parent_ui_component.__items__[name] = self

    def find_children(self, name):
        """ same as __getattr__ when name is the name of a child object,
            except that it returns a list.
            but if many uiObject are found then a list is return.
            In fact it is very close the QObject.findChildren method
        """
        if name in self.__items__:
            children = self.__items__[name]
            if isinstance(children, list):
                return children
            else:
                return [children]
        else:
            res = []
            for child in self.__items__.values():
                if not isinstance(child, list):
                    res.extend(child.find_children(name))
            return res

    def __getattr__(self, name):
        """ Monster overload that allows to find attributes recursively,
        and find Qt attributes directly on UIObjects """
        if (name in self.__dict__):
            return self.__dict__[name]
        else:
            z = self.noex_getattr(name)
            return z

    def noex_getattr(self, name):
        """  Used in recursions """

        if 'qtObject' in self.__dict__ and \
                hasattr(self.__dict__['qtObject'], name):
            return getattr(self.__dict__['qtObject'], name)
        elif ('__items__' in self.__dict__ and
              name in self.__dict__['__items__']):
            return self.__dict__['__items__'][name]
        else:
            for i in self.__dict__['__items__']:
                try:
                    x = self.__dict__['__items__'][i].noex_getattr(name)
                    if x:
                        return x
                except AttributeError:
                    # let a chance to find something
                    pass

        raise AttributeError(
            "%s (with name %s) has no attribut %s" % (
                self, self.name, name))

    def setAction(self, typeAction, func):
        self.qtObject.connect(self.qtObject, QtCore.SIGNAL(
            _fromUtf8(typeAction)), func)

    def value(self):
        """ Each Class which may have a value must
        redefine this method to read value from the qtObject
        with specific method """
        return None

    # To setup style from CSS with only one line
    def style(self):
        self.qtObject.setStyleSheet(CONFIG['css'])

    # To ensure a default behaviour:
    # def setValidStyle(self):
    #     self.qtObject.setStyleSheet("""
    # background-color:#c0ec94;
    # color: #000000;
    # border: 1px solid #9ae54f;"""
    #                                 )

    def resetStyle(self):
        self.setStyleSheet("")

    def setInvalidStyle(self):
        self.qtObject.setStyleSheet("""
            background-color:#c95a5a;
            color: #000000;
            border: 1px solid #ff0000;""")

    # Used to simulate a signal sent by the Object (can be used by other
    # objects ...)
    def simulate(self, x):
        self.emit(QtCore.SIGNAL(x))

    # It is a simplifier form of the 'values' method
    # at least the way the filter is computed
    @staticmethod
    def subst_filter(ui_content, filters, masters, prefix):
        for master in masters:
            uiMaster = getattr(ui_content, prefix + master[0], None)
            if uiMaster:
                value = unicode(uiMaster.value())

                for (k, v) in filters.iteritems():
                    if v == master[0]:
                        if k.endswith('code'):
                            value = str(value)
                        filters[k] = value
                    elif isinstance(v, str) and v.startswith(master[0]):
                        obj = uiMaster.valueObject()
                        value = getattr_ex(obj, v.split(master[0] + '_', 1)[1])
                        filters[k] = value

        return filters

    def values(self, linkedUI, pk_field='code'):
        # from sysgrove.utils import debug_trace
        # debug_trace()
        if not linkedUI.linkSlave:
            # nothing to do here
            return linkedUI.klass.query.order_by(pk_field).all()
        else:   # linkedUI.linkfilters must be extended
            from sysgrove.context import kernel
            context = kernel.get_current_context()
            multiple = getattr(context.screen, 'multiple', None)

            # two differents case depending on FK or allocation table
            myFilter = context.screen.build_filter(linkedUI.linkFilters)
            for master in linkedUI.linkMasters:
                if multiple:
                    parent = self.getMyParentFromThem(multiple)
                    uiMaster = getattr(
                        parent,
                        linkedUI.linkPrefix + master[0],
                        None)
                else:
                    uiMaster = getattr(
                        context.screen.ui_space,
                        linkedUI.linkPrefix + master[0],
                        None)
                if uiMaster:
                    value = unicode(uiMaster.value())
                else:
                    value = getattr(
                        context.context_vars['newValue']['value'],
                        master[0])
                classMaster = master[1]
                if hasattr(linkedUI.klass, classMaster):
                    if isinstance(value, unicode):
                        myFilter[classMaster + '__code'] = value
                    else:
                         # FIXME workoraround for DjangoQuery
                        myFilter[classMaster + '__code'] = getattr(
                            value, 'code')
                else:  # use the namming convention
                    if isinstance(value, unicode):
                        klass = retreive_class(classMaster)
                        value = klass.query.filter_by(
                            **{pk_field: value}).one()
                    myFilter[classMaster + 's__code'] = value.code
            return linkedUI.klass.query.filter_by(**myFilter) \
                                       .order_by(pk_field) \
                                       .order_by(**linkedUI.linkOrder).all()

    def setReadOnly(self, ro):
        if hasattr(self.qtObject, 'setReadOnly'):
            # I can not fing the right parent with the right color...
            # parent_bg_color = str(self.parent.palette().color(10).name())
            self.qtObject.setReadOnly(ro)
            if ro:
                self.setStyleSheet(READONLY_CSS)
            else:
                self.setStyleSheet("")
        for children in self.__items__.values():
            if isinstance(children, list):
                for child in children:
                    child.setReadOnly(ro)
            else:
                children.setReadOnly(ro)

    def getFirsInline(self):
        """ Return the first UIInline parent of the instance"""
        from sysgrove.ui.classes.mixins.inline_mixin import InLineMixin
        if isinstance(self, InLineMixin):
            return self
        else:
            return self.parent.getFirsInline()

    def getMyParentFromThem(self, contents):
        if self in contents:
            return self
        else:
            return self.parent.getMyParentFromThem(contents)

    # debug purpose function
    def li(self, preStr=" "):
        for i in self.__items__:
            print preStr + " " + i
            map_ex(lambda x: x.li(preStr + "---"), self.__items__[i])
            # self.__items__[i].li(preStr + "---")
