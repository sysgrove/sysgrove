#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from .uiobject import UIObject

log = logging.getLogger('sysgrove')


class UIButtonBar(UIObject):

    def __init__(self, parent, name):
        super(UIButtonBar, self).__init__(parent, name)

        self.qtObject = QtGui.QWidget(parent.qtObject)
        self.generalLayout = QtGui.QHBoxLayout(self.qtObject)
        self.buttonsWidget = QtGui.QWidget(self.qtObject)
        self.buttonsLayout = QtGui.QHBoxLayout(self.buttonsWidget)

        self.leftSpacer = QtGui.QSpacerItem(
            1, 1, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.rightSpacer = QtGui.QSpacerItem(
            1, 1, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        self.generalLayout.addSpacerItem(self.leftSpacer)
        self.generalLayout.addWidget(self.buttonsWidget)
        self.generalLayout.addSpacerItem(self.rightSpacer)

    def addButton(self, text):

        button = QtGui.QToolButton(self.buttonsWidget)
        button.setText(text)
        # button.setStyleSheet("border:2px solid black")
        self.buttonsLayout.addWidget(button)
        self.parent.buttons.append(button)
        self.parent.unmark(button)


class UIButtonBarContent(UIObject):

    def __init__(self, parent, name):
        super(UIButtonBarContent, self).__init__(parent, name)

        self.qtObject = QtGui.QWidget(parent.qtObject)

        self.parent.addContent(self)

        self.index = self.parent.contents.index(self)

        self.parent.buttons[self.index].connect(
            self.parent.buttons[self.index],
            QtCore.SIGNAL("clicked()"),
            lambda: self.parent.printContent(self.index)
        )


class UIButtonBarZone(UIObject):

    CSS = """
            border: 2px solid #8f8f91;
            border-radius: 6px;
            font-weight: bold;
            font-size: 10px;
            font-weight: bold;
            background-color:
                qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                stop: 0 %s, stop: 1 %s);
        """

    def __init__(self, parent, name):
        super(UIButtonBarZone, self).__init__(parent, name)

        self.qtObject = QtGui.QWidget(parent.qtObject)

        self.buttons = []
        self.contents = []
        self.active_button = 0

    def addContent(self, content):

        self.contents.append(content)
        if self.contents[0] != content:
            content.hide()
        else:
            self.marker(self.buttons[0])

    def printContent(self, num):

        for content in self.contents:
            content.hide()
        for button in self.buttons:
            if self.buttons.index(button) != num:
                self.unmark(button)
            else:
                self.marker(button)
        self.active_button = num
        self.contents[num].show()

    # FIXME : Duplicated value  with unmark !!!!

    def marker(self, button):
        button.setStyleSheet(self.CSS % ('#aadbde', '#f6f7fa'))

    def unmark(self, button):
        button.setStyleSheet(self.CSS % ('#f6f7fa', '#dadbde'))
