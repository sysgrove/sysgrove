# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides a widget to select date ranges (or two dates)

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
NOTE:
To see an example on how to use the Calendar class go to the end of the file
"""

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import (QWidget, QPushButton, QHBoxLayout,
                         QVBoxLayout, QDialog, QSpacerItem)
from PyQt4.QtCore import QDate, pyqtSignal
from calendar import Calendar
from sysgrove.ui.classes.mixins.dialog_mixin import QDialogMixin
from sysgrove import i18n
_ = i18n.language.ugettext


class DateRange(QDialog, QDialogMixin):

    confirmed = pyqtSignal(object, object)
    canceled = pyqtSignal()

    def __init__(self, parent, widget_type=Calendar.DAY_CALENDAR,
                 *args, **kwargs):
        super(DateRange, self).__init__(None)
        self.left_date = None
        self.right_date = None

        today = QDate.currentDate()
        layout = QVBoxLayout(self)
        layout.addWidget(self.create_calendars(widget_type, today))

        layout.addWidget(self.create_buttons())
        self.parentUIObject = parent
        self.actu = True

    def create_calendars(self, widget_type, start_date):
        calendars_widget = QWidget(self)
        layout = QHBoxLayout(calendars_widget)
        widget1 = QWidget(calendars_widget)
        widget2 = QWidget(calendars_widget)

        layoutV1 = QVBoxLayout(widget1)
        c = Calendar(start_date, widget_type)
        c.selection_finished.connect(self.setMinDate)
        layoutV1.addWidget(c)
        self.left_calendar = c

        Spacer1 = QSpacerItem(
            0, 5, QtGui.QSizePolicy.Minimum,
            QtGui.QSizePolicy.MinimumExpanding)
        layoutV1.addItem(Spacer1)

        layoutV2 = QVBoxLayout(widget2)
        self.c2 = Calendar(start_date, widget_type)
        self.c2.selection_finished.connect(self.right_selected)
        layoutV2.addWidget(self.c2)
        self.right_calendar = self.c2
        Spacer1 = QSpacerItem(
            0, 5, QtGui.QSizePolicy.Minimum,
            QtGui.QSizePolicy.MinimumExpanding)
        layoutV2.addItem(Spacer1)

        # layout.addLayout(layoutV1)
        layout.addWidget(widget1)
        layout.addWidget(widget2)
        return calendars_widget

    def setMinDate(self, date):
        self.left_selected(date)
        self.c2.setMinDate(date)

    def create_buttons(self):

        buttons_widget = QWidget(self)
        layout = QHBoxLayout(buttons_widget)

        cancel_button = QPushButton(_("Cancel"))

        cancel_button.connect(cancel_button, QtCore.SIGNAL(
            "clicked()"), self.hide)
        layout.addWidget(cancel_button)

        confirm_button = QPushButton(_("Confirm"))
        confirm_button.clicked.connect(self.confirm)

        layout.addWidget(confirm_button)

        return buttons_widget

    def left_selected(self, date):
        self.left_date = date

    def right_selected(self, date):
        self.right_date = date

    def confirm(self):

        self.confirmed.emit(self.left_date, self.right_date)


if __name__ == '__main__':

    import sys
    from PyQt4.QtGui import QApplication

    def print_dates(left, right):
        print 'left date: %s, right date: %s' % (left.toString(),
                                                 right.toString())

    app = QApplication(sys.argv)

    w = QWidget()

    date = QDate.currentDate()
    calendar = DateRange(Calendar.DAY_CALENDAR, w)
    calendar.confirmed.connect(print_dates)

    w.resize(700, 350)
    w.show()
    sys.exit(app.exec_())
