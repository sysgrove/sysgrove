# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
NOTE:
To see an example on how to use the Calendar class go to the end of the file
"""
from PyQt4 import QtCore, QtGui
from functools import partial
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import (QDialog, QLabel, QWidget, QPixmap, QPushButton,
                         QVBoxLayout, QHBoxLayout, QApplication,
                         QTreeWidget, QTreeWidgetItem, QHeaderView, QIcon)
from overlay_widget import OverlayWidget
from popup_menu import MenuButton, MenuEntry
from ..mixins.dialog_mixin import QDialogMixin
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove import i18n
_ = i18n.language.ugettext


class MessagePopup(QDialog, QDialogMixin):

    MESSAGE_BUTTONS = 0
    WARNING_BUTTONS = 1
    ERROR_BUTTONS = 2

    ICONS = [u'dialog-information.svg',
             u'dialog-warning.svg',
             u'dialog-error.svg']

    closed = pyqtSignal()
    proceed = pyqtSignal()

    def __init__(self, messages=None, warnings=None, errors=None,
                 *args, **kwargs):

        #super(MessagePopup, self).__init__(*args, **kwargs)
        super(MessagePopup, self).__init__(None)
        self.parentUIObject = args
        self.actu = True
        layout = QVBoxLayout(self)
        tree = QTreeWidget()
        tree.setHeaderLabel("Messages")
        self.layoutButton = None
        w = self.create_messages(tree, "Informations", messages, False)
        level = None
        if w:
            tree.addTopLevelItem(w)
            level = self.MESSAGE_BUTTONS

        w = self.create_messages(tree, "Warnings", warnings, True)
        if w:
            tree.addTopLevelItem(w)
            level = self.WARNING_BUTTONS

        w = self.create_messages(tree, "Errors", errors, True)
        if w:
            tree.addTopLevelItem(w)
            level = self.ERROR_BUTTONS
        self.resize(370, 400)

        layout.addWidget(tree)
        if level is not None:
            layout.addWidget(self.create_buttons(level))
            # to be fixed later
            layout.insertWidget(0, self.create_icon(level))

    def copy_to_clipboard(self, messages):
        c = QApplication.clipboard()
        c.setText(u'\n'.join(messages))

    def create_icon(self, level):
        path = u":" + self.ICONS[level]
        l = QLabel(self.ICONS[level])
        l.setPixmap(QPixmap(path))
        l.setAlignment(QtCore.Qt.AlignHCenter)
        return l

    def create_messages(self, parent, label, messages, copy_to_clipboard):

        if messages is None:
            return None

        tree = QTreeWidgetItem(parent)

        if copy_to_clipboard:
            entries = [MenuEntry("Copy messages to clipboard",
                                 True,
                                 partial(self.copy_to_clipboard, messages))]

            button = partial(MenuButton, entries, "...")
            overlay = OverlayWidget(partial(QLabel, label), button)
            overlay.overlay.resize(20, 18)
            tree.treeWidget().setItemWidget(tree, 0, overlay)
        else:
            tree.setText(0, label)

        lines = []
        for m in messages:
            m = QLabel(m)
            m.setWordWrap(True)
            line = QTreeWidgetItem()
        tree.treeWidget().addTopLevelItem(line)
        tree.treeWidget().setItemWidget(line, 0, m)

        if len(messages) == 1:
            tree.setExpanded(True)
        parent.setColumnWidth(0, 370)
        parent.resizeColumnToContents(0)
        return tree

    def create_buttons(self, level):

        w = QWidget(self)
        w.setStyleSheet("border:None;")
        l = QHBoxLayout(w)
        self.StyleSheetButton = """
            QWidget{
                border: none;
            }
            QToolButton{
            border: 2px solid #8f8f91;
                border-radius: 6px;
                font-weight: bold;
                font-size: 10px;
                background-color: qlineargradient(
            x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde);
             }
            QToolButton:hover{
            /*border: 2px solid #8f8f91;*/
            border: 2px solid #F5BC40;
                border-radius: 6px;
                font-weight: bold;
                font-size: 10px;
                background-color: qlineargradient(
            x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dadbde, stop: 1 #f6f7fa);
            }"""
        self.iconOk = QIcon()
        self.iconOk.addPixmap(QPixmap(_fromUtf8(':dialog-ok.svg')),
                              QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.iconCancel = QIcon()
        self.iconCancel.addPixmap(QPixmap(_fromUtf8(':edit-clear.svg')),
                                  QtGui.QIcon.Normal, QtGui.QIcon.Off)
        if level == self.MESSAGE_BUTTONS or \
           level == self.ERROR_BUTTONS:
            b = QtGui.QToolButton()
            b.setIcon(self.iconOk)
            b.setIconSize(QtCore.QSize(30, 30))
            b.setText("    Ok    ")
            b.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            b.clicked.connect(partial(self.button_signal, self.closed))
            b.setStyleSheet(self.StyleSheetButton)
            #use for validmessagepopup
            l.addWidget(b)
            self.layoutButton = l
        else:
            b = QtGui.QToolButton()
            b.setText(_('Continue'))
            b.setIcon(self.iconOk)
            b.setIconSize(QtCore.QSize(30, 30))
            b.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            b.setStyleSheet(self.StyleSheetButton)
            b.clicked.connect(partial(self.button_signal, self.proceed))
            l.addWidget(b)

            b = QtGui.QToolButton()
            b.setText(_('Cancel'))
            b.setIcon(self.iconCancel)
            b.setIconSize(QtCore.QSize(30, 30))
            b.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            b.setStyleSheet(self.StyleSheetButton)
            b.clicked.connect(partial(self.button_signal, self.closed))
            l.addWidget(b)
        return w

    def button_signal(self, signal):
        signal.emit()

# Usefull to ease i18n
from collections import namedtuple
QuestionChoice = namedtuple('QuestionChoice', 'text key')


class QuestionPopup(QDialog):

    selected = pyqtSignal(object)

    def __init__(self, choices, question, *args, **kwargs):
        super(QuestionPopup, self).__init__(*args, **kwargs)

        l = QVBoxLayout(self)

        l.addWidget(QLabel(question))
        l.addWidget(self.create_buttons(choices))

    def create_buttons(self, choices):

        w = QWidget(self)
        l = QHBoxLayout(w)

        for c in choices:
            b = QPushButton(c.text)
            b.clicked.connect(partial(self.on_selection, c.key))
            l.addWidget(b)

        return w

    def on_selection(self, choice):
        self.selected.emit(choice)


if __name__ == '__main__':
    import sys

    def msg(dialog, text):
        print text
        dialog.hide()

    def open_dialog(parent, messages=None, warnings=None, errors=None):
        dialog = MessagePopup(messages, warnings, errors, parent)
        dialog.closed.connect(partial(msg, dialog, 'closed dialog'))
        dialog.proceed.connect(partial(msg, dialog, 'proceed'))
        dialog.exec_()

    def open_question(parent, questions):
        dialog = QuestionPopup(questions, 'Is this working fine?', parent)
        dialog.selected.connect(partial(msg, dialog))
        dialog.exec_()

    app = QApplication(sys.argv)

    w = QWidget()
    l = QVBoxLayout(w)
    b = QPushButton("open messages", w)
    b.clicked.connect(partial(open_dialog, w, ['msg1', 'msg2'], None, None))
    l.addWidget(b)

    b = QPushButton("open warnings", w)
    b.clicked.connect(partial(open_dialog, w, None, ['warn1', 'warn2'], None))
    l.addWidget(b)

    b = QPushButton("open errors", w)
    b.clicked.connect(partial(open_dialog, w, None, None, ['err1', 'err2']))
    l.addWidget(b)

    b = QPushButton("open question", w)
    b.clicked.connect(
        partial(open_question, w, (QuestionChoice(text=u'Yes', key='Y'),
                                   QuestionChoice(text=u'No', key='N'),
                                   QuestionChoice(text=u'Maybe', key='M'))))
    l.addWidget(b)

    w.resize(200, 200)
    w.show()
    sys.exit(app.exec_())
