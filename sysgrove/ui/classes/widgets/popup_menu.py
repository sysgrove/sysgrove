# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides a widget to select date ranges (or two dates)

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
NOTE:
To see an example on how to use the Calendar class go to the end of the file
"""

from collections import namedtuple
from functools import partial
from PyQt4 import QtGui, QtCore


MenuEntry = namedtuple("MenuEntry", "text enabled call_back")
MenuSeparator = MenuEntry(None, False, None)


class PopupMenu(QtGui.QMenu):

    """
    PopupMenu is an utility class that creates a popup menu with entries
    specified by 'menu_entries' which is a list of MenuEntry objects
    to find out which entry has been selected it is possible to specify
    a callback in to the call_back property of a menu entry or connecting to
    the triggered signal
    """
    triggered = QtCore.pyqtSignal(object)

    def __init__(self, menu_entries, *args, **kwargs):
        super(PopupMenu, self).__init__(*args, **kwargs)

        for e in menu_entries:
            if e == MenuSeparator:
                self.addSeparator()
            else:
                a = QtGui.QAction(e.text, self.parent())
                if e.enabled:
                    f = e.call_back or partial(self.triggered.emit, e.text)
                    a.triggered.connect(f)

                self.addAction(a)


class MenuButton(QtGui.QPushButton):
    """
    MenuButton is a button that shows a contextual menu when left clicked or
    right clicked
    menu_entries is a list of items to be shown in the menu
    each item is an object of the type MenuEntry
    to find out which entry has been selected it is possible to specify
    a callback in to the call_back property of a menu entry or connecting to
    the triggered signal
    """
    triggered = QtCore.pyqtSignal(object)

    def __init__(self, menu_entries, *args, **kwargs):
        super(MenuButton, self).__init__(*args, **kwargs)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)
        self.clicked.connect(self.open_menu)

        self.popup_menu = PopupMenu(menu_entries, self)
        self.popup_menu.triggered.connect(self.triggered.emit)

    def open_menu(self):
        # show context menu
        self.popup_menu.exec_(self.mapToGlobal(QtCore.QPoint(0, 0)))

    def on_context_menu(self, point):
        self.popup_menu.exec_(self.mapToGlobal(point))


if __name__ == '__main__':
    import sys

    class MainForm(QtGui.QMainWindow):
        def __init__(self, parent=None):
            super(MainForm, self).__init__(parent)

            entries = [MenuEntry("test1", True, None),
                       MenuEntry("test2", False, None),
                       MenuSeparator,
                       MenuEntry("test3", True, partial(self.print_entry,
                                                        "wow"))]

            self.button = MenuButton(entries, "test button", self)
            self.button.resize(100, 30)
            self.button.triggered.connect(self.print_entry)

        def print_entry(self, text):
            print text

    app = QtGui.QApplication(sys.argv)
    form = MainForm()
    form.show()

    app.exec_()
