# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides a widget to select date ranges (or two dates)

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
NOTE:
To see an example on how to use the Calendar class go to the end of the file
"""

from functools import partial
from PyQt4 import QtGui


class OverlayWidget(QtGui.QWidget):
    def __init__(self, back_widget, overlay, *args, **kwargs):
        super(OverlayWidget, self).__init__(*args, **kwargs)

        self.back_widget = back_widget(self)
        self.overlay = overlay(self)
        #self.overlay.hide()
        self.overlay.resize(20, 18)
        self.resize(self.back_widget.size())
        self.overlay.show()
        #top_right = self.width() - self.overlay.width()
        top_right = 300 - self.overlay.width()
        self.overlay.move(top_right, 0)

    #def enterEvent(self, event):
        #self.overlay.show()
        #top_right = self.width() - self.overlay.width()
        #self.overlay.move(top_right, 0)

    #def leaveEvent(self, event):
        #self.overlay.hide()


if __name__ == '__main__':
    import sys

    class MainForm(QtGui.QMainWindow):
        def __init__(self, parent=None):
            super(MainForm, self).__init__(parent)

            button = partial(QtGui.QPushButton, "...")

            label = partial(QtGui.QLabel, "this is some\n text\naaaaa\nbbb")

            w = OverlayWidget(label, button, self)
            w.resize(320, 100)
            w.overlay.clicked.connect(partial(self.print_msg, "clicked!!!"))
            w.overlay.resize(20, 18)

        def print_msg(self, text):
            print text

    app = QtGui.QApplication(sys.argv)
    form = MainForm()
    form.show()
    form.resize(400, 200)

    app.exec_()
