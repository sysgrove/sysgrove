# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :  Provide a tree widget with the following behavior if
    a child of this tree has a checkBox which is checked, then
    only  top level widgetItem containing
    this item could be partially (or totally) check.
    A the parent checkbox is checked if all its children are checked
    A partily checked if at least one of it childre is checked
    and unchecked if none of its children are check
contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""


from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove import i18n
_ = i18n.language.ugettext


class ExclusiveTreeWidget(QtGui.QTreeWidget):

    def __init__(self, parent):
        QtGui.QTreeWidget.__init__(self, parent)
        self.itemChanged.connect(self.handleItemChecked)

    def handleItemChecked(self, item, column):
        tree = item.treeWidget()
        state = item.checkState(column)

        parent = item.parent()
        if parent:
            # compute the new parent state
            same_state = True
            i = 0
            while same_state and (i in xrange(parent.childCount())):
                same_state = same_state and \
                    parent.child(i).checkState(column) == state
                i = i + 1
            if same_state:
                parent_state = state
            else:
                parent_state = QtCore.Qt.PartiallyChecked

            if not parent.checkState(column) == parent_state:
                parent.setCheckState(column, parent_state)

        else:  # top level item, handle exclusive property
            if not state == QtCore.Qt.Unchecked:
                item_index = tree.indexOfTopLevelItem(item)
                for i in xrange(tree.topLevelItemCount()):
                    topLevel = tree.topLevelItem(i)
                    if not i == item_index:
                        topLevel.setCheckState(column, QtCore.Qt.Unchecked)
                    else:
                        topLevel.setCheckState(column, state)

        # handle the checkState of children
        if state in [QtCore.Qt.Checked, QtCore.Qt.Unchecked]:
            for i in xrange(item.childCount()):
                item.child(i).setCheckState(column, state)


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    def add_checkBox(item):
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        item.setCheckState(0, QtCore.Qt.Unchecked)

    app = QApplication(sys.argv)

    w = QtGui.QWidget()
    l = QtGui.QVBoxLayout(w)

    tree = ExclusiveTreeWidget(w)

    soldTo = QtGui.QTreeWidgetItem(tree)
    soldTo.setText(0, _("soldTo"))
    soldTo.setText(1, "1")
    add_checkBox(soldTo)

    billTo = QtGui.QTreeWidgetItem(soldTo)
    billTo.setText(0, _("billTo"))
    soldTo.setText(1, "3")
    add_checkBox(billTo)

    shipTo = QtGui.QTreeWidgetItem(soldTo)
    shipTo.setText(0, _("shipTo"))
    soldTo.setText(1, "2")
    add_checkBox(shipTo)

    agent = QtGui.QTreeWidgetItem(tree)
    agent.setText(0, _("agent"))
    soldTo.setText(1, "4")
    add_checkBox(agent)

    l.addWidget(tree)

    it = QtGui.QTreeWidgetItemIterator(tree)
    while it.value() is not None:
        print 'child', it.value().text(0)
        it +=  1


    w.resize(400, 300)
    w.show()
    sys.exit(app.exec_())
