# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides a widget to select dates (day, month or year)

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
NOTE:
To see an example on how to use the Calendar class go to the end of the file
"""
from PyQt4 import QtGui
from functools import partial
from PyQt4.QtGui import (
    QWidget, QGridLayout,
    QPushButton, QLabel,
    QVBoxLayout, QHBoxLayout, QSpacerItem)
from PyQt4.QtCore import QDate, pyqtSignal


def format_month(date):
        return '%s, %s' % (QDate.shortMonthName(date.month()), date.year())


def clean_layout(layout):

    while layout.count():
        i = layout.takeAt(0)
        i.widget().setParent(None)


class UpperSelectorWidget(QWidget):

    zoom_in = pyqtSignal(object)
    selection = pyqtSignal(object, int)

    def __init__(self, text, *args, **kwargs):
        super(UpperSelectorWidget, self).__init__(*args, **kwargs)

        vbox = QVBoxLayout(self)
        widget = QWidget(self)

        hbox = QHBoxLayout(widget)

        left_button = QPushButton()
        left_button.setText('<')
        left_button.clicked.connect(self.send_previous_signal)

        right_button = QPushButton()
        right_button.setText('>')
        right_button.clicked.connect(self.send_next_signal)

        label = QPushButton()
        label.setText(text)
        label.clicked.connect(self.send_zoom_in_signal)
        self.main_label = label

        hbox.addWidget(left_button)
        hbox.addWidget(label)
        hbox.addWidget(right_button)

        # Spacer1 = QSpacerItem(
        #     0, 50, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        # vbox.addItem(Spacer1)

        vbox.addWidget(widget)
        # Spacer2 = QSpacerItem(
        #     0, 30, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        # vbox.addItem(Spacer2)

    def setText(self, text):
        self.main_label.setText(text)

    def send_zoom_in_signal(self):
        self.zoom_in.emit(self)

    def send_previous_signal(self):
        self.selection.emit(self, -1)

    def send_next_signal(self):
        self.selection.emit(self, 1)


class PickerButton(QPushButton):

    button_stylesheet = '''
    border: 1px solid #333333;
    '''

    highlighted_stylesheet = '''
    background-color: #F5BC40;
    border: 1px solid #44447f;
    '''

    alternate_stylesheet = '''
    color: #444444;
    border: 1px solid #222222;
    '''

    disabled_stylesheet = '''
    color: #aa4444;
    border: 1px solid #444444;
    '''

    hover_stylesheet = '''
    background-color:#45b545;
    border: 1px solid #222222;
    '''

    today_styleSheet = '''
    color: #E75232;
    border: 1px solid #E75232;
    '''

    doubleClicked = pyqtSignal()

    def __init__(self, *args, **kwargs):
        super(PickerButton, self).__init__(*args, **kwargs)
        self.setStyleSheet(self.button_stylesheet)

    def set_alternate_style(self):
        self.setStyleSheet(self.alternate_stylesheet)

    def set_highlighted_style(self):
        self.setStyleSheet(self.highlighted_stylesheet)

    def set_disabled_style(self):
        self.setStyleSheet(self.disabled_stylesheet)

    def set_today_style(self):
        self.setStyleSheet(self.today_styleSheet)

    def mouseDoubleClickEvent(self, e):
        self.doubleClicked.emit()

    def enterEvent(self, event):
        self.old_stylesheet = self.styleSheet()
        self.setStyleSheet(self.hover_stylesheet)

    def leaveEvent(self, event):
        self.setStyleSheet(self.old_stylesheet)


class DayWidget(QWidget):

    zoom_in = pyqtSignal(object)
    date_picked = pyqtSignal(object, object)

    def send_zoom_in_signal(self):
        self.zoom_in.emit(self)

    def send_date_picked_signal(self, date):
        self.date_picked.emit(self, date)

    def __init__(self, starting_date, max_date, min_date,
                 months, *args, **kwargs):

        super(DayWidget, self).__init__(*args, **kwargs)

        self.max_date = max_date
        self.min_date = min_date
        self.selected_date = starting_date
        self.changeMonth = starting_date
        self.vbox = QVBoxLayout(self)
        self.create_interface(starting_date)

    def create_interface(self, starting_date):

        upper_area = self.create_upper_area(starting_date)
        self.vbox.addWidget(upper_area)
        grid = self.create_day_grid(starting_date, 6, 7)
        self.vbox.addWidget(grid)

    def create_upper_area(self, day):

        u = UpperSelectorWidget(day.toString())
        u.zoom_in.connect(self.send_zoom_in_signal)
        u.selection.connect(self.update_interface)
        return u

    def create_day_grid(self, starting_date, weeks, days):
        today = QDate.currentDate()

        day = QDate(starting_date.year(), starting_date.month(), 1)
        day = day.addDays(-day.dayOfWeek() + 1)

        month = starting_date.month()
        grid_widget = QWidget()
        layout = QGridLayout(grid_widget)
        for d in range(days):
            week_label = QLabel(QDate.shortDayName(d + 1))
            layout.addWidget(week_label, 0, d + 1)

        for w in range(weeks):

            week_label = QLabel(str(day.weekNumber()[0]))
            layout.addWidget(week_label, w + 1, 0)
            for d in range(days):

                b = PickerButton()
                b.setText(str(day.day()))
                if day.month() != month:
                    b.set_alternate_style()
                elif day == self.selected_date:
                    b.set_highlighted_style()

                if ((self.max_date is None or day <= self.max_date) and
                   (self.min_date is None or day >= self.min_date)):
                    b.clicked.connect(partial(self.send_date_picked_signal,
                                              day))
                else:
                    b.set_disabled_style()

                if day == today:
                    b.set_today_style()

                layout.addWidget(b, w + 1, d + 1)

                day = day.addDays(1)

        return grid_widget

    def update_interface(self, selector, dir):
        self.changeMonth = self.changeMonth.addMonths(dir)
        selector.setText(self.changeMonth.toString())
        self.vbox.takeAt(1).widget().setParent(None)
        self.vbox.addWidget(self.create_day_grid(self.changeMonth, 6, 7))


class MonthWidget(QWidget):

    zoom_in = pyqtSignal(object)
    date_picked = pyqtSignal(object, object)

    def send_zoom_in_signal(self):
        self.zoom_in.emit(self)

    def send_date_picked_signal(self, date):
        self.date_picked.emit(self, date)

    def __init__(self, starting_date, max_date, min_date,
                 months, *args, **kwargs):

        super(MonthWidget, self).__init__(*args, **kwargs)

        self.max_date = max_date
        self.min_date = min_date
        if months is None:
            months = range(1, 13)
        self.months = months
        selected_date = QDate(starting_date.year(),
                              starting_date.month(), 1)
        self.selected_date = selected_date
        self.changeYear = selected_date
        self.vbox = QVBoxLayout(self)
        self.create_interface(selected_date)

    def create_interface(self, starting_date):
        year = starting_date.year()
        self.vbox.addWidget(self.create_upper_area(year))
        grid = self.create_grid(year)
        self.vbox.addWidget(grid)

    def create_upper_area(self, year):
        u = UpperSelectorWidget(format_month(self.selected_date))
        u.zoom_in.connect(self.send_zoom_in_signal)
        u.selection.connect(self.update_interface)
        return u

    def create_grid(self, year):
        today = QDate.currentDate()
        grid_widget = QWidget()
        layout = QGridLayout(grid_widget)
        date = QDate(year, 1, 1)

        for w in range(4):
            for d in range(3):

                b = PickerButton()
                b.setText(QDate.shortMonthName(date.month()))
                if date == self.selected_date:
                    b.set_highlighted_style()
                if ((self.max_date is None or date <= self.max_date) and
                   (self.min_date is None
                    or
                    (date.year() > self.min_date.year()
                        or (date.year() == self.min_date.year()
                            and date.month() >= self.min_date.month()))) and
                        date.month() in self.months):
                    b.clicked.connect(partial(self.send_date_picked_signal,
                                              date))
                else:
                    b.set_disabled_style()
                if (date.month() == today.month()
                   and date.year() == today.year()):
                    b.set_today_style()

                layout.addWidget(b, w, d)

                date = date.addMonths(1)
        return grid_widget

    def update_interface(self, selector, dir):
        self.changeYear = self.changeYear.addYears(dir)
        selector.setText(format_month(self.changeYear))
        self.vbox.takeAt(1).widget().setParent(None)
        self.vbox.addWidget(self.create_grid(self.changeYear.year()))

    def setMonths(self, months):
        self.months = months


class YearWidget(QWidget):

    zoom_in = pyqtSignal(object)
    date_picked = pyqtSignal(object, object)

    def send_zoom_in_signal(self):
        self.zoom_in.emit(self)

    def send_date_picked_signal(self, date):
        self.date_picked.emit(self, date)

    def __init__(self, starting_date, max_date, min_date,
                 months, *args, **kwargs):
        super(YearWidget, self).__init__(*args, **kwargs)
        self.max_date = max_date
        self.min_date = min_date
        selected_date = QDate(starting_date.year(), 1, 1)
        self.selected_date = selected_date
        self.vbox = QVBoxLayout(self)
        self.create_interface(selected_date)

    def create_interface(self, starting_date):
        year = starting_date.year()
        self.vbox.addWidget(self.create_upper_area(year))
        grid = self.create_grid(year)
        self.vbox.addWidget(grid)

    def create_upper_area(self, year):
        start = int(year / 10) * 10
        end = start + 9
        u = UpperSelectorWidget('(%s) %s-%s' % (year, start, end))
        u.selection.connect(self.update_interface)
        return u

    def create_grid(self, year):
        today = QDate.currentDate()
        grid_widget = QWidget()
        layout = QGridLayout(grid_widget)
        start = int(year / 10) * 10
        end = start + 9
        date = QDate(start - 2, 1, 1)
        for w in range(4):
            for d in range(4):

                b = PickerButton()

                b.setText(str(date.year()))
                if date.year() < start or date.year() > end:
                    b.set_alternate_style()

                if ((self.max_date is None or date <= self.max_date) and
                   ((self.min_date is None) or
                        (date.year() >= self.min_date.year()))):
                    b.clicked.connect(partial(self.send_date_picked_signal,
                                              date))
                else:
                    b.set_disabled_style()
                if (date.year() == today.year()):
                    b.set_today_style()

                layout.addWidget(b, w, d)

                date = date.addYears(1)
        return grid_widget

    def update_interface(self, selector, dir):
        selected_year = self.selected_date.year()
        decade = int(selected_year / 10)
        year = (decade + dir) * 10
        self.selected_date = QDate(year, 1, 1)
        selector.setText('(%s) %s-%s' % (selected_year, year, year + 9))
        self.vbox.takeAt(1).widget().setParent(None)
        self.vbox.addWidget(self.create_grid(year))


class Calendar(QWidget):
    YEAR_CALENDAR = 2
    MONTH_CALENDAR = 1
    DAY_CALENDAR = 0

    WIDGETS = {
        YEAR_CALENDAR: YearWidget,
        MONTH_CALENDAR: MonthWidget,
        DAY_CALENDAR: DayWidget
    }

    DATE_FORMATTERS = [
        lambda d: d.toString(),
        lambda d: format_month(d),
        lambda d: str(d.year())
    ]

    selection_finished = pyqtSignal(object)

    def __init__(self, starting_date, widget_type,
                 *args, **kwargs):
        super(Calendar, self).__init__(*args, **kwargs)
        self.max_date = None
        self.min_date = None
        self.months = None
        self.widget_type = widget_type
        self.selected_date = starting_date
        self.starting_date = starting_date
        self.vbox = QVBoxLayout(self)
        self.create_ui(widget_type, starting_date)

    def create_ui(self, widget_type, date):
        self.current_level = widget_type
        date_label = PickerButton(self)
        date_label.setText(self.DATE_FORMATTERS[widget_type](
            self.starting_date))
        date_label.clicked.connect(self.reset_ui)
        date_label.doubleClicked.connect(self.select_starting_date)
        self.vbox.addWidget(date_label)

        picker_class = self.WIDGETS[widget_type]
        picker = picker_class(date, self.max_date,
                              self.min_date, self.months, self)
        picker.zoom_in.connect(self.on_picker_zoom_in)
        picker.date_picked.connect(self.on_date_picked)
        self.vbox.addWidget(picker)

    def select_starting_date(self):
        self.selected_date = self.starting_date
        self.selection_finished.emit(self.starting_date)

    def reset_ui(self):
        clean_layout(self.layout())
        self.create_ui(self.widget_type, self.starting_date)

    def on_date_picked(self, picker, date):
        level = self.current_level
        if level == self.widget_type:
            self.selected_date = date
            #
            clean_layout(self.layout())
            self.create_ui(self.widget_type, date)
            #
            self.selection_finished.emit(date)
        else:
            clean_layout(self.layout())
            if isinstance(picker, YearWidget):
                date = QDate(date.year(),
                             self.selected_date.month(),
                             self.selected_date.day())

            elif isinstance(picker, MonthWidget):
                date = QDate(date.year(),
                             date.month(),
                             self.selected_date.day())

            self.create_ui(level - 1, date)

    def on_picker_zoom_in(self, picker):
        clean_layout(self.layout())
        self.create_ui(self.current_level + 1, picker.selected_date)

    def setSelectedDate(self, date):
        self.selected_date = date
        self.starting_date = date
        self.reset_ui()

    def setMaxDate(self, date):
        self.max_date = date
        self.reset_ui()

    def setMinDate(self, date):
        self.min_date = date
        self.reset_ui()

    def setMonths(self, months):
        self.months = months
        self.reset_ui()


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    def print_date(date):
        print date.toString()

    app = QApplication(sys.argv)

    w = QWidget()

    date = QDate.currentDate()
    calendar = Calendar(date, Calendar.DAY_CALENDAR, w)
    calendar.setMaxDate(date.addMonths(2))
    calendar.selection_finished.connect(print_date)

    w.resize(400, 300)
    w.show()
    sys.exit(app.exec_())
