# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides a widget to select dates (day, month or year)

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
NOTE:
To see an example on how to use the Calendar class go to the end of the file
"""
from PyQt4 import QtGui, QtCore
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from functools import partial
from PyQt4.QtGui import (
    QWidget, QGridLayout, QLineEdit,
    QPushButton, QLabel, QTableWidget,
    QVBoxLayout, QHBoxLayout, QSpacerItem,
    QListWidgetItem, QDialog, QListWidget,
    QTableWidgetItem)
from PyQt4.QtCore import QDate, pyqtSignal
from sysgrove.ui.classes import UIObject
from sysgrove.ui.classes import FocusStyledLineEdit
from sysgrove.ui.classes.mixins.dialog_mixin import QDialogMixin
from sysgrove import i18n
_ = i18n.language.ugettext

focus_out = """
    background-color:#ffffff;
    color: #000000;
    border: 1px solid #aaaaaa;
"""


class MultiSelectPopup(QDialog, QDialogMixin):
    def __init__(self, parent, name, lc, codeInDescription, *args, **kwargs):
        super(MultiSelectPopup, self).__init__(None)
        self.actu = True
        self.parent = parent
        self.codeInDescription = codeInDescription
        # Page layout
        self.outList = []
        layoutV = QVBoxLayout(self)

        topWidget = QWidget(self)
        self.word = ''
        self.lineSearch = QLineEdit(topWidget)
        self.lineSearch.textChanged.connect(self.setFilterSearch)
        self.lineSearch.setFixedWidth(185)
        labelSearch = QLabel("Quick Search ", self)
        layoutHTop = QHBoxLayout(topWidget)
        spacer = QSpacerItem(1, 1, QtGui.QSizePolicy.Expanding,
                             QtGui.QSizePolicy.Minimum)
        layoutHTop.addWidget(labelSearch)
        layoutHTop.addWidget(self.lineSearch)
        layoutHTop.addSpacerItem(spacer)
        layoutV.addWidget(topWidget)

        widgetSort = QWidget(self)
        self.layoutHSort = QHBoxLayout(widgetSort)
        self.iconDown = QtGui.QIcon()
        self.iconDown.addPixmap(QtGui.QPixmap(':fi-arrow-down.svg'))

        buttonsortCode = QPushButton("", widgetSort)
        buttonsortCode.setIcon(self.iconDown)
        buttonsortCode.clicked.connect(partial(self.sort, "code"))
        self.layoutHSort.addWidget(buttonsortCode)
        if self.codeInDescription:
            buttonsortDescription = QPushButton(_("sort description"), widgetSort)
            buttonsortDescription.setIcon(self.iconDown)
            buttonsortDescription.clicked.connect(
                partial(self.sort, "description"))
            self.layoutHSort.addWidget(buttonsortDescription)
        self.layoutHSort.addSpacerItem(spacer)
        layoutV.addWidget(widgetSort)
        self.sortTxt = "code"
        mainWidget = QWidget(self)
        layoutV.addWidget(mainWidget)
        layoutH = QHBoxLayout(mainWidget)

        self.firstList = QListWidget(mainWidget)
        self.firstList.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        layoutH.addWidget(self.firstList)

        widgetButton = QWidget(mainWidget)
        buttonIn = QPushButton(">>", widgetButton)
        buttonIn.clicked.connect(self.addItem)

        buttonOut = QPushButton("<<", widgetButton)
        buttonOut.clicked.connect(self.removeItem)
        layoutButton = QVBoxLayout(widgetButton)
        layoutButton.addWidget(buttonIn)
        layoutButton.addWidget(buttonOut)
        layoutH.addWidget(widgetButton)

        self.secondList = QListWidget(mainWidget)
        self.secondList.setSelectionMode(
            QtGui.QAbstractItemView.MultiSelection)
        layoutH.addWidget(self.secondList)

        bottomWidget = QWidget(self)
        layoutBottomH = QHBoxLayout(bottomWidget)

        self.StyleSheetButton = """
            QWidget{
                border: none;
            }
            QToolButton{
            border: 2px solid #8f8f91;
                border-radius: 6px;
                font-weight: bold;
                font-size: 10px;
                background-color: qlineargradient(
            x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde);
             }
            QToolButton:hover{
            /*border: 2px solid #8f8f91;*/
            border: 2px solid #F5BC40;
                border-radius: 6px;
                font-weight: bold;
                font-size: 10px;
                background-color: qlineargradient(
            x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dadbde, stop: 1 #f6f7fa);
            }"""

        self.iconOk = QtGui.QIcon()
        self.iconOk.addPixmap(QtGui.QPixmap(':dialog-ok.svg'))
        self.iconCancel = QtGui.QIcon()
        self.iconCancel.addPixmap(QtGui.QPixmap(':edit-clear.svg'))
        buttonCancel = QtGui.QToolButton()
        buttonCancel.setText(_("Cancel"))
        buttonCancel.clicked.connect(self.close)
        buttonCancel.setIcon(self.iconCancel)
        buttonCancel.setIconSize(QtCore.QSize(30, 30))
        buttonCancel.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        buttonCancel.setStyleSheet(self.StyleSheetButton)

        buttonOk = QtGui.QToolButton()
        buttonOk.setText("   Ok   ")
        buttonOk.clicked.connect(self.accept)
        buttonOk.setIcon(self.iconOk)
        buttonOk.setIconSize(QtCore.QSize(30, 30))
        buttonOk.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        buttonOk.setStyleSheet(self.StyleSheetButton)

        layoutBottomH.addWidget(buttonOk)
        layoutBottomH.addWidget(buttonCancel)
        layoutV.addWidget(bottomWidget)

        self.secondList.setSortingEnabled(True)
        self.changeLc(lc)

    def changeLc(self, lc):
        self.firstList.clear()
        self.secondList.clear()
        self.lc = lc
        lcChoose = []
        self.parent.setVal(lcChoose)
        self.sort("code")

    def addItem(self):
        lcSelected = self.firstList.selectedItems()
        for l in lcSelected:
            self.secondList.addItem(
                self.firstList.takeItem(self.firstList.row(l)))
        self.sort(self.sortTxt)

    def removeItem(self):
        lcSelected = self.secondList.selectedItems()
        for l in lcSelected:
            self.firstList.addItem(
                self.secondList.takeItem(self.secondList.row(l)))
        self.sort(self.sortTxt)

    def accept(self):
        self.outList = []
        for l in range(self.secondList.count()):
            self.outList.append(
                [self.secondList.item(l).data(QtCore.Qt.UserRole).toInt()[0],
                 self.secondList.item(l).text()])
        self.parent.setVal(self.outList)
        self.close()

    def setFilterSearch(self, word):
        self.word = word
        self.fillFirstList()

    def sort(self, text):
        def comp(v1, v2):
            if text == "description":
                self.sortTxt = "description"
                description1 = v1[1].split("-")[1]
                description2 = v2[1].split("-")[1]
            else:
                self.sortTxt == "code"
                description1 = v1[1]
                description2 = v2[1]
            if description1 < description2:
                return -1
            elif description1 > description2:
                return 1
            else:
                return 0
        self.lc.sort(cmp=comp)
        self.fillFirstList()

    def fillFirstList(self):
        self.firstList.clear()
        for l in self.lc:
            itemString = QtCore.QString(l[1])
            if itemString.contains(self.word, 0):
                item = QListWidgetItem()
                item.setData(QtCore.Qt.UserRole, l[0])
                item.setData(QtCore.Qt.DisplayRole, l[1])
                if(self.secondList.findItems(
                   l[1], QtCore.Qt.MatchExactly) == []):
                    self.firstList.addItem(item)


class MultiSelect(UIObject):

    def __init__(self, parent_ui_component, name, lc, codeInDescription):
        super(MultiSelect, self).__init__(parent_ui_component, name)
        self.lc = lc
        self.lcChoose = []
        self.name = name
        self.codeInDescription = codeInDescription
        self.qtObject = QtGui.QWidget(parent_ui_component.qtObject)
        self.qtObject.setStyleSheet("border:None;")
        self.qtObject.setMaximumHeight(32)
        self.qtObject.setMinimumWidth(130)
        self.qtObject.setMaximumWidth(130)
        self.qtObjectLayout = QtGui.QHBoxLayout(self.qtObject)
        self.qtObjectLayout.setSpacing(0)
        self.qtObjectLayout.setMargin(0)

        self.lineEdit = FocusStyledLineEdit("""
                                            background-color:#ddddff;
                                            color: #000000;
                                            border: 1px solid #aaaaaa;
                                            """,
                                            focus_out,
                                            parent_ui_component.qtObject)

        self.lineEdit.setMinimumHeight(20)
        self.lineEdit.setMaximumHeight(20)
        self.lineEdit.setMinimumWidth(100)
        self.lineEdit.setMaximumWidth(100)
        self.lineEdit.setStyleSheet("border: 1px solid #aaaaaa;")
        self.lineEdit.setEnabled(False)

        self.qtObjectLayout.addWidget(self.lineEdit)

        self.qtButton = QtGui.QToolButton(self.qtObject)
        self.qtButton.setStyleSheet("""
            QToolButton{
                background-color: transparent;
            }

            QToolButton:hover{
                background-color: transparent;
                border: 1px solid #F5BC40;
            }""")

        self.qtButton.setText("")

        self.icon = QtGui.QIcon()
        self.icon.addPixmap(QtGui.QPixmap(_fromUtf8(':lookup.svg')),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)

        self.qtButton.setIcon(self.icon)
        self.qtButton.setIconSize(QtCore.QSize(20, 20))
        self.qtButton.setToolTip("'+Search in list+'")
        self.qtButton.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.qtObjectLayout.addWidget(self.qtButton)

        self.qtButton.clicked.connect(self.open_popup)

        self.popup = MultiSelectPopup(
            self, name+"popup", self.lc, self.codeInDescription)
        self.popup.setWindowTitle(name)

    def open_popup(self):
        self.popup.show()

    def setVal(self, lcChoose):
        self.lcChoose = lcChoose
        text = ""
        self.lineEdit.clear()
        for l in lcChoose:
            code = l[1].split(" -")[0]
            text += code+";"
        self.lineEdit.setText(text)


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    app = QApplication(sys.argv)

    w = QWidget()

    lc = [[1, "un"], [2, "deux"], [5, "cinq"], [10, "dix"], [11, "onze"]]
    popup = MultiSelectPopup(w, "selectMulti", lc)
    popup.show()

    #w.resize(400, 300)
    #w.show()
    sys.exit(app.exec_())
