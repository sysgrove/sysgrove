# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides a widget to select dates (day, month or year)

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

"""
from PyQt4 import QtGui, QtCore


class BoolCkeckWidget(QtGui.QWidget):

    """ This widget aims to represent a boolean value, possibly not set
        we use the Qt constant Qt.CheckState with the following convention :
        - Checked : True value
        - Unchecked : no value has been set (None)
        - PartiallyChecked : False value
    """

    def __init__(self, tlabel, flabel, *args, **kwargs):
        super(BoolCkeckWidget, self).__init__(*args, **kwargs)
        vbox = QtGui.QHBoxLayout(self)
        self.tcheck = QtGui.QCheckBox()
        self.tcheck.setText(tlabel)

        self.fcheck = QtGui.QCheckBox()
        self.fcheck.setText(flabel)
        vbox.addWidget(self.tcheck)
        vbox.addWidget(self.fcheck)

        self.tcheck.stateChanged.connect(self.tchanged)
        self.fcheck.stateChanged.connect(self.fchanged)

    def tchanged(self, state):
        if state == QtCore.Qt.Checked:
            if self.fcheck.checkState() == state:
                self.fcheck.setCheckState(QtCore.Qt.Unchecked)

    def fchanged(self, state):
        if state == QtCore.Qt.Checked:
            if self.tcheck.checkState() == state:
                self.tcheck.setCheckState(QtCore.Qt.Unchecked)

    def checkState(self):
        if self.tcheck.checkState() == QtCore.Qt.Checked:
            return QtCore.Qt.Checked
        if self.fcheck.checkState() == QtCore.Qt.Checked:
            return QtCore.PartiallyChecked
        return QtCore.Qt.Unchecked

    def setCheckState(self, state):
        if state == QtCore.Qt.Checked:
            self.tcheck.setCheckState(self, state)
        if state == QtCore.Qt.PartiallyChecked:
            self.fcheck.setCheckState(self, QtCore.Qt.Checked)
        if state == QtCore.Qt.Unchecked:
            self.fcheck.setCheckState(self, QtCore.Qt.Unchecked)
            self.tcheck.setCheckState(self, QtCore.Qt.Unchecked)

    def isChecked(self):
        if self.tcheck.isChecked():
            return True
        if self.fcheck.isChecked():
            return False
        return None


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    app = QApplication(sys.argv)

    w = QtGui.QWidget()
    l = QtGui.QVBoxLayout(w)
    test = BoolCkeckWidget('ok', 'not ok', w)
    l.addWidget(test)

    w.resize(400, 300)
    w.show()
    sys.exit(app.exec_())
