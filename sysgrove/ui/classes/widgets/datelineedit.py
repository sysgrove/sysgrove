#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import re
from PyQt4 import QtCore, QtGui


class DateLineEdit(QtGui.QLineEdit):

    def __init__(self, parent=None):
        super(DateLineEdit, self).__init__(parent)
        self.setNormalStyle()
        self.setMaxLength(10)
        # self.val = '//'
        # self.setText(self.val)
        # self.val = '//'
        self.setText('//')
        self.value = None
        self.home(True)
        self.deselect()
        self.textChanged.connect(self.validate)
        self.max_date = None

    def setMaxDate(self, date):
        self.max_date = date

    def setNormalStyle(self):
        self.setStyleSheet("border: 1px solid #aaaaaa;")

    def setInvalidStyle(self):
        self.setStyleSheet("""
                background-color: #ffdddd;
                border: 1px solid red;
                """)

    def focusInEevent(self, event):
        super(DateLineEdit, self).focusInEvent(event)
        self.home(True)

    def keyPressEvent(self, event):
        cur = self.cursorPosition()
        value = self.text()
        key = event.key()
        if self.hasSelectedText() and\
            (key == QtCore.Qt.Key_Delete or
             key == QtCore.Qt.Key_Backspace):

            selection = self.selectedText()
            slashes = selection.count('/')
            self.setText(value.replace(selection, '/' * slashes))
        elif key == QtCore.Qt.Key_Delete:
            while value[cur:cur + 1] == '/':
                cur += 1
            value = value[0:cur] + value[cur + 1:]
            self.setText(value)
            self.setCursorPosition(cur)
        elif key == QtCore.Qt.Key_Backspace:
            cur -= 1
            while cur >= 0 and value[cur] == '/':
                cur -= 1

            if cur < 0:
                return
            value = value[0:cur] + value[cur + 1:]
            self.setText(value)
            self.setCursorPosition(cur)
        elif key >= QtCore.Qt.Key_0 and key <= QtCore.Qt.Key_9 or\
                key == QtCore.Qt.Key_Left or \
                key == QtCore.Qt.Key_Right or \
                key == QtCore.Qt.Key_Tab:

            super(DateLineEdit, self).keyPressEvent(event)

    def validate(self):
        # This statement prevents from unicode input
        try:
            s = str(self.text())
        except:
            self.setInvalidStyle()
            return

        if len(s) > 10 or s == '//':
            return

        d = QtCore.QDate.fromString(s, "dd/MM/yyyy")
        if not d.isValid() or (self.max_date and d.toPyDate() > self.max_date):
            self.value = None
            self.setInvalidStyle()
            return

        self.value = d
        self.setNormalStyle()
        self.check_position()

    def check_position(self):
        s = self.text()
        slices = [str(sl) for sl in s.split('/')]
        cur = self.cursorPosition()

        length = [len(sl) for sl in slices]
        if cur <= length[0]:  # editing first block
            if re.match(r'[4-9]+', slices[0]):
                s = '0' + s
                cur += 2
            elif length[0] == 2:
                cur = 3

        if cur <= length[0] + 1 + length[1]:  # editing second block
            if re.match(r'[2-9]+', slices[1]):
                slices[1] = '0' + slices[1]
                cur += 2
                s = '/'.join(slices)
            elif length[1] == 2:
                cur += 1

        if s != self.text():
            self.setText(s)

        self.setCursorPosition(cur)


class MonthLineEdit(DateLineEdit):

    def __init__(self, parent=None):
        super(MonthLineEdit, self).__init__(parent)
        self.setNormalStyle()
        self.setMaxLength(7)
        self.setText('mm/yyyy')
        self.value = None
        self.home(True)
        self.deselect()
        self.textChanged.connect(self.validate)
        self.max_date = None

    def setMaxDate(self, date):
        self.max_date = date

    def setNormalStyle(self):
        self.setStyleSheet("border: 1px solid #aaaaaa;")

    def setInvalidStyle(self):
        self.setStyleSheet("""
                background-color: #ffdddd;
                border: 1px solid red;
                """)

    def validate(self):
        # This statement prevents from unicode input
        try:
            s = str(self.text())
        except:
            self.setInvalidStyle()
            return

        if len(s) > 7 or s == 'mm/yyyy':
            return

        d = QtCore.QDate.fromString('01/' + s, "dd/MM/yyyy")
        if not d.isValid() or (self.max_date and d.toPyDate() > self.max_date):
            self.value = None
            self.setInvalidStyle()
            return

        self.value = d
        self.setNormalStyle()
        self.check_position()

    def check_position(self):
        s = self.text()
        slices = [str(sl) for sl in s.split('/')]
        cur = self.cursorPosition()

        length = [len(sl) for sl in slices]
        if cur <= length[0]:  # editing month block
            if re.match(r'[2-9]+', slices[0]):
                s = '0' + s
                cur += 2
            elif length[0] == 2:
                cur = 3

        if s != self.text():
            self.setText(s)

        self.setCursorPosition(cur)


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    def print_date(date):
        print date.toString()

    app = QApplication(sys.argv)

    w = QtGui.QWidget()
    l = QtGui.QVBoxLayout(w)

    date = QtCore.QDate.currentDate()
    lineedit = DateLineEdit()
    l.addWidget(lineedit)
    lineedit.setMaxDate(date.addDays(2))

    monthedit = MonthLineEdit()
    l.addWidget(monthedit)
    monthedit.setMaxDate(date.addMonths(2))

    w.resize(400, 300)
    w.show()
    sys.exit(app.exec_())
