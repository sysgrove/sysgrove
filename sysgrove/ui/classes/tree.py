#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sqlalchemy.orm.exc import NoResultFound
from sysgrove.utils import retreive_class
from .uiobject import UIObject


class TreeWidget(QtGui.QTreeWidget):

    itemChecked = QtCore.pyqtSignal(object, int)

    def __init__(self, parent):
        QtGui.QTreeWidget.__init__(self, parent)
        self.itemChecked.connect(self.handleItemChecked)
        self.itemExpanded.connect(self.onItemExpanded)
        # FIXME: useless PyQt provides all necessary method
        # see also QtGui.QTreeWidgetItemIterator
        self.childs = []

    def onItemExpanded(self, item):
        if isinstance(item.parent, TreeWidget) \
                or isinstance(item.parent, UITree):
            for i in item.parent.childs:
                if i != item:
                    self.collapseItem(i)
                else:
                    for j in i.childs:
                        self.collapseItem(j)
        if isinstance(item.parent, TreeWidgetItem):
            for i in item.parent.childs:
                if i != item:
                    self.collapseItem(i)

    def handleItemChecked(self, item, column):
        if item.checkState(column) == QtCore.Qt.Checked:
            toCheck = True
            for n in item.parent.childs:
                toCheck = toCheck and n.checkState(0) == QtCore.Qt.Checked
            if toCheck:
                item.checkParent()
            else:
                item.partiallyCheckParent()
            item.checkChilds()

        elif item.checkState(column) == QtCore.Qt.Unchecked:
            Uncheck = True
            for n in item.parent.childs:
                Uncheck = Uncheck and n.checkState(0) == QtCore.Qt.Unchecked
            if Uncheck:
                item.uncheckParent()
            else:
                item.partiallyCheckParent()
            item.uncheckChilds()


class UITree(UIObject):

    def __init__(self, parent, name):
        super(UITree, self).__init__(parent, name)
        if hasattr(parent, 'qtObject'):
            self.qtObject = TreeWidget(parent.qtObject)
        elif isinstance(parent, QtGui.QLayout):
            self.qtObject = TreeWidget(None)
            parent.addWidget(self.qtObject)
        else:
            self.qtObject = TreeWidget(None)
        self.style()
        self.code = None

    def setTitle(self, title):
        self.qtObject.setHeaderLabel(title)

    def clear(self):
        def clearChild(child2):
            child2.setCheckState(0, QtCore.Qt.Unchecked)
            for child in child2.childs:
                clearChild(child)

        for menu in self.childs:
            clearChild(menu)

    def setVal(self, val):
        self.clear()
        val_code = map(lambda x: x.code, val)

        def setValChild(child2, code):
            for children in child2.childs:
                if children.childs == []:
                    if children.code in code:
                        children.setCheckState(0, QtCore.Qt.Checked)
                else:
                    setValChild(children, code)
        setValChild(self, val_code)

    def valueObject(self):
        res = []
        for children in self.childs:
            if children.childs == [] and \
                    children.checkState(0) == QtCore.Qt.Checked:
                code = children.code
                try:
                    val = self.klass.query.filter_by(code=code).one()
                    res.append(val)
                except NoResultFound:
                    pass
            else:
                res.extend(children.value(self.klass))
        return res

    def value(self):
        return map(lambda x: x.code, self.valueObject())

    def setLink(self, klass, prefix, slave=None,
                masters=[], filters={}, order={}):
        if slave:
            self.klass = retreive_class(slave[1], klass)
        else:
            self.klass = klass

    # def setReadOnly(self, ro):
    #     self.qtObject.setReadOnly(ro)
    #     for child in self.childs:
    #         child.setReadOnly(ro)


class TreeWidgetItem(QtGui.QTreeWidgetItem):

    def __init__(self, parent, name='', code=''):
        if hasattr(parent, 'qtObject'):
            QtGui.QTreeWidgetItem.__init__(self, parent.qtObject)
        else:
            QtGui.QTreeWidgetItem.__init__(self, parent)
        self.childs = []
        self.parent = parent
        self.code = code
        self.setText(0, name)
        self.parent.childs.append(self)

    def setData(self, column, role, value):
        state = self.checkState(column)
        QtGui.QTreeWidgetItem.setData(self, column, role, value)
        if(role == QtCore.Qt.CheckStateRole and
           state != self.checkState(column)):
            treewidget = self.treeWidget()
            if treewidget is not None:
                treewidget.itemChecked.emit(self, column)

    def checkChilds(self):
        for child in self.childs:
            child.setCheckState(0, QtCore.Qt.Checked)

    def uncheckChilds(self):
        for child in self.childs:
            child.setCheckState(0, QtCore.Qt.Unchecked)

    def partiallyCheckParent(self):
        if isinstance(self.parent, TreeWidgetItem):
            self.parent.setCheckState(0, QtCore.Qt.PartiallyChecked)
            self.parent.partiallyCheckParent()

    def checkParent(self):
        if isinstance(self.parent, TreeWidgetItem):
            self.parent.setCheckState(0, QtCore.Qt.Checked)

    def uncheckParent(self):
        if isinstance(self.parent, TreeWidgetItem):
            self.parent.setCheckState(0, QtCore.Qt.Unchecked)

    def value(self, klass):
        res = []
        for children in self.childs:
            if children.childs == [] and \
                    children.checkState(0) == QtCore.Qt.Checked:
                code = children.code
                try:
                    val = klass.query.filter_by(code=code).one()
                    res.append(val)
                except NoResultFound:
                    pass
            else:
                res.extend(children.value(klass))
        return res

    def setCheckBox(self, useless=None):
        self.setFlags(
            self.flags() | QtCore.Qt.ItemIsUserCheckable)
        self.setCheckState(0, QtCore.Qt.Unchecked)
