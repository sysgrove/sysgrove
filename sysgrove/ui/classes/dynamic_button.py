#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from .uiobject import UIObject
from .widget import UIWidget

log = logging.getLogger('sysgrove')


class UIDynamicButtonZoneInst(UIObject):

    """ Represents a dynamicbutton zone. Each screen has its own """

    def __init__(self, parent, name):
        """ UIDynamicButtonZone Constructor """

        super(UIDynamicButtonZoneInst, self).__init__(parent, name)

        self.dynamic_buttons = {}
        self.dynamic_useless_widgets = {}
        self.qtObject = QtGui.QWidget(parent.qtObject)

        self.layout = QtGui.QGridLayout(self.qtObject)
        self.layout.setMargin(3)
        self.layout.setSpacing(2)

        parent.layout.addWidget(self.qtObject)

        for i in range(0, 9):
            self.dynamic_useless_widgets[i] = UIWidget(
                self, "uselessDynamic" + str(i))
            self.dynamic_useless_widgets[i].setMinimumSize(60, 60)
            self.dynamic_useless_widgets[i].setStyleSheet("border: none;")

            self.layout.addWidget(
                self.dynamic_useless_widgets[i].qtObject, i / 3, i % 3)

        self.hide()

    def setGrid(self):
        for i in range(0, 9):
            self.dynamic_useless_widgets[i] = UIWidget(
                self, "uselessDynamic" + str(i))
            self.dynamic_useless_widgets[i].setMinimumSize(60, 60)
            self.dynamic_useless_widgets[i].setStyleSheet("border: none;")

            self.layout.addWidget(
                self.dynamic_useless_widgets[i].qtObject, i / 3, i % 3)

    def addDynamicButton(self, name):

        # self.dynamic_useless_widgets[len(self.dynamic_buttons)]
        dynamic_button = UIDynamicButton(self, name)
        self.dynamic_buttons[dynamic_button.name] = dynamic_button
        return dynamic_button


class UIDynamicButton(UIObject):

    """ Represents one button of the dynamic button zone """

    def __init__(self, parent, name):
        super(UIDynamicButton, self).__init__(parent, name)

        self.qtObject = QtGui.QToolButton(parent.qtObject)
        self.qtObject.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        self.qtObject.setMinimumSize(QtCore.QSize(64, 64))
        self.qtObject.setMaximumSize(QtCore.QSize(64, 64))
        self.qtObject.setObjectName(name)
        # Just to get some text in the button
        self.qtObject.setText('')
        self.style()
        parent.layout.addWidget(
            self.qtObject,
            len(parent.dynamic_buttons) / 3,
            len(parent.dynamic_buttons) % 3)
        # sparent.layout.addWidget(self, 2, 2)

    def setText(self, text):
        self.qtObject.setText(text)

    def addImage(self, img):
        self.iconName = img
        self.icon = QtGui.QIcon()
        self.icon.addPixmap(QtGui.QPixmap(_fromUtf8(
            ':' + img)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtObject.setIcon(self.icon)
        self.qtObject.setIconSize(QtCore.QSize(36, 36))

    def addLink(self, context_code):
        from sysgrove.context import Context
        self.code = context_code
        self.setAction(
            "clicked()", lambda: Context.set_current_context(context_code))


class UIStaticButtonZoneClass(UIObject):

    def __init__(self, area, layout, name):
        self.__items__ = {}
        self.static_buttons = {}
        self.qtObject = area
        self.layout = layout

    def addStaticButton(self, name, code):
        static_button = UIStaticButtonClass(self, name, code)
        self.static_buttons[static_button.name] = static_button
        return static_button


class UIStaticButtonClass(UIObject):

    def __init__(self, parent, name, code):
        super(UIStaticButtonClass, self).__init__(parent, name)

        self.code = code
        self.iconName = ""

        self.qtObject = QtGui.QToolButton(parent.qtObject)
        self.qtObject.setObjectName(name)
        self.qtObject.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        self.qtObject.setMinimumSize(QtCore.QSize(64, 64))
        self.qtObject.setMaximumSize(QtCore.QSize(64, 64))
        self.qtObject.setText('')  # Just to get some text in the button
        self.qtObject.setProperty('class', 'StaticButton')
        self.style()

        parent.layout.addWidget(self.qtObject, 0, 2 - (
            len(parent.static_buttons) % 3))

    def addImage(self, img):
        self.iconName = img
        self.icon = QtGui.QIcon()
        self.icon.addPixmap(
            QtGui.QPixmap(_fromUtf8(':' + img)),
            QtGui.QIcon.Normal, QtGui.QIcon.Off
        )
        self.qtObject.setIcon(self.icon)
        self.qtObject.setIconSize(QtCore.QSize(36, 36))

    # def addLink(self, context_code):
    #     from sysgrove.context import kernel
    #     self.setAction(
    #         "clicked()", lambda: kernel.set_current_context(context_code))


class UIDynamicButtonZoneClass(UIObject):

    def __init__(self, area, layout, name):
        self.qtObject = area
        self.layout = layout
        self.name = name
        self.__items__ = {}
