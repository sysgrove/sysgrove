#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from .uiobject import UIObject
from .widget import UIWidget

log = logging.getLogger('sysgrove')


# don't inherit from UIObject, this makes a weird behavior with __items__
class UITabButtonClass(UIObject):

    def __init__(self, tabbar, layout, name,
                 assoc_context_num, caption, image=None):
        # FIXME :Call super() instead
        self.name = name

        self.context_num = assoc_context_num

        self.qtObject = QtGui.QToolButton(tabbar)
        self.qtObject.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)

        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.qtObject.sizePolicy().hasHeightForWidth())
        self.qtObject.setMinimumSize(QtCore.QSize(72, 72))
        self.qtObject.setMaximumSize(QtCore.QSize(72, 72))
        self.qtObject.resize(72, 72)
        self.qtObject.setStyleSheet(
            "border: 1px solid #000000; outline: none;")
        self.qtObject.setObjectName(name)
        # addWidget(self.qtObject)
        layout.addWidget(self.qtObject, 0, QtCore.Qt.AlignLeft)

        self.qtObject.setText(caption)

    def getContextNum(self):
        return self.context_num

    def setText(self, text):
        self.qtObject.setText(text)

    def marker(self):
        self.qtObject.setProperty("menuselected", "true")
        self.qtObject.update()
        self.style()

    def unmark(self):
        self.qtObject.setProperty("menuselected", "false")
        self.qtObject.update()
        self.style()

    def setAction(self, typeAction, func):
        self.qtObject.connect(self.qtObject, QtCore.SIGNAL(
            _fromUtf8(typeAction)), func)  # "clicked()"

    def delAction(self, typeAction, func):
        self.qtObject.disconnect(self.qtObject, QtCore.SIGNAL(
            _fromUtf8(typeAction)), func)  # "clicked()"

    def setIcon(self, iconName):
        if iconName:
            self.icon = QtGui.QIcon()
            self.icon.addPixmap(QtGui.QPixmap(_fromUtf8(
                ':' + iconName)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.qtObject.setIcon(self.icon)
            self.qtObject.setIconSize(QtCore.QSize(42, 42))


class UITabBarClass(UIObject):

    def __init__(self, tabbar_area, tabbar_area_layout, name, max_tabs):
        self.name = name

        self.qtObject = QtGui.QWidget(tabbar_area)

        self.qtObject.setStyleSheet("border: none;")

        self.qtObjectLayout = QtGui.QHBoxLayout(self.qtObject)
        self.qtObjectLayout.setDirection(QtGui.QBoxLayout.LeftToRight)
#        self.qtObjectLayout.setSpacing(0)
        self.qtObjectLayout.setMargin(0)
        self.spacer = QtGui.QSpacerItem(
            1, 1, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        tabbar_area_layout.addSpacerItem(self.spacer)
        tabbar_area_layout.addWidget(self.qtObject)

        self.how_many_tabs = 0
        self.max_tabs = max_tabs
        self.tabbar_area = tabbar_area
        self.tabbar_area_layout = tabbar_area_layout
        self.tabs = {}

    def getTabByContextNum(self, ct):
        return self.tabs[ct]

    def addTab(self, context_num):
        self.tabs[context_num] = UITabButtonClass(
            self.qtObject,
            self.qtObjectLayout,
            "tabbarButton" + str(self.how_many_tabs),
            context_num,
            "")
        return self.tabs[context_num]

    def removeTab(self):
        if self.how_many_tabs <= 0:
            raise Exception('UI_TabBarCannotCloseTab')

    # EPIC FIXME? MEMORY LEAKS ?
    def clear(self):
        for t in self.tabs:
            self.tabs[t].hide()
            self.tabs[t].deleteLater()
            self.tabs[t].destroy()
        self.tabs = {}


class TabPanel(QtGui.QTabWidget):

    def __init__(self, parent):
        super(QtGui.QTabWidget, self).__init__(parent)
    #     self.connect(self, QtCore.SIGNAL(
    #         "currentChanged(int)"), self.currentChangedSlot)

    # def currentChangedSlot(self):
    # log.debug("CHANGED TAB")
    # self.resize(200,200)
    # self.setStyleSheet("background-color: blue;")
    #     pass


class UITabPanel(UIObject):

    def __init__(self, parent_ui_component, name):
        super(UITabPanel, self).__init__(parent_ui_component, name)
        self.qtObject = TabPanel(parent_ui_component.qtObject)
        # self.qtObject.setDocumentMode(True)
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.qtObject.setSizePolicy(sizePolicy)
        self.tabs = {}
        self.style()
        # FIXME FIXME : this doesn't work, but the next line works
        # self.setStyleSheet("""
        #   """)

    def addTab(self, tab, Qstr):
        self.qtObject.addTab(tab.qtObject, "")
        self.tabs[tab.name] = tab
        tab.style()

    def setPosition(self, pos):
        if (pos == "N"):
            self.qtObject.tabPosition = "North"
        elif (pos == "S"):
            self.qtObject.tabPosition = "South"

    def setShape(self, shape):
        if (shape == "R"):
            self.qtObject.tabShape = "Rounded"
        elif (shape == "T"):
            self.qtObject.tabShape = "Traingular"


class UITabWidget(UIWidget):

    def __init__(self, parent, name):
        super(UITabWidget, self).__init__(parent, name)
        parent.addTab(self, name)

    def setTitle(self, x):
        self.parent.setTabText(self.parent.indexOf(self.qtObject), x)
