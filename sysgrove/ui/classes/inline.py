#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : contact@sysgrove.com

20/02/2013
Version 1.0

revision # 1
Date of revision: 25/08/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  cou can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from functools import partial
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove.utils import debug_trace, get_class
from .uiobject import UIObject
from .table import LinkedTable, build_columns
from .layout import UIVLayout
from .mixins.inline_mixin import InLineMixin
from .widgets.message_popup import QuestionPopup, QuestionChoice


def is_blank_row(row):
    res = True
    for item in row:
        res = res and str(item) == ''
    return res


class UIInline(InLineMixin, UIObject):

    def __init__(self, parent, name, klass=None,
                 with_table=True, scrolling=True):
        super(UIInline, self).__init__(parent, name)
        if hasattr(parent, 'qtObject'):
            self.qtObject = QtGui.QWidget(parent.qtObject)
        elif isinstance(parent, QtGui.QLayout):
            self.qtObject = QtGui.QWidget(parent)
            parent.addWidget(self.qtObject)
        else:
            self.qtObject = QtGui.QWidget(None)
        self.layout = UIVLayout(None, 'InLIneVLayout%s' % name)
        self.qtObject.setLayout(self.layout.qtObject)

        self.cur_index = None
        self.table = None
        self.ui_prefix = '_'
        self.with_table = with_table
        self.scrolling = scrolling

        if klass:
            self.setAlchemyClass(klass)
            self.buildUI()

    def setAlchemyClass(self, klass):
        # Cycling import
        from sysgrove.ui.classes import UIForm

        if isinstance(klass, str):
            self.klass = get_class(klass)
        else:
            self.klass = klass
        self.fields = UIForm.form2attr_list(self.klass.list_display)

    def clickOnTableLine(self, index):
        from sysgrove.core.screen import Screen
        self.cur_index = index.row()
        Screen.uiTable2ui(self.uiform,
                          self.table, index,
                          self.ui_prefix,
                          self.klass, self.fields)

    def doubleClickOnTableLine(self, index):
        # self.clickOnTableLine(index)
        pass

    def clearfields(self, uiform, fields):
        from sysgrove.core.screen import Screen
        Screen.reset_fields(uiform, fields, self.ui_prefix)

    def clear(self):
        self.table.clear()
        self.clearfields(self.uiform, self.fields)

    # FIXME : we should not use .save in the following function
    # add, update, delete, but how to retreive object then
    # without any 'pk'?
    def add(self, uiform, fields, blankLine=0):
        from sysgrove.core.screen import Screen, handle_valueError
        cur = self.klass()
        errors = Screen.ui2bdd(
            cur, uiform, fields, self.klass, self.ui_prefix)
        if not errors:
            values = self.valueObject()[:]
            values.append(cur)
            Screen.bdd2uiTable(self.klass, self.table, values)
            self.clearfields(uiform, fields)
            return cur
        else:
            handle_valueError(cur, errors, uiform,
                              self.ui_prefix)

    def update(self, uiform, fields):
        if isinstance(self.cur_index, int):
            from sysgrove.core.screen import Screen, handle_valueError
            values = self.valueObject()[:]
            cur = values[self.cur_index]
            errors = Screen.ui2bdd(cur,
                                   uiform,
                                   fields,
                                   self.klass,
                                   self.ui_prefix)
            if not errors:
                Screen.bdd2uiTable(self.klass, self.table, values)
                self.clearfields(uiform, fields)
            else:
                handle_valueError(cur, errors, uiform,
                                  self.ui_prefix)

    def delete(self):
        if isinstance(self.cur_index, int):
            from sysgrove.core.screen import Screen
            values = self.valueObject()[:]
            cur = values[self.cur_index]
            values.remove(cur)
            cur.cancel()
            # FIXME : cur has to be deleted
            # but when to main object is updated..
            # cur.delete()
            Screen.bdd2uiTable(self.klass, self.table, values)
            self.clearfields(self.uiform, self.fields)
            return cur
        self.clearfields(self.uiform, self.fields)

    def setVal(self, values):
        self.table.setVal(values)

    def valueObject(self):
        return self.table.valueObject()

    def setLink(self, klass, prefix, slave=None,
                masters=[], filters={}, order={}):
        pass

    # Required to avoid label for inline  in formular
    def setTitle(self, title):
        pass

    def buildTable(self):
        self.table = LinkedTable(self,
                                 "table_%s" % self.klass.__name__,
                                 columns=build_columns(self.klass),
                                 klass=self.klass)

        # Add action
        self.table.clicked.connect(self.clickOnTableLine)
        self.table.doubleClicked.connect(self.doubleClickOnTableLine)
        self.table.setMinimumHeight(100)

    def buildUIForm(self, parent, name='', form=None):
        # Cycling import ....
        from sysgrove.ui.classes import UIForm
        from sysgrove.core.screen import Screen
        uiform = UIForm(parent, 'UIForm%s%s' % (self.klass.__name__, name))
        uiform.setAlchemyClass(self.klass, self.ui_prefix)
        fields = self.fields
        if form:
            uiform.setDisplay(form)
            fields = UIForm.form2attr_list(form)
        uiform.attributes()
        for f in fields:
            Screen.addDataLookUpLink(
                f, uiform, fields, self.klass, self.ui_prefix)
        return(uiform, fields)

    def buildUI(self):
        # I have try to forget as much as I can UIObject
        # let's create the widget
        widget = QtGui.QWidget()
        layout = QtGui.QVBoxLayout(widget)

        # the form and the button
        hlayout = QtGui.QHBoxLayout()
        (self.uiform, self.fields) = self.buildUIForm(self)
        hlayout.addWidget(self.uiform.qtObject)
        # Build buttons
        buttonLayout = QtGui.QVBoxLayout()
        addButton = QtGui.QPushButton()
        # addButton.setText(u'Add')
        # addButton.setStyleSheet(CONFIG['css'])
        addButton.setIcon(QtGui.QIcon(':fi-plus.svg'))
        addButton.setIconSize(QtCore.QSize(20, 20))

        updateButton = QtGui.QPushButton()
        # updateButton.setText(u'Update')
        # updateButton.setStyleSheet(CONFIG['css'])
        updateButton.setIcon(QtGui.QIcon(':fi-loop.svg'))
        updateButton.setIconSize(QtCore.QSize(20, 20))

        deleteButton = QtGui.QPushButton()
        # deleteButton.setText(u'Delete')
        # deleteButton.setStyleSheet(CONFIG['css'])
        deleteButton.setIcon(QtGui.QIcon(':fi-x.svg'))
        deleteButton.setIconSize(QtCore.QSize(20, 20))

        buttonLayout.addWidget(addButton)
        buttonLayout.addWidget(updateButton)
        buttonLayout.addWidget(deleteButton)

        if self.with_table:
            hlayout.addLayout(buttonLayout)
        hspacer = QtGui.QSpacerItem(
            1, 1, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        hlayout.addSpacerItem(hspacer)

        layout.addLayout(hlayout)
        if self.with_table:
            self.buildTable()
            layout.addWidget(self.table.qtObject)

            # Set the actions
            addButton.clicked.connect(partial(self.add,
                                              self.uiform,
                                              self.fields))
            updateButton.clicked.connect(partial(self.update,
                                                 self.uiform,
                                                 self.fields))
            deleteButton.clicked.connect(self.delete)

        scroll = QtGui.QScrollArea()
        scroll.setWidget(widget)
        scroll.setWidgetResizable(True)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        scroll.setFixedWidth(widget.sizeHint().width() + 40)

        if self.with_table:
            scroll.setFixedHeight(220)
        else:
            scroll.setFixedHeight(150)

        if not self.scrolling:
            scroll.setFixedHeight(widget.sizeHint().height() + 40)

        self.layout.qtObject.addWidget(scroll)


# WIthout table
class UIInlineSimple(UIInline):

    def __init__(self, parent, name, klass=None):
        super(UIInlineSimple, self).__init__(parent, name, klass, False)


# special case for serviveProvider because
# updatting or adding and sp may change the price of a document header...
class UIInlineSP(UIInline):

    def __init__(self, parent, name):
        from sysgrove.core.document.models import ServiceProvider
        super(UIInlineSP, self).__init__(parent, name,
                                         klass=ServiceProvider,
                                         scrolling=False)

    def add(self, uiform, fields):
        cur = super(UIInlineSP, self).add(uiform, fields)
        from sysgrove.core.screen import Screen
        from sysgrove.core.document.models import update_total_price
        from sysgrove.core.document.models import get_psdetail_for
        document = Screen.get_current_value()
        already_in_priceSchema = len(
            get_psdetail_for(document.priceSchemaHeader,
                             cur.priceCondition)) > 0
        if not already_in_priceSchema:
            # popup  the vat question to the user
            dialog = QuestionPopup(
                (QuestionChoice(text=u'Yes', key='Y'),
                 QuestionChoice(text=u'No', key='N')),
                u"%s is not in price schema. Add a VAT condition?" %
                cur.priceCondition,
                self.parent.qtObject)

            def msg(dialog, text):
                if text == 'Y':
                    cur.vatCondition = True
                dialog.hide()
            dialog.selected.connect(partial(msg, dialog))
            dialog.exec_()

        document.serviceProviders = self.valueObject()
        update_total_price()

    def update(self, uiform, fields):
        deleted_sp = super(UIInlineSP, self).update(uiform, fields)
        if deleted_sp:
            from sysgrove.core.screen import Screen
            from sysgrove.core.document.models import update_total_price
            document = Screen.get_current_value()
            document.serviceProviders = self.valueObject()
        update_total_price()

    def delete(self):
        deleted_sp = super(UIInlineSP, self).delete()
        if deleted_sp:
            from sysgrove.core.screen import Screen
            from sysgrove.core.document.models import update_total_price
            deleted_sp.documentHeader = None
            document = Screen.get_current_value()
            document.serviceProviders = self.valueObject()
        update_total_price()
