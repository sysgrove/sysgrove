"""
      ____             ____
     / ___| _   _ ___ / ___|_ __ _____   _____
     \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
      ___) | |_| \__ \ |_| | | | (_) \ V /  __/
     |____/ \__, |___/\____|_|  \___/ \_/ \___|
            |___/



    Copyright SARL SysGrove
    contributor(s) : [name of the individuals]
    [date of creation]
    Version 0.0

    Revisions:
    [revision #]
    [date of revision]
    [reason for revision]

    contact:
    contact@sysgrove.com

    This software is a computer program whose purpose is to [describe
    functionalities and technical features of your software].

    This software is governed by the CeCILL license under French law and
    abiding by the rules of distribution of free software.  You can  use,
    modify and/ or redistribute the software under the terms of the CeCILL
    license as circulated by CEA, CNRS and INRIA at the following URL
    "http://www.cecill.info".

    As a counterpart to the access to the source code and  rights to copy,
    modify and redistribute granted by the license, users are provided only
    with a limited warranty  and the software's author,  the holder of the
    economic rights,  and the successive licensors  have only  limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using,  modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean  that it is complicated to manipulate,  and  that  also
    therefore means  that it is reserved for developers  and  experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards their
    requirements in conditions enabling the security of their systems and/or
    data to be ensured and,  more generally, to use and operate it in the
    same conditions as regards security.

    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL license and that you accept its terms.


    ----
"""
from sysgrove.ui.classes.window import UIWindow, SysGroveMain
from sysgrove.ui.window import GfxTabBar, GfxMenuBar, GfxContent
from PyQt4 import QtCore, QtGui
from sysgrove.ui.classes.popup import UIMaskPartial


class QDialogMixin(object):

    def __init__(*args, **kwargs):
        super(QDialogMixin, self).__init__(*args, **kwargs)

    def paintEvent(self, event):
        from sysgrove.context import kernel
        try:
            kernel.get_current_context().ui_space.qtObject
            self.currentContext = kernel.get_current_context(
            ).ui_space.qtObject
        except:
            self.currentContext = ""
        if not self.contextAssociate or \
                (self.contextAssociate == self.currentContext):
            if (self.actu is False):
                self.hide()
                self.show()
                self.setFocus()
                self.actu = True
                self.mask.hide()
                self.mask.resize(
                    self.contextAssociate.width(),
                    self.contextAssociate.height())
        else:
            try:
                self.parentUIObject.parent.parent.inlineExist
                if(self.parentUIObject.parent.parent.inlineExist is False):
                    self.actu = False
                    super(QDialogMixin, self).lower()
                    super(QDialogMixin, self).clearFocus()
            except:
                # if not inline Popup
                self.actu = False
                self.lower()
                self.clearFocus()
                # Management of mask
                self.mask.hide()
        if (SysGroveMain.isHidden()):
            self.close()
        if(SysGroveMain.isMinimized()):
            self.lower()
        self.update()

    def showEvent(self, event):
        # if inlineExist
        # do a second mask with parent is the QDialogInline
        from sysgrove.context import kernel
        self.contextAssociate = kernel.get_current_context().ui_space.qtObject
        try:
            self.parentUIObject.parent.parent.inlineExist
            if self.parentUIObject.parent.parent.inlineExist is True:
                self.setModal(True)
                # don't work because the path back to the popup has changed
                self.mask2 = UIMaskPartial(
                    self.parentUIObject.parent.parent.parent, "mask2", self)
                self.mask2.show()

        except:
            self.mask = UIMaskPartial(GfxContent, "mask", self)
            self.mask.show()

        self.showEvent(event)

    def closeEvent(self, event):
        try:
            self.mask2
            self.mask2.hide()
        except:
            self.mask.hide()
        self.closeEvent(event)

    def hideEvent(self, event):
        # if mask2 exist -> Inline exist
        try:
            self.mask2
            self.mask2.hide()
        except:
            self.mask.hide()

        self.hideEvent(event)
