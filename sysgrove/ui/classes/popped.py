# !/usr/bin/env python
# -*- coding: utf-8 -*-
# """
#   ____             ____
#  / ___| _   _ ___ / ___|_ __ _____   _____
#  \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
#   ___) | |_| \__ \ |_| | | | (_) \ V /  __/
#  |____/ \__, |___/\____|_|  \___/ \_/ \___|
#         |___/


# Copyright SARL SysGrove
# contributor(s) : S. Collins, A. Mokhtari, G. Souveton
# 20/02/2013
# Version 1.0
# This file regroups all important GUI classes (about one per type
# of widget we can find)
#     Role :
#         - Provides an overlay for Qt objects that can be handled
#           easily by Kernel and modules
#         - Uses a specific architecture based on UIWidget to explore
#           GUI items with an universal getter overload
# revision # 1
# Date of revision: 25/02/2013
# reason for revision: - This version can manage TabBar
# contact:
# contact@sysgrove.com
# This software is a computer program whose purpose is to manage the
# logistic execution, production, analysis, finance, transportation,
# master data, reporting and communication of end user companies.
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
# ----
# """
import logging
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from .uiobject import UIObject
from .window import UIWindow
from .utils import QMovable
from .popup import UIMaskPartial
from .label import UILabel
from .window import UIWindow, SysGroveMain
from sysgrove.ui.window import GfxTabBar, GfxMenuBar, GfxContent
log = logging.getLogger('sysgrove')


class WidgetPooped(QtGui.QWidget):

    def __init__(self, parent, poop):
        super(WidgetPooped, self).__init__(parent)
        self . parent = parent
        self.poop = poop
        self.justDoubleClicked = False

 #    def keyPressEvent(self, event):
        #   self.WidgetPooped.keyPressEvent(event)
        #   if (event.key() == QtCore.Qt.Key_Escape):
        #       self.poop.hide()

        # def mouseMoveEvent(self, event):
        #   self.poop.box.z.mouseMoveEvent(event)
        #   self.setMouseTracking(True)
        # Moving events whitout the click
        #   print "Move"

        # def mouseDoubleClickEvent(self, event):
        #   self.justDoubleClicked = True
        #   log.debug("DoubleClick")
        #   self.update()

        # def mouseReleaseEvent(self, event):
        #   if self.justDoubleClicked:
        #       print "Release"


class UIPoppedWidget(UIObject):

    def __init__(self, parent, name):
        from sysgrove.ui.classes import UIContent
        super(UIPoppedWidget, self).__init__(parent, name)
        self.allPopup = WidgetPooped(UIContent.luisbi.qtObject, self)

        # luisbi forced, if not, the popup won't disappear at context change.
        # FIXME : moving fails
        self.allPopup = QtGui.QWidget(UIContent.luisbi . qtObject)
        # UIWindow.qtObject)
        # UIContent.luisbi . qtObject ) #FIXME test
        # UIWindow.qtObject )#parent.qtObject ) UI        Content.Luisbi:
        # get the current page from the classe UISpace
        self.allPopup.setStyleSheet("background-color: #dddddd;")
        self.alpoLayout = QtGui.QVBoxLayout(self.allPopup)
        self.alpoLayout.setMargin(0)
        self.alpoLayout.setSpacing(0)

        # FIXME
        self.titlebar = QMovable(
            self.allPopup, self.allPopup, UIContent.luisbi . qtObject)
        self.titlebar.setMinimumHeight(20)
        self.titlebar.setStyleSheet("background-color: red;")

        self.alpoLayout.addWidget(self.titlebar)
        self.box = QtGui.QWidget(self.allPopup)
        self.alpoLayout.addWidget(self.box)
        self.box.qtObject = self.box
        self.qtObject = self.box

        # set the title
        self.title = UILabel(self, "TitlePopped")
        self.title.setAlignment(QtCore.Qt.AlignCenter)
        # provide a default title
        self.title.setText(u'')
        self.title.setStyleSheet("""
        background-color: #3E4D58;
        color: white;
        border-radius: 0px;
        margin-bottom: 10px;
        border-bottom: 2px solid #F5BC40;
        """)

        self.layout = QtGui.QVBoxLayout(self.box)
        self.layout.setMargin(2)
        self.layout.setSpacing(2)
        # add the title in the layout
        self.layout.addWidget(self.title.qtObject)

        w, h = 465, 295
        self.resizePopup(w, h)
        self.allPopup.hide()

    def hideEv(self, x):
        self.hide()

    def show(self):
    # Mask must be generated here .
    # if not, it's not uptodate, since the calendar is created
    # when the screen is being created.
        self.mask = UIMaskPartial(UIWindow, "mask")
        self.mask.qtObject.mouseDoubleClickEvent = self.hideEv
        self.mask.show()
        self.allPopup.show()
        self.allPopup.raise_()

    def hide(self):
        self.mask.hide()
        self.allPopup.hide()
        self.mask.deleteLater()

    def resizePopup(self, w, h):
        from sysgrove.ui.classes import UIContent
        self.allPopup.setMinimumSize(QtCore.QSize(w, h))
        self.allPopup.setMaximumSize(QtCore.QSize(w, h))
        self.allPopup.resize(w, h)
        x = (UIContent.qtObject.frameGeometry().width() - w) / 2
        y = (UIContent.qtObject.frameGeometry().height() - h) / 2
        # FIXME: use max or min to have shorter code... ;-)
        # or use only (0,0) to put it in the midle?
        if x < 0:
            x = 0
        if y < 0:
            y = 0
        self.allPopup.move(x, y)
