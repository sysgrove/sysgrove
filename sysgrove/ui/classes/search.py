#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from .uiobject import UIObject


class UISearchClass (UIObject):

    def __init__(self, gfx, layout,  name=""):

        self.__items__ = {}
        self.name = name

        if isinstance(gfx, QtGui.QWidget):
            self.qtObject = QtGui.QLineEdit(gfx)
        else:
            self.qtObject = QtGui.QLineEdit(gfx.qtObject)

        self.setMinimumSize(192, 20)
        self.setStyleSheet("""
            padding-left: 20px;
            border: 1px solid #8f8f91;
            border-radius: 6px;
            font-weight: bold;
            font-size: 10px;
            background-color:
                qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                stop: 0 #f6f7fa, stop: 1 #dadbee);
            background-image: url(':magnifier-16.png');
            background-repeat: no-repeat;
            """)

        self.qtObject.connect(
            self.qtObject,
            QtCore.SIGNAL(_fromUtf8("returnPressed()")),
            lambda: UISearchClass.doSearch(self))

    def addSearchbarToLayout(self, layout):
        layout.addWidget(self.qtObject)

    def doSearch(self):

        query = str(_fromUtf8(self.qtObject.text()))
        from sysgrove.context import kernel
        result = kernel.lookupContextByCode(query)
