# !/usr/bin/env python
# -*- coding: utf-8 -*-
# """
#   ____             ____
#  / ___| _   _ ___ / ___|_ __ _____   _____
#  \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
#   ___) | |_| \__ \ |_| | | | (_) \ V /  __/
#  |____/ \__, |___/\____|_|  \___/ \_/ \___|
#         |___/



# Copyright SARL SysGrove
# contributor(s) : S. Collins, A. Mokhtari, G. Souveton
# 20/02/2013
# Version 1.0

# This file regroups all important GUI classes (about one per type
# of widget we can find)

#     Role :

#         - Provides an overlay for Qt objects that can be handled
#           easily by Kernel and modules
#         - Uses a specific architecture based on UIWidget to explore
#           GUI items with an universal getter overload

# revision # 1
# Date of revision: 25/02/2013
# reason for revision: - This version can manage TabBar

# contact:
# contact@sysgrove.com

# This software is a computer program whose purpose is to manage the
# logistic execution, production, analysis, finance, transportation,
# master data, reporting and communication of end user companies.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


# ----
# """
# import logging

# from PyQt4 import QtCore, QtGui
# try:
#     _fromUtf8 = QtCore.QString.fromUtf8
# except AttributeError:
#     _fromUtf8 = lambda s: s

# from sysgrove.ui.window import GfxWindow
# from .uiobject import UIObject
# from .widget import UIWidget

# log = logging.getLogger('sysgrove')


# class UIMask (UIWidget):

#     def __init__(self, parent, name, blur=8):
#         super(UIMask, self).__init__(parent, name)

#         self.qtObject.setMinimumSize(GfxWindow.size())
#         self.qtObject.setMaximumSize(GfxWindow.size())
#         self.qtObject.resize(GfxWindow.size())
# self.qtObject.setStyleSheet("background-color: #ffffff")

#         self.screenshot = QtGui.QPixmap.grabWidget(GfxWindow)
#         self.ly = QtGui.QHBoxLayout(self.qtObject)
#         self.lb = QtGui.QLabel(self.qtObject)
#         self.ly.setMargin(0)
#         self.ly.setSpacing(0)
#         self.lb.resize(GfxWindow.size())

#         self.lb.setPixmap(self.screenshot)
#         self.ly.addWidget(self. lb)

#         if blur:
#             self.bl = QtGui.QGraphicsBlurEffect()
#             self.bl.setBlurRadius(blur)
#             self.lb.setGraphicsEffect(self.bl)


# class UIMaskPartial (UIWidget):

#     def __init__(self, parent, name, blur=8):
#         from sysgrove.context import kernel
#         from sysgrove.ui.classes import UIContent
# print kernel.contexts[kernel.current_context].ui_space
#         super(UIMaskPartial, self).__init__(
#             kernel.get_current_context().ui_space,
#             name)
#         from sysgrove.ui.window import GfxWindow
# print UIContent
#         self.qtObject.setMinimumSize(UIContent.size())
#         self.qtObject.setMaximumSize(UIContent.size())
#         self.qtObject.resize(UIContent.size())
#         self.qtObject.setStyleSheet("background-color: rgba(0.0,0.0,0.0,0.3)")

# self.screenshot = QtGui.QPixmap.grabWidget(UIContent.qtObject)
#         self.ly = QtGui.QHBoxLayout(self.qtObject)
#         self.lb = QtGui.QLabel(self.qtObject)
#         self.ly.setMargin(0)
#         self.ly.setSpacing(0)
#         self.lb.resize(GfxWindow.size())

# self.lb.setPixmap(self.screenshot)
# self.lb.setStyleSheet("background-color: #333333;")
#         self.ly.addWidget(self. lb)

# if blur:
# self.bl = QtGui.QGraphicsBlurEffect ( )
# self.bl.setBlurRadius ( blur )
# self.lb.setGraphicsEffect ( self.bl )

#         self.o = QtGui.QGraphicsOpacityEffect()
#         self.o.setOpacity(0.3)
#         self.lb.setGraphicsEffect(self.o)

#     def show(self):
#         from sysgrove.ui.window import ui
#         ui.nofs()
#         self.qtObject.show()

#     def hide(self):
#         from sysgrove.ui.window import ui
#         ui.refs()
#         self.qtObject.hide()


# class UIPopup(UIObject):

# def __init__(self, parent, name, content):

# frameMask is the blur mask that shows up above
# the screen, behind the popup. The popup is a child of frameMask
# super(UIPopup, self).__init__(parent, name)

# self.mask = UIMask(parent, "mask")
# self.box = QtGui.QWidget(parent.qtObject)
# self.layout = QtGui.QVBoxLayout(self.qtObject)

# from sysgrove.ui.window import GfxWindow

# w, h = 400, 272
# x = (GfxWindow.frameGeometry().width() - w) / 2
# y = (GfxWindow.frameGeometry().height() - h) / 2

# self.box.setMinimumSize(QtCore.QSize(w, h))
# self.box.setMaximumSize(QtCore.QSize(w, h))
# self.box.resize(w, h)
# self.box.move(x, y)

# self.op = QtGui.QGraphicsOpacityEffect()
# self.op .setOpacity(0.8)
# self.mask.setGraphicsEffect(self.op)

# self.boxLayout = QtGui.QVBoxLayout(self.box)

# self.boxContent = QtGui.QWidget(self.box)

# self.boxContentLayout = QtGui.QVBoxLayout(self.boxContent)

# self.boxLayout.addWidget(self.boxContent)

# self.buttonsWidget = QtGui.QWidget(self.box)
# self.buttonsWidget.setMaximumSize(8000, 50)

# self.buttonsLayout = QtGui.QHBoxLayout(self.buttonsWidget)

# for c in content:

# if c == 'content':

# if content[c] != '':
# x = QtGui.QLabel(self.box)
# x.setGeometry(100, 100, 200, 100)
# x.setText(content[c])
# self.boxLayout.addWidget(x)

# elif c == "buttons":

# for b in content[c]:
# x = QtGui.QToolButton(self.buttonsWidget)
# x.setMinimumSize(100, 40)
# x.setText(b)
# if content[c][b] == 'close':
# if content[c][b] == 'close':  # don't ask why !
# x.connect(x, QtCore.SIGNAL(_fromUtf8(
# "clicked()")), lambda: self.close())
# else:
# from sysgrove.context import kernel
# x.connect(
# x,
# QtCore.SIGNAL(_fromUtf8("clicked()")),
# lambda: kernel.lin_trap(content[c][b]))

# self.buttonsLayout.addWidget(x)

# self.boxLayout.addWidget(self.buttonsWidget)

# def open(self):
# self.mask.show()
# self.box.show()

# def close(self):
# self.mask.hide()
# self.box.hide()
# self.mask.deleteLater()
# self.box.deleteLater()
