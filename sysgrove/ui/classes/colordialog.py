#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0

This file regroups all important GUI classes (about one per type
of widget we can find)

    Role :

        - Provides an overlay for Qt objects that can be handled
          easily by Kernel and modules
        - Uses a specific architecture based on UIWidget to explore
          GUI items with an universal getter overload

revision # 1
Date of revision: 25/02/2013
reason for revision: - This version can manage TabBar

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from datetime import datetime
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from .uiobject import UIObject
from .calendar import UIPoppedCalendar, UIPoppedMonth
from .mixins.dialog_mixin import QDialogMixin

log = logging.getLogger('sysgrove')


class UIColorDialog(UIObject):
    # FIXME : need to emulate a editingFinished() when
    # all fields are non-empty and we're leaving one of them.
    edited = QtCore.pyqtSignal(str)

    def __init__(self, parent_ui_component, name):
        super(UIColorDialog, self).__init__(parent_ui_component, name)
        self.qtObject = QtGui.QWidget(parent_ui_component.qtObject)
        self.qtObject.setStyleSheet("border: none;")
        self.qtObject.setMaximumSize(QtCore.QSize(144, 32))
        self.qtObjectLayout = QtGui.QHBoxLayout(self.qtObject)
        self.qtObjectLayout.setSpacing(0)
        self.qtObjectLayout.setMargin(0)

        # Button outside the dateFormat
        self.qtButton = QtGui.QToolButton(self.qtObject)
        self.qtButton.setStyleSheet("""
            QToolButton:hover{
                background-color: transparent;
                border: 1px solid #F5BC40;
            }""")
        self.color = QtGui.QColor(0, 0, 0)
        self.px = QtGui.QPixmap(24, 24)
        self.px.fill(self.color)

        self.icon = QtGui.QIcon()
        self.icon.addPixmap(self.px, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtButton.setIcon(self.icon)
        self.qtButton.setIconSize(QtCore.QSize(24, 24))
        self.name = name
        self.parent_ui_component = parent_ui_component
        self.qtButton.connect(self.qtButton, QtCore.SIGNAL(
            _fromUtf8("clicked()")), self.select_color)
        self.qtObjectLayout.addWidget(self.qtButton)
        # self.colorDialog = QtGui.QColorDialog()
        # self.colorDialog.setModal(True)
        self.colorDialog = ColorPopup()
        self.colorDialog.colorSelected.connect(self.valid_color)

    def select_color(self):
        self.colorDialog.show()

    def valid_color(self, color):
        self.color = color
        self.px.fill(self.color)
        self.icon = QtGui.QIcon()
        self.icon.addPixmap(self.px, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtButton.setIcon(self.icon)
        self.qtButton.setIconSize(QtCore.QSize(24, 24))
        
    def value(self):
        try:
            return str(self.color.red()) + ", " \
                + str(self.color.green()) + ", " \
                + str(self.color.blue())
        except ValueError:
            return None

    def setVal(self, val):
        if not val or val == unicode(None) or val == '':
            self.color = QtGui.QColor(0, 0, 0)
        else:
            color = val.split(",")
            r = int(color[0])
            g = int(color[1])
            b = int(color[2])
            self.color = QtGui.QColor(r, g, b)

        self.px = QtGui.QPixmap(24, 24)
        self.px.fill(self.color)
        self.icon = QtGui.QIcon()
        self.icon.addPixmap(self.px, QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtButton.setIcon(self.icon)
        self.colorDialog.setCurrentColor(self.color)

    def clear(self):
        pass


class ColorPopup(QtGui.QColorDialog, QDialogMixin):

    def __init__(self, *args, **kwargs):
        super(ColorPopup, self).__init__(None)
        self.actu = True
    pass
