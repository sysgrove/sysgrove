#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton, S. Collins
07/02/2013
Version 1.0

    Role :

        - This file is the main file for the main program

revision #
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

"""

# Site effect : the as sysgrove/__init__.py is loaded
# the table in the database are created, if not.
import sys
import os.path
import datetime
from sqlalchemy import create_engine, Table
from sqlalchemy.event import listen
from sqlalchemy.orm import sessionmaker, scoped_session
import logging


def init_logging(config):
    # init the logger
    log = logging.getLogger('sysgrove')
    log.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('sysgrove.log')
    fh.setLevel(logging.DEBUG)
    # create a consol handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    log.addHandler(fh)
    log.addHandler(ch)

    if config['agent'] == 'logstash':
        import logstash
        host = config['host']
        port = config['port']
        log.addHandler(logstash.LogstashHandler(host, port, version=1))

    return log

# test

# to be compliant with py2exe.
# see http://www.py2exe.org/index.cgi/WhereAmI
# and http://stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-
# the-current-executed-file-in-python
# These function should be placed in sysgrove.utils module
# but, there are used to append 'sysgrove' to the sys.path
# so .... they must be here.


def we_are_frozen():
    """Returns whether we are frozen via py2exe.
    This will affect how we find out where we are located."""

    return hasattr(sys, "frozen")


def module_path():
    """ This will get us the program's directory,
    even if we are frozen using py2exe"""

    if we_are_frozen():
        return os.path.dirname(unicode(
            sys.executable,
            sys.getfilesystemencoding()))
    return os.path.dirname(__file__)

my_path = module_path()
sys.path.append(os.path.join(my_path, '..'))
from sysgrove.config import CONFIG
from sysgrove.djangoQuery import DjangoQuery
from sysgrove.ui.window import run_app, launcher
from sysgrove.utils import get_class

log = init_logging(CONFIG['logging'])


def log_except(excType, excValue, traceback, logger=log):
    logger.error("Logging an uncaught exception",
                 exc_info=(excType, excValue, traceback))
sys.excepthook = log_except


try:
    ENGINE = create_engine(CONFIG['db'], echo=False)
except ImportError as e:
    log.warning('Cannot used database configurations')
    log.warning(e)
    ENGINE = create_engine(CONFIG['db_default'], echo=False)


Session = sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=ENGINE)
Session.configure(query_cls=DjangoQuery)
db_session = scoped_session(Session)


def init_db(create=False, load_pkg=True):
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.
    # TODO : this has to be check, but we have to import manually
    # all python modules 'models', owe cannot do it in run time
    # because of creating .exe file
    from sysgrove.models import Base, add_pkg_classes
    import sysgrove.modules.masterdata.models
    import sysgrove.modules.purchase.models
    import sysgrove.modules.sales.models
    import sysgrove.modules.help.models
    import sysgrove.core.document.models
    import sysgrove.modules.finance.models
    import sysgrove.modules.reporting.models

    # Useless since alembic is in place.
    # Kept for test/initdb.py
    if create:
        Base.metadata.create_all(ENGINE)

    # Keep reference in the database itself
    if load_pkg:
        add_pkg_classes(sysgrove.modules.masterdata.models)
        add_pkg_classes(sysgrove.core.document.models)
        add_pkg_classes(sysgrove.modules.purchase.models)
        add_pkg_classes(sysgrove.modules.sales.models)
        add_pkg_classes(sysgrove.modules.help.models)
        add_pkg_classes(sysgrove.modules.finance.models)
        add_pkg_classes(sysgrove.modules.reporting.models)
        add_sqlalchemy_listerner()


def add_sqlalchemy_listerner():
    """
       Add SQLAlchemy listen for many classes accordinf to their mixin
    """
    from sysgrove.models import SG_Classes, SimplMixin

    # add validate_code listen
    for c in SG_Classes.query.all():
        try:
            klass = get_class("%s.%s" % (c.package, c.name))
        except AttributeError:
            klass = None
        # FIXME: we could use the 'propagate=True' arg of the listen
        # function... it seems to to the job we want.
        # read : http://docs.sqlalchemy.org/en/rel_0_8/orm/events.html
        if klass and not isinstance(klass, Table) and SimplMixin in klass.__mro__:
            listen(klass.code, 'set', klass.validate_code, retval=True)


def main():
    log.debug('Started %s' % datetime.datetime.now())

    init_db()

    # DONT REMOVE the screen and ui import stuff, we must have them make an
    # .exe file
    import sysgrove.core.screen
    import sysgrove.core.document.ui
    import sysgrove.core.document.ui.customs
    import sysgrove.modules.masterdata.screens
    import sysgrove.modules.masterdata.ui
    import sysgrove.modules.purchase.screens
    import sysgrove.modules.purchase.ui
    import sysgrove.modules.sales.screens
    import sysgrove.modules.sales.ui
    import sysgrove.modules.finance.ui
    import sysgrove.modules.finance.screens
    import sysgrove.modules.help.screens
    import sysgrove.modules.log.screens
    import sysgrove.modules.log.ui
    import sysgrove.modules.reporting.screens
    import sysgrove.modules.reporting.ui

    launcher.start(CONFIG)
    run_app()
    log.debug('Finished--------------')

# Main program
if __name__ == "__main__":
    main()
