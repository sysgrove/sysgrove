#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import datetime
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from sysgrove.settings import (
    SOLDETO_CODE, BILLTO_CODE,
    SHIPTO_CODE, UNSET,
    DG_ORDER, STATUS_OPEN
)
from sysgrove.utils import debug_trace, mode_to_code, slugify, cc_print
from sysgrove.core.screen import Screen, ExceptionMessagePopup
from sysgrove.core.screen import save_or_error_popup
from sysgrove.core.screen import handle_valueError
from sysgrove.core.document.models import ServiceProvider
from sysgrove.modules.masterdata.models import Status
from sysgrove.modules.masterdata.models import ThirdParty
from sysgrove.modules.masterdata.models import Address
from sysgrove.modules.masterdata.models import Menu
from sysgrove.modules.masterdata.models import DocCopyRule
from sysgrove.modules.masterdata.models import PriceCondition
from sysgrove.core.document.models import DocumentDetail
from sysgrove.ui.classes.widgets.message_popup import MessagePopup
from sysgrove.ui.classes.tree import TreeWidgetItem
from sysgrove.ui.classes.form import UIForm
from sysgrove.ui.classes.uiobject import create_icon
from sysgrove.core.document.ui.customs import DocLinkPopup
from sysgrove.core.document.ui.customs import update_total_price
from sysgrove.core.document.ui.customs import update_price_ui
from sysgrove.core.document.ui.customs import update_doc_unitPrice
from sysgrove import i18n
_ = i18n.language.ugettext


def add_tp_addr_fields(tpname):
    lf = Address.list_columns[:]
    if 'id' in lf:
        lf.remove('id')
    return map(lambda x: '%s_address_%s' % (tpname, x), lf)


def retreive_gb(ui_content, klass, name):
    return getattr(ui_content,
                   'GroupBox%s%s' % (klass.__name__, slugify(name)), None)


class DocumentValidPopup(MessagePopup):

    def __init__(self, parent, document, screen, name="ValidPopUp"):

        message = u"Document %s - %s #%s has been saved" % (
            document.docType.code,
            document.docType.description,
            document.documentNumber)

        self.document = document
        super(DocumentValidPopup, self).__init__([message], None, None, parent)
        self.screen = screen

        b = QtGui.QToolButton()
        b.setIcon(QtGui.QIcon(":document-print.svg"))
        b.setIconSize(QtCore.QSize(30, 30))
        b.setText(_("   Print  "))
        b.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        b.clicked.connect(self.printX)
        b.setStyleSheet(self.StyleSheetButton)
        self.layoutButton.addWidget(b)
        # the title is inherited, we just need to set the text
        self.setWindowTitle(message)

        self.closed.connect(self.hide)

    def printX(self):
        self.document.generatePdf()
        self.close()


class ScreenDocumentCommun(Screen):

    def __init__(self, *p, **k):
        self.menu = Menu.query.filter_by(code=unicode(self.context_code)).one()
        self.fields = UIForm.form2attr_list(self.klass.list_creation)
        # linked objects
        self.linkTo = [{
            'slave': ('docType', 'docType'),
            'masters': [('site', 'site')],
            'filters':  {'docGroup_id': self.menu.docGroup.id,
                         'flowID_id': self.menu.flowID.id},
        }, ]
        super(ScreenDocumentCommun, self).__init__(*p, **k)

    def build_value(self, document):
        self.set_newValue({'value': document})


class ScreenDocument(ScreenDocumentCommun):

    def __init__(self, *p, **k):
        super(ScreenDocument, self).__init__(*p, **k)
        self.fields.extend(
            ['documentNumber', 'documentNumber_documentValueDate'])
        self.linkTo.append({
            'slave': ('documentNumber', 'documentHeader'),
            'masters': [('docType', 'docType')]
        })

    def display(self):
        from sysgrove.context import kernel
        valid = True
        docType_ui = getattr(self.ui_space, self.ui_prefix + 'docType')
        docType_code = unicode(docType_ui.value())
        if docType_code == '':
            valid = False
            docType_ui.setInvalidStyle()

        docNumber_ui = getattr(
            self.ui_space, self.ui_prefix + 'documentNumber')
        docNumber = unicode(docNumber_ui.value())
        if docNumber == '':
            valid = False
            docNumber_ui.setInvalidStyle()

        if valid:
            document = docNumber_ui.valueObject()
            if document:
                self.build_value(document)
                code = self.__class__.__name__[6:][:-2]
                kernel.set_current_context(
                    '%s_main%s' % (code, mode_to_code(self.mode)))
            else:
                docNumber_ui.setInvalidStyle()


class ScreenDocument01(ScreenDocumentCommun):

    def __init__(self, *p, **k):

        # we have many ui container to be handled
        # self.multiple = self.ui.multiple_content()
        # Becareful 'super' is called with 'ScreenDocument'
        # because we don't want to overwrite self.fields
        # super(ScreenDocument, self).__init__(*p, **k)
        self.multiple = self.ui.multiple_content()
        super(ScreenDocument01, self).__init__(*p, **k)
        self.fields.extend([
            'parentDocumentHeader_site',
            'parentDocumentHeader_docType',
            'parentDocumentHeader_documentNumber',
            'parentDocumentHeader_documentNumber_documentDetails'])

        # DOTO: use DocCopyRule as filter
        self.linkTo.extend([{
            'slave': ('parentDocumentHeader_site', 'site', 'site'),
            'masters': [('site', 'site')],
            'join': (DocCopyRule, {
                'flowIDTarget_id': self.menu.flowID.id,
                'siteTarget__code': 'site'}),
        }, {
            'slave': ('parentDocumentHeader_docType', 'docType',
                      'docTypeCode'),
            'masters': [('parentDocumentHeader_site', 'site'),
                        ('docType', 'docType'),
                        ('site', 'site')],
            'join': (DocCopyRule,
                     {'flowIDTarget_id': self.menu.flowID.id,
                      'siteTarget__code': 'site_code',
                      'docGroupTarget__code': 'docType_docGroup_code',
                      'docTypeCodeTarget__code': 'docType_code'})
        }, {
            'slave': ('parentDocumentHeader_documentNumber',
                      'documentHeader'),
            'masters': [('parentDocumentHeader_docType', 'docType')]
        }, {
            'slave': ('parentDocumentHeader_documentNumber_documentDetails',
                      DocumentDetail),
            'masters': [('parentDocumentHeader_documentNumber',
                         'documentHeader')]
        }])

    def create(self):
        document = self.klass(creationDate=datetime.datetime.now())
        visible = filter(lambda x: x.isVisible(), self.multiple)
        if not len(visible) == 1:
            raise Exception('Nothing is Visible in create')
        visible = visible[0]
        valid = True
        errors = Screen.ui2bdd(document,
                               visible,
                               UIForm.form2attr_list(self.klass.list_creation),
                               self.klass, self.ui_prefix)
        # check if we are creating with reference or not
        parentUi = getattr(
            visible,
            self.ui_prefix + 'parentDocumentHeader_documentNumber', None)
        if parentUi:
            parent = parentUi.valueObject()
            detailsUI = getattr(
                visible,
                self.ui_prefix +
                "parentDocumentHeader_documentNumber_documentDetails")
            try:
                document.copy(parent, detailsUI.valueObject())
            except Exception as e:
                valid = False

        if valid and not errors:
            from sysgrove.context import kernel
            # default value
            document.documentValueDate = datetime.date.today()
            document.status = Status.query.filter_by(id=STATUS_OPEN).one()

            self.build_value(document)
            document.set_amount()
            code = self.__class__.__name__[6:][:-2]
            kernel.set_current_context('%s_main01' % code)
        else:
            if errors:
                handle_valueError(document, errors,
                                  visible, self.ui_prefix)
            if not valid:
                exPopup = ExceptionMessagePopup(self.ui_space.qtObject,
                                                e, self)
                exPopup.show()


class ScreenDocument_main(Screen):

    def __init__(self, *p, **k):
        self.menu = Menu.query.filter_by(
            code=unicode(cc_print(self.context_code))).one()

        self.linkTo = [{
            # the filters on soldTo, shipTo and BillTo are done on
            # ThirPartyTypes
            'slave': ('soldToCode', 'thirdParty'),
            'masters': [('site', 'site')],
            'filters': {'id': SOLDETO_CODE}
        }, {
            'slave': ('shipToCode', 'thirdParty'),
            'masters':  [('site', 'site')],
            'filters': {'id': SHIPTO_CODE,
                        'parentThirdParty_id': 'soldToCode_id'}
        }, {
            'slave': ('billToCode', 'thirdParty'),
            'masters':  [('site', 'site')],
            'filters': {'id': BILLTO_CODE,
                        'parentThirdParty_id': 'soldToCode_id'}
        }, {
            'slave': ('incoterm', 'incoterm'),
            'masters':  [('site', 'site')],
        }, {
            'slave': ('incotermText', 'incotermText'),
            'masters':  [('incoterm', 'incoterm')],
        }, {
            'slave': ('paymentTermText', 'paymentTermText'),
            'masters':  [('paymentTerm', 'paymentTerm')],
        }, {
            'slave': ('salesRep', 'salesRep'),
            'masters':  [('site', 'site')],
        }, {
            'slave': ('bank', 'bank'),
            'masters':  [('site', 'site')]}
        ]

        self.fields = UIForm.form2attr_list(self.klass.list_display)[:]
        self.fields.append('documentValueDate')
        self.fields.append('documentDetails')
        self.fields.extend(add_tp_addr_fields('soldToCode'))
        self.fields.extend(add_tp_addr_fields('shipToCode'))
        self.fields.extend(add_tp_addr_fields('billToCode'))

        self.updatable_fields = self.fields[:]
        self.updatable_fields.remove('parentDocumentHeader_docType')
        self.updatable_fields.remove('parentDocumentHeader_documentNumber')

        self.properties = [
            'docTotalAmountExclVatForeignCurrency',
            'docTotalVatForeignCurrency',
            'docTotalAmountInclVatForeignCurrency',
        ]
        self.defaults = [
            ('soldToCode', 'incoterm', 'incoterm'),
            ('soldToCode', 'paymentTerm', 'paymentTerm'),
            ('soldToCode', '', 'shipToCode'),
            ('soldToCode', '', 'billToCode'),
        ]

        self.link_popup = DocLinkPopup(self,
                                       self.menu.flowID,
                                       self.menu.docGroup)
        super(ScreenDocument_main, self).__init__(*p, **k)
        self.document = self.newValue['value']
        self.showTables()
        # Update some of the ui objects
        mainGroupBox = getattr(self.ui_space, 'mainGroupBox')
        if not (self.document.documentNumber or
                self.document.documentNumber == UNSET):
            mainGroupBox.setTitle("%s - %s" % (
                self.document.docType.code,
                self.document.docType.description))
        else:
            mainGroupBox.setTitle("%s - %s #%s" % (
                self.document.docType.code,
                self.document.docType.description,
                self.document.documentNumber))
        refToGb = retreive_gb(self.ui_space, self.klass, u'Related Document')
        if not self.document.hasParent():
            refToGb.hide()
        refToGb.setReadOnly(True)

        amountGb = retreive_gb(self.ui_space, self.klass, u"Amount")
        amountGb.setReadOnly(True)

        fixingTab = getattr(self.ui_space, 'TabWidgetFixing', None)
        premiumTab = getattr(self.ui_space, 'TabWidgetPremium', None)
        tabPanel = premiumTab.parent
        if self.document.futureFixingFlag:
            gb_prem = getattr(fixingTab,
                              'GroupBoxFuturePricingpremium_fixing',
                              None)
            gb_prem_info = getattr(fixingTab,
                                   'GroupBoxFuturePricingpremiun_info',
                                   None)

            if self.document.premium_flat:
                gb_prem.close()
                gb_prem_info.close()
            else:
                tabPanel.removeTab(tabPanel.indexOf(premiumTab.qtObject))
                if self.document.premiumFlag is None:
                    gb_prem.close()
                    gb_prem_info.close()

            if not self.document.forexFlag:
                gb_forex = getattr(fixingTab,
                                   'GroupBoxFuturePricingforex_fixing',
                                   None)
                gb_forex_info = getattr(fixingTab,
                                        'GroupBoxFuturePricingforex_info',
                                        None)

                gb_forex.close()
                gb_forex_info.close()
            if not self.document.priceFlag:
                gb_price = getattr(fixingTab,
                                   'GroupBoxFuturePricingfuture_fixing',
                                   None)
                gb_price_info = getattr(fixingTab,
                                        'GroupBoxFuturePricingfuture_info',
                                        None)
                gb_price.close()
                gb_price_info.close()

        else:
            tabPanel.removeTab(tabPanel.indexOf(premiumTab.qtObject))
            tabPanel.removeTab(tabPanel.indexOf(fixingTab.qtObject))

    def printX(self):
        if self.document.id:
            self.document.generatePdf()

    def linePlus(self):
        uiDD = getattr(
            self.ui_space, self.ui_prefix + 'documentDetails', None)
        if uiDD:
            uiDD.popup.show()

    def lineMinus(self):
        uiDD = getattr(
            self.ui_space, self.ui_prefix + 'documentDetails', None)
        try:
            uiDD.removeSelectedDD()
        except Exception as e:
            exPopup = ExceptionMessagePopup(self.ui_space.qtObject,
                                            e, self)
            exPopup.show()

    def setLinks(self):
        super(ScreenDocument_main, self).setLinks()
        # FIXME : This is not very Nice, but setLinks is always called
        # and UIComboxBox are filled by the previous line
        # mode = self.newValue['mode']
        document = self.newValue['value']
        Screen.bdd2ui(document, self.ui_space, self.fields, self.ui_prefix)

        # Let's set the serviceProvider_thirdParty link by hand...
        # no other solution, for now
        uiServicesProviders = getattr(
            self.ui_space, self.ui_prefix + 'serviceProviders', None)
        if uiServicesProviders:
            ui3party = uiServicesProviders._thirdParty
            sp_filter = {
                'slave': ('thirdParty', 'thirdParty'),
                'filters': {'serviceProviderFlag': True,
                            'sites__id': document.site.id},
            }
            ui3party.setLink(
                klass=ThirdParty,
                prefix=uiServicesProviders.ui_prefix,
                title=ServiceProvider.verbose_name,
                **sp_filter
            )
            uiPriceCondition = uiServicesProviders._priceCondition
            pc_filter = {
                'slave': ('priceCondition', 'priceCondition'),
                'filters': {'serviceFlag': True}
            }
            uiPriceCondition.setLink(
                klass=PriceCondition,
                prefix=uiServicesProviders.ui_prefix,
                **pc_filter
            )

    def save(self):
        document = self.newValue['value']
        valid = save_or_error_popup(document, self, self.updatable_fields)

        if valid:
            if not document.documentNumber or document.documentNumber == UNSET:
                document.set_documentNumber()
            try:
                document.save()
            except Exception as e:
                exPopup = ExceptionMessagePopup(
                    self.ui_space.qtObject, e, self)
                exPopup.show()
                return
            okPopup = DocumentValidPopup(self.ui_space.qtObject,
                                         document, self)
            self.cancel()
            okPopup.show()

    def cancel(self):
        from sysgrove.main import db_session
        document = self.newValue['value']
        if document in db_session:
            db_session.expunge(document)
        super(ScreenDocument_main, self).cancel()

    def setActions(self):
        super(ScreenDocument_main, self).setActions()
        reverse_flow = self.menu.flowID.reverse()
        # text = u"Link to %s %s " % (
        #     reverse_flow.description, self.menu.docGroup.description)
        text = _("Link to ")+reverse_flow.description+" "+self.menu.docGroup.description
        iconLink = create_icon('insert-link.svg')

        uiDD = getattr(self.ui_space,
                       self.ui_prefix + 'documentDetails',
                       None)
        if uiDD:
            uiLinkDetail = getattr(uiDD, uiDD.ui_prefix + 'linkDetail', None)
            if uiLinkDetail:
                uiLinkDetail.qtObject.setIcon(iconLink)
                uiLinkDetail.qtObject.setIconSize(QtCore.QSize(30, 30))
                uiLinkDetail.setText(text)
                uiLinkDetail.clicked.connect(self.link_popup.show)

        uiLink = getattr(self.ui_space, self.ui_prefix + 'link', None)
        if uiLink:
            uiLink.qtObject.setIcon(iconLink)
            uiLink.qtObject.setIconSize(QtCore.QSize(30, 30))
            uiLink.setText(text)
            uiLink.clicked.connect(self.link_popup.show)

        uiButton_addPremium = getattr(self.ui_space,
                                      uiDD.ui_prefix + 'addPremium',
                                      None)
        if uiButton_addPremium:
            uiButton_addPremium.setIcon(QtGui.QIcon(':fi-loop.svg'))
            uiButton_addPremium.setIconSize(QtCore.QSize(20, 20))
            uiButton_addPremium.clicked.connect(self.addPremium)

    def addPremium(self):
        uiDD = getattr(self.ui_space,
                       self.ui_prefix + 'documentDetails',
                       None)
        docDetail = uiDD.get_selected_dd()
        errors = Screen.ui2bdd(
            docDetail,
            self.ui_space.TabWidgetPremium,
            UIForm.form2attr_list(DocumentDetail.prem_list),
            DocumentDetail,
            uiDD.ui_prefix)

        if not errors:
            docDetail.premiumFixing.fixedQuantity = docDetail.lineQuantity
            docDetail.premiumFixing.unitOfMeasure = docDetail.lineUoM
            docDetail.premiumFixing.documentDetail = docDetail
            docDetail.premiumFixing.set_fixedPriceDocUnitPriceUoM()
            prm_price = docDetail.get_premium()
            prm_price.futurePricings = [docDetail.premiumFixing]
            prm_price.compute_future()
            if not docDetail.premiumFixing in docDetail.futurePricings:
                docDetail.futurePricings.append(docDetail.premiumFixing)

            if docDetail.is_fixed():
                update_total_price()
            update_price_ui(docDetail.prices)
            update_doc_unitPrice(docDetail)
            Screen.bdd2ui(docDetail, uiDD, uiDD.fields, uiDD.ui_prefix)
        else:
            handle_valueError(docDetail, errors,
                              self.ui_space.TabWidgetPremium, uiDD.ui_prefix)


class ScreenDocument_main04(Screen):

    def __init__(self, *p, **k):
        super(ScreenDocument_main04, self).__init__(*p, **k)
        document = self.newValue['value']
        uiTree = getattr(self.ui_space, self.klass.get_ui_prefix() + 'flow')
        self.build_tree(document.root_document(), uiTree)

        # be able to open all children
        uiTree.qtObject.itemExpanded.disconnect(uiTree.qtObject.onItemExpanded)
        self.top = uiTree.topLevelItem(0)
        self.hide_uselessQuotation(document, self.top)
        uiTree.expandItem(self.top)
        self.expand_all(self.top)

    def isLine(self, tree):
        return isinstance(tree.code, tuple) and tree.code[0] == 'detail'

    def contains_doc(self, document, tree):
        if not self.isLine(tree) and tree.code == document.id:
            return True
        else:
            ok = False
            for child in tree.childs:
                if not self.isLine(child):
                    ok = ok or self.contains_doc(document, child)
            return ok

    # take care to skip lines
    def hide_uselessQuotation(self, document, tree):
        # for the first round of recursion
        if tree.code == document.id:
                return
        else:
            for child in tree.childs:
                if child.code == document.id:
                    return
                if not self.contains_doc(document, child):
                    child.setHidden(True)
                else:
                    for child2 in child.childs:
                        self.hide_useless(document, child2)

    def hide_useless(self, document, tree):
        if not self.isLine(tree):
            if tree.code == document.id:
                return
            if not self.contains_doc(document, tree):
                tree.setHidden(True)
            else:
                for child in tree.childs:
                    self.hide_useless(document, child)

    def build_tree(self, document, uiParent):
        uiDoc = TreeWidgetItem(uiParent, document.name(), document.id)
        color = document.docType.color.split(",")
        r = int(color[0])
        g = int(color[1])
        b = int(color[2])
        brush = QtGui.QBrush(QtGui.QColor(r, g, b))
        uiDoc.setForeground(0, brush)
        for detail in document.documentDetails:
            lineParent = detail.name().split(",")
            uiDoc1 = TreeWidgetItem(
                uiDoc, detail.name(), ('detail', detail.id))
            uiDoc1.setForeground(0, brush)
            for doc in document.childrenDocumentHeader:
                self.comparison(lineParent, doc, uiDoc1)

    def comparison(self, lineParent, doc, uiParent):
        for detail in doc.documentDetails:
            lineEnfant = detail.name().split(",")
            if lineEnfant[1] == lineParent[1]:
                self.build_treeLine(doc, uiParent, lineParent)

    def build_treeLine(self, document, uiParent, line):
        uiDoc = TreeWidgetItem(uiParent, document.name(), document.id)

        color = document.docType.color.split(",")
        r = int(color[0])
        g = int(color[1])
        b = int(color[2])
        brush = QtGui.QBrush(QtGui.QColor(r, g, b))

        uiDoc.setForeground(0, brush)
        for detail in document.documentDetails:
            lineParent = detail.name().split(",")
            if line[1] == lineParent[1]:
                uiDoc2 = TreeWidgetItem(
                    uiDoc, detail.name(), ('detail', detail.id))
                uiDoc2.setForeground(0, brush)
                for doc in document.childrenDocumentHeader:
                    self.comparison(lineParent, doc, uiDoc)

    def expand_all(self, tree):
        tree.setExpanded(True)
        for child in tree.childs:
            self.expand_all(child)
