#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [Claude Huchet]
[07:08/2013]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact: contact@sysgrove.com

Role :
    This module aims to defined general classes to build ui for
simple alchemy classes, the classes that inherite only from SGMixin.
I would like to use inheritance instead of specif xml files.


This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sysgrove.ui.classes import UIDynamicButtonZoneInst

from .gen import UI01, UI, UI_main, UI_main04
from sysgrove import i18n
_ = i18n.language.ugettext


class Dyn(UIDynamicButtonZoneInst):

    def setupUi(self):
        self.cancel = self.addDynamicButton("cancel")
        self.cancel.setText(_("Cancel"))
        self.cancel.setToolTip(u"Cancel")
        self.cancel.addImage("edit-undo.svg")


class Dyn01(Dyn):

    def setupUi(self):
        super(Dyn01, self).setupUi()
        self.create = self.addDynamicButton("create")
        self.create.setText(_("Create"))
        self.create.setToolTip(u"Create")
        self.create.addImage("document-properties.svg")


class Dyn02(Dyn):

    def setupUi(self):
        super(Dyn02, self).setupUi()
        self.display = self.addDynamicButton("display")
        self.display.setText(_("Display"))
        self.display.setToolTip(u"Display")
        self.display.addImage("document-properties.svg")

Dyn03 = Dyn02
Dyn04 = Dyn02


class Dyn_main(Dyn):

    def setupUi(self):
        super(Dyn_main, self).setupUi()

        self.save = self.addDynamicButton("save")
        self.save.setText(_("Save"))
        self.save.setToolTip(u"Save")
        self.save.addImage("document-save.svg")

        self.linePlus = self.addDynamicButton("linePlus")
        self.linePlus.setText(_("Line +"))
        self.linePlus.setToolTip(u"Add document details")
        self.linePlus.addImage('sub_blue_add.svg')

        self.lineMinus = self.addDynamicButton("lineMinus")
        self.lineMinus.setText(_("Line -"))
        self.lineMinus.setToolTip(u"Remode Selected Document Details")
        self.lineMinus.addImage('sub_blue_delete.svg')

        self.printX = self.addDynamicButton("printX")
        self.printX.setText(_("Print"))
        self.printX.setToolTip(u"Print")
        self.printX.addImage('document-print.svg')

Dyn_main01 = Dyn_main
Dyn_main02 = Dyn_main
Dyn_main03 = Dyn_main


class Dyn_main04(Dyn):

    def setupUi(self):
        super(Dyn_main04, self).setupUi()

        self.exel = self.addDynamicButton("exel")
        self.exel.setText(u"Excel")
        self.exel.setToolTip(u"Exel")

        self.printX = self.addDynamicButton("printX")
        self.printX.setText(_("Print"))
        self.printX.setToolTip(u"Print")
        self.printX.addImage('document-print.svg')
