#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0


    Role :


revision # 1
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
import datetime
import locale
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove.settings import READONLY_CSS
from sysgrove.settings import (
    STATUS_WASHOUT, STATUS_STRING, STATUS_CIRCLE,
    STATUS_CANCEL, STATUS_FROZEN,
    STATUS_OPEN
)
from sysgrove.config import CONFIG
from sysgrove.utils import UserMessage, debug_trace
from sysgrove.context import kernel
from sysgrove.ui.classes import UIObject
from sysgrove.ui.classes import FocusStyledLineEdit
from sysgrove.ui.classes import ExceptionMessagePopup
from sysgrove.ui.classes import UIWidget
from sysgrove.ui.classes import LinkedTable, CheckedTable, SelectQtyTable
from sysgrove.ui.classes.table import build_columns
from sysgrove.ui.classes import UIInline
from sysgrove.ui.classes import UIDataLookupPopup
from sysgrove.ui.classes.popup import UIMaskPartial
from sysgrove.ui.classes import UIDataLookup, show_info
from sysgrove.ui.classes import GfxContent
from sysgrove.ui.classes.scrollarea import ExpandScrollArea
from sysgrove.ui.classes import UIVLayout
from sysgrove.ui.classes import UIScrollArea
from sysgrove.ui.classes import UIVSpacer
from sysgrove.ui.classes.lineedit import NumericLineEdit
from sysgrove.ui.classes.uiobject import create_icon
from sysgrove.modules.masterdata.models import PriceCondition, MarketCommodity
from sysgrove.modules.masterdata.models import UnitOfMeasure
from sysgrove.modules.masterdata.models import Status
from sysgrove.core.document.models import Price, DocumentDetail, FuturePricing
from sysgrove.core.document.models import DocumentHeader
from sysgrove.core.document.models import \
    PurchaseAndSalesDocumentLogisticLink as PASDLL
from sysgrove.ui.classes.mixins.dialog_mixin import QDialogMixin
from sysgrove.ui.classes.mixins.inline_mixin import InLineMixin
from sysgrove.ui.classes.widgets.groupBox import GroupBox
from sysgrove.core.screen import Screen, handle_valueError
from sysgrove.core.document.models import update_total_price
from sysgrove import i18n
_ = i18n.language.ugettext

log = logging.getLogger('sysgrove')


def get_uiDD():
    screen = kernel.get_current_context().screen
    ddUI = getattr(screen.ui_space,
                   screen.ui_prefix + 'documentDetails',
                   None)
    return ddUI


def get_current_dd():
    return get_uiDD().get_selected_dd()


def update_price_ui(prices):
    prices_ui = kernel.get_current_context().screen.ui_space._prices
    prices_ui.setVal(prices)


def update_doc_unitPrice(docDetail):
    docDetail.set_unitPrice()
    uiDD = get_uiDD()
    price_index = uiDD.columns.index('linePrice')
    # update the main table
    item = QtGui.QTableWidgetItem(unicode(docDetail.linePrice))
    item.setData(QtCore.Qt.UserRole,
                 QtCore.QVariant(docDetail.linePrice))
    item.setData(QtCore.Qt.DisplayRole,
                 QtCore.QVariant(locale.format('%.3f', docDetail.linePrice, grouping=True)))
    uiDD.table.setItem(uiDD.cur_index, price_index, item)
    # update the resumeTable
    uiDD.update_resumeTable()


class UIAmount(UIObject):

    def __init__(self, parent, name):
        super(UIAmount, self).__init__(parent, name)

        self.qtObject = QtGui.QWidget(parent.qtObject)
        self.layout = QtGui.QHBoxLayout(self.qtObject)
        self.layout.setSpacing(2)
        self.layout.setMargin(0)

    def setDocument(self):
        screen = kernel.get_current_context().screen
        self.document = screen.get_current_value()
        self.lineEdit = NumericLineEdit("""
                                        background-color:#ddddff;
                                        color: #000000;
                                        border: 1px solid #aaaaaa;
                                        """,
                                        """
                                        background-color:#ffffff;
                                        color: #000000;
                                        border: 1px solid #aaaaaa;
                                        """,
                                        self.parent.qtObject)

        self.lineEdit.setMinimumHeight(20)
        self.lineEdit.setMaximumHeight(20)
        # self.lineEdit.setMaximumWidth(60)
        self.lineEdit.setStyleSheet("border: 1px solid #aaaaaa;")
        self.currency = QtGui.QLabel(self.parent.qtObject)
        # self.currency.setMaximumWidth(40)
        # self.currency.setMinimumWidth(40)

        self.changedValue = NumericLineEdit("""
                                            background-color:#ddddff;
                                            color: #000000;
                                            border: 1px solid #aaaaaa;
                                            """,
                                            """
                                            background-color:#ffffff;
                                            color: #000000;
                                            border: 1px solid #aaaaaa;
                                            """,
                                            self.parent.qtObject)

        self.changedValue.setMinimumHeight(20)
        self.changedValue.setMaximumHeight(20)
        # self.changedValue.setMaximumWidth(60)
        self.changedValue.setStyleSheet("border: 1px solid #aaaaaa;")
        self.foreign_currency = QtGui.QLabel(self.parent.qtObject)

        self.layout.addWidget(self.lineEdit)
        self.layout.addWidget(self.currency)
        self.layout.addWidget(self.changedValue)
        self.layout.addWidget(self.foreign_currency)

    def setVal(self, value):
        if isinstance(value, str) or isinstance(value, unicode):
            if value == '':
                value = 0.0
            else:
                value = float(value)
        self.lineEdit.setText(value)
        self.currency.setText(self.document.site.company.currency.code)

        # if self.document.exchangeRate:
        #     changedValue = self.document.exchangeRate.exchangeRateValue * value
        #     self.foreign_currency.setText(
        #         self.document.exchangeRate.targetCurrency.code)
        # else:
        #     changedValue = 0.0
        #     self.foreign_currency.setText('   ')
        # FIXME to be review with many ExchangeRate values
        changedValue = 0.0
        self.foreign_currency.setText('   ')
        self.changedValue.setText(changedValue)

    def clear(self):
        self.lineEdit.clear()
        self.changedValue.clear()
        self.foreign_currency.clear()
        self.currency.clear()

    def setReadOnly(self, ro):
        self.lineEdit.setReadOnly(ro)
        self.lineEdit.setStyleSheet(READONLY_CSS)
        self.changedValue.setReadOnly(ro)
        self.changedValue.setStyleSheet(READONLY_CSS)


# FIXME: we have to keep ui object to keep comptibilitu with the
# other part of the programme....but, I have tryed to avaoid them
# as much as possible... so it looks rather strange.
class PriceTable(LinkedTable):

    """
        The first column (step) is used to make distinction
        between lines of the table
    """

    def __init__(self, parent, name="PriceTable", *args, **kwargs):
        klass = Price
        self.data = {}
        attr_columns = klass.list_display[:]
        attr_columns.append('id')
        super(PriceTable, self).__init__(
            parent, name,
            klass=klass,
            columns=build_columns(klass, attr_columns),
            showReadOnly=True)

    def steps(self):
        res = []
        # step are store in the first columns
        for row in xrange(self.rowCount()):
            step, ok = self.item(row, 0).data(QtCore.Qt.UserRole).toInt()
            if ok:
                res.append(step)
        return res

    def setVal(self, values):
        values = sorted(values, cmp=lambda x, y: cmp(x.step, y.step))
        super(PriceTable, self).setVal(values)
        self.sortItems(0)
        self.data = {}
        for value in values:
            self.data[value.step] = value

    def valueObject(self):
        # self.data has to be update from the table
        # we cannot use the LinkedTable valueObject methode
        # because it create new instance for object without id
        for row in xrange(self.rowCount()):
            step, ok = self.item(row, 0).data(QtCore.Qt.UserRole).toInt()
            if ok:
                for col in self.attr_columns[1:]:
                    self._cell2obj(self.data[step], row,
                                   self.attr_columns.index(col))
        return self.data.values()

    def _addRow(self, row):
        super(PriceTable, self)._addRow(row)
        self.data[row[0]] = self.klass(step=row[0])

    def removeRow(self, row):
        step, ok = self.item(row, 0).data(QtCore.Qt.UserRole).toInt()
        if ok:
            del self.data[step]
            self.qtObject.removeRow(row)


class PriceWidget(UIWidget):

    """ A special Widget to handle price in document detail
    """

    def __init__(self, *args, **kwargs):
        super(PriceWidget, self).__init__(*args, **kwargs)
        layout = QtGui.QVBoxLayout(self.qtObject)

        # lines managers
        vboxAdd = QtGui.QHBoxLayout()
        addLabel = QtGui.QLabel(u'Add step after step number')
        self.addComboBox = QtGui.QComboBox()
        vboxAdd.addWidget(addLabel)
        vboxAdd.addWidget(self.addComboBox)
        vboxAdd.addStretch(1)
        layout.addLayout(vboxAdd)
        self.addComboBox.currentIndexChanged.connect(self.add_after_step)

        vboxRemove = QtGui.QHBoxLayout()
        removeLabel = QtGui.QLabel(u'Remove Step')
        self.removeComboBox = QtGui.QComboBox()
        vboxRemove.addWidget(removeLabel)
        vboxRemove.addWidget(self.removeComboBox)
        vboxRemove.addStretch(1)
        layout.addLayout(vboxRemove)
        self.removeComboBox.currentIndexChanged.connect(self.remove_step)

        # price table
        self.priceTable = PriceTable(None)
        width = min(750, self.priceTable.width())
        self.priceTable.setMinimumWidth(width)
        layout.addWidget(self.priceTable.qtObject)
        self.priceTable.itemChanged.connect(self.priceItemChanged)

        # update buttons
        vboxUpdate = QtGui.QHBoxLayout()
        self.updatePrice = QtGui.QPushButton('Update Price')
        self.updatePrice.setStyleSheet(CONFIG['css'])
        vboxUpdate.addWidget(self.updatePrice)
        self.updatePrice.clicked.connect(self.compute)

        vboxUpdate.addStretch(1)
        layout.addLayout(vboxUpdate)

        # store the documentDetail that prices are dealing with
        self.documentDetail = None

    def priceItemChanged(self, item):
        item.setData(QtCore.Qt.UserRole, item.data(QtCore.Qt.DisplayRole))

    def add_after_step(self, index):
        step, ok = self.addComboBox.currentText().toInt()
        if ok:
            # compute a fake step to be able to add a newline in the table
            follow_step, ok = self.addComboBox.itemText(index + 1).toInt()
            if ok:
                middle = step + (follow_step - step) / 2
            else:
                middle = step + 10
            row = [""] * len(self.priceTable.columns)
            row[0] = middle
            self.priceTable.addRow(row)
            self.priceTable.sortItems(0)
            self.update_combobox_buttons()

    def remove_step(self, index):
        step, ok = self.removeComboBox.currentText().toInt()
        if ok:
            row = None
            # Look for the row corresponding to the step
            for i in xrange(self.priceTable.rowCount()):
                value, ok = self.priceTable.item(
                    i, 0).data(QtCore.Qt.UserRole).toInt()
                if ok and step == value:
                    row = i
                    break
            if row:
                self.priceTable.removeRow(row)
            self.update_combobox_buttons()
            self.priceTable.sortItems(0)

    def compute(self):
        # we need to retreive the documentDetail object in order to
        # recompute the price.
        # FIXME but it does not work if the documentDetail does not have
        # been saved.
        prices = self.priceTable.valueObject()

        if len(prices) > 0:
            if not self.documentDetail:
                raise Exception(u"unknow document Detail")
            for p in prices:
                if not p.documentDetail:
                    p.documentDetail = self.documentDetail
                else:
                    assert(p.documentDetail == self.documentDetail)
            self.documentDetail.prices = prices
            valid = True
            try:
                self.documentDetail.compute_amount()
            except Exception as e:
                valid = False
                exPopup = ExceptionMessagePopup(
                    kernel.get_current_context().screen.ui_space.qtObject,
                    e, None)
                exPopup.show()
            if valid:
                self.setVal(self.documentDetail.prices)
                update_total_price()

    def update_combobox_buttons(self):
        steps = ['']
        steps.extend(map(str, self.priceTable.steps()))
        self.addComboBox.clear()
        self.removeComboBox.clear()
        self.addComboBox.addItems(steps)
        self.removeComboBox.addItems(steps)

    def setVal(self, values):
        values = sorted(values, cmp=lambda x, y: cmp(x.step, y.step))
        self.priceTable.setVal(values)
        self.update_combobox_buttons()
        # FIXME: repartition cost have to be set here to....
        if not values == []:
            # remember the documentDetail object,
            # it is necessairy until it has not been saved.
            self.documentDetail = values[0].documentDetail

    def valueObject(self):
        values = self.priceTable.valueObject()
        return values

    def clear(self):
        self.priceTable.clear()
        self.addComboBox.clear()
        self.removeComboBox.clear()

    # Required to avoid label for inline  in formular
    def setTitle(self, title):
        pass


# special case to handle document detail priceSchemaHeader
# Special case to handle DocCopyRule filter
class UIDataLookupPopupPSH(UIDataLookupPopup):

    def valid(self, and_hide=True):
        initial = self.selectedObject is None
        super(UIDataLookupPopupPSH, self).valid(and_hide)
        if not self.selectedObject or initial:
            return
        screen = kernel.get_current_context().screen
        pricesUI = getattr(screen.ui_space, '_prices', None)

        # first update the current line with the priceSchema
        documentDetailsUI = getattr(screen.ui_space,
                                    screen.ui_prefix + 'documentDetails',
                                    None)
        cur = documentDetailsUI.get_selected_dd()
        if cur:
            cur.priceSchemaHeader = self.selectedObject
            # update the screen
            Screen.bdd2ui(cur, screen.ui_space, ['prices'], '_')
        # recompute prices
        pricesUI.compute()


class UIDataLookupPSH(UIDataLookup):

    def create_popup(self):
        self.popup = UIDataLookupPopupPSH(
            self,
            pk_field='code',
        )


# class TargetTable(LinkedTable):

#     def valueObject(self):
# retreive checked lines and the new quantity
#         index_id = len(self.columns) - 1
#         editables = self.editableColumns()
#         index_qty2link = editables[1]
#         res = []
#         nb_rows = self.rowCount()
#         for row in range(nb_rows):
#             item_check = self.item(row, 0)
#             if item_check.checkState() == QtCore.Qt.Checked:
#                 item_id, valid = self.item(row, index_id).data(
#                     QtCore.Qt.DisplayRole).toInt()
#                 if valid:
#                     line = self.klass.query.filter_by(id=item_id).one()
#                     nQty, ok = self.item(row, index_qty2link).data(
#                         QtCore.Qt.DisplayRole).toFloat()
#                     res.append((line, nQty))
#         return res


class DocLinkPopup(QtGui.QDialog, QDialogMixin):

    def __init__(self, parent, flowID, docGroup, name="linkedDD"):
        super(DocLinkPopup, self).__init__(None, QtCore.Qt.Dialog)
        self.qtObject = self

        layout = QtGui.QVBoxLayout(self)
        self.flowID = flowID
        self.docGroup = docGroup
        self.reverse_flow = flowID.reverse()
        # Setting the tables
        self.gbLinked = GroupBox(self)
        self.gbLinked.setTitle(u"%s Linked" % self.docGroup.description)
        # self.gbLinked.setStyleSheet(CONFIG['css'])
        self.gbOrigin = GroupBox(u"%s Quantity Not Yet Linked" %
                                 self.flowID.description,
                                 self)
        # self.gbOrigin.setStyleSheet(CONFIG['css'])
        self.gbTarget = GroupBox(u"%s %s To Be Linked" %
                                 (self.reverse_flow.description,
                                  self.docGroup.description),
                                 self)
        self.setTables()

        # Setting buttons
        self.styleSheetButton = """
            QWidget{
                border: none;
            }
            QToolButton{
            border: 2px solid #8f8f91;
                border-radius: 6px;
                font-weight: bold;
                font-size: 10px;
                background-color: qlineargradient(
            x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde);
             }
            QToolButton:hover{
            /*border: 2px solid #8f8f91;*/
            border: 2px solid #F5BC40;
                border-radius: 6px;
                font-weight: bold;
                font-size: 10px;
                background-color: qlineargradient(
            x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dadbde, stop: 1 #f6f7fa);
            }"""

        unlinkButton = QtGui.QToolButton()
        unlinkButton.setIcon(create_icon('unlink.png'))
        unlinkButton.setIconSize(QtCore.QSize(30, 30))
        unlinkButton.setStyleSheet(self.styleSheetButton)
        unlinkButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        unlinkButton.setText(_("Unlink"))
        unlinkButton.clicked.connect(self.unlink)

        linkButton = QtGui.QToolButton()
        linkButton.setIcon(create_icon("insert-link.svg"))
        linkButton.setIconSize(QtCore.QSize(30, 30))
        linkButton.setStyleSheet(self.styleSheetButton)
        linkButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        linkButton.setText(_("  Link  "))
        linkButton.clicked.connect(self.link)

        cancelButton = QtGui.QToolButton()
        cancelButton.setIcon(create_icon('edit-clear.svg'))
        cancelButton.setIconSize(QtCore.QSize(30, 30))
        cancelButton.setStyleSheet(self.styleSheetButton)
        cancelButton.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        cancelButton.setText(_(" Close "))
        cancelButton.clicked.connect(self.hide)

        layout.addWidget(self.gbLinked)
        unlinkLayout = QtGui.QHBoxLayout()
        unlinkLayout.addWidget(unlinkButton)
        unlinkLayout.addStretch(1)
        layout.addLayout(unlinkLayout)
        layout.addWidget(self.gbOrigin)
        layout.addWidget(self.gbTarget)
        buttonsLayout = QtGui.QHBoxLayout()
        buttonsLayout.addWidget(linkButton)
        buttonsLayout.addWidget(cancelButton)
        buttonsLayout.addStretch(1)
        layout.addLayout(buttonsLayout)

        self.resize(800, 400)
        self.actu = True

    def setTables(self):
        initial = u"%s%s" % (self.flowID.initial(), self.docGroup.initial())
        reverse_initial = u"%s%s" % (
            self.reverse_flow.initial(), self.docGroup.initial())
        # linked Table
        linked_columns = [
            (u"", bool, True, False),
            (u"%s #" % initial, int, False, False),
            (u"%s Line" % initial, int, False, False),
            (DocumentDetail.itemDetail.info['verbose_name'],
             str, False, False),
            (DocumentDetail.lineQuantity.info[
             'verbose_name'], float, False, False),
            (DocumentDetail.lineUoM.info['verbose_name'], str, False, False),
            (DocumentDetail.linePrice.info['verbose_name'],
             float, False, False),
            (DocumentDetail.lineCurrency.info[
             'verbose_name'], str, False, False),
            (u"%s Type" % reverse_initial, str, False, False),
            (u"%s #" % reverse_initial, str, False, False),
            (u"%s Line #" % reverse_initial, int, False, False),
            (u"%s Line Qty" % reverse_initial, float, False, False),
            (u"Qty linked", float, False, False),
            ('id', int, False, True)
        ]
        tmp = [[u[i] for u in linked_columns] for i in xrange(4)]
        tmp.append(PASDLL.linked_list_columns(self.flowID))
        linked_columns = zip(*tmp)

        self.linkedTable = LinkedTable(
            None, 'linkedTable',
            selectAll=0,
            columns=linked_columns,
            klass=PASDLL)
        linkedLayout = QtGui.QVBoxLayout()
        linkedLayout.addWidget(self.linkedTable.qtObject)
        self.gbLinked.setLayout(linkedLayout)

        # origin table
        origin_columns = [
            (u"", bool, True, False, ""),
            (u"%s #" % initial, int, False, False, 'lineNumber'),
            (DocumentDetail.itemDetail.info['verbose_name'],
             str, False, False, 'itemDetail'),
            (DocumentDetail.lineQuantity.info['verbose_name'],
             float, False, False, 'lineQuantity'),
            (DocumentDetail.lineUoM.info['verbose_name'],
             str, False, False, 'lineUoM'),
            (DocumentDetail.linePrice.info['verbose_name'],
             float, False, False, 'linePrice'),
            (DocumentDetail.lineCurrency.info['verbose_name'],
             str, False, False, 'lineCurrency'),
            (u"Qty linked", float, False, False, 'lineQtyLinked'),
            (u"Remaining Qty to be linked", float,
             False, False, 'lineQtyNYLinked'),
            ("id", int, False, True, 'id')
        ]

        self.originTable = CheckedTable(None, 'originTable',
                                        klass=DocumentDetail,
                                        columns=origin_columns)

        originLayout = QtGui.QVBoxLayout()
        originLayout.addWidget(self.originTable.qtObject)
        self.gbOrigin.setLayout(originLayout)

        # Target table
        target_columns = [
            (u"", bool, True, False, ""),
            (u"%s #" % reverse_initial, int, False,
             False, 'documentHeader_documentNumber'),
            (u"%s Line" % reverse_initial, int, False, False, 'lineNumber'),
            (DocumentDetail.itemDetail.info['verbose_name'],
             str, False, False, 'itemDetail'),
            (u"%s Quantity" %
             reverse_initial, float, False, False, 'lineQuantity'),
            (DocumentDetail.lineUoM.info['verbose_name'],
             str, False, False, 'lineUoM'),
            (DocumentDetail.linePrice.info['verbose_name'],
             float, False, False, 'linePrice'),
            (DocumentDetail.lineCurrency.info['verbose_name'],
             str, False, False, 'lineCurrency'),
            (u"%s Qty linked" %
             reverse_initial, float, False, False, 'lineQtyLinked'),
            (u"%s Remaining Qty to be linked" %
             reverse_initial, float, False, False, 'lineQtyNYLinked'),
            (u"%s Qty to Link" % reverse_initial, float, True, False, ''),
            ("id", int, False, True, 'id')

        ]
        self.targetTable = SelectQtyTable(None, 'targetTable',
                                          klass=DocumentDetail,
                                          columns=target_columns)
        targetLayout = QtGui.QVBoxLayout()
        targetLayout.addWidget(self.targetTable.qtObject)
        self.gbTarget.setLayout(targetLayout)

    @show_info
    def link(self, *args, **kwargs):
        # first check that exactly one line in originTable  is checked
        checked = self.originTable.valueObject()
        if len(checked) == 0:
            raise UserMessage(u"Nothing checked")
        if len(checked) > 1:
            raise UserMessage(u"Too many document details checked")
        # this case really save
        dd = checked[0]
        flow = dd.documentHeader.docType.flowID
        targets = self.targetTable.valueObject()
        for target in targets:
            target_line, qty = target
            PASDLL.add_link(dd, flow, target_line, qty)
        # adjuste the tables
        self.update_tables()

    def unlink(self):
        checked = self.linkedTable.valueObject()
        for link in checked:
            link.cancel()
            link.add()
        # adjuste the tables
        self.update_tables()

    def show(self):
        self.update_tables()
        super(DocLinkPopup, self).show()

    def update_tables(self):
        screen = kernel.get_current_context().screen
        self.document = screen.document
        uiDetails = getattr(screen.ui_space,
                            screen.ui_prefix + 'documentDetails', None)
        if uiDetails and uiDetails.get_selected_dd():
            docDetails = [uiDetails.get_selected_dd()]
        else:
            docDetails = uiDetails.valueObject()

        pasdll = []
        for detail in docDetails:
            pasdll.extend(PASDLL.getLinked(self.flowID, detail))
        linkable = []
        for detail in docDetails:
            linkable.extend(detail.linkableDetails())
        self.linkedTable.setVal(pasdll)
        self.originTable.setVal(docDetails)
        self.targetTable.setVal(linkable)


# DocumentDetail Inline
def sort_lines(values):
    return sorted(values, lambda x, y: cmp(x.lineNumber, y.lineNumber))


class UIInlineDDPopup(InLineMixin, QtGui.QDialog):

    def __init__(self, parent,
                 title=u"Add Document Details", name="inlineDD"):
        super(UIInlineDDPopup, self).__init__(parent.qtObject)
        #super(UIInlineDDPopup, self).__init__(None)
        self.qtObject = self  # FIXME should be done is super?

        self.layout = QtGui.QVBoxLayout(self)
        # settings the form
        (self.uiform, self.fields) = parent.buildUIForm(
            self,
            name,
            parent.klass.list_display)
        uiLineNumber = getattr(
            self.uiform, parent.ui_prefix + 'lineNumber', None)
        if uiLineNumber:
            uiLineNumber.hide()
        self.layout.addWidget(self.uiform.qtObject)

        # the title is inherited, we just need to set the text
        self.setWindowTitle(title)
        actu = True
        # Setting buttons
        self.buttons = QtGui.QWidget(self)
        self.layout.addWidget(self.buttons)
        self.buttonsLayout = QtGui.QHBoxLayout(self.buttons)

        size = self.uiform.sizeHint()
        # self.setMinimumSize(self.uiform.sizeHint())
        self.resize(size.width(), size.height() + 70)
        self.setModal(True)
        self.setStyleSheet("""
        QWidget{
            border: 2px solid #F5BC40;
            font-weight: bold;
         }
         QScrollBar{
            border: none;
         }
         QLineEdit{
           background-color: #ffffff;
           border: 1px solid #aaaaaa;
         }
         """)
        self.parentUIObject = parent
        self.actu = True

        # set defaults
        self.defaults = [
            ('', UnitOfMeasure.master().code, 'lineUoM'),
            ('itemDetail', 'uoMCodeDocument', 'lineUoM'),
            ('', parent.document.currency.code, 'lineCurrency'),
            ('itemDetail', 'currency', 'lineCurrency'),
            ('', UnitOfMeasure.master().code, 'linePriceUoM'),
            ('itemDetail', 'uoMCodePrice', 'linePriceUoM'),
            ('', datetime.date.today(), 'reqDeliveryStart'),
            ('', datetime.date.today(), 'reqDeliveryEnd'),
            ('', Status.query.filter_by(id=STATUS_OPEN).one(), 'status'),
        ]

        Screen.setDefaults(self.uiform, self.defaults, parent.ui_prefix)

    def show(self):
        super(UIInlineDDPopup, self).show()
        self.mask = UIMaskPartial(GfxContent, "mask", self)
        self.mask.show()
        self.inlineExist = True

    def hide(self):
        super(UIInlineDDPopup, self).hide()
        self.mask.hide()
        self.inlineExist = False

    def closeEvent(self, event):
        self.mask.hide()
        super(UIInlineDDPopup, self).closeEvent(event)
        self.inlineExist = False

    def getFirsInline(self):
        return self

    def check_status(self, cur):
        mess = u''
        if not cur.itemDetail:
            mess = mess + u"ItemDetail is mandatory\n"
        if not cur.lineQuantity:
            mess = mess + u"Line Qty is mandatory\n"
        if cur.status.id is STATUS_CANCEL and cur.lineQtyLinked > 0:
            mess = mess + u"Cannot set the Cancel status because of linked Qty"
        if not mess == u"":
            raise UserMessage(mess)

    def check_unitPrice(self, cur):
        if (not cur.documentHeader.futureFixingFlag or
                not cur.documentHeader.priceFlag):
            if not cur.linePrice:
                cur.documentHeader = None
                self.parentUIObject.details.remove(cur)
                raise UserMessage(u"Unit Price is mandatory")

    def check_uom(self, old_uom, cur):
        if not old_uom == cur.lineUoM.id and cur.lineQtyLinked > 0:
            raise UserMessage(
                u"Cannot change Unit Of Measure. Please inlink Qty first")

    def check_warehouse(self, cur):
        if not cur.warehouse:
            if (cur.documentHeader.docType.lotFlag and
                    not cur.itemDetail.itemGroup.itemFamily.serviceFlag):
                docType = cur.documentHeader.docType.code
                cur.documentHeader = None
                self.parentUIObject.details.remove(cur)
                raise UserMessage(
                    "Warehouse is mandatory for document type %s" % docType)


class UIInlineCreateDDPopup(UIInlineDDPopup):

    def __init__(self, parent, name="inlineCreateDD"):

        super(UIInlineCreateDDPopup, self).__init__(
            parent,
            name=name + parent.name,
            title=u"New Document Details")

        # Setting buttons
        self.more = QtGui.QCheckBox(self.buttons)
        self.more.setText(_('Create another'))

        self.addButton = QtGui.QToolButton(self.buttons)
        self.addButton.setText(_("Create"))

        self.cancelButton = QtGui.QToolButton(self.buttons)
        self.cancelButton.setText(_("Cancel"))

        self.buttonsLayout.addWidget(self.more)
        self.buttonsLayout.addWidget(self.addButton)
        self.buttonsLayout.addWidget(self.cancelButton)

        self.cancelButton.clicked.connect(self.cancel)
        self.addButton.clicked.connect(self.valid)
        self.new_dd = None

    @show_info
    def cancel(self, *args, **kwargs):
        if self.new_dd:
            self.parentUIObject.details.remove(self.new_dd)
            Screen.bdd2uiTable(self.parentUIObject.klass,
                               self.parentUIObject.table,
                               self.parentUIObject.details)

            self.new_dd = None
            try:
                update_total_price()
            except Exception as e:
                raise UserMessage(e.message)
        self.hide()

    @show_info
    def valid(self, *args, **kwargs):
        cur = self.parentUIObject.klass()
        errors = Screen.ui2bdd(cur, self.uiform, self.fields,
                               self.parentUIObject.klass,
                               self.parentUIObject.ui_prefix)
        if errors:
            handle_valueError(cur, errors, self.uiform,
                              self.parentUIObject.ui_prefix)
            return

        # adjust lineNumber
        nb_line = len(self.parentUIObject.details)
        if nb_line == 0:  # First line
            cur.lineNumber = 1
        else:
            cur.lineNumber = self.parentUIObject.details[
                nb_line - 1].lineNumber + 1

        self.check_status(cur)
        cur.documentHeader = self.parentUIObject.document
        self.parentUIObject.details.append(cur)
        self.check_unitPrice(cur)
        self.check_warehouse(cur)

        # keep a ref in case a cancel is required
        self.new_dd = cur

        # update price
        try:
            cur.set_basic_price()
        except Exception as e:
            raise UserMessage(e.message)

        # refill the table
        Screen.bdd2uiTable(self.parentUIObject.klass,
                           self.parentUIObject.table,
                           self.parentUIObject.details)
        self.parentUIObject.clearfields(self.uiform, self.fields)
        self.parentUIObject.clearfields(self.parentUIObject.uiform,
                                        self.parentUIObject.fields)

        # update documentHeader total price
        # take care, update_total_price use the current state
        # of the self.parentUIObject.table
        try:
            update_total_price()
        except Exception as e:
            raise UserMessage(e.message)

        if not self.more.isChecked():
            self.hide()
        else:
            # refill the default
            Screen.setDefaults(self.uiform,
                               self.defaults,
                               self.parentUIObject.ui_prefix)

    def show(self):
        super(UIInlineCreateDDPopup, self).show()
        Screen.setDefaults(self.uiform,
                           self.defaults,
                           self.parentUIObject.ui_prefix)
        self.parentUIObject.clear_resumeTable()


class UIInlineUpdateDDPopup(UIInlineDDPopup):

    def __init__(self, parent, name="inlineUpdateDD"):

        super(UIInlineUpdateDDPopup, self).__init__(
            parent,
            name=name + parent.name,
            title=u'Update Document Detail')

        # Setting buttons
        self.updateButton = QtGui.QToolButton(self.buttons)
        self.updateButton.setText(_("Update"))
        self.cancelButton = QtGui.QToolButton(self.buttons)
        self.cancelButton.setText(_("Cancel"))
        self.buttonsLayout.addWidget(self.updateButton)
        self.buttonsLayout.addWidget(self.cancelButton)
        self.cancelButton.clicked.connect(self.hide)
        self.updateButton.clicked.connect(self.update)

    @show_info
    def update(self, *args, **kwargs):
        cur = self.parentUIObject.get_selected_dd()
        if cur:
            index = self.parentUIObject.details.index(cur)
            old_uom = cur.lineUoM.id
            errors = Screen.ui2bdd(cur, self.uiform, self.fields,
                                   self.parentUIObject.klass,
                                   self.parentUIObject.ui_prefix)
            if errors:
                handle_valueError(cur, errors, self.uiform,
                                  self.parentUIObject.ui_prefix)
            else:
                self.check_uom(old_uom, cur)
                self.check_status(cur)
                cur.documentHeader = self.parentUIObject.document
                self.parentUIObject.details[index] = cur
                self.check_unitPrice(cur)
                self.check_warehouse(cur)
                # uptate the table
                Screen.bdd2uiTable(self.parentUIObject.klass,
                                   self.parentUIObject.table,
                                   self.parentUIObject.details)

                # update the bottom part
                self.parentUIObject.update_resumeTable()
                Screen.bdd2ui(cur, self.parentUIObject.uiform,
                              self.parentUIObject.fields,
                              self.parentUIObject.ui_prefix)
                update_total_price()
                self.parentUIObject.clearfields(self.uiform, self.fields)
                self.hide()


class UIInlineDD(UIInline):

    def __init__(self, parent, name):
        self.document = Screen.get_current_value()
        self.details = sort_lines(self.document.documentDetails)

        self.popup = None
        self.popupUpdate = None
        self.links_popup = None

        # Be Carefull, we do need to handle qtObject
        super(UIInline, self).__init__(parent, name)
        self.cur_index = None
        self.table = None
        self.ui_prefix = '_'

        self.qtObject = ExpandScrollArea(parent.qtObject)
        self.qtObject.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.qtObject.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.layout = UIVLayout(self, 'InLIneVLayout%s' % name)
        self.qtObject.setLayout(self.layout.qtObject)

        self.klass = DocumentDetail
        self.setAlchemyClass(self.klass)
        self.buildUI()
        self.inlineExist = True

    def buildUI(self):
        # settings the form for the popup
        self.popup = UIInlineCreateDDPopup(self)
        self.popupUpdate = UIInlineUpdateDDPopup(self)

        self.buildTable()
        self.layout.addWidget(self.table)

        bottom_scrollArea = UIScrollArea(self.parent, 'bottomScrollArea')
        widget = UIWidget(bottom_scrollArea, 'Details')
        widgetlayout = UIVLayout(widget, 'DatailsLayout')

        bottom_scrollArea.view(widget)

        # Build the resume Table
        self.resumeTable = LinkedTable(self,
                                       "table_%sLine" % self.klass.__name__,
                                       klass=self.klass)
        # self.resumeTable.setColumnHidden(self.columns.index('id'), True)
        self.resumeTable.setMaximumHeight(65)
        widgetlayout.addWidget(self.resumeTable)
        spacer = UIVSpacer(self, 'spacer')
        widgetlayout.addSpacerItem(spacer)

        # Build the detail uiform
        (self.uiform, self.fields) = self.buildUIForm(
            self,
            form=self.klass.list_detail)

        # update some part of the form
        for qty in getattr(self.klass, 'qty_resume_list'):
            uiObject = getattr(self.uiform, self.ui_prefix + qty)
            uiObject.setReadOnly(True)

        # update also the fields list
        if not self.document.premium_flat:
            self.fields = filter(lambda x: not x.startswith('premiumFixing'),
                                 self.fields)

        widgetlayout.addWidget(self.uiform)

    def update_resumeTable(self):
        row = self.table.rowObject(self.cur_index)
        self.resumeTable.clear()
        self.resumeTable.setVal([row])

    def clear_resumeTable(self):
        self.keep_cur_details()
        self.resumeTable.clear()
        self.cur_index = None

    def clickOnTableLine(self, index):
        # first you have to keep the previous modifs if any
        if not index.isValid():
            return
        self.keep_cur_details()
        self.clearfields(self.uiform, self.fields)
        self.cur_index = index.row()
        self.update_resumeTable()
        cur = self.get_selected_dd()
        if cur:
            Screen.bdd2ui(cur, self.uiform,
                          self.fields, self.ui_prefix)
            # default coming from de documentDetail
            other_default = [
                ('', lambda: cur.lineUoM.code, 'unitOfMeasure'),
                ('', lambda: datetime.date.today(), 'fixingDate')
            ]
            if cur.itemDetail.marketCommodity:
                other_default.extend([
                    ('',
                     lambda: cur.itemDetail.marketCommodity.unitOfMeasure.code,
                     'premiumFixing_priceUoM'),
                    ('',
                     lambda: cur.itemDetail.marketCommodity.currency.code,
                     'premiumFixing_currency'),
                ])
            Screen.setDefaults(self.uiform, other_default, self.ui_prefix)

    def doubleClickOnTableLine(self, index):
        # the index has already been handled by simple click action
        cur = self.get_selected_dd()
        if cur:
            Screen.bdd2ui(cur, self.popupUpdate.uiform,
                          self.popupUpdate.fields, self.ui_prefix)
            self.popupUpdate.show()

    def itemChangedTable(self, item):
        print 'itemChange', item

    def dataChangedTable(self, item1, item2):
        print 'dataChange', item1, item2

    def keep_cur_details(self):
        if isinstance(self.cur_index, int):
            cur = self.get_selected_dd()
            if cur:
                index = self.details.index(cur)
                fields = self.fields[:]
                # removed inline
                fields.remove('futurePricings')
                errors = Screen.ui2bdd(cur, self.uiform, fields,
                                       self.klass, self.ui_prefix)
                # I could be possible that some other fields
                # not the screen have to be updated or saved too
                if cur.documentHeader.premium_flat:
                    if not cur.premiumFixing.priceCondition:
                        cur.premiumFixing.priceCondition =  \
                            PriceCondition.prm_condition()
                        cur.premiumFixing.price = cur.get_premium()
                        cur.premiumFixing.documentDetail = cur
                if errors:
                    handle_valueError(cur, errors, self.uiform,
                                      self.ui_prefix)
                cur.documentHeader = self.document
                self.details[index] = cur

    def get_selected_dd(self):
        if isinstance(self.cur_index, int):
            line_nb, ok = self.table.item(self.cur_index, 0)\
                                    .data(QtCore.Qt.UserRole).toInt()
            if ok:
                cur = filter(lambda x: x.lineNumber == line_nb, self.details)
                if len(cur) == 1:
                    return cur[0]
        else:
            return None

    def removeSelectedDD(self):
        cur = self.get_selected_dd()
        if not cur:
            raise Exception('Inline Update')
        # suppr_index = self.val.index(cur)
        if cur.childrenDocumentDetail == []:
            self.details.remove(cur)
        else:
            raise Exception(u"""
                Cannot delete line %s it occurs
                in other documents""" % cur.lineNumber)
        self.resumeTable.clear()
        self.setVal(self.details)
        self.cur_index = None
        self.clearfields(self.uiform, self.fields)
        update_total_price()

    def setVal(self, values):
        # self.details are always sorted by lineNumber
        self.details = sort_lines(values)
        super(UIInlineDD, self).setVal(self.details)
        self.table.sortItems(0)

    def valueObject(self):
        self.keep_cur_details()
        return self.details

    def clear(self):
        self.resumeTable.clear()
        super(UIInlineDD, self).clear()


# special case for FutureFixing because
# the formular use attr in many place so some
# uiObject have the same name
class InlineFixing(UIInline):

    def __init__(self, parent, name):
        super(InlineFixing, self).__init__(parent, name,
                                           klass=FuturePricing,
                                           scrolling=False)
        # remove multiple values
        self.fields = list(set(self.fields))

        self.contents = {
            self.GroupBoxFuturePricingfuture_fixing:
            PriceCondition.future_condition(),
            self.GroupBoxFuturePricingpremium_fixing:
            PriceCondition.prm_condition(),
            self.GroupBoxFuturePricingforex_fixing:
            PriceCondition.forex_condition(),
            self.GroupBoxFuturePricingspread_fixing:
            PriceCondition.spread_condition()
        }
        self.rev_contents = {v: k for k, v in self.contents.iteritems()}

        self.info_contents = {
            self.GroupBoxFuturePricingpremiun_info:
            PriceCondition.prm_condition(),
            self.GroupBoxFuturePricingforex_info:
            PriceCondition.forex_condition(),
            self.GroupBoxFuturePricingspread_info:
            PriceCondition.spread_condition(),
            self.GroupBoxFuturePricingfuture_info:
            PriceCondition.future_condition()
        }

        # set datalookups and some readOnly Flat
        for content in self.contents:
            for f in self.klass.list_fixing:
                Screen.addDataLookUpLink(f, content,
                                         self.fields,
                                         self.klass,
                                         self.ui_prefix)
            for f in self.klass.list_readonly:
                getattr(content, self.ui_prefix + f).setReadOnly(True)

        # set default
        defaults = [
            ('marketType', 'unitOfMeasure', 'priceUoM'),
            ('marketType', 'currency', 'currency'),
            ('', '', 'spreadMonth'),
            ('', '', 'marketMonth'),

        ]
        Screen.setDefaults(self, defaults, self.ui_prefix)

    def has_value(self, groupBox):
        for f in ['fixedQuantity', 'fixedPrice']:
            uiObject = getattr(groupBox, self.ui_prefix + f, None)
            if uiObject.value():
                return True
        return False

    @show_info
    def add(self, uiform, fields, blankLine=0):
        docDetail = get_current_dd()
        new_fixing = []
        for content in self.contents:
            if self.has_value(content):
                # the futurePrice has to build in two times
                cur = self.klass()
                cur.documentDetail = docDetail
                errors = Screen.ui2bdd(cur, content,
                                       self.klass.list_fixing,
                                       self.klass,
                                       self.ui_prefix)
                if not errors:
                    cur.priceCondition = self.contents[content]
                    cur.price = docDetail\
                        .get_futurePrice(pc=self.contents[content])
                    cur.price.futurePricings.append(cur)
                    docDetail.futurePricings.append(cur)
                    errors = Screen.ui2bdd(cur, self.uiform,
                                           self.klass.list_common,
                                           self.klass,
                                           self.ui_prefix)

                if not errors:
                    cur.set_fixedPriceDocUnitPriceUoM()
                    values = self.valueObject()[:]
                    values.append(cur)
                    Screen.bdd2uiTable(self.klass, self.table, values)
                    new_fixing.append(cur)
                else:
                    handle_valueError(cur, errors, self.uiform,
                                      self.ui_prefix)

            self.clearfields(content, self.klass.list_clear)

        if len(new_fixing) > 0:
            self.update_info(self.valueObject())
            if docDetail.is_fixed():
                update_total_price()
            update_price_ui(docDetail.prices)
            update_doc_unitPrice(cur.documentDetail)

        self.clearfields(uiform, self.klass.list_clear)

    @show_info
    def update(self, uiform, fields, *args, **kwargs):
        if isinstance(self.cur_index, int):
            values = self.valueObject()[:]
            cur = values[self.cur_index]
            content = self.rev_contents[cur.priceCondition]

            errors = Screen.ui2bdd(cur, content,
                                   self.klass.list_fixing,
                                   self.klass,
                                   self.ui_prefix)
            if not errors:
                    errors = Screen.ui2bdd(cur, self.uiform,
                                           self.klass.list_common,
                                           self.klass,
                                           self.ui_prefix)
            if not errors:
                cur.set_fixedPriceDocUnitPriceUoM()
                Screen.bdd2uiTable(self.klass, self.table, values)
                self.update_info([cur])
                self.clearfields(uiform, self.klass.list_clear)
                if cur.documentDetail.is_fixed():
                    update_total_price()
                update_price_ui(cur.documentDetail.prices)
                update_doc_unitPrice(cur.documentDetail)
            else:
                handle_valueError(cur, errors, self.uiform,
                                  self.ui_prefix)

    @show_info
    def delete(self, *args, **kwargs):
        cur = super(InlineFixing, self).delete()
        if cur:
            cur.price.futurePricings.remove(cur)
            cur.documentDetail.futurePricings.remove(cur)
            self.update_info(self.valueObject())
            update_total_price(cur.documentDetail)
            update_price_ui(cur.documentDetail.prices)
            update_doc_unitPrice()
        self.clearfields(uiform, self.klass.list_clear)

    def clickOnTableLine(self, index):
        self.cur_index = index.row()
        cur = self.table.rowObject(self.cur_index)
        content = self.rev_contents[cur.priceCondition]
        Screen.uiTable2ui(content,
                          self.table,
                          index,
                          self.ui_prefix,
                          self.klass,
                          self.klass.list_fixing + self.klass.list_readonly)
        Screen.uiTable2ui(self.uiform,
                          self.table,
                          index,
                          self.ui_prefix,
                          self.klass,
                          self.klass.list_common,
                          reset=False)

    def update_info(self, values):
        for content, pc in self.info_contents.iteritems():
            fixed = [x for x in values if x.priceCondition == pc]
            if len(fixed) > 0:
                fixed[0].price.compute_future()
                Screen.bdd2ui(fixed[0], content,
                              self.klass.list_info,
                              self.ui_prefix)
            else:
                Screen.reset_fields(content,
                                    self.klass.list_info,
                                    self.ui_prefix)

    def setVal(self, values):
        super(InlineFixing, self).setVal(values)
        # filll the info boxes
        self.update_info(values)


# special case to handle market in future pricing
class DataLookupMarketPopup(UIDataLookupPopup):

    def handle_defaults(self, ui_space):
        marketCommodity = MarketCommodity\
            .query\
            .filter_by(marketType_id=self.selectedObject.id)\
            .filter_by(itemGroup_id=get_current_dd().itemDetail.itemGroup.id)\
            .one()
        if hasattr(ui_space.parent, 'contents'):
            contents = ui_space.parent.contents
            for (attr, target) in self.parentUIObject.defaults:
                for content in contents:
                    uiTarget = getattr(content,
                                       self.parentUIObject.linkPrefix + target,
                                       None)
                    selectedAttr = getattr(marketCommodity, attr, None)
                    uiTarget.setVal(selectedAttr.code)
        else:
            # handle the premium flat case
            for (attr, target) in self.parentUIObject.defaults:
                uiTarget = getattr(ui_space,
                                   self.parentUIObject.linkPrefix + target,
                                   None)
                selectedAttr = getattr(marketCommodity, attr, None)
                uiTarget.setVal(selectedAttr.code)

    def valid(self, and_hide=True):
        super(DataLookupMarketPopup, self).valid(and_hide)
        if self.selectedObject:
            marketCommodity = MarketCommodity\
                .query\
                .filter_by(marketType_id=self.selectedObject.id)\
                .filter_by(itemGroup_id=
                           get_current_dd().itemDetail.itemGroup.id)\
                .one()
            ui_space = self.parentUIObject.getFirsInline()
            uiMonth = getattr(ui_space,
                              ui_space.ui_prefix + 'marketMonth',
                              None)
            uiSpread = getattr(ui_space,
                               ui_space.ui_prefix + 'spreadMonth',
                               None)

            docDetail = get_current_dd()
            date = docDetail.reqDeliveryStart
            spread = docDetail.spread_month
            uiMonth.setVal(marketCommodity.select_month(date, spread))
            uiSpread.setVal(marketCommodity.select_spread_month(date, spread))


class DataLookupMarket(UIDataLookup):

    def create_popup(self):
        self.popup = DataLookupMarketPopup(
            self,
            pk_field='code',
        )

    def values(self, linkedUI, pk_field='code'):
        docDetail = get_current_dd()
        query = MarketCommodity\
            .query\
            .filter_by(itemGroup_id=docDetail.itemDetail.itemGroup.id)\
            .all()
        return map(lambda m: m.marketType, query)


# UICombox does not the job, as setLinks is call too late
class UIStatus(UIObject):

    def __init__(self, parent_ui_component, name):
        super(UIStatus, self).__init__(parent_ui_component, name)
        self.qtObject = QtGui.QComboBox(parent_ui_component.qtObject)
        self.setObjectName(name)
        self.klass = Status
        for obj in self.query().all():
            self.addItem(obj.description, QtCore.QVariant(obj))
        self.setCurrentIndex(-1)
        self.currentIndexChanged.connect(self.onCurrentIndexChanged)

    def query(self):
        return self.klass.query

    def clear(self):
        pass

    def value(self):
        return self.currentText()

    def valueObject(self):
        return self.itemData(self.currentIndex()).toPyObject()

    def setVal(self, val):
        if val:
            if isinstance(val, unicode):
                val = self.klass.query.filter_by(code=val).one()
            self.setCurrentIndex(self.findText(val.description))

    def onCurrentIndexChanged(self, val):
        newstatus = self.valueObject()
        if not newstatus:
            newstatus = self.klass.query.filter_by(id=STATUS_OPEN).one()
        model = self.model()
        for i in xrange(self.count()):
            model.item(i).setEnabled(True)

        if newstatus.internalFlag:
            for i in xrange(self.count()):
                model.item(i).setEnabled(False)
        else:
            for i in xrange(self.count()):
                item = model.item(i)
                status = item.data(QtCore.Qt.UserRole).toPyObject()
                if status.internalFlag:
                    item.setEnabled(False)

    def setLink(self, klass, prefix, slave=None,
                masters=[], filters={}, order={}):
        self.linkPrefix = prefix
        self.linkSlave = slave
        self.linkMasters = masters
        self.linkFilters = filters
        self.linkOrder = order
        context = kernel.get_current_context()
        if masters:
            for master in masters:
                uiMaster = getattr(
                    context.screen.ui_space, prefix + master[0], None)
                if uiMaster and isinstance(uiMaster, UIDataLookup):
                    uiMaster.linkedUIObject = self


class DHStatus(UIStatus):

    def onCurrentIndexChanged(self, val):
        super(DHStatus, self).onCurrentIndexChanged(val)
        newstatus = self.valueObject()
        if not newstatus:
            return
        document = Screen.get_current_value()
        oldstatus = document.status
        for dd in document.documentDetails:
            if dd.status == oldstatus:
                dd.status = newstatus
        uiDD = get_uiDD()
        Screen.bdd2uiTable(uiDD.klass,
                           uiDD.table,
                           document.documentDetails)
        uiDD.setVal(document.documentDetails)
        # If a documentDetail has already been selected,
        # then the bottom part need to be update
        # (because lineQuantityOpen can change)

        # update the bottom part
        uiDD.update_resumeTable()
        cur = uiDD.get_selected_dd()
        Screen.bdd2ui(cur, uiDD.uiform,
                      uiDD.fields,
                      uiDD.ui_prefix)
        # update price?
        update_total_price()


class DDStatus(UIStatus):

    def __init__(self, parent_ui_component, name):
        super(DDStatus, self).__init__(parent_ui_component, name)

    def query(self):
        return self.klass.query\
            .filter_by(headerOnlyFlag=False)
