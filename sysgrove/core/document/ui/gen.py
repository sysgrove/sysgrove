#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [Claude Huchet]
[07:08/2013]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact: contact@sysgrove.com

Role :
    This module aims to defined general classes to build ui for
simple alchemy classes, the classes that inherite only from SGMixin.
I would like to use inheritance instead of specif xml files.


This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import datetime
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s
from sysgrove.settings import STATUS_OPEN, STATUS_PARTEXEC
from sysgrove.settings import ROUNDING_MIN
from sysgrove.utils import retreive_class, debug_trace
from sysgrove.ui.classes import (
    UIWidget, UIGroupBox, UIForm,
    UIVLayout, UIHLayout, UIGridLayout,
    UIVSpacer, UIHSpacer,
    UILabel, UIDataLookup,
    UIButtonBarZone, UIButtonBar, UIButtonBarContent,
    UIDataLookupDocument, UIDataLookupDocument_O_PE,
    UIFileDialog,
    UIScrollArea,  UIVSplitter,
    UIDataLookupSiteWithDCR, UIGroupBoxWM,
    UITree, SelectQtyTable,
    ExceptionMessagePopup
)
from sysgrove.core.document.ui.customs import UIInlineDD
from sysgrove import i18n
_ = i18n.language.ugettext


class DDLinkedTable2(SelectQtyTable):

    def __init__(self, parent, name):
        from sysgrove.core.document.models import DocumentDetail
        self.klass = DocumentDetail
        self.columns = [
            (u"", bool, True, False, ""),
            (self.klass.lineNumber.info['verbose_name'],
             int, False, False, 'lineNumber'),
            (self.klass.itemDetail.info['verbose_name'],
             str, False, False, 'itemDetail'),
            (u"Description", str, False, False, 'itemDetail_description'),
            (self.klass.lineQuantity.info['verbose_name'],
             float, False, False, 'lineQuantity'),
            (self.klass.lineUoM.info['verbose_name'],
             str, False, False, 'lineUoM'),
            (u"Renaming Qty to copy", float, False, False, 'lineQuantityOpen'),
            (u"Qty to copy", float, True, False, ''),
            (self.klass.linePrice.info['verbose_name'],
             float, False, False, 'linePrice'),
            (self.klass.lineCurrency.info['verbose_name'],
             str, False, False, 'lineCurrency'),
            (self.klass.warehouse.info['verbose_name'],
             str, False, False, 'warehouse'),
            (u'Delivery Start Date', datetime.date,
             False, False, 'calcDeliveryStart'),
            (u'Delivery End Date', datetime.date,
             False, False, 'calcDeliveryEnd'),
            (self.klass.status.info['verbose_name'],
             str, False, False, 'status'),
            ("id", int, False, True, 'id')
        ]
        super(DDLinkedTable2, self).__init__(parent, name, klass=self.klass,
                                             columns=self.columns,
                                             selectAll=0)
        self.cellChanged.connect(self.onCellChanged)
        self.openQty_index = 6
        self.qtyToCopy_index = 7
        self.changeRed.connect(self.redBackground)

    def setVal(self, values):
        # we keep only not frozen lines
        selectedValues = []
        for value in values:
            if value.status.id in [STATUS_OPEN, STATUS_PARTEXEC]:
                selectedValues.append(value)
        super(DDLinkedTable2, self).setVal(selectedValues)
        self.setMinimumHeight(self.height())

    def redBackground(self, row, col, red):
        item = self.item(row, col)
        if item:
            self.cellChanged.disconnect(self.onCellChanged)
            if red:
                item.setBackground(QtGui.QBrush(QtCore.Qt.red))
            else:
                item.setBackground(QtGui.QBrush(QtCore.Qt.white))
            item.setSelected(False)
            self.cellChanged.connect(self.onCellChanged)

    def onCellChanged(self, row, col):
        if col == 0:
            state = self.item(row, col).checkState()
            itemQty = self.item(row, self.qtyToCopy_index)
            if state == QtCore.Qt.Checked:
                itemOpenQty = self.item(row, self.openQty_index)
                itemQty.setData(QtCore.Qt.DisplayRole,
                                itemOpenQty.data(QtCore.Qt.DisplayRole))
            elif state == QtCore.Qt.Unchecked and itemQty:
                itemQty.setData(QtCore.Qt.DisplayRole, QtCore.QVariant())
        elif col == self.qtyToCopy_index:
            item = self.item(row, col)
            qty, ok = item.data(QtCore.Qt.DisplayRole).toFloat()
            openQty, o_ok = self.item(row, self.openQty_index)\
                .data(QtCore.Qt.DisplayRole).toFloat()
            if ok and not qty == 0.0:
                assert(o_ok)
                itemCheck = self.item(row, 0)
                if qty < openQty:
                    itemCheck.setCheckState(QtCore.Qt.PartiallyChecked)
                if abs(qty - openQty) < ROUNDING_MIN:
                    itemCheck.setCheckState(QtCore.Qt.Checked)
                if qty > openQty:
                    exPopup = ExceptionMessagePopup(
                        self.qtObject,
                        "Qty %s is not possble, max is %s" % (qty, openQty),
                        None)
                    exPopup.show()
                    self.changeRed.emit(row, col, True)
                else:
                    # reset the background color
                    self.changeRed.emit(row, col, False)


class UIMain(object):

    def __init__(self, klass, code):
        self.klass = retreive_class(klass)
        self.code = code
        self.prefix = self.klass.get_ui_prefix()


class UI01(UIMain):

    def __init__(self, klass, price=False):
        super(UI01, self).__init__(klass, "01")
        self.price = price

    def buildSelection(self, parent, simple=False):
        selection = UIGroupBox(parent, "selection")
        selection.setTitle(u"Selection")
        layout = UIHLayout(None, "selectionLayout")
        layout.addWidget(selection)

        self.form = UIForm(selection, self.klass.__name__)
        self.form.setAlchemyClass(self.klass)

        if simple:
            display = ['site', 'docType']
        else:
            display = self.klass.list_creation

        self.form.setDisplay(display)
        self.form.attributes(self.code, space=False)
        return selection

    # FIXME all size and text should come from sqlalchemy attributes
    def setupUi(self, Screen):
    # according to
    # http://stackoverflow.com/questions/10047321/qt-using-one-widget-in-several-layouts
    # there is no way to reused the same group box in two tabs....
        self.screen = UIWidget(Screen, "screen")

        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)

        self.principalButtonBarZone = UIButtonBarZone(
            self.screen, "principalButtonBarZone")
        self.buttonBarZoneLayout = UIVLayout(
            self.principalButtonBarZone, "buttonBarZoneLayout")
        self.principalButtonBar = UIButtonBar(
            self.principalButtonBarZone, "principalButtonBar")

        # the tabs
        self.principalButtonBar.addButton(_("Creation Without Reference"))
        self.principalButtonBar.addButton(_("Creation With Reference To"))
        self.principalButtonBar.addButton(_("Data Import"))

        # first tab widget
        self.buttonBarContent1 = UIButtonBarContent(
            self.principalButtonBarZone, "buttonBarContent1")
        self.buttonBarContentLayout1 = UIHLayout(
            self.buttonBarContent1, "buttonBarContentLayout1")
        self.buttonBarContentSpacerLeft1 = UIHSpacer(
            self.buttonBarContent1, "buttonBarContentSpacerLeft1")

        selection1 = self.buildSelection(self.buttonBarContent1,
                                         not self.price)
        self.buttonBarContentSpacerRight1 = UIHSpacer(
            self.buttonBarContent1, "buttonBarContentSpacerRight1")
        self.buttonBarContentLayout1.addSpacerItem(
            self.buttonBarContentSpacerLeft1)
        self.buttonBarContentLayout1.addWidget(selection1)
        self.buttonBarContentLayout1.addSpacerItem(
            self.buttonBarContentSpacerRight1)

        # Second tab widget
        self.buttonBarContent2 = UIButtonBarContent(
            self.principalButtonBarZone, "buttonBarContent2")
        self.buttonBarContentLayout2 = UIHLayout(
            self.buttonBarContent2, "buttonBarContentLayout2")
        self.buttonBarContentSpacerLeft2 = UIHSpacer(
            self.buttonBarContent2, "buttonBarContentSpacerLeft2")

        self.buttonBarContentInsideLayout2 = UIVLayout(
            None, "buttonBarContentInsideLayout2")
        selection2 = self.buildSelection(self.buttonBarContent2, simple=True)

        # ReferenceTo groupBox
        self.withReferenceTo = UIGroupBox(
            self.buttonBarContent2, "withReferenceTo")
        self.withReferenceTo.setTitle(u"With reference to")
        self.withReferenceToGeneralLayout = UIHLayout(
            self.withReferenceTo, "withReferenceToGeneralLayout")
        self.withReferenceToLayout = UIGridLayout(
            None, "withReferenceToLayout")

        # Fixme should reuse a form.
        self.siteRefToLabel = UILabel(self.withReferenceTo, "siteRefToLabel")
        self.siteRefToLabel.setText(_("Site"))
        self.parent_site = UIDataLookupSiteWithDCR(
            self.withReferenceTo, self.prefix + "parentDocumentHeader_site")
        self.parent_site.setCharSize(10)
        self.parent_site_description = UILabel(
            self.withReferenceTo,
            self.prefix + "parentDocumentHeader_site_description")
        self.parent_site_description.setCharSize(40)

        self.docTypeLabelRef2 = UILabel(
            self.withReferenceTo, "docTypeLabelRef2")
        self.docTypeLabelRef2.setText(_("Document Type"))
        self.parent_docType = UIDataLookupSiteWithDCR(
            self.withReferenceTo, self.prefix + "parentDocumentHeader_docType")
        self.parent_docType.setCharSize(10)
        self.parent_docType_description = UILabel(
            self.withReferenceTo,
            self.prefix + "parentDocumentHeader_docType_description")
        self.parent_docType_description.setCharSize(40)

        self.docNumberLabel = UILabel(self.withReferenceTo, "docNumberLabel")
        self.docNumberLabel.setText(_("Document Number"))
        self.parent_documentNumber = UIDataLookupDocument_O_PE(
            self.withReferenceTo,
            self.prefix + "parentDocumentHeader_documentNumber")
        self.parent_documentNumber.setCharSize(10)

        self.withReferenceToLayout.addWidget(self.siteRefToLabel, 0, 0)
        self.withReferenceToLayout.addWidget(self.parent_site, 0, 1)
        self.withReferenceToLayout.addWidget(
            self.parent_site_description, 0, 2)

        self.withReferenceToLayout.addWidget(self.docTypeLabelRef2, 1, 0)
        self.withReferenceToLayout.addWidget(self.parent_docType, 1, 1)
        self.withReferenceToLayout.addWidget(
            self.parent_docType_description, 1, 2)

        self.withReferenceToLayout.addWidget(self.docNumberLabel, 2, 0)
        self.withReferenceToLayout.addWidget(
            self.parent_documentNumber, 2, 1)

        self.withReferenceToGeneralSpacer = UIHSpacer(
            self.withReferenceTo, "withReferenceToGeneralSpacer")
        self.withReferenceToGeneralLayout.addLayout(self.withReferenceToLayout)
        self.withReferenceToGeneralLayout.addSpacerItem(
            self.withReferenceToGeneralSpacer)

        # Items table groupBox
        self.tableGB = UIGroupBoxWM(self.buttonBarContent2, "tableGB")
        self.tableGB.setTitle(u"Document Details Selection")
        self.tableGBLayout = UIHLayout(self.tableGB, "tableGBLayout")

        self.details = DDLinkedTable2(
            self.tableGB,
            self.prefix + "parentDocumentHeader_documentNumber_documentDetails"
        )
        # self.details.addRow(
        #     [True, 1, 'pop', 'pop', 100, 'KG', 100, 0, 12.9, 'EUR',
        #      'HH',
        #      datetime.date.today(), datetime.date.today(), 'OPEN']
        # )

        # row = self.details.model.rowCount()
        # self.details.model.insertRows(row)

        # self.details.setMinimumWidth(1000)
        self.tableGBLayout.addWidget(self.details)
        # to add some example data: self.details.model.setData(...)
        # it is done cells by cell

        # add widget in the layout
        self.buttonBarContentInsideLayout2.addWidget(selection2)
        self.buttonBarContentInsideLayout2.addWidget(self.withReferenceTo)
        self.buttonBarContentInsideLayout2.addWidget(self.tableGB)

        self.buttonBarContentSpacerRight2 = UIHSpacer(
            self.buttonBarContent2, "buttonBarContentSpacerRight2")
        self.buttonBarContentLayout2.addSpacerItem(
            self.buttonBarContentSpacerLeft2)
        self.buttonBarContentLayout2.addLayout(
            self.buttonBarContentInsideLayout2)
        self.buttonBarContentLayout2.addSpacerItem(
            self.buttonBarContentSpacerRight2)

         # Third ButtonBar content
        self.buttonBarContent3 = UIButtonBarContent(
            self.principalButtonBarZone, "buttonBarContent3")
        self.buttonBarContentLayout3 = UIHLayout(
            self.buttonBarContent3, "buttonBarContentLayout3")
        self.buttonBarContentSpacerLeft3 = UIHSpacer(
            self.buttonBarContent3, "buttonBarContentSpacerLeft3")

        self.uploadAFileGroupBox = UIGroupBox(
            self.buttonBarContent3, "uploadAFileGroupBox")
        self.uploadAFileGroupBox.setTitle("Upload a file")
        self.uploadAFileLayout = UIVLayout(
            self.uploadAFileGroupBox, "uploadAFileLayout")
        self.uploadAFile = UIFileDialog(
            self.uploadAFileGroupBox, "uploadAFile")
        self.uploadAFileLayout.addWidget(self.uploadAFile)
        self.buttonBarContentSpacerRight3 = UIHSpacer(
            self.buttonBarContent3, "buttonBarContentSpacerRight3")
        self.buttonBarContentLayout3.addSpacerItem(
            self.buttonBarContentSpacerLeft3)
        self.buttonBarContentLayout3.addWidget(self.uploadAFileGroupBox)
        self.buttonBarContentLayout3.addSpacerItem(
            self.buttonBarContentSpacerRight3)
        self.buttonBarZoneLayout.addWidget(self.principalButtonBar)
        self.buttonBarZoneLayout.addWidget(self.buttonBarContent1)
        self.buttonBarZoneLayout.addWidget(self.buttonBarContent2)
        self.buttonBarZoneLayout.addWidget(self.buttonBarContent3)
        self.endSpacer = UIVSpacer(self.screen, "endSpacer")
        self.mainLayout.addWidget(self.principalButtonBarZone)
        self.mainLayout.addSpacerItem(self.endSpacer)
        Screen.layout.addWidget(self.screen)

    def multiple_content(self):
        return [self.buttonBarContent1, self.buttonBarContent2]


class UI(UIMain):

    def setupUi(self, Screen):
        self.screen = UIWidget(Screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.generalLayout = UIHLayout(None, "generalLayout")
        self.generalLayout.setMargin(0)
        self.generalLayout.setSpacing(0)
        self.mainLayoutLeftSpacer = UIHSpacer(
            self.screen, "mainLayoutLeftSpacer")

        self.documentSelection = UIGroupBox(
            self.screen, "documentSelection")
        self.documentSelection.setTitle(u'Document Selection')

        self.documentSelectionLayout = UIGridLayout(
            self.documentSelection, "documentSelectionLayout")

        # FIXME: size should come from sqlalchemy attributes
        # FIXME: same thing for text
        # site
        self.siteLabel = UILabel(self.documentSelection, "siteLabel")
        self.siteLabel.setText(_('Site'))
        self.site = UIDataLookup(
            self.documentSelection, self.prefix + "site")
        self.site.setCharSize(10)
        self.site_description = UILabel(
            self.documentSelection, self.prefix + "site_description")
        self.site_description.setCharSize(40)
        # docType
        self.docTypeLabel = UILabel(self.documentSelection, "docTypeLabel")
        self.docTypeLabel.setText(_('Document Type'))
        self.docType = UIDataLookup(
            self.documentSelection, self.prefix + "docType")
        self.docType.setCharSize(10)
        self.docType_description = UILabel(
            self.documentSelection, self.prefix + "docType_description")
        self.docType_description.setCharSize(40)
        # documentNumber
        self.docNumberLabel = UILabel(self.documentSelection, "docNumberLabel")
        self.docNumberLabel.setText(_("Document Number"))
        self.documentNumber = UIDataLookupDocument(
            self.documentSelection, self.prefix + "documentNumber")
        self.documentNumber.setCharSize(10)

        # add them in the gridLayout
        self.documentSelectionLayout.addWidget(self.siteLabel, 0, 0)
        self.documentSelectionLayout.addWidget(self.site, 0, 1)
        self.documentSelectionLayout.addWidget(self.site_description, 0, 2)

        self.documentSelectionLayout.addWidget(self.docTypeLabel, 1, 0)
        self.documentSelectionLayout.addWidget(self.docType, 1, 1)
        self.documentSelectionLayout.addWidget(self.docType_description, 1, 2)

        self.documentSelectionLayout.addWidget(self.docNumberLabel, 2, 0)
        self.documentSelectionLayout.addWidget(self.documentNumber, 2, 1)

        self.mainLayoutRightSpacer = UIHSpacer(
            self.screen, "mainLayoutRightSpacer")
        self.generalLayout.addSpacerItem(self.mainLayoutLeftSpacer)
        self.generalLayout.addWidget(self.documentSelection)
        self.generalLayout.addSpacerItem(self.mainLayoutRightSpacer)
        self.generalSpacer = UIVSpacer(self.screen, "generalSpacer")
        self.mainLayout.addLayout(self.generalLayout)
        self.mainLayout.addSpacerItem(self.generalSpacer)
        Screen.layout.addWidget(self.screen)


class UI_main(UIMain):

    def setupUi(self, Screen):
        self.screen = UIWidget(Screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)

        self.splitter = UIVSplitter(self.screen, "splitter")
        self.scroll = UIScrollArea(self.splitter, "scroll")

        self.mainGroupBox = UIGroupBoxWM(self.scroll, "mainGroupBox")
        self.salesOrder = UIForm(self.mainGroupBox, "salesOrder")
        self.salesOrder.setAlchemyClass(self.klass)
        self.salesOrder.attributes(self.code)

        self.scroll.view(self.mainGroupBox)

        self.documentDetails = UIInlineDD(
            self.splitter,
            self.prefix + "documentDetails"
        )
        self.mainLayout.addWidget(self.splitter)
        Screen.layout.addWidget(self.screen)


class UI_main04(UIMain):

    def setupUi(self, Screen):
        self.screen = UIWidget(Screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)

        self.groupBox = UIGroupBoxWM(self.screen, "groupBox")
        self.groupBox.setTitle("Document Flow")
        self.gbLayout = UIHLayout(self.groupBox, "gbLayout")
        self.flow = UITree(self.groupBox, self.prefix + "flow")
        header = self.flow.headerItem()
        header.setHidden(True)
        self.gbLayout.addWidget(self.flow)

        self.mainLayout.addWidget(self.groupBox)
        Screen.layout.addWidget(self.screen)
