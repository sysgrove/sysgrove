#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  alsof
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

[1]:
http://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.solve.html

----
"""
import logging
import locale
import re
import StringIO
import os
from PyQt4 import QtCore, QtGui
from sqlalchemy import (
    Column, ColumnDefault,
    Integer,
    ForeignKey,
    Boolean,
    String, Unicode,
    Date,
    Float,
    DateTime, Text,
    ForeignKeyConstraint
)
from sqlalchemy import event
from sqlalchemy.orm import relationship, backref
from sysgrove.utils import classproperty, prefix, have_same_value
from sysgrove.utils import UserMessage, debug_trace
from sysgrove.settings import UNSET, TP_DATAKOOKUP
from sysgrove.settings import (
    STATUS_OPEN, STATUS_EXEC,
    STATUS_CANCEL,
    STATUS_PARTEXEC,
    STATUS_FROZEN,
    SALES_ID, PURCHASE_ID,
    SIMPLE_DATALOOKUP,
    ROUNDING_MIN
)
from sysgrove.models import Base
from sysgrove.models import InitMixin
from sysgrove.modules.masterdata.mixin import SiteMixin
from sysgrove.modules.masterdata.models import PriceSchemaHeader as _PSH
from sysgrove.modules.masterdata.models import ItemDetail as _ItemDetail
from sysgrove.modules.masterdata.models import ItemGroup as _ItemGroup
from sysgrove.modules.masterdata.models import Address as _Address
from sysgrove.modules.masterdata.models import Bank as _Bank
from sysgrove.modules.masterdata.models import DocType as _DocType
from sysgrove.modules.masterdata.models import DocGroup as _DocGroup
from sysgrove.modules.masterdata.models import DocCopyRule as _DCR
from sysgrove.modules.masterdata.models import PriceCondition as _PC
from sysgrove.modules.masterdata.models import Currency as _Currency
from sysgrove.modules.masterdata.models import Site as _Site
from sysgrove.modules.masterdata.models import UnitOfMeasure as _UOM
from sysgrove.modules.masterdata.models import TransportType as _TransportType
from sysgrove.modules.masterdata.models import Vessel as _Vessel
from sysgrove.modules.masterdata.models import ExchangeRate as _ExchangeRate
from sysgrove.modules.masterdata.models import UnitOfMeasureConversion as _UoMC
from sysgrove.modules.masterdata.models import Warehouse as _Warehouse
from sysgrove.modules.masterdata.models import Lot as _Lot
from sysgrove.modules.masterdata.models import Packing as _Packing
from sysgrove.modules.masterdata.models import Country as _Country
from sysgrove.modules.masterdata.models import SalesRep as _SalesRep
from sysgrove.modules.masterdata.models.third_party import (
    ThirdParty as _ThirdParty)
from sysgrove.modules.masterdata.models.payment import (
    PaymentTerm as _PaymentTerm)
from sysgrove.modules.masterdata.models.payment import (
    PaymentTermText as _PaymentTermText)
from sysgrove.modules.masterdata.models.incoterm import (
    Incoterm as _Incoterm)
from sysgrove.modules.masterdata.models.incoterm import (
    IncotermText as _IncotermText)
from sysgrove.modules.masterdata.models.status import (
    HeaderStatusComputation as _HSC)
from sysgrove import i18n
_ = i18n.language.ugettext
# temp import for report
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s


log = logging.getLogger('sysgrove')
tp_documentdatalookup = \
    'sysgrove.modules.masterdata.ui.customs.UIDataLookupTPDocument'
bank_datalookup = 'sysgrove.modules.masterdata.ui.customs.UIDataLookupBank'
num_lineedit = 'sysgrove.ui.classes.lineedit.UINumericLineEdit'
lineedit = 'sysgrove.ui.classes.lineedit.UISLineEdit'
bc_widget = 'sysgrove.ui.classes.checkbox.UIBoolCheckBox'
line_month = 'sysgrove.ui.classes.date.LineMonth'
combobox = 'sysgrove.core.document.ui.customs.DDStatus'
dhstatus = 'sysgrove.core.document.ui.customs.DHStatus'


RESULT = 'Result'
UNITPRICE = 'UnitPrice'
QUANTITY = 'Quantity'


def update_total_price():
    from sysgrove.core.screen import Screen
    # uiObjects that need to be updated, when a documentDetail is added or
    # updated
    fields_on_fly = [
        'status',
        'docTotalAmountExclVat',
        'docTotalVat',
        'docTotalAmountInclVat'
    ]
    # uiObject to be taken in into account (input objects)
    fields_to_fly = [
        'documentDetails',
        'serviceProviders',
    ]
    Screen.update_on_fly(fields_to_fly, fields_on_fly, 'set_amount')


_cmp_steps = lambda x, y: cmp(x.step, y.step)


def _subs(s, d):
    values = d.keys()
    values = reversed(sorted(values, key=len))
    for v in values:
        s = re.sub(v + '(?![0-9]+)', str(d[v]), s)
    return s


def _prefix(psh):
    if hasattr(psh, 'step'):
        return '#%s' % psh.step
    else:
        return '#%s' % psh


def convert2Uom(value, orignUoM, targetUoM):
    if orignUoM == targetUoM:
        return value
    else:
        conversion = _UoMC.query.filter_by(
            unitOfMeasureOrigin_id=orignUoM.id,
            unitOfMeasureTarget_id=targetUoM.id).all()
        if len(conversion) == 1:
            return ((value * conversion[0].quantityTarget) /
                    conversion[0].quantityOrigin)
        elif len(conversion) == 0:
            conversion = _UoMC.query.filter_by(
                unitOfMeasureOrigin_id=orignUoM.id).all()
            if len(conversion) == 0:
                raise Exception(u"""
                    Unit of Measure Conversion between %s and %s are missing
                    """ % (orignUoM, targetUoM))
            if len(conversion) == 1:
                return convert2Uom(
                    value * conversion[0].quantityTarget /
                    conversion[0].quantityOrigin,
                    conversion[0].unitOfMeasureTarget, targetUoM)
            else:
                raise Exception(u"""
                    Duplicated Unit of Measure Conversion between %s and %s
                    """ % (orignUoM, targetUoM))


def get_psdetail_for(priceSchemaHeader, priceCondition):
    candidates = filter(lambda x: x.priceCondition == priceCondition,
                        priceSchemaHeader.details)
    if len(candidates) == 1:
        result = candidates
        step = candidates[0].step
        involved = [d
                    for d in priceSchemaHeader.details
                    if not d.priceCondition.calculationType == "Sum"]
        if involved:
            involved = filter(lambda x: "#%s" % step in x.action, involved)
            result.extend(involved)
            return result
    return []


def add_tp_addr_fields(tpname):
    def pref(a):
        if isinstance(a, list):
            return map(pref, a)
        return "%s_%s" % (tpname, a)

    return pref(prefix(_Address))


def get_exchangeRateValue(serviceExRate, ddExRate, documentHeader):
    if not ddExRate:
        return serviceExRate.exchangeRateValue
    if serviceExRate.targetCurrency == ddExRate.company.currency:
        return serviceExRate.exchangeRateValue
    if serviceExRate.targetCurrency == ddExRate.targetCurrency and \
            serviceExRate.company.currency == ddExRate.company.currency:
        return 1
    for exRate in documentHeader.exchangeRates:
        if exRate.company.currency == serviceExRate.company.currency and \
                exRate.targetCurrency == ddExRate.lineCurrency:
            return exRate.exchangeRateValue
    log.debug('No exchange found between %s and %s,' %
             (serviceExRate.company.currency,
              ddExRate.company.currency))
    log.debug('compute one from %s and  %s')
    return (serviceExRate.exchangeRateValue /
            ddExRate.exchangeRateValue)


def compute_repartion_cost(documentDetail, service):
    """
    To compute the result you may use the following variables:
    - service : the service Provider that need to be reparted
    - documentDetail : the current document details
    - result : the returning value of the repartition cost
        for the current documentDetail

    """
    input = StringIO.StringIO(
        service.repartitionCosts.repartitionCostsUserExit)
    output = StringIO.StringIO()
    output.write('def myfunc(documentDetail, service):\n')
    for line in input:
        output.write('    %s' % line)
    output.write('    return result')
    exec_action = {}
    exec(output.getvalue()) in exec_action

    return exec_action['myfunc'](documentDetail, service)


# return the last step number
def add_priceLine_for_service(documentDetail, docHeader, service, max_step):
    # the priceSchemaDetail that have to be added in documentDetail.prices
    # to handle the service
    psd_list = get_psdetail_for(
        docHeader.priceSchemaHeader,
        service.priceCondition)

    for detail in psd_list:
        price = Price()
        price.documentDetail = documentDetail
        price.from_priceSchemaDetail(detail)
        price.exchangeRate = service.exchangeRate
        max_step = max_step + 10
        price.step = max_step
        price.serviceProvider = service
        documentDetail.prices.append(price)

    if psd_list == []:
        # nothing exists in priceSchema, build price lines is also
        # required due to business rule saying that "all service providers
        # have to appears in documentDetail.prices computation
        price = Price()
        price.documentDetail = documentDetail
        price.priceCondition = service.priceCondition
        price.exchangeRate = service.exchangeRate
        max_step = max_step + 10
        price.step = max_step
        documentDetail.prices.append(price)

        if service.vatCondition:
            price = Price()
            price.documentDetail = documentDetail
            price.priceCondition.id = _PC.vat_condition().id
            price.exchangeRate = service.exchangeRate
            price.action = _prefix(max_step)
            max_step = max_step + 10
            price.step = max_step
            documentDetail.prices.append(price)
    return (max_step)


class ServiceProvider(InitMixin, Base):
    verbose_name = u'Service Provider'
    list_display = [
        'thirdParty',
        'quantity', 'unitOfMeasure',
        [
            'amount',
            (SIMPLE_DATALOOKUP, 'currency'), (('Label', '/'), 'slashLabel'),
            (SIMPLE_DATALOOKUP, 'priceUoM')
        ],
        'exchangeRate',
        'priceCondition',
        'repartitionCosts',
    ]

    thirdParty_id = Column(
        "ThirdParty_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )
    thirdParty = relationship(
        "ThirdParty",
        info={'verbose_name': _('Service Provider'),
              'datalookup': TP_DATAKOOKUP})

    quantity = Column(
        "Quantity", Float, ColumnDefault(0.0),
        info={'verbose_name': _('Quantity')})

    # UnitOfMeasure FK
    unitOfMeasure_id = Column(
        "UnitOfMeasure_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk'))

    unitOfMeasure = relationship(
        "UnitOfMeasure",
        primaryjoin="ServiceProvider.unitOfMeasure_id==UnitOfMeasure.id",
        info={'verbose_name': _UOM.verbose_name})

    priceUoM_id = Column(
        "PriceUoM_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk'))

    priceUoM = relationship(
        "UnitOfMeasure",
        primaryjoin="ServiceProvider.priceUoM_id==UnitOfMeasure.id",
        info={'verbose_name': _UOM.verbose_name})

    amount = Column("Amount", Float, info={'verbose_name': _('Unit Price')})

    currency_id = Column("Currency_pk",
                         Integer,
                         ForeignKey('Currency.Currency_pk'))
    currency = relationship(
        "Currency",
        info={'verbose_name': _Currency.verbose_name})

    exchangeRate_id = Column("ExchangeRate_pk",
                             Integer,
                             ForeignKey('ExchangeRate.ExchangeRate_pk'))
    exchangeRate = relationship(
        "ExchangeRate",
        info={'verbose_name': _("Exchange Rate"),
              'datalookup': 'sysgrove.ui.classes.UIDataLookupExchangRate',
              'description': ['targetCurrency_code']})

    repartitionCosts_id = Column(
        "RepartitionCosts_pk",
        Integer,
        ForeignKey('RepartitionCosts.RepartitionCosts_pk')
    )
    repartitionCosts = relationship(
        "RepartitionCosts",
        info={'verbose_name': _('Repartition Costs')}
    )

    documentHeader_id = Column(
        "DocumentHeader_pk",
        Integer,
        ForeignKey('DocumentHeader.DocumentHeader_pk')
    )
    documentHeader = relationship("DocumentHeader")

    priceCondition_id = Column(
        "PriceCondition_pk",
        Integer,
        ForeignKey('PriceCondition.PriceCondition_pk')
    )
    priceCondition = relationship(
        "PriceCondition",
        info={'verbose_name': _('Type of cost')}
    )

    vatCondition = Column("VATCondition",
                          Boolean, ColumnDefault(False))

    def duplicate(self, documentHeader):
        new_sp = ServiceProvider(documentHeader=documentHeader)
        fields = self._fields().keys()
        fields.remove('documentHeader')
        for f in fields:
            setattr(new_sp, f, getattr(self, f))
        return new_sp

    @classproperty
    def list_columns(cls):
        return ['thirdParty', 'thirdParty_description',
                'quantity',
                'unitOfMeasure',
                'priceCondition', 'priceCondition_description',
                'amount', 'currency', 'priceUoM',
                'exchangeRate',
                'repartitionCosts', 'repartitionCosts_description']

    @property
    def unitPrice4QtyUoM(self):
        if self.unitOfMeasure == self.priceUoM:
            return self.amount
        else:
            return convert2Uom(self.amount,
                               self.priceUoM,
                               self.unitOfMeasure)


class DocumentHeader(InitMixin, SiteMixin, Base):
    verbose_name = _('Document Header')
    datalookup = 'sysgrove.ui.classes.UIDataLookupDocument'

    docType_id = Column(
        "DocType_pk",
        Integer,
        ForeignKey('DocType.DocType_pk')
    )
    docType = relationship(
        "DocType",
        primaryjoin="DocumentHeader.docType_id==DocType.id",
        info={'verbose_name': _("Document Type Code")}
    )
    documentNumber = Column(
        "DocumentNumber",
        String(),
        ColumnDefault(UNSET),
        info={'verbose_name': _("Document Number")}
    )

    # The future Flags
    # (no need for spread flag, it is always there)
    # If a flag is true, this means that it is a future fixing
    @property
    def futureFixingFlag(self):
        return not (self.priceFlag is None
                    and self.premiumFlag is None
                    and self.forexFlag is None)

    priceFlag = Column(
        "FutureFixingFlag",
        Boolean,
        info={'verbose_name': _('Price Fixing')})

    # If true, then premium is not future premium but flat premium
    premiumFlag = Column(
        "PremiumFlag",
        Boolean,
        info={'verbose_name': _(' Premium Fixing')})

    # link to exchange rate and future?
    forexFlag = Column(
        "ForexFlag",
        Boolean,
        info={'verbose_name': _(' Forex Fixing')})

    @property
    def premium_flat(self):
        return self.premiumFlag is not None and not self.premiumFlag

    # Currency FK
    currency_id = Column(
        "Currency_pk",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    currency = relationship(
        "Currency",
        info={'verbose_name': _Currency.verbose_name}
    )

    # FK to the parent DocumentHeader
    parentDocumentHeader_id = Column(
        "ParentDocumentHeader_pk",
        Integer,
        ForeignKey('DocumentHeader.DocumentHeader_pk')
    )

    childrenDocumentHeader = relationship(
        'DocumentHeader',
        backref=backref("parentDocumentHeader",
                        remote_side='DocumentHeader.id',
                        uselist=False,
                        single_parent=True,
                        cascade="save-update, merge, expunge",
                        info={'verbose_name': _('Parent Document'),
                              'datalookup': datalookup,
                              'description': ['docType', 'documentNumber']
                              }),
        info={'verbose_name': _('Children Document')},
    )

    creationDate = Column(
        "CreationDate",
        DateTime, info={'verbose_name': _('Creation Date')})

    documentValueDate = Column(
        "DocumentValueDate", Date,
        nullable=False,
        info={'verbose_name': _('Document date')})

    soldToCode_id = Column(
        "SoldToCode_pk",
        Integer, ForeignKey('ThirdParty.ThirdParty_pk'),
        nullable=False)
    soldToCode = relationship(
        "ThirdParty",
        primaryjoin="DocumentHeader.soldToCode_id==ThirdParty.id",
        info={'verbose_name': _("Ordering Party"),
              'datalookup': tp_documentdatalookup
              }
    )

    shipToCode_id = Column(
        "ShipToCode_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk'))
    shipToCode = relationship(
        "ThirdParty",
        primaryjoin="DocumentHeader.shipToCode_id==ThirdParty.id",
        info={'verbose_name':  _("Delivery Party"),
              'datalookup': tp_documentdatalookup
              }
    )

    billToCode_id = Column(
        "BillToCode_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk'))
    billToCode = relationship(
        "ThirdParty",
        primaryjoin="DocumentHeader.billToCode_id==ThirdParty.id",
        info={'verbose_name': _("Invoicing Party"),
              'datalookup': tp_documentdatalookup
              }
    )

    externalDocRef = Column(
        "ExternalDocRef", Unicode(),
        info={'verbose_name': _('Third Party Reference')}
    )

    paymentTerm_id = Column(
        "PaymentTerm_pk",
        Integer,
        ForeignKey('PaymentTerm.PaymentTerm_pk'),
        nullable=False,

    )
    paymentTerm = relationship(
        "PaymentTerm", info={'verbose_name': _("Payment Term")})

    paymentTermText_id = Column(
        "PaymentTermText_pk",
        Integer,
        ForeignKey('PaymentTermText.PaymentTermText_pk'),
    )
    paymentTermText = relationship(
        "PaymentTermText", info={'verbose_name': _("Payment Term Text")})

    incoterm_id = Column(
        "Incoterm_pk",
        Integer,
        ForeignKey('Incoterm.Incoterm_pk'),
        nullable=False,

    )
    incoterm = relationship("Incoterm",  info={'verbose_name': _("Incoterm")})

    incotermText_id = Column(
        "IncotermText_pk",
        Integer,
        ForeignKey('IncotermText.IncotermText_pk')
    )
    incotermText = relationship(
        "IncotermText",  info={'verbose_name': _("Incoterm Text")})

    printedComments = Column(
        "PrintedComments", Text, ColumnDefault(u''),
        info={'verbose_name': _('Printed Comment')})

    internalComments = Column(
        "InternalComments", Text, ColumnDefault(u''),
        info={'verbose_name': _('Internal Comment')})

    # backref relation
    serviceProviders = relationship(
        "ServiceProvider",
        info={'verbose_name': ServiceProvider.verbose_name})

    # Amount
    docTotalAmountExclVat = Column(
        "DocTotalAmountExclVat", Float,
        ColumnDefault(0.0),
        info={'verbose_name': _("Total Amount Exclude Vat")})
    docTotalVat = Column(
        "DocTotalVat", Float,
        ColumnDefault(0.0),
        info={'verbose_name': u"Total Vat"})
    docTotalAmountInclVat = Column(
        "DocTotalAmountInclVat", Float,
        ColumnDefault(0.0),
        info={'verbose_name': _("Total Amount Include Vat")})

    # internal value: no need for 'info
    totalQuantity = Column("TotalQuantity",
                           Float,
                           ColumnDefault(0.0))

    priceSchemaHeader_id = Column(
        "PriceSchemaHeader_pk",
        Integer,
        ForeignKey('PriceSchemaHeader.PriceSchemaHeader_pk'),
        nullable=False,
    )
    priceSchemaHeader = relationship(
        "PriceSchemaHeader",
        info={'verbose_name': _("Price Schema Header")})

    status_id = Column(
        "Status_pk",
        Integer,
        ForeignKey('Status.Status_pk'),
        ColumnDefault(STATUS_OPEN),
        nullable=False,

    )
    status = relationship("Status",
                          cascade="save-update, merge, expunge",
                          info={'verbose_name': _("Document Status")})

    documentDetails = relationship(
        "DocumentDetail",  info={'verbose_name': _("Document Details")},
        order_by="DocumentDetail.lineNumber",
        cascade="save-update, merge, expunge",
    )

    exchangeRates = relationship(
        "ExchangeRate",
        secondary="DocumentHeader_ExchangeRate_Allocation",
        info={'verbose_name': _ExchangeRate.verbose_name},
        cascade="save-update, merge, expunge",
    )

    discriminator = Column('SG_type', String(50))
    __mapper_args__ = {'polymorphic_on': discriminator}

    def __repr__(self):
        return u"<%s(%s)>" % (self.__tablename__, self.documentNumber)

    salesRep_id = Column(
        "SalesRep_pk",
        Integer, ForeignKey('SalesRep.SalesRep_pk')
    )

    salesRep = relationship(
        "SalesRep",
        info={'verbose_name': _SalesRep.verbose_name}
    )

    bank_id = Column(
        "Bank_pk",
        Integer, ForeignKey('Bank.Bank_pk'))

    bank = relationship(
        "Bank",
        primaryjoin="DocumentHeader.bank_id==Bank.id",
        info={'verbose_name': _Bank.verbose_name,
              'datalookup': bank_datalookup
              }
    )

    bank_list = ['bank', 'bank_description']

    # Building the formular
    soldTo_list = ['soldToCode', 'soldToCode_description']
    soldTo_list.extend(add_tp_addr_fields('soldToCode'))

    billTo_list = ['billToCode', 'billToCode_description']
    billTo_list.extend(add_tp_addr_fields('billToCode'))

    shipToCode_list = ['shipToCode', 'shipToCode_description']
    shipToCode_list.extend(add_tp_addr_fields('shipToCode'))

    logistics_list = [[
        ('DateGB', ('documentValueDate', 'today')),
        (('GroupBox', externalDocRef.info[
            'verbose_name']), 'externalDocRef'),
        (('GroupBox', status.info['verbose_name']),
         [(dhstatus, 'status')]),

        (('GroupBox', u"Sales Rep"), ['salesRep']),

        (('GroupBox', u'Related Document'),
         ['parentDocumentHeader_docType',
          'parentDocumentHeader_documentNumber']),
    ], [
        (('GroupBoxRes', soldToCode.info['verbose_name']),
         soldTo_list),
        (('GroupBoxRes', shipToCode.info['verbose_name']),
         shipToCode_list),
        (('GroupBoxRes', billToCode.info['verbose_name']),
         billTo_list),

    ], [
        # ('DataLookUpGB', 'exchangeRate'),
        (('GroupBox', u"Currencies"), ['currency']),
        (('GroupBox', u"Amount"), [
            ('UIAmount', 'docTotalAmountExclVat'),
            ('UIAmount', 'docTotalVat'),
            ('UIAmount', 'docTotalAmountInclVat'),
        ]),
        #(('GroupBox', u"bank"), ['bank']),
        (('GroupBoxRes', bank.info['verbose_name']),
         bank_list),

    ],
        (('Button', 'link button'), 'link'),

        [(('GroupBox', incoterm.info['verbose_name']), [
            'incoterm',
            'incotermText', 'incotermText_text']),

         (('GroupBox', paymentTerm.info['verbose_name']), [
             'paymentTerm',
             'paymentTermText', 'paymentTermText_text']),
         ]
    ]

    comments_list = [[
        (('GroupBox', printedComments.info['verbose_name']), [
         'printedComments']),
        (('GroupBox', internalComments.info['verbose_name']), [
         'internalComments']),
    ]]

    add_costs_list = [
        ('sysgrove.ui.classes.inline.UIInlineSP',
         'serviceProviders')]

    list_display = [('TabPanel', [
        (u'Logistics', logistics_list),
        (u'Comments', comments_list),
        (u'Additional Costs', add_costs_list),
    ])]

    list_creation = [
        'site', 'docType',
        (('ClickShow', 'future', u'Future Pricing'),
         # 'premiumFlag'
         [((bc_widget, u"Future", u"Flat"), 'priceFlag'),
          ((bc_widget, u"Future", u"Flat"), 'premiumFlag'),
          ((bc_widget, u"Future", u"Flat"), 'forexFlag')])
    ]

    def set_documentNumber(self):
        docNumbering = self.docType.docNumbering
        number = docNumbering.documentNumber + 1
        docNumbering.documentNumber = number
        docNumbering.save()
        docNumber = u"%s" % number
        if not self.documentNumber or self.documentNumber == UNSET:
            self.documentNumber = docNumber
        else:
            raise Exception("trying to reset document number %s with %s" % (
                self.documentNumber, docNumber))

    def are_in_same_docGroup(self, document):
        return self.docType.docGroup == document.docType.docGroup

    def finishing_a_docFlow(self):
        query = _DCR.query.filter_by(docTypeCodeOrigin_id=self.docType.id) \
            .filter_by(docGroupOrigin_id=self.docType.docGroup.id) \
            .filter_by(flowIDOrigin_id=self.docType.flowID.id) \
            .filter_by(siteOrigin_id=self.site.id).all()
        return query == []

    # FIXME: must rewritten.
    # first case self and docOrigin have the same docGroup: this means
    # docOrigin is
    # used as a template, copy every thing and the the document flow.
    # The openQuantity do not changed
    # second case self is a child of docOrigin in a flow...
    # If self.docGroup is at the
    # of a flow them all line must be set to excutted
    # docOrign open lines quanty have to been updated, as a consequence the
    # status too...
    # documentDetails, serviceProviders need to be duplicated (take care
    # that service providers referes to documentHeader)
    # as documentDetail.prices.price refereces to services providers, the
    # a correspondance list has to be kept
    def copy(self, docOrign, details_with_qty):
        fields = self._fields().keys()
        fields.remove('docType')
        fields.remove('documentNumber')
        fields.remove('site')
        fields.remove('serviceProviders')
        fields.remove('documentDetails')
        fields.remove('parentDocumentHeader')
        fields.remove('childrenDocumentHeader')
        fields.remove('creationDate')
        fields.remove('status')
        fields.remove('docTotalAmountInclVat')
        fields.remove('docTotalAmountExclVat')
        fields.remove('docTotalVat')
        # fields.remove('futureFixingFlag')
        # fields.remove('premiumFlag')
        # fields.remove('forexFlag')

        refs_to_parent = not self.are_in_same_docGroup(docOrign)
        set_status_to_exec = self.finishing_a_docFlow()
        for f in fields:
            setattr(self, f, getattr(docOrign, f))
        sp_duplicate = {}
        for service in docOrign.serviceProviders:
            sp_duplicate[service] = service.duplicate(self)
            self.serviceProviders.append(sp_duplicate[service])
        self.parentDocumentHeader = docOrign
        for dq in details_with_qty:
            detail, qty = dq
            if detail.lineQuantityOpen >= qty:
                detail.duplicate(self,
                                 qty,
                                 sp_duplicate,
                                 refs_to_parent,
                                 set_status_to_exec)
            else:
                raise Exception(
                    "Cannot copy %s for Qty %s" % (detail.lineNumber, qty))
                return

    def hasParent(self):
        if self.parentDocumentHeader:
            return self.parentDocumentHeader.documentNumber \
                and not self.parentDocumentHeader.documentNumber == UNSET
        else:
            return False

    def root_document(self):
        if self.parentDocumentHeader:
            return self.parentDocumentHeader.root_document()
        else:
            return self

    def name(self):
        return u"%s %s #%s" % (self.docType.docGroup.description,
                               self.docType.code,
                               self.documentNumber)

    def compute_totalQty(self):
        if not _UOM.master():
            raise Exception(u"No Master Unit of Measure found in datas")

        result = 0.0
        for detail in self.documentDetails:
            if detail.lineUoM.masterUoM:
                result = result + detail.lineQuantity
            elif detail.lineUoM.standAlone:
                pass
            else:
                result = result + convert2Uom(detail.lineQuantity,
                                              detail.lineUoM,
                                              _UOM.master())
        return result

    def is_fixed(self):
        if not self.futureFixingFlag:
            return True
        res = True
        i = 0
        while res and i < len(self.documentDetails):
            res = res and self.documentDetails[i].is_fixed()
            i = i + 1
        return res

    # Do so check and add documentDetail calcultation price.
    # The docTotalQuantity is also set here.
    def set_amount(self):
        # do nothing if some data are lacking
        if self.documentDetails == []:
            return
        if self.priceSchemaHeader.details == []:
            return

        self.totalQuantity = self.compute_totalQty()
        # lets check that PS for DocHeader and prices for
        # all docDetail are linked.
        # link is done by the priceCondition
        first_psd = self.priceSchemaHeader.details[0]
        for detail in self.documentDetails:
            detail_last_psd = detail.priceSchemaHeader.details[-1]
            if not first_psd.priceCondition == detail_last_psd.priceCondition:
                raise Exception(u"""Price Schemas for Header and
                    Detail document does not mach""")

        # The price is computed in documentHeader currency

        # lets do the first step, which must match
        # the document details last one
        tot_invat = 0.0
        vat = 0.0

        for detail in self.documentDetails:
            if not detail.status.id == STATUS_CANCEL:
                (d_invat, d_vat) = detail.compute_amount()
                tot_invat = tot_invat + d_invat
                vat = vat + d_vat

        # check services sums: this step is a check one.
        # as amout has been computed for each document details
        # it is now possible to do this check.
        # The check is done in service currency, so no exchange computation
        # are required
        if self.is_fixed():
            for service in self.serviceProviders:
                service_sum = service.unitPrice4QtyUoM * service.quantity
                compute_sum = 0.0
                for line in self.documentDetails:
                    for price in line.prices:
                        if price.serviceProvider == service:
                            if price.exchangeRate:
                                compute_sum = compute_sum + \
                                    price.resultExtraCurrency
                            else:
                                compute_sum = compute_sum + price.result

                diff = abs(service_sum - compute_sum)
                if diff > 0.01:
                    raise Exception("""
                        Service Provider %s is not well
                        split in document details""" %
                                    service.thirdParty)
            # FIXME : this check has to be done also for SP

        # with priceCondition not in PriceSchema

        self.docTotalAmountExclVat = tot_invat - vat
        self.docTotalVat = vat
        self.docTotalAmountInclVat = tot_invat

    def generatePdf(self):
        # Infos to complete
        self.icon = QtGui.QPixmap(":IconRevLum.png")
        #self.backgroundPixmap = QtGui.QPixmap("background.jpg")
        self.backgroundPixmap = QtGui.QPixmap("")
        #import popplerqt4
        #self.backgroundPixmap = QtGui.QPixmap("background.pdf")
        # self.backgroundPicture= QtGui.QPicture()
        # self.backgroundPicture.load("background.pdf")
        # print "**Background : "
        # print self.backgroundPicture.load("background.pdf")

        #posIcon == Left or Right
        self.posIcon = "Right"
        self.widthPage = 2340
        self.heigthPage = 3322
        self.margin = 100
        self.heightLine = 50
        heightLine = self.heightLine
        self.heightHead = 600
        color = QtGui.QColor(252, 183, 62)
        colorBlack = QtGui.QColor(0, 0, 0)
        #True or False
        self.numPageT = False
        self.numPage = 1
        # ordre des adresses
        self.soldToPos = 2
        self.shipToPos = 0
        self.billToPos = 1
        self.soldToHeader = ""
        self.shipToHeader = "Adresse de livraison :"
        self.billToHeader = "Adresse de facturation :"
        self.widthAdress = 700
        # title pos = Left Right or Center
        self.titlePos = QtCore.Qt.AlignLeft
        self.sizePoliceTitle = 18
        # small Table
        self.listHeaderSmallT = [
            "Votre Référence", "Date de devis",
            "Vendeur", "Condition de règlement"]
        self.listHeaderBigT = [
            "  Description", "Taxe", "Qte", "PU HT", "Prix "]
        # name that can be taken into account
        #Line, Code, Description, Quantity, UoM, UnitPriceHT, VAT, PriceHT
        self.listFields = [
            "Description", "VAT", "Quantity", "UnitPriceHT", "PriceHT"]

        def addY(value, RectHeightAfter=0):
            if(self.posY + value + RectHeightAfter > 3050):
                self.printer.newPage()
                self.numPage += 1
                background()
                self.posY = self.heightHead
            else:
                self.posY += value

        def background():

            # FULL PAGE
            painter.drawPixmap(
                0, 0, self.widthPage,
                self.heigthPage, self.backgroundPixmap)

            # numero page
            if self.numPageT is True:
                painter.setFont(QtGui.QFont("Arial", 11))
                painter.fillRect(
                    self.margin,  self.heigthPage -
                    self.margin - self.heightLine,
                    200, self.heightLine, color)
                painter.drawText(
                    self.margin, self.heigthPage -
                    self.margin - self.heightLine,
                    200, heightLine,
                    QtCore.Qt.AlignLeft, "page " + str(self.numPage))

        def adress():
            fontBold = QtGui.QFont("Arial", 10)
            fontBold.setBold(True)
            # request
            soldTo = _ThirdParty.query.filter(
                _ThirdParty.id == ligneDocHeader.soldToCode_id).one(). \
                description
            shipTo = _ThirdParty.query.filter(
                _ThirdParty.id == ligneDocHeader.shipToCode_id).one(). \
                description
            billTo = _ThirdParty.query.filter(
                _ThirdParty.id == ligneDocHeader.billToCode_id).one(). \
                description
            adress1 = _ThirdParty.query.filter(
                _ThirdParty.id == ligneDocHeader.soldToCode_id).one().address
            adress2 = _ThirdParty.query.filter(
                _ThirdParty.id == ligneDocHeader.shipToCode_id).one().address
            adress3 = _ThirdParty.query.filter(
                _ThirdParty.id == ligneDocHeader.billToCode_id).one().address

            # Adress
            # Adress sold to
            posTopAdress = self.posY
            self.soldToPos = ((self.widthPage - 2 * self.margin) / 3) * \
                self.soldToPos + self.margin
            painter.setFont(fontBold)
            painter.drawText(
                self.soldToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, self.soldToHeader)
            painter.setFont(QtGui.QFont("Arial", 11))
            addY(heightLine)
            # painter.drawText(
            #                 200, self.posY, 550, 200,
            #                 QtCore.Qt.AlignLeft,
            #        "account Number : " + str(ligneDocHeader.soldToCode_id))
            # addY(heightLine)
            painter.drawText(
                self.soldToPos, self.posY, self.widthAdress,
                self.heightLine, QtCore.Qt.AlignLeft, soldTo)
            addY(heightLine)
            painter.drawText(
                self.soldToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, adress1.addressLine1)

            if adress1.addressLine2:
                addY(heightLine)
                painter.drawText(
                    self.soldToPos, self.posY, self.widthAdress,
                    self.heightLine,
                    QtCore.Qt.AlignLeft, adress1.addressLine2)

            addY(heightLine)
            painter.drawText(
                self.soldToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, adress1.zIPCode + " " + adress1.city)
            addY(heightLine)
            painter.drawText(
                self.soldToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, _Country.query.filter(
                    _Country.id == adress1.country_id).one().description)

            # Adress ship to
            self.posY = posTopAdress
            self.shipToPos = (
                (self.widthPage - 2 * self.margin) / 3) * self.shipToPos + self.margin
            painter.setFont(fontBold)
            painter.drawText(
                self.shipToPos, self.posY, self.widthAdress,
                self.heightLine, QtCore.Qt.AlignLeft, self.shipToHeader)
            painter.setFont(QtGui.QFont("Arial", 11))
            addY(heightLine)
            # painter.drawText(
            #     750, self.posY, 550, 200,
            #     QtCore.Qt.AlignLeft,
            #     "account Number : " + str(ligneDocHeader.shipToCode_id))
            # addY(heightLine)
            painter.drawText(
                self.shipToPos, self.posY, self.widthAdress,
                self.heightLine, QtCore.Qt.AlignLeft, shipTo)
            addY(heightLine)
            painter.drawText(
                self.shipToPos, self.posY, self.widthAdress,
                self.heightLine, QtCore.Qt.AlignLeft, adress2.addressLine1)
            addY(heightLine)
            if adress2.addressLine2:
                painter.drawText(
                    self.shipToPos, self.posY,
                    self.widthAdress, self.heightLine,
                    QtCore.Qt.AlignLeft, adress2.addressLine2)
                addY(heightLine)

            painter.drawText(
                self.shipToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, adress2.zIPCode + " " + adress2.city)
            addY(heightLine)
            painter.drawText(
                self.shipToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, _Country.query.filter(
                    _Country.id == adress2.country_id).one().description)

            # Adress bill to
            self.posY = posTopAdress
            self.billToPos = (
                (self.widthPage - 2 * self.margin) / 3) * self.billToPos + self.margin
            painter.setFont(fontBold)
            painter.drawText(
                self.billToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, self.billToHeader)
            painter.setFont(QtGui.QFont("Arial", 11))
            addY(heightLine)
            # painter.drawText(
            #     1300, self.posY, 550, 200,
            #     QtCore.Qt.AlignLeft,
            #     "account Number : " + str(ligneDocHeader.billToCode_id))
            # addY(heightLine)
            painter.drawText(
                self.billToPos, self.posY, self.widthAdress,
                self.heightLine, QtCore.Qt.AlignLeft, billTo)
            addY(heightLine)
            painter.drawText(
                self.billToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, adress3.addressLine1)
            addY(heightLine)
            if adress3.addressLine2:
                painter.drawText(
                    self.billToPos, self.posY, self.widthAdress,
                    self.heightLine,
                    QtCore.Qt.AlignLeft, adress3.addressLine2)
                addY(heightLine)
            painter.drawText(
                self.billToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, adress3.zIPCode + " " + adress3.city)
            addY(heightLine)
            painter.drawText(
                self.billToPos, self.posY, self.widthAdress, self.heightLine,
                QtCore.Qt.AlignLeft, _Country.query.filter(
                    _Country.id == adress3.country_id).one().description)
            addY(2 * heightLine)
            # painter.setFont(QtGui.QFont("Arial", 11))

        def title(text, sizePoliceTitle, pos):
            fontBold = QtGui.QFont("Arial", sizePoliceTitle)
            fontBold.setBold(True)
            painter.setFont(fontBold)
            painter.drawText(
                self.margin, self.posY, self.widthPage - 2 * self.margin, 200,
                pos, text)
            painter.setFont(QtGui.QFont("Arial", 10))
            addY(3 * heightLine)

        def simpleTable(listHeader, listItem):
            fontBold = QtGui.QFont("Arial", 10)
            fontBold.setBold(True)
            painter.setFont(fontBold)
            nbColumn = len(listHeader)
            posIn = self.posY
            widthColumn = (self.widthPage - 2 * self.margin) / nbColumn
            i = 0
            painter.setPen(color)
            painter.drawLine(
                self.margin, self.posY,
                self.widthPage - self.margin, self.posY)
            painter.setPen(colorBlack)
            for n in listHeader:
                painter.drawText(
                    self.margin + i * widthColumn, self.posY,
                    widthColumn, self.heightLine,
                    QtCore.Qt.AlignHCenter, _fromUtf8(n))
                i += 1
            addY(heightLine)
            painter.setPen(color)
            painter.drawLine(
                self.margin, self.posY,
                self.widthPage - self.margin, self.posY)
            painter.setPen(colorBlack)
            painter.setFont(QtGui.QFont("Arial", 10))
            for n in listItem:
                i = 0
                for nn in n:
                    painter.drawText(
                        self.margin + i * widthColumn, self.posY,
                        widthColumn, self.heightLine,
                        QtCore.Qt.AlignHCenter, _fromUtf8(nn))
                    i += 1
                addY(heightLine)
                painter.setPen(color)
                painter.drawLine(
                    self.margin, self.posY,
                    self.widthPage - self.margin, self.posY)
                painter.setPen(colorBlack)

            posOut = self.posY
            # line vertical
            painter.setPen(color)
            painter.drawLine(
                self.margin, posIn,
                self.margin, posOut)
            i = 1
            for n in listHeader:
                painter.drawLine(
                    self.margin + i * widthColumn, posIn,
                    self.margin + i * widthColumn, posOut)
                i += 1
            painter.setPen(colorBlack)

        def itemTable(listHeader, listFields):
            # name that can be taken into account
            #Line, Code, Description, Quantity, UoM, UnitPriceHT, VAT, PriceHT
            # 50     360                350     150    300       150  360
            widthColumnLine = 50
            widthColumnCode = 360
            widthColumnQuantity = 220
            widthColumnUoM = 150
            widthColumnUnitPriceHT = 200
            widthColumnVAT = 150
            widthColumnPriceHT = 360

            # calcul width for description
            totalWidthColumn = 0
            for n in listFields:
                if n == "Line":
                    totalWidthColumn += widthColumnLine
                elif n == "Code":
                    totalWidthColumn += widthColumnCode
                elif n == "Quantity":
                    totalWidthColumn += widthColumnQuantity
                elif n == "UoM":
                    totalWidthColumn += widthColumnUoM
                elif n == "UnitPriceHT":
                    totalWidthColumn += widthColumnUnitPriceHT
                elif n == "VAT":
                    totalWidthColumn += widthColumnVAT
                elif n == "PriceHT":
                    totalWidthColumn += widthColumnPriceHT
            widthColumnDescription = self.widthPage - 2 * self.margin
            widthColumnDescription -= totalWidthColumn
            painter.fillRect(
                self.margin, self.posY, self.widthPage - 2 * self.margin,
                self.heightLine, color)
            i = 0
            x = self.margin
            for n in listFields:
                if n == "Line":
                    painter.drawText(
                        x, self.posY,
                        widthColumnLine, self.heightLine,
                        QtCore.Qt.AlignHCenter, _fromUtf8(listHeader[i]))
                    x += widthColumnLine
                elif n == "Code":
                    painter.drawText(
                        x, self.posY,
                        widthColumnCode, self.heightLine,
                        QtCore.Qt.AlignHCenter, _fromUtf8(listHeader[i]))
                    x += widthColumnCode
                elif n == "Quantity":
                    painter.drawText(
                        x, self.posY,
                        widthColumnQuantity, self.heightLine,
                        QtCore.Qt.AlignRight, _fromUtf8(listHeader[i]))
                    x += widthColumnQuantity
                elif n == "UoM":
                    painter.drawText(
                        x, self.posY,
                        widthColumnUoM, self.heightLine,
                        QtCore.Qt.AlignRight, _fromUtf8(listHeader[i]))
                    x += widthColumnUoM
                elif n == "UnitPriceHT":
                    painter.drawText(
                        x, self.posY,
                        widthColumnUnitPriceHT, self.heightLine,
                        QtCore.Qt.AlignRight, _fromUtf8(listHeader[i]))
                    x += widthColumnUnitPriceHT
                elif n == "VAT":
                    painter.drawText(
                        x, self.posY,
                        widthColumnVAT, self.heightLine,
                        QtCore.Qt.AlignRight, _fromUtf8(listHeader[i]))
                    x += widthColumnVAT
                elif n == "PriceHT":
                    painter.drawText(
                        x, self.posY,
                        widthColumnPriceHT, self.heightLine,
                        QtCore.Qt.AlignRight, _fromUtf8(listHeader[i]))
                    x += widthColumnPriceHT
                elif n == "Description":
                    painter.drawText(
                        x, self.posY,
                        widthColumnDescription, self.heightLine,
                        QtCore.Qt.AlignLeft, _fromUtf8(listHeader[i]))
                    x += widthColumnDescription
                i += 1

            listDocumentDetail = DocumentDetail.query\
                .filter(DocumentDetail.documentHeader_id == self.id)\
                .order_by(DocumentDetail.lineNumber).all()

            addY(heightLine)
            painter.setFont(QtGui.QFont("Arial", 10))
            # l = article
            for l in listDocumentDetail:
                for p in l.prices:
                    if p.priceCondition_id == 1:
                        unitPrice = p.unitPrice
                        result = p.result
                    if p.priceCondition_id == 2:
                        VAT = p.conditionValue
                x = self.margin
                # n = name column
                for n in listFields:
                    if n == "Line":
                        painter.drawText(
                            x, self.posY,
                            widthColumnLine, self.heightLine,
                            QtCore.Qt.AlignLeft, str(l.lineNumber))
                        x += widthColumnLine
                    elif n == "Code":
                        itemDCode = _ItemDetail.query.filter(
                            _ItemDetail.id == l.itemDetail_id).one().code
                        painter.drawText(
                            x, self.posY,
                            widthColumnCode, self.heightLine,
                            QtCore.Qt.AlignLeft, itemDCode)
                        x += widthColumnCode
                    elif n == "Quantity":
                        painter.drawText(x,
                                         self.posY,
                                         widthColumnQuantity,
                                         self.heightLine,
                                         QtCore.Qt.AlignRight,
                                         locale.format('%.3f',
                                                       l.lineQuantity,
                                                       grouping=True))
                        x += widthColumnQuantity
                    elif n == "UoM":
                        painter.drawText(
                            x, self.posY,
                            widthColumnUoM, self.heightLine,
                            QtCore.Qt.AlignHCenter, str(l.lineUoM))
                        x += widthColumnUoM
                    elif n == "UnitPriceHT":
                        painter.drawText(
                            x, self.posY,
                            widthColumnUnitPriceHT, self.heightLine,
                            QtCore.Qt.AlignRight, "%.2f" % unitPrice)
                        x += widthColumnUnitPriceHT
                    elif n == "VAT":
                        painter.drawText(
                            x, self.posY,
                            widthColumnVAT, self.heightLine,
                            QtCore.Qt.AlignRight, "%.2f" % float(VAT))
                        x += widthColumnVAT
                    elif n == "PriceHT":
                        painter.drawText(
                            x, self.posY,
                            widthColumnPriceHT, self.heightLine,
                            QtCore.Qt.AlignRight,
                            "%.2f" % result + " " + str(l.lineCurrency))
                        x += widthColumnPriceHT
                    elif n == "Description":
                        posIn = self.posY
                        detailDescription = _ItemDetail.query.filter(
                            _ItemDetail.id == l.itemDetail_id).one(). \
                            description
                        fontBold = QtGui.QFont("Arial", 10)
                        fontBold.setBold(True)
                        painter.setFont(fontBold)
                        painter.drawText(
                            x, self.posY,
                            widthColumnDescription, 200,
                            QtCore.Qt.TextWordWrap, detailDescription)
                        painter.setFont(QtGui.QFont("Arial", 10))
                        itemDD = _ItemDetail.query.filter(
                            _ItemDetail.id == l.itemDetail_id).one(). \
                            printedComment
                        addY(heightLine)
                        if itemDD:
                            rectMin = painter.drawText(
                                x, self.posY,
                                widthColumnDescription, 200,
                                QtCore.Qt.TextWordWrap, itemDD)
                            addY(rectMin.height())

                        if l.printedComment:
                            rectMin = painter.drawText(
                                x, self.posY,
                                widthColumnDescription, 600,
                                QtCore.Qt.TextWordWrap, l.printedComment)
                            addY(rectMin.height())
                    # if l.reqDeliveryEnd:
                    #     addY(heightLine)
                    #     painter.drawText(
                    #         self.margin+i*widthColumn, self.posY,
                    #            2*widthColumn, 200,
                    #         QtCore.Qt.TextWordWrap,
                    #         "from " +
                    #         l.reqDeliveryStart.strftime('%d/%m/%Y') +
                    #         " to " + l.reqDeliveryEnd.strftime('%d/%m/%Y'))
                            painter.setFont(QtGui.QFont("Arial", 11))
                        posOut = self.posY
                        self.posY = posIn
                        x += widthColumnDescription
                self.posY = posOut
                addY(heightLine)

        def total():
            addY(2 * self.heightLine)
            painter.setPen(color)
            painter.drawLine(
                self.margin, self.posY - 20,
                self.widthPage - self.margin, self.posY - 20)
            painter.setPen(colorBlack)
            currencyHeader = str(_Currency.query.filter(
                _Currency.id == ligneDocHeader.currency_id).one().code)
            painter.drawText(
                1450, self.posY, 300, 200, QtCore.Qt.AlignRight, "Total HT")
            painter.drawText(
                self.widthPage - self.margin - 400, self.posY, 400, 200,
                QtCore.Qt.AlignRight,
                "%.2f" % ligneDocHeader.docTotalAmountExclVat +
                " " + currencyHeader)
            addY(heightLine)
            painter.drawText(
                1450, self.posY, 300, 200, QtCore.Qt.AlignRight, "TVA")
            painter.drawText(
                self.widthPage - self.margin - 400, self.posY, 400, 200,
                QtCore.Qt.AlignRight,
                "%.2f" % ligneDocHeader.docTotalVat + " " + currencyHeader)
            addY(heightLine)
            font = QtGui.QFont("Arial", 11)
            font.setBold(True)
            painter.setFont(font)
            painter.drawText(
                1430, self.posY, 320, 200, QtCore.Qt.AlignRight, "Total TTC")
            painter.drawText(
                self.widthPage - self.margin - 400, self.posY, 400, 200,
                QtCore.Qt.AlignRight,
                "%.2f" % ligneDocHeader.docTotalAmountInclVat +
                " " + currencyHeader)
            painter.setFont(QtGui.QFont("Arial", 11))

        def bottom():
            addY(2 * heightLine)
            # bank
            if ligneDocHeader.bank_id:
                bank = _Bank.query.filter(
                    _Bank.id == ligneDocHeader.bank_id).one()
                currency = _Currency.query.filter(
                    _Currency.id == bank.currency_id).one().description
                painter.drawText(
                    self.margin, self.posY,
                    self.widthPage - 2 * self.margin, 200,
                    QtCore.Qt.TextWordWrap,
                    bank.code + " " + bank.description + " " +
                    currency + " " + bank.iBANCode)

            addY(heightLine)
            paymentTerm = _PaymentTerm.query.filter(
                _PaymentTerm.id == ligneDocHeader.paymentTerm_id). \
                one().description
            painter.drawText(
                self.margin, self.posY, self.widthPage - 2 * self.margin, 200,
                QtCore.Qt.TextWordWrap, paymentTerm)
            if ligneDocHeader.paymentTermText_id:
                paymentTermText = _PaymentTermText.query.filter(
                    _PaymentTermText.id == ligneDocHeader.paymentTermText_id)\
                    .one().text
                if paymentTermText:
                    addY(heightLine)
                    rectMin = painter.drawText(
                        self.margin, self.posY,
                        self.widthPage - 2 * self.margin, 700,
                        QtCore.Qt.TextWordWrap, paymentTermText)
                    addY(rectMin.height())
            addY(heightLine)
            incoT = _Incoterm.query.filter(
                _Incoterm.id == ligneDocHeader.incoterm_id).one().description
            painter.drawText(
                self.margin, self.posY, self.widthPage - 2 * self.margin,
                200, QtCore.Qt.TextWordWrap, incoT)
            if ligneDocHeader.incotermText_id:
                incoTText = _IncotermText.query.filter(
                    _IncotermText.id == ligneDocHeader.incotermText_id). \
                    one().text
                if incoTText:
                    addY(heightLine)
                    rectMin = painter.drawText(
                        self.margin, self.posY,
                        self.widthPage - 2 * self.margin, 700,
                        QtCore.Qt.TextWordWrap, incoTText)
                    addY(rectMin)
            pass
        #**************************************************************
        #**************************************************************
        #**************************************************************
        self.printer = QtGui.QPrinter()
        self.printer.setResolution(300)
        self.printer.setOutputFormat(QtGui.QPrinter.PdfFormat)
        self.printer.setOutputFileName("myPDF.pdf")
        painter = QtGui.QPainter()
        painter.begin(self.printer)
        ligneDocHeader = DocumentHeader.query.filter(
            DocumentHeader.id == self.id).one()
        self.posY = self.margin
        background()
        self.posY = self.heightHead
        adress()
        ti = ligneDocHeader.docType.description + " # " + ligneDocHeader. \
            documentNumber
        title(ti, self.sizePoliceTitle, self.titlePos)

        self.date = ligneDocHeader.documentValueDate.strftime('%d/%m/%Y')
        paymentTerm = _PaymentTerm.query.filter(
            _PaymentTerm.id == ligneDocHeader.paymentTerm_id). \
            one().description

        if ligneDocHeader.salesRep_id:
            salesRep = _SalesRep.query.filter(
                _SalesRep.id == ligneDocHeader.salesRep_id).one().description
        else:
            salesRep = ""
        simpleTable(
            self.listHeaderSmallT,
            [[ligneDocHeader.externalDocRef,
              self.date, salesRep, paymentTerm]])
        addY(heightLine)
        itemTable(self.listHeaderBigT, self.listFields)
        total()
        bottom()
        painter.end()
        os.startfile("myPDF.pdf")
        #**************************************************************
        #**************************************************************
        #**************************************************************


def validate_docType(target, value, oldvalue, initiator):
    if not value:
        raise ValueError(u'docType: null value')
    if not target.priceSchemaHeader:
        if isinstance(value, int):
            docType = _DocType.query.filter_by(id=value).one()
        else:
            docType = value
        target.priceSchemaHeader = docType.priceHeader
    return value
event.listen(DocumentHeader.docType_id,
             'set', validate_docType, propagate=True)
event.listen(DocumentHeader.docType, 'set', validate_docType, propagate=True)


def validate_site(target, value, oldvalue, initiator):
    if isinstance(value, int):
        site = _Site.query.filter_by(id=value).one()
    else:
        site = value
    if value is None:
        raise ValueError('site: could not be null\n')
    if not target.currency:
        target.currency = site.company.currency
    return value
event.listen(DocumentHeader.site_id,
             'set', validate_site, propagate=True)
event.listen(DocumentHeader.site, 'set', validate_site, propagate=True)


class Price(InitMixin, Base):
    verbose_name = _('Document Detail Price')
    list_display = [
        'step',
        'priceCondition', 'priceCondition_description',
        'conditionValue',  'unitPrice', 'quantity',
        'action', 'result',
        'resultExtraCurrency', 'exchangeRate_company_currency',
        'exchangeRate_exchangeRateValue'
    ]

    list_columns_editable = [
        'step', 'priceCondition',
        'conditionValue', 'action', 'result',
    ]

    # step = Column("Step", Unicode(), info={'verbose_name': u'Step'})
    step = Column("Step", Integer, info={'verbose_name': _('Step')})
    action = Column("Action", Unicode(), info={'verbose_name': _('Action')})
    conditionValue = Column("ConditionValue", Unicode(),
                            info={'verbose_name': _('Condition Value')})
    quantity = Column(QUANTITY, Float, info={'verbose_name': QUANTITY})
    unitPrice = Column(UNITPRICE, Float,
                       info={'verbose_name': UNITPRICE})
    result = Column(RESULT, Float, info={'verbose_name': RESULT})
    resultExtraCurrency = Column(
        "ResultExtraCurrency",
        Float,
        info={'verbose_name': _("Extra Currency Result")})

    documentDetail_id = Column(
        "DocumentDetail_pk",
        Integer,
        ForeignKey('DocumentDetail.DocumentDetail_pk'),
        primary_key=True
    )
    documentDetail = relationship("DocumentDetail")

    priceCondition_id = Column(
        "PriceCondition_pk",
        Integer,
        ForeignKey('PriceCondition.PriceCondition_pk'),
        primary_key=True
    )
    priceCondition = relationship(
        "PriceCondition",
        info={'verbose_name': _PC.verbose_name})

    exchangeRate_id = Column("ExchangeRate_pk",
                             Integer,
                             ForeignKey('ExchangeRate.ExchangeRate_pk'))
    exchangeRate = relationship(
        "ExchangeRate",
        info={'verbose_name': _("Exchange Rate"),
              'datalookup': 'sysgrove.ui.classes.UIDataLookupExchangRate',
              'description': ['targetCurrency_code']})

    # This a technical fk to keep link between price and service
    # when price.priceCondition.serviceFlag = True
    # It should not been show the user.
    serviceProvider_id = Column(
        "ServiceProvider_pk", Integer,
        ForeignKey('ServiceProvider.ServiceProvider_pk'))
    serviceProvider = relationship("ServiceProvider")

    # For futurePricing total result
    quantityToBeFixed = Column(
        "QuantityToBeFixed",
        Float,
        info={'verbose_name': _("Quantity to be fixed")})
    weightedAverageValue = Column(
        "WeightedAverageValue", Float,
        info={'verbose_name': _("Weighted Average Value")})

    futurePricings = relationship(
        "FuturePricing",
        primaryjoin="Price.id==FuturePricing.price_id",
        cascade="save-update, merge, expunge")

    def from_priceSchemaDetail(self, psd):
        self.priceCondition = psd.priceCondition
        self.step = psd.step
        self.action = psd.action
        self.conditionValue = psd.conditionValue
        self.result = 0.0

    def duplicate(self, docDetail, sp_duplicate):
        new_price = Price(documentDetail=docDetail)
        fields = self._fields().keys()
        fields.remove('documentDetail')
        fields.remove('result')
        fields.remove('serviceProvider')
        fields.remove('futurePricings')
        fields.remove('quantityToBeFixed')
        fields.remove('weightedAverageValue')

        if self.serviceProvider:
            new_price.serviceProvider = sp_duplicate[self.serviceProvider]
        for f in fields:
            setattr(new_price, f, getattr(self, f))
        for future in self.futurePricings:
            nfuture = future.duplicate(new_price)
            new_price.futurePricings.append(nfuture)
            docDetail.futurePricings.append(nfuture)
            if (docDetail.documentHeader.premium_flat and
                    nfuture.priceCondition == _PC.prm_condition()):
                docDetail.premiumFixing = nfuture
        if new_price.quantity and new_price.unitPrice:
            new_price.quantity = docDetail.lineQuantity
            new_price.unitPrice = docDetail.linePrice
        return new_price

    def compute_future(self):
        if not self.documentDetail.documentHeader.futureFixingFlag:
            return

        # Premium Flat case
        if (self.documentDetail.documentHeader.premium_flat and
                self.priceCondition == _PC.prm_condition()):
            assert(len(self.futurePricings) <= 1)
            if self.futurePricings:
                if self.futurePricings[0].fixedPrice > 0:
                    self.weightedAverageValue = self.futurePricings[0]\
                        .fixedPrice
                    # FlatPremium should not appear in the price computation
                    # before futurePrice is fixed.
                    self.quantityToBeFixed = 0.0
            return

        # Case when the price is flat
        if (not self.documentDetail.documentHeader.priceFlag and
                self.priceCondition == _PC.future_condition()):
            assert(len(self.futurePricings) <= 1)
            # let use the documentDetail.linePrice
            if not self.futurePricings:
                future = FuturePricing()
                future.unitOfMeasure = self.documentDetail.lineUoM
                future.currency = self.documentDetail.lineCurrency
                future.priceUoM = self.documentDetail.linePriceUoM
                future.documentDetail = self.documentDetail
                future.price = self
                future.priceCondition = self.priceCondition
                future.fixedPrice = self.documentDetail.linePrice
                self.futurePricings.append(future)
                self.documentDetail.futurePricings.append(future)
                self.quantityToBeFixed = 0.0
                self.weightedAverageValue = self.documentDetail.linePrice

            return

        # ajuste quantityToBeFixed and weightedAverageValue
        # from the futurePricings values
        totalFixedQuantity = 0.0
        total = 0.0
        # check the UoM
        if len(self.futurePricings) > 0:
            uom = self.futurePricings[0].unitOfMeasure
            currency = self.futurePricings[0].currency
            for future in self.futurePricings:
                if not uom == future.unitOfMeasure:
                    raise UserMessage('Too Many Unit Of Measure in fixing %s' %
                                      self.priceCondition.description)
                if not currency == future.currency:
                    raise UserMessage('Many currency in fixing %s' %
                                      self.priceCondition.description)
                totalFixedQuantity = totalFixedQuantity + \
                    future.fixedQuantity
                total = total + future.fixedQuantity * future.fixedPrice

            if totalFixedQuantity:
                self.quantityToBeFixed = abs(
                    self.documentDetail.lineQuantity -
                    convert2Uom(totalFixedQuantity,
                                uom,
                                self.documentDetail.lineUoM))
                if self.priceCondition == _PC.spread_condition():
                    self.weightedAverageValue = total / \
                        self.documentDetail.lineQuantity
                else:
                    self.weightedAverageValue = total / totalFixedQuantity

    def is_fixed(self):
        if self.priceCondition.is_flat():
            return True
        if self.priceCondition == _PC.spread_condition():
            return True
        if (not self.documentDetail.documentHeader.priceFlag and
                self.priceCondition == _PC.future_condition()):
            return True
        if self.futurePricings == []:
            return False
        return self.quantityToBeFixed < ROUNDING_MIN

    def weightedAverageValueCurrency(self):
        currency = set([x.currency for x in self.futurePricings])
        if len(currency) > 1:
            raise UserMessage('Too Many Currency in fixing %s' %
                              self.priceCondition.description)
        if len(currency) == 1:
            return currency.pop()

    def weightedAverageValueUoM(self):
        uom = set([x.priceUoM for x in self.futurePricings])
        if len(uom) > 1:
            raise UserMessage('Too Many Unit Of Measure in fixing %s' %
                              self.priceCondition.description)
        if len(uom) == 1:
            return uom.pop()


# quantityToBeFixed
def validate_quantityToBeFixed(target, value, oldvalue, initiator):
    if value < 0:
        raise UserMessage('quantityToBeFixed: can not be negative')
    return value
event.listen(Price.quantityToBeFixed, 'set',
             validate_quantityToBeFixed, propagate=True)


# weightedAverageValue
# to be review when implementing SG - 395
def validate_weightedAverageValue(target, value, oldvalue, initiator):
    if value == oldvalue:
        return value
    target.unitPrice = convert2Uom(value,
                                   target.weightedAverageValueUoM(),
                                   target.documentDetail.lineUoM)
    return value
event.listen(Price.weightedAverageValue, 'set',
             validate_weightedAverageValue, propagate=True)


class FuturePricing(InitMixin, Base):

    verbose_name = _('Future Pricing')

    currency_id = Column("Currency_pk",
                         Integer,
                         ForeignKey('Currency.Currency_pk'))
    currency = relationship("Currency",
                            info={'verbose_name': _Currency.verbose_name})

    # UnitOfMeasure FK
    unitOfMeasure_id = Column(
        "UnitOfMeasure_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk'))
    unitOfMeasure = relationship(
        "UnitOfMeasure",
        primaryjoin="FuturePricing.unitOfMeasure_id==UnitOfMeasure.id",
        info={'verbose_name': _UOM.verbose_name})

    priceUoM_id = Column(
        "PriceUoM_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk'))
    priceUoM = relationship(
        "UnitOfMeasure",
        primaryjoin="FuturePricing.priceUoM_id==UnitOfMeasure.id",
        info={'verbose_name': _UOM.verbose_name})

    __table_args__ = (
        ForeignKeyConstraint(
            ['MarketType', 'ItemGroup'],
            ['MarketCommodity.MarketType', 'MarketCommodity.ItemGroup_pk']),
        ForeignKeyConstraint(
            ["Price_pk", "PriceCondition_pk", "DocumentDetail_pk"],
            [
                'Price.Price_pk',
                'Price.PriceCondition_pk',
                'Price.DocumentDetail_pk'
            ]),
    )

    # Price Table relation
    price_id = Column("Price_pk", Integer)
    price = relationship(
        "Price",
        primaryjoin="FuturePricing.price_id==Price.id")

    priceCondition_id = Column("PriceCondition_pk", Integer)
    priceCondition = relationship(
        "PriceCondition",
        foreign_keys=priceCondition_id,
        primaryjoin="FuturePricing.priceCondition_id==PriceCondition.id",
        info={'verbose_name': _PC.verbose_name})

    documentDetail_id = Column("DocumentDetail_pk", Integer)
    documentDetail = relationship(
        "DocumentDetail",
        foreign_keys=documentDetail_id,
        primaryjoin="FuturePricing.documentDetail_id==DocumentDetail.id")

    # MarketCommodity FK
    marketType_id = Column("MarketType", Integer)
    marketType = relationship(
        "MarketType",
        primaryjoin="foreign(FuturePricing.marketType_id)==MarketType.id",
        info={'verbose_name': _("Market Place"),
              'datalookup':
              'sysgrove.core.document.ui.customs.DataLookupMarket'})

    itemGroup_id = Column("ItemGroup", Integer)
    itemGroup = relationship(
        "ItemGroup",
        primaryjoin=
        "foreign(FuturePricing.itemGroup_id)==ItemGroup.id",
        info={'verbose_name': _ItemGroup.verbose_name})

    marketCommodity = relationship("MarketCommodity")

    marketMonth = Column('MarketMonth', Date,
                         info={'verbose_name': _('Market Month')})

    spreadMonth = Column('SpreadMonth', Date,
                         info={'verbose_name': _('Spread New Month')})

    fixedPrice = Column("FixedPrice", Float,
                        info={'verbose_name': _("Fixed Price")})

    fixedQuantity = Column("FixedQuantity", Float,
                           info={'verbose_name': _("Fixed Quantity")})

    fixingDate = Column("FixingDate", Date,
                        info={'verbose_name': _("Date")})

    fixedPriceDocUnitPriceUoM = Column(
        'FixedPriceDocUnitPriceUoM',
        Float,
        info={'verbose_name': _("Fixed Price in Document UnitPrice UoM")})

    # This formular is a real pb, because some datalookup
    # and uiObject have the same name.
    list_display = [[
        ('DateGB', 'fixingDate'),
        ('DataLookUpGB', 'marketType'),
        (('GroupBox', marketMonth.info['verbose_name']),
            [(line_month, 'marketMonth')]),
        (('GroupBox', spreadMonth.info['verbose_name']),
         [(line_month, 'spreadMonth')]),
    ],
        [('Vlist', [
            (('GroupBox', u"Future Fixing"),
             [['fixedQuantity', (SIMPLE_DATALOOKUP, 'unitOfMeasure'),
               'fixedPrice',
               (SIMPLE_DATALOOKUP, 'currency'), (('Label', '/'), 'slashLabel'),
               (SIMPLE_DATALOOKUP, 'priceUoM')],
              [('Spacer', 'H'), 'fixedPriceDocUnitPriceUoM',
               (lineedit, 'documentDetail_lineCurrency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'documentDetail_linePriceUoM')]]),
            (('GroupBox', u"Forex Fixing"),
             [['fixedQuantity', (SIMPLE_DATALOOKUP, 'unitOfMeasure'),
               'fixedPrice',
               (SIMPLE_DATALOOKUP, 'currency'), (('Label', '/'), 'slashLabel'),
               (SIMPLE_DATALOOKUP, 'priceUoM')],
              [('Spacer', 'H'), 'fixedPriceDocUnitPriceUoM',
               (lineedit, 'documentDetail_lineCurrency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'documentDetail_linePriceUoM')]]),
            (('GroupBox', u"Spread Fixing"),
             [['fixedQuantity', (SIMPLE_DATALOOKUP, 'unitOfMeasure'),
               'fixedPrice',
               (SIMPLE_DATALOOKUP, 'currency'), (('Label', '/'), 'slashLabel'),
               (SIMPLE_DATALOOKUP, 'priceUoM')],
              [('Spacer', 'H'), 'fixedPriceDocUnitPriceUoM',
               (lineedit, 'documentDetail_lineCurrency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'documentDetail_linePriceUoM')]]),
            (('GroupBox', u"Premium Fixing"),
             [['fixedQuantity', (SIMPLE_DATALOOKUP, 'unitOfMeasure'),
               'fixedPrice',
               (SIMPLE_DATALOOKUP, 'currency'), (('Label', '/'), 'slashLabel'),
               (SIMPLE_DATALOOKUP, 'priceUoM')],
              [('Spacer', 'H'), 'fixedPriceDocUnitPriceUoM',
               (lineedit, 'documentDetail_lineCurrency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'documentDetail_linePriceUoM')]]),
        ]), ('Vlist', [
            (('GroupBox', u"Future Info"),
             [['documentDetail_lineQuantity',
               (lineedit, 'documentDetail_lineUoM')],
              ['price_quantityToBeFixed',
               (lineedit, 'documentDetail_lineUoM')],
              ['price_weightedAverageValue',
               (lineedit, 'currency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'priceUoM')]]),
            (('GroupBox', u"Forex Info"),
             [['documentDetail_lineQuantity',
               (lineedit, 'documentDetail_lineUoM')],
              ['price_quantityToBeFixed',
               (lineedit, 'documentDetail_lineUoM')],
              ['price_weightedAverageValue',
               (lineedit, 'currency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'priceUoM')]]),
            (('GroupBox', u"Spread Info"),
             [['documentDetail_lineQuantity',
               (lineedit, 'documentDetail_lineUoM')],
              ['price_quantityToBeFixed',
               (lineedit, 'documentDetail_lineUoM')],
              ['price_weightedAverageValue',
               (lineedit, 'currency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'priceUoM')]]),
            (('GroupBox', u"Premiun Info"),
             [['documentDetail_lineQuantity',
              (lineedit, 'documentDetail_lineUoM')],
              ['price_quantityToBeFixed',
               (lineedit, 'documentDetail_lineUoM')],
              ['price_weightedAverageValue',
               (lineedit, 'currency'),
               (('Label', '/'), 'slashLabel'),
               (lineedit, 'priceUoM')]]),
        ])]]

    list_readonly = ['fixedPriceDocUnitPriceUoM',
                     'documentDetail_lineCurrency',
                     'documentDetail_linePriceUoM']

    list_fixing = ['fixedQuantity', 'unitOfMeasure',
                   'fixedPrice', 'currency', 'priceUoM',
                   ]
    # list_fixing.extend(list_readonly)
    list_common = ['fixingDate', 'marketType', 'marketMonth', 'spreadMonth']
    list_info = ['documentDetail_lineQuantity',
                 'documentDetail_lineUoM',
                 'price_quantityToBeFixed',
                 'price_weightedAverageValue',
                 'currency', 'priceUoM']
    list_columns = list_fixing + list_readonly + \
        list_common + ['priceCondition']

    list_clear = ['fixedQuantity', 'fixedPriceDocUnitPriceUoM',
                  'fixedQuantity', 'fixedPrice',
                  'marketType_description'] + list_common

    def duplicate(self, price):
        new_fixing = FuturePricing(price=price,
                                   priceCondition=price.priceCondition)
        fields = self._fields().keys()
        fields.remove('documentDetail')
        fields.remove('price')
        fields.remove('priceCondition')
        fields.remove('itemGroup')

        for f in fields:
            setattr(new_fixing, f, getattr(self, f))
        new_fixing.documentDetail = price.documentDetail
        new_fixing.price = price
        return new_fixing

    def set_fixedPriceDocUnitPriceUoM(self):
        if self.priceUoM == self.documentDetail.linePriceUoM:
            if self.currency == self.documentDetail.lineCurrency:
                self.fixedPriceDocUnitPriceUoM = self.fixedPrice
            else:
                raise UserMessage(
                    "Which Exchange rate value has to be choosen?")
        else:
            if self.currency == self.documentDetail.lineCurrency:
                self.fixedPriceDocUnitPriceUoM = convert2Uom(
                    self.fixedPrice,
                    self.priceUoM,
                    self.documentDetail.linePriceUoM)
            else:
                raise UserMessage(
                    "Which Exchange rate value has to be choosen?")


# documentDetail
def validate_docDetail(target, value, oldvalue, initiator):
    if isinstance(value, int):
        dd = DocumentDetail.query.filter_by(id=value).one()
    else:
        dd = value
    # set the itemGroup too
    target.itemGroup = dd.itemDetail.itemGroup
    return value

event.listen(FuturePricing.documentDetail_id, 'set',
             validate_docDetail, propagate=True)
event.listen(FuturePricing.documentDetail, 'set',
             validate_docDetail, propagate=True)


class DocumentDetail(InitMixin, Base):
    verbose_name = _('Document Detail')

    # DocumentHeader FK
    documentHeader_id = Column(
        "DocumentHeader_pk",
        Integer,
        ForeignKey('DocumentHeader.DocumentHeader_pk')
    )
    documentHeader = relationship("DocumentHeader")

    lineNumber = Column(
        "LineNumber", Integer,
        nullable=False,
        info={'verbose_name': _('Line #')})

    itemDetail_id = Column(
        "ItemDetail_pk", Integer,
        ForeignKey('ItemDetail.ItemDetail_pk'),
        nullable=False,
    )
    itemDetail = relationship(
        "ItemDetail",
        info={
            'verbose_name': _('Mat Code'),
            'datalookup':
            'sysgrove.modules.masterdata.ui.customs.DataLookupItemDetail'}
    )

    status_id = Column(
        "Status_pk",
        Integer,
        ForeignKey('Status.Status_pk'),
        ColumnDefault(STATUS_OPEN),
        nullable=False,

    )
    status = relationship(
        "Status",  info={'verbose_name': _("Document Detail Status")})

    linePrice = Column(
        "LinePrice", Float,
        # ColumnDefault(0.0),
        info={'verbose_name': _('Price')},
        # nullable=False,
    )

    lineQuantity = Column(
        "LineQuantity", Float,
        info={'verbose_name': _('Quantity')},
        nullable=False,
    )

    lineQuantityOpen = Column(
        "LineQuantityOpen", Float,
        info={'verbose_name': _('Qty Open')})
    # FIXME: to be removed???
    lineQuantityReleased = Column(
        "LineQuantityReleased", Float,
        info={'verbose_name': _('Qty Released')})
    lineQuantityDelivered = Column(
        "LineQuantityDelivered", Float,
        info={'verbose_name': _('Qty Delivered')})
    lineQuantityInvoiced = Column(
        "LineQuantityInvoiced", Float,
        info={'verbose_name': _('Qty Invoiced')})

    # UnitOfMeasure FK
    lineUoM_id = Column(
        "LineUoM",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk'),
        nullable=False,)

    lineUoM = relationship(
        "UnitOfMeasure",
        primaryjoin="DocumentDetail.lineUoM_id==UnitOfMeasure.id",
        info={'verbose_name': _UOM.verbose_name})

    linePriceUoM_id = Column(
        "LinePriceUoM",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk'))

    linePriceUoM = relationship(
        "UnitOfMeasure",
        primaryjoin="DocumentDetail.linePriceUoM_id==UnitOfMeasure.id",
        info={'verbose_name': _UOM.verbose_name})

    # Currency FK
    lineCurrency_id = Column(
        "LineCurrency",
        Integer,
        ForeignKey('Currency.Currency_pk'),
        nullable=False)

    lineCurrency = relationship(
        "Currency",
        info={'verbose_name': _Currency.verbose_name})

    exchangeRate_id = Column("ExchangeRate_pk",
                             Integer,
                             ForeignKey('ExchangeRate.ExchangeRate_pk'))
    exchangeRate = relationship(
        "ExchangeRate",
        info={'verbose_name': _("Exchange Rate"),
              'datalookup': 'sysgrove.ui.classes.UIDataLookupExchangRate',
              'description': ['targetCurrency_code']})

    # Warehouse FK
    warehouse_id = Column(
        "Warehouse_pk",
        Integer,
        ForeignKey('Warehouse.Warehouse_pk'),
    )

    warehouse = relationship(
        "Warehouse",
        info={'verbose_name': _Warehouse.verbose_name})

    # lot FK
    lot_id = Column("Lot_pk", Integer,
                    ForeignKey('Lot.Lot_pk'))

    lot = relationship("Lot",
                       info={'verbose_name': _Lot.verbose_name})

    # Packing FK
    packing_id = Column("Packing_pk", Integer,
                        ForeignKey('Packing.Packing_pk'))

    packing = relationship("Packing",
                           info={'verbose_name': _Packing.verbose_name})

    reqDeliveryStart = Column(
        "ReqDeliveryStart", Date,
        info={'verbose_name': _('Required Delivery Start Date')},
        nullable=False,
    )
    reqDeliveryEnd = Column(
        "ReqDeliveryEnd", Date,
        info={'verbose_name': _('Required Delivery End Date')},
        nullable=False,
    )
    calcDeliveryStart = Column(
        "CalcDeliveryStart", Date,
        info={'verbose_name': _('Calculated Delivery Start Date')}
    )
    calcDeliveryEnd = Column(
        "CalcDeliveryEnd", Date,
        info={'verbose_name': _('Calculated Delivery End Date')}
    )

    printedComment = Column(
        "PrintedComment",
        Text,
        info={'verbose_name': _('Printed Comment')}
    )
    internalComment = Column(
        "InternalComment",
        Text,
        info={'verbose_name': _('Internal Comment')})

    priceSchemaHeader_id = Column(
        "PriceSchemaHeader_pk",
        Integer,
        ForeignKey('PriceSchemaHeader.PriceSchemaHeader_pk'),
        nullable=False)

    priceSchemaHeader = relationship(
        "PriceSchemaHeader",
        info={
            'verbose_name': _PSH.verbose_name,
            'datalookup': 'sysgrove.core.document.ui.customs.UIDataLookupPSH'})

    # TransportType Fk
    transportType_id = Column(
        "TransportType_pk",
        Integer,
        ForeignKey('TransportType.TransportType_pk'))

    transportType = relationship(
        "TransportType",
        info={'verbose_name': _TransportType.verbose_name})

    # Vessel FK
    vessel_id = Column(
        "Vessel_pk",
        Integer,
        ForeignKey('Vessel.Vessel_pk'))

    vessel = relationship(
        "Vessel",
        info={'verbose_name': _Vessel.verbose_name})

    # FK to the parent DocumentHeader
    parentDocumentDetail_id = Column(
        "ParentDocumentDetail_pk",
        Integer,
        ForeignKey('DocumentDetail.DocumentDetail_pk'))

    childrenDocumentDetail = relationship(
        'DocumentDetail',
        backref=backref("parentDocumentDetail",
                        remote_side='DocumentDetail.id',
                        uselist=False,
                        single_parent=True,
                        info={'verbose_name': _('Parent Document Details')}),
        info={'verbose_name': _('Children Document Detail')})

    prices = relationship(
        "Price",
        info={'verbose_name': Price.verbose_name},
        order_by="Price.step",
        cascade="save-update, merge, expunge",
    )

    futurePricings = relationship(
        "FuturePricing",
        # foreign_keys="id",
        primaryjoin=
        "DocumentDetail.id==foreign(FuturePricing.documentDetail_id)",
        info={'verbose_name': FuturePricing.verbose_name},
        cascade="save-update, merge, expunge")

    # to be used only if DocumentHeader.PriceSchemaHeader.premium_flat
    premiumFixing = relationship(
        "FuturePricing",
        # secondary='FuturePricing',
        info={'verbose_name': FuturePricing.verbose_name},
        cascade="save-update, merge, expunge",
        primaryjoin="and_"
        "(DocumentDetail.id==foreign(FuturePricing.documentDetail_id),"
        "FuturePricing.priceCondition_id==foreign(PriceCondition.id),"
        "PriceCondition.futureType=='Premium')",
        uselist=False
    )

    # Building the formular
    list_display = [
        'itemDetail', 'lineQuantity', 'lineUoM',
        [
            'linePrice',
            (SIMPLE_DATALOOKUP, 'lineCurrency'),
            (('Label', '/'), 'slashLabel'),
            (SIMPLE_DATALOOKUP, 'linePriceUoM')
        ],
        'exchangeRate',
        'warehouse', 'reqDeliveryStart', 'reqDeliveryEnd',
        (combobox, 'status')
    ]

    # FIXME : list_details is not finished. It describs
    # the detail of the documentDetails
    qty_resume_list = [
        'lineQuantity',
        'lineQuantityOpen',
        'lineQuantityReleased',
        'lineQuantityDelivered',
        'lineQuantityInvoiced'
    ]
    logistics_list = [
        (('STable', 'quantityResume'), qty_resume_list),
        [
            ('DataLookUpGB', 'lot'),
            ('DataLookUpGB', 'packing'),
        ],
        (('Button', 'link button'), 'linkDetail'),
        [
            ('DateGB', 'reqDeliveryStart'),
            ('DateGB', 'reqDeliveryEnd'),
            ('DateGB', 'calcDeliveryStart'),
            ('DateGB', 'calcDeliveryEnd')
        ]
    ]
    comments_list = [[
        (('GroupBox', printedComment.info['verbose_name']), [
         'printedComment']),
        (('GroupBox',
          _ItemDetail.printedComment.info['verbose_name']),
         ['itemDetail_printedComment']),
        (('GroupBox', internalComment.info['verbose_name']), [
         'internalComment']),
    ]]

    prices_list = [
        [('DataLookUpGB', 'priceSchemaHeader')],
        [(('GroupBox', Price.verbose_name), [
            ('sysgrove.core.document.ui.customs.PriceWidget', 'prices')])]
    ]

    prem_list = [[
        (('GroupBox', FuturePricing.fixedPrice.info['verbose_name']),
         'premiumFixing_fixedPrice'),
        [('DataLookUpGB', 'premiumFixing_currency')],
        ('DataLookUpGB',  'premiumFixing_priceUoM')
    ], (('Button', 'add/update'), 'addPremium')
    ]

    fix_list = [
        ('sysgrove.core.document.ui.customs.InlineFixing',
         'futurePricings')
    ]

    list_detail = [('TabPanel', [
        (u'Logistics', logistics_list),
        (u'Comments', comments_list),
        (u'Prices', prices_list),
        (u'Premium', prem_list),
        (u'Fixing', fix_list)
    ])]

    # FIXME should defined automatically from list_display
    list_columns = [
        'lineNumber',
        'itemDetail', 'itemDetail_description',
        'lineQuantity', 'lineUoM',
        'linePrice', 'lineCurrency', 'linePriceUoM',
        'warehouse', 'reqDeliveryStart', 'reqDeliveryEnd', 'status'
    ]

    # FIXME: use it only when UITable have been rewritten
    list_columns_editable = []
    #     'lineNumber',
    #     'itemDetail', 'lineQuantity', 'lineUoM',
    #     'linePrice', 'lineCurrency',
    #     'warehouse', 'reqDeliveryStart', 'reqDeliveryEnd', 'status'
    # ]
    # End of formular

    # self is duplicated
    def duplicate(self, docHeader, newQty, sp_duplicate, refs_to_parent=False,
                  set_status_to_exec=False):
        from sysgrove.modules.masterdata.models import Status
        open_status = Status.query.filter_by(id=STATUS_OPEN).one()
        exec_status = Status.query.filter_by(id=STATUS_EXEC).one()
        partexec_status = Status.query.filter_by(id=STATUS_PARTEXEC).one()

        new_detail = self.__class__()
        fields = self._fields().keys()
        fields.remove('documentHeader')
        fields.remove('lineQuantity')
        fields.remove('lineQuantityOpen')
        fields.remove('status')
        fields.remove('parentDocumentDetail')
        fields.remove('childrenDocumentDetail')
        fields.remove('prices')
        fields.remove('linePrice')
        fields.remove('futurePricings')
        fields.remove('premiumFixing')

        # listener does not update the lineQtyValue value alone
        # because lineQuantity is not yet set (otherwise it would cicle)
        # BeCarful  the order is important because of listener:
        # + prices must be set before priceSchemaHeader
        # + prices need lineQuantity and linePrice to be available
        new_detail.lineQuantity = newQty
        new_detail.lineQuantityOpen = newQty
        new_detail.linePrice = self.linePrice
        new_detail.itemDetail = self.itemDetail

        new_detail.documentHeader = docHeader
        # take care the previous line is initiated the prices.
        # In this case we have to remove them, because prices will be
        # duplicated from the self object
        for p in new_detail.prices:
            p.documentDetail = None
        new_detail.prices = []

        for p in self.prices:
            new_detail.prices.append(p.duplicate(new_detail, sp_duplicate))
        for f in fields:
            setattr(new_detail, f, getattr(self, f))

        if refs_to_parent:
            new_detail.parentDocumentDetail = self
            self.lineQuantityOpen = self.lineQuantityOpen - newQty
            if self.lineQuantityOpen > ROUNDING_MIN:
                self.status = partexec_status
            else:
                self.status = exec_status
        if set_status_to_exec:
            new_detail.lineQuantityOpen = 0.0
            new_detail.status = exec_status
        else:
            new_detail.status = open_status
        docHeader.documentDetails.append(new_detail)

    def lineQuantity_per_masterUoM(self):
        if self.lineUoM.standAlone:
            return 0.0
        else:
            return convert2Uom(self.lineQuantity,
                               self.lineUoM,
                               _UOM.master())

    @property
    def unitPrice4QtyUoM(self):
        if self.lineUoM == self.linePriceUoM:
            return self.linePrice
        else:
            return self.linePrice / convert2Uom(1.0,
                                                self.linePriceUoM,
                                                self.lineUoM)

    # take care it has to be done only once
    def set_basic_price(self):
        # look for the basic price condition
        # the priceSchema has to start with the basic price
        basic_step = min(map(lambda x: x.step, self.priceSchemaHeader.details))
        basic_ps = filter(lambda x: x.step == basic_step,
                          self.priceSchemaHeader.details)[0]
        basic_pc = basic_ps.priceCondition
        basic = filter(lambda x: x.priceCondition == basic_pc,
                       self.prices)[0]

        # is basic already set?
        if basic.action and not basic.action == '':
            return

        basic.quantity = self.lineQuantity
        if not self.documentHeader.futureFixingFlag:
            basic.action = '%s * %s' % (_prefix(QUANTITY), _prefix(UNITPRICE))
            basic.unitPrice = self.unitPrice4QtyUoM
        else:
            # add the step for future fixing
            # spread, premiun and future pricing are mandatory
            # forex is up the forexFlag our should be
            # add automatically according to the currency of the marketCommodty
            if not self.itemDetail.itemGroup.marketCommodity:
                raise UserMessage("Market Commodity for %s is missing" %
                                  self.itemDetail.code)
            # Even if futureFlag is False, in case of future pricing we
            # keep the future price condition, to be able to handle
            # the spread, if one is added.
            future = Price(step=1,
                           quantity=self.lineQuantity,
                           action='%s * %s' %
                                  (_prefix(QUANTITY), _prefix(UNITPRICE)),
                           priceCondition=_PC.future_condition())
            future.documentDetail = self
            self.prices.append(future)

            spread = Price(step=2,
                           quantity=self.lineQuantity,
                           action='%s * %s' %
                           (_prefix(QUANTITY), _prefix(UNITPRICE)),
                           priceCondition=_PC.spread_condition(),
                           # could overwritten, if spread is fixed
                           unitPrice=0.0)
            spread.documentDetail = self
            self.prices.append(spread)

            if self.documentHeader.premiumFlag is not None:
                premium = Price(step=3,
                                quantity=self.lineQuantity,
                                action='%s * %s' %
                                       (_prefix(QUANTITY),
                                        _prefix(UNITPRICE)),
                                priceCondition=_PC.prm_condition())
                premium.documentDetail = self
                self.prices.append(premium)

                basic.action = '#1 + #2 + #3'
            else:
                basic.action = '#1 + #2'

            if self.documentHeader.forexFlag:
                forex = Price(step=4,
                              priceCondition=_PC.forex_condition())
                forex.documentDetail = self
                self.prices.append(forex)

    # a future documentDetail is fixed, if every price is fixed
    def is_fixed(self):
        if not self.documentHeader.futureFixingFlag:
            return True
        res = True
        i = 0
        while res and i < len(self.prices):
            res = res and self.prices[i].is_fixed()
            i = i + 1
        return res

    # price are calculated in documentDetail currency : lineCurrency
    # but the return value has to be in documentHeader currency, using
    # self.exchangeRate value
    def compute_amount(self):

        # FIXME: it is a rather simple way to solve a lineare system of
        # equations. A better way could be to use tool such as [1], but
        # as the system is style diagonalized..., not need yet.

        # First check if no new serviceProvider have been add, since laste
        # price computation.
        prices_sp = set([s.serviceProvider
                         for s in self.prices if s.serviceProvider])
        header_sp = set(self.documentHeader.serviceProviders)
        new_sp = header_sp - prices_sp

        need_total = False
        max_step = max(map(lambda x: x.step, self.prices))
        total_line = filter(lambda x: x.step == max_step,
                            self.prices)[0]
        for service in new_sp:
            # first check if a price line with the same priceCondition
            # and without any serviceProvider yet set
            done = False
            for price in self.prices:
                if (price.priceCondition == service.priceCondition and
                        not price.serviceProvider):
                    price.serviceProvider = service
                    done = True
            if not done:
                # add the corresponding priceline
                need_total = True
                max_step = add_priceLine_for_service(self,
                                                     self.documentHeader,
                                                     service,
                                                     max_step)
        if need_total:
            price = Price()
            price.documentDetail = self
            price.from_priceSchemaDetail(total_line)
            max_step = max_step + 10
            price.step = max_step
            self.prices.append(price)

        # Service providers may have been removed from the header
        # It has to handled in price
        removed_sp = prices_sp - header_sp
        for price in self.prices:
            if price.serviceProvider in removed_sp:
                price.serviceProvider = None
                price.result = 0.0

        # take care, the order of steps could be manadatory here
        # and we can not trust sqlalchemy
        prices = sorted(self.prices, cmp=_cmp_steps)

        # set exchangeRate with documentHeader.currency at the
        # right place, if needed
        if not self.lineCurrency == self.documentHeader.currency:
            # reset old exchange
            for price in prices:
                if price.priceCondition.calculationType == 'Sum':
                    price.exchangeRate = None
            # the last one is a Sum
            assert(price.priceCondition.calculationType == 'Sum')
            price.exchangeRate = self.exchangeRate

        for price in self.prices:
            price.compute_future()
        # compute nothing if is not fixed
        if not self.is_fixed():
            return (0.0, 0.0)

        # Calculation start here
        vat = 0.0
        values = {_prefix(QUANTITY): self.lineQuantity,
                  _prefix(UNITPRICE): self.linePrice}

        # first pass: compute values, eache one are computed in their own
        # currency

        for price in prices:
            try:
                # FIXME: temp code valiting for the vat determination
                value = float(price.conditionValue)
            except Exception:
                value = 0.0

            # FIXME : some UoM converion and exchange rate are missing
            # but i need to known more about future unit price...
            if price.priceCondition.serviceFlag:
                if price.serviceProvider:
                    value = compute_repartion_cost(self, price.serviceProvider)
            elif not price.priceCondition.is_flat():
                # price.unitPrice is always a unitPrice in
                # documentDetail.lineUoM
                # FIXME : this value is already set!!!!
                # price.unitPrice = convert2Uom(
                #     price.weightedAverageValue,
                #     price.futurePricings[0].unitOfMeasure,
                #     self.lineUoM)
                value = 0.0

            values[_prefix(price)] = value
            price.result = value

        # and apply necessary exchangeRate (except for last line)
        # possible for sp, their tax condition and the last Sum price line
        for price in prices[:-1]:
            if price.exchangeRate:
                # could we apply exchangeRate directly
                exchangeRateValue = get_exchangeRateValue(price.exchangeRate,
                                                          self.exchangeRate,
                                                          self.documentHeader)

                price.resultExtraCurrency = price.result
                price.result = price.result * exchangeRateValue
                values[_prefix(price)] = price.result
        # the forex is an average exchange rate, applied only on future
        # priceCondtion
        if self.documentHeader.forexFlag:
            forex = self.get_futurePrice(pc=_PC.forex_condition())
            for price in [self.get_futurePrice(),
                          self.get_futurePrice(pc=_PC.prm_condition()),
                          self.get_futurePrice(pc=_PC.spread_condition())]:
                price = self.get_futurePrice()
                price.resultExtraCurrency = price.result
                price.result = price.result * forex.weightedAverageValue
                value[_prefix(price)] = price.result

        # second pass: adjust 'Sum' priceCondition
        lines_for_add = []
        for price in prices:
            if price.priceCondition.calculationType == 'Sum':
                # Check that action is correct
                price.action = ' + '.join(
                    map(lambda x: '#%s' % x.step, lines_for_add))
                lines_for_add = [price]
            elif price.priceCondition.futureType == 'Flat':
                lines_for_add.append(price)

        # third pass: apply action
        for price in prices:
            if price.action:
                values[_prefix(UNITPRICE)] = price.unitPrice
                action = _subs('res =' + price.action, values)
                exec_action = {}
                try:
                    exec(action) in exec_action
                except Exception:
                    raise Exception(u"step %s: wrong expression %s" %
                                   (price.step, action))
                if price.priceCondition.calculationType == "Percentage":
                    values[_prefix(price)] = \
                        (exec_action['res'] *
                         values[_prefix(price)] / 100.00)
                else:
                    values[_prefix(price)] = exec_action['res']

            if price.priceCondition and \
                    price.priceCondition.typeCondition == "VAT":
                vat = vat + values[_prefix(price)]
            price.result = values[_prefix(price)]

        # the last prices line has to be a subtotal
        if self.lineCurrency == self.documentHeader.currency:
            return (price.result, vat)
        else:
            if self.exchangeRate:
                price.resultExtraCurrency = \
                    price.result * self.exchangeRate.exchangeRateValue
                return (price.resultExtraCurrency,
                        vat * self.exchangeRate.exchangeRateValue)
            else:
                raise UserMessage(
                    "Exchange Rate is missing for document Detail #%" %
                    self.lineNumber)

    def name(self):
        return u"%(docT)s #%(docN)s, line %(lineN)s," \
            u" %(qty)s%(uom)s of %(item)s" % {
                'docT': self.documentHeader.docType.code,
                'docN': self.documentHeader.documentNumber,
                'lineN': self.lineNumber,
                'item': self.itemDetail.code,

                'qty': self.lineQuantity,
                'uom': self.lineUoM,
            }

    @property
    def lineQtyLinked(self):
        return PurchaseAndSalesDocumentLogisticLink.linkedQty(
            self.documentHeader.docType.flowID,
            self)

    @property
    def lineQtyNYLinked(self):
        return self.lineQuantity - self.lineQtyLinked

    def linkableDetails(self):
        if self.lineQtyLinked == self.lineQuantity:
            return []
        flow = self.documentHeader.docType.flowID.reverse()
        docGroup = self.documentHeader.docType.docGroup
        sameItem = self.query.filter_by(itemDetail_id=self.itemDetail.id)\
                             .filter_by(lineUoM_id=self.lineUoM.id)\
                             .exclude_by(status_id=STATUS_CANCEL)\
                             .all()
        res = []
        for detail in sameItem:
            if detail.lineQtyNYLinked > 0 and \
                    detail.documentHeader.docType.flowID == flow and\
                    detail.documentHeader.docType.docGroup == docGroup:
                res.append(detail)
        return res

    # to return the unique PriceFixing storing the flat premium value
    def get_premium(self):
        if self.documentHeader.premium_flat:
            for price in self.prices:
                if price.priceCondition == _PC.prm_condition():
                    return price

    def get_futurePrice(self, pc=None):
        if not pc:
            pc = _PC.future_condition()
        if not self.documentHeader.futureFixingFlag:
            return None
        if not self.prices:
            return None
        for price in self.prices:
            if price.priceCondition == pc:
                return price

    # FIXME : see SG-395
    def set_unitPrice(self):
        if not self.documentHeader.futureFixingFlag:
            return
        if not self.documentHeader.priceFlag:
            return
        # compute a temp unit Price from the fixing
        # future price average value + premium average value + spread average
        # value
        res = 0.0
        fprice = self.get_futurePrice()
        if fprice and fprice.unitPrice:
            res = res + fprice.unitPrice
        prm = self.get_futurePrice(_PC.prm_condition())
        if prm and prm.unitPrice:
            res = res + prm.unitPrice
        spread = self.get_futurePrice(_PC.spread_condition())
        if spread and spread.unitPrice:
            res = res + spread.unitPrice
        self.linePrice = res

    @property
    def spread_month(self):
        if not self.documentHeader.futureFixingFlag:
            return None
        spread_price = self.get_futurePrice(pc=_PC.spread_condition())
        fixing = spread_price.futurePricings
        if fixing == []:
            return None
        fixing = sorted(fixing,
                        cmp=lambda x, y: cmp(x.fixingDate, y.fixingDate),
                        reverse=True)
        return fixing[0].spreadMonth


#
# The listener methods. Part of the business rules are implemented here
#
# docHeader
def validate_docHeader(target, value, oldvalue, initiator):
    if isinstance(value, int):
        docHeader = DocumentHeader.query.filter_by(id=value).one()
    else:
        docHeader = value
    if not target.priceSchemaHeader:
        # target.priceSchemaHeader_id = docHeader.docType.priceDetail.id
        target.priceSchemaHeader = docHeader.docType.priceDetail

        # add ServiceProvider from document.priceSchemaHeader priceSchemaHeader
        # it is not possible to do it in validate_priceSchema
        # because target.documentheader is not yet set
        max_step = max(map(lambda x: x.step, target.prices))
        first_total = filter(lambda x: x.step == max_step,
                             target.prices)[0]

        for service in docHeader.serviceProviders:
            max_step = add_priceLine_for_service(target,
                                                 docHeader,
                                                 service,
                                                 max_step)

        if len(docHeader.serviceProviders) > 0:
        # lets do a sum
            price = Price()
            price.documentDetail = target
            price.from_priceSchemaDetail(first_total)
            max_step = max_step + 10
            price.step = max_step
            target.prices.append(price)

        # let's check if some priceCondtion with serviceFlag == True
        # are not yet used by service provider
        pc_in_psh = set(map(lambda x: x.priceCondition,
                            filter(lambda x: x.priceCondition.serviceFlag,
                                   docHeader.priceSchemaHeader.details)))
        pc_in_sp = set(map(lambda x: x.priceCondition,
                           docHeader.serviceProviders))

        remainding = pc_in_psh - pc_in_sp
        for priceCondition in remainding:
            psd_list = get_psdetail_for(docHeader.priceSchemaHeader,
                                        priceCondition)

            for detail in psd_list:
                price = Price()
                price.documentDetail = target
                price.from_priceSchemaDetail(detail)
                max_step = max_step + 10
                price.step = max_step
                target.prices.append(price)

        # lets do the last "Sum", if we have add some lines
        if len(remainding) > 0:
            price = Price()
            price.documentDetail = target
            price.from_priceSchemaDetail(first_total)
            max_step = max_step + 10
            price.step = max_step
            target.prices.append(price)

    return value
event.listen(DocumentDetail.documentHeader_id, 'set',
             validate_docHeader, propagate=True)
event.listen(DocumentDetail.documentHeader, 'set',
             validate_docHeader, propagate=True)


def validate_priceSchema(target, value, oldvalue, initiator):
    if value == oldvalue:
        return value
    if isinstance(value, int):
        pc = _PSH.query.filter_by(id=value).one()
    else:
        pc = value
    if target.priceSchemaHeader == pc:
        return value

    # the prices has been copied from a previous document : do nothing
    if not oldvalue and target.prices and not target.prices == []:
        return value

    # remove old prices
    for price in target.prices:
        price.cancel()
    target.prices = []

    for detail in pc.details:
        price = Price()
        price.documentDetail = target
        price.from_priceSchemaDetail(detail)
        price.currency = target.lineCurrency
        target.prices.append(price)

    return value
event.listen(DocumentDetail.priceSchemaHeader_id, 'set',
             validate_priceSchema, propagate=True)
event.listen(DocumentDetail.priceSchemaHeader, 'set',
             validate_priceSchema, propagate=True)


# status
# could update its DocHeader status according to the status
# of the other lines
# be careful the connection between ui and database nether
# take place at level of _id
def validate_status(target, value, oldvalue, initiator):
    value_id = value.id

    # update the doc header status
    if target.documentHeader and target.documentHeader.id:
        # Check the status if other lines
        otherdocLine = target.documentHeader.documentDetails[:]
        otherdocLine.remove(target)
        status_list = map(lambda x: x.status, otherdocLine)
        status_list.append(value)
        status_list = set(status_list)
        dh_status = _HSC.from_status_combinaison(status_list)
        if dh_status:
            target.documentHeader.status = dh_status

        else:
            raise ValueError("Cannot compute DocumentHeader status")

        # if value_id in STATUS_FROZEN:
        #     if have_same_value(otherdocLineStatus, value_id):
        #         target.documentHeader.status = value
        # elif value_id is STATUS_EXEC:
        #     possible_status = [STATUS_EXEC]
        #     possible_status.extend(STATUS_FROZEN)
        #     others = filter(
        #         lambda x: not x in possible_status, otherdocLineStatus)
        #     if others == []:
        #         target.documentHeader.status = value
        # elif value_id is STATUS_PARTEXEC:
        #     if target.documentHeader.status.id == STATUS_OPEN:
        #         target.documentHeader.status = value
        # elif value_id is STATUS_OPEN and not value == oldvalue:
        #     if not target.documentHeader.status.id == STATUS_PARTEXEC:
        #         target.documentHeader.status = value

    # update the lineQuantityOpen
    if oldvalue:
        if not value == oldvalue and target.lineQuantity:
            if oldvalue.cancelLikeFlag and value_id == STATUS_OPEN:
                target.lineQuantityOpen = target.lineQuantity
            elif oldvalue.id == STATUS_OPEN and value.cancelLikeFlag:
                target.lineQuantityOpen = 0.0

    return value
event.listen(DocumentDetail.status, 'set', validate_status, propagate=True)


# lineQuantity
def validate_Quantity(target, value, oldvalue, initiator):
    # initialisation
    if value == oldvalue:
        return value

    if not target.lineQuantityOpen:
        target.lineQuantityOpen = value
    else:
        if target.status and target.status.id in STATUS_FROZEN:
            raise ValueError(u"""lineQuantity: impossible to update.
                Please set status to Open first""")
        else:
            if value > oldvalue:
                if target.parentDocumentDetail:
                    maxValue = target.parentDocumentDetail.lineQuantityOpen
                    if (value - oldvalue) > maxValue:
                        raise ValueError(
                            u"lineQuantity: Cannot change line Qty from"
                            " %s to %s."
                            " Parent Open Qty is %s" %
                            (oldvalue, value, maxValue))
                    else:
                        target.parentDocumentDetail.lineQuantityOpen = \
                            maxValue - (value - oldvalue)

                target.lineQuantityOpen = target.lineQuantityOpen + \
                    (value - oldvalue)
            elif value < oldvalue:
                linkedQty = oldvalue - target.lineQuantityOpen
                if value < linkedQty:
                    raise ValueError(
                        u"lineQuantity: impossible to update Qty from"
                        " %s to %s."
                        "The already linked Qty is %s" %
                        (oldvalue, value, linkedQty))
                else:
                    if target.parentDocumentDetail:
                        target.parentDocumentDetail.lineQuantityOpen = \
                            target.parentDocumentDetail.lineQuantityOpen + \
                            (oldvalue - value)

                    target.lineQuantityOpen = value - linkedQty
    return value
event.listen(DocumentDetail.lineQuantity,
             'set', validate_Quantity, propagate=True)


# lineQuantityOpen
def validate_QuantityOpen(target, value, oldvalue, initiator):
    if value == oldvalue:
        return value
    if value < 0.0:
        raise Exception(u"lineQuantityOpen: can not be negative")

    # if value == 0.0:
    #     if not target.status or target.status.id in [STATUS_PARTEXEC,
    #                                                  STATUS_OPEN]:
    #         from sysgrove.modules.masterdata.models import Status
    #         target.status = Status.query.filter_by(id=STATUS_EXEC).one()
    #         target.status_id = STATUS_EXEC
    return value

event.listen(DocumentDetail.lineQuantityOpen,
             'set', validate_QuantityOpen, propagate=True)


class PurchaseAndSalesDocumentLogisticLink(InitMixin, Base):
    verbose_name = _('Purchase And Sales Document Logistic Link')

    quantityLinked = Column(
        "QuantityLinked", Float,
        info={'verbose_name': _('Quantity Linked')})

    documentPurchase_id = Column(
        "DocumentPurchase",
        Integer,
        ForeignKey('DocumentDetail.DocumentDetail_pk'),
        nullable=False)

    documentPurchase = relationship(
        "DocumentDetail",
        primaryjoin="PurchaseAndSalesDocumentLogisticLink.documentPurchase_id"
                    "==DocumentDetail.id",
        cascade="save-update, merge, expunge",
        info={'verbose_name': _("Purchase Document")})

    documentSales_id = Column(
        "DocumentSales",
        Integer,
        ForeignKey('DocumentDetail.DocumentDetail_pk'),
        nullable=False)

    documentSales = relationship(
        "DocumentDetail",
        primaryjoin="PurchaseAndSalesDocumentLogisticLink.documentSales_id"
                    "==DocumentDetail.id",
        cascade="save-update, merge, expunge",
        info={'verbose_name': _("Sales Document")})

    @staticmethod
    def linked_list_columns(flow):
        if flow.id == SALES_ID:
            prefix = "documentSales_"
            reverse_prefix = "documentPurchase_"
        else:
            prefix = "documentPurchase_"
            reverse_prefix = "documentSales_"

        attr_columns = [
            "",
            prefix + 'documentHeader_documentNumber',
            prefix + 'lineNumber',
            prefix + 'itemDetail',
            prefix + 'lineQuantity',
            prefix + 'lineUoM',
            prefix + 'linePrice',
            prefix + 'lineCurrency',
            reverse_prefix + 'documentHeader_docType',
            reverse_prefix + 'documentHeader_documentNumber',
            reverse_prefix + 'lineNumber',
            reverse_prefix + 'lineQuantity',
            'quantityLinked', 'id'
        ]
        return attr_columns

    @staticmethod
    def linkedQty(flow, docD):
        res = 0.0
        linked = PurchaseAndSalesDocumentLogisticLink.getLinked(flow, docD)
        for link in linked:
            res = res + link.quantityLinked
        return res

    # return the list of PurchaseAndSalesDocumentLogisticLink object
    # for docD a DocumentDetail object, according to the flow
    @staticmethod
    def getLinked(flow, docD):
        from sysgrove.main import db_session
        in_db_session = []
        for obj in db_session:
            if isinstance(obj, PurchaseAndSalesDocumentLogisticLink):
                if obj.cancelData is None or not obj.cancelData:
                    in_db_session.append(obj)

        res = []
        for link in in_db_session:
            if flow.id == SALES_ID and link.documentSales.id == docD.id:
                res.append(link)
            elif flow.id == PURCHASE_ID and  \
                    link.documentPurchase.id == docD.id:
                res.append(link)

        if flow.id == SALES_ID:
            res.extend(PurchaseAndSalesDocumentLogisticLink.query
                       .filter_by(documentSales_id=docD.id)
                       .filter_by(cancelData=False)
                       .all())
        else:
            res.extend(PurchaseAndSalesDocumentLogisticLink.query
                       .filter_by(documentPurchase_id=docD.id)
                       .filter_by(cancelData=False)
                       .all())
        res = filter(lambda x: x.cancelData is None or not x.cancelData, res)
        return list(set(res))

    @staticmethod
    def add_link(dd, flow, target_line, qty):
        if not dd.lineUoM == target_line.lineUoM:
            raise UserMessage(u"I do not know yet how to deal with many UoM")
        if qty > dd.lineQtyNYLinked:
            raise UserMessage(u"Can only link %s (not %s)" %
                              (dd.lineQtyNYLinked, qty))

        link = PurchaseAndSalesDocumentLogisticLink()
        if flow.id == SALES_ID:
            link.documentSales = dd
            link.documentPurchase = target_line
        else:
            link.documentSales = target_line
            link.documentPurchase = dd
        link.quantityLinked = qty
        link.add()


class PurchaseAndSalesDocumentPricingLink(InitMixin, Base):
    verbose_name = _('Purchase And Sales Document Logistic Princing Link')

    quantityLinked = Column(
        "QuantityLinked", Float,
        info={'verbose_name': _('Quantity Linked')})

    futurePricing_id = Column(
        "FuturePricing_pk",
        Integer,
        ForeignKey('FuturePricing.FuturePricing_pk'),
        nullable=False)

    futurePricing = relationship(
        "FuturePricing",
        info={'verbose_name': _("Future Pricing")})

    documentPurchase_id = Column(
        "DocumentPurchase",
        Integer,
        ForeignKey('DocumentDetail.DocumentDetail_pk'),
        nullable=False)

    documentPurchase = relationship(
        "DocumentDetail",
        primaryjoin="PurchaseAndSalesDocumentPricingLink.documentPurchase_id"
                    "==DocumentDetail.id",
        info={'verbose_name': _("Origin Document")})

    documentSales_id = Column(
        "DocumentSales",
        Integer,
        ForeignKey('DocumentDetail.DocumentDetail_pk'),
        nullable=False)

    documentSales = relationship(
        "DocumentDetail",
        primaryjoin="PurchaseAndSalesDocumentPricingLink.documentSales_id"
                    "==DocumentDetail.id",
        info={'verbose_name': _("Target Document")})
