#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
import locale
import importlib
import datetime
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from sqlalchemy import Boolean, Float, String, Date, DateTime, Enum, Integer
from sysgrove.utils import (
    package_to_path,
    retreive_class, retreive_class_ex, getattr_info,
    minimalize,
    getattr_ex, map_ex,
    debug_trace,
    code_to_mode, mode_to_code, ctm
)
from sysgrove.settings import ALTCODE, MODE_02, MODE_03, MODE_nn
from sysgrove.main import db_session
from sysgrove.models import Base
from sysgrove.ui.classes import (
    UIObject,
    UIDynamicButtonZone,
    UISpace,
    UIDataLookup, SDataLookup,
    UIForm, UITabWidget,
    LinkedTable, ExceptionMessagePopup
)
from sysgrove.ui.classes.tree import TreeWidgetItem

log = logging.getLogger('sysgrove')


def first_level_fields(fields):
    res = []
    for field in fields:
        if not field.startswith('_'):
            f = field.split('_')[0]
            if not f in res:
                res.append(f)
    return res


def second_level_fields(fields, attr):
    res = []
    for field in fields:
        if field.startswith(attr + '_'):
            sp = field.split('_')
            candidate = sp[0] + '_' + sp[1]
            if not candidate in res:
                res.append(candidate)
    return res


def handle_valueError(obj, errors, ui_content, prefix):
    if obj in db_session:
        db_session.expunge(obj)
    error_mess = "\n"
    for (attr, mess) in errors.args:
        uiObject = getattr(ui_content, prefix + attr, None)
        uiObject.setInvalidStyle()
        error_mess = error_mess + mess
    exPopup = ExceptionMessagePopup(ui_content.qtObject,
                                    ValueError(error_mess),
                                    None)
    exPopup.show()


def save_or_error_popup(obj, screen, fields):
    errors = Screen.ui2bdd(obj,
                           screen.ui_space,
                           fields, screen.klass,
                           screen.ui_prefix)
    if errors:
        handle_valueError(obj, errors, screen.ui_space, screen.ui_prefix)
        return False

    try:
        obj.validate()
    except ValueError as e:
        handle_valueError(obj, e, screen.ui_space, screen.ui_prefix)
        return False

    try:
        obj.save()
    except Exception as e:
        if obj in db_session:
            db_session.expunge(obj)
        exPopup = ExceptionMessagePopup(screen.ui_space.qtObject,
                                        e, screen)
        exPopup.show()
        return False
    return True


class Screen(object):

    def __new__(cls, *p, **k):
        if 'code' in k:
            code = k.pop('code')
            cls.context_code = code
        else:
            cls.context_code = cls.__name__[6:]
        code = cls.context_code[-2:]
        cls.mode = code_to_mode(code)

        if 'module' in k:
            module = k.pop('module')
            cls.module = module
        else:
            cls.module = cls.__module__

        pack = cls.module[:cls.module.find('.screen')]

        # if cls.mode == MODE_nn:
        #     xmlScreenCode = cls.context_code
        # else:
        #     xmlScreenCode = cls.context_code[:-2]
        xmlScreenCode = cls.context_code[:-2]
        cls.dyns = None
        cls.ui_space = None
        ui_name = 'ui%s%s' % (xmlScreenCode, code)
        try:
            ui_module = importlib.import_module(pack + '.ui')
            cls.ui = getattr(ui_module, ui_name, None)
        except ImportError:
            cls.ui = None
            # dynsClass = None
        if cls.ui:
            if 'main' in cls.__name__:
                dynsClass = getattr(ui_module, 'Dyn_main%s' % code)
            else:
                dynsClass = getattr(ui_module, 'Dyn%s' % code, None)
            cls.ui_space = UISpace("")
            cls.ui.setupUi(cls.ui_space)
            if dynsClass:
                cls.dyns = dynsClass(UIDynamicButtonZone, 'codeContext')
                cls.dyns.setupUi()
        return super(Screen, cls).__new__(cls)

    def __init__(self, module='sysgrove.modules',
                 newValue=None, klass=None, code=''):
        # FIXME curToUpdate and _newValue are very similaire....

        if not hasattr(self, 'klass'):
            self.klass = klass

        # Lets keep all case
        if not hasattr(self, 'fields'):
            if hasattr(self.klass, 'list_display'):
                self.fields = UIForm.form2attr_list(self.klass.list_display)
            else:
                self.fields = []

        # Usefull only in case of classes without 'code' field
        self.curToUpdate = None
        self.set_newValue(newValue)
        if not hasattr(self, 'tables'):
            self.tables = []

        if self.klass:
            self.ui_prefix = self.klass.get_ui_prefix()
        defaults = getattr(self, 'defaults', None)
        if defaults:
            self.setDefaults(self.ui_space, defaults, self.ui_prefix)
        self.setTables()
        self.setActions()

    @property
    def newValue(self):
        return self._newValue

    def set_newValue(self, cur):
        self._newValue = cur

    # FIXME : some parameter are now useless
    @staticmethod
    def bdd2uiTable(klass, table, dataSet):
        table.clear()
        table.setVal(dataSet)

    @staticmethod
    def reset_fields(ui_content, fields, ui_prefix):
        for field in fields:
            if field == '':
                ui_field = getattr(ui_content, ui_prefix[:-1], None)
            else:
                ui_field = getattr(ui_content, ui_prefix + field, None)
            if ui_field:
                map_ex(lambda x: x.clear(), ui_field)

    @staticmethod
    def uiTable2ui(ui_content, table, index,
                   ui_prefix, klass, fields, reset=True):
        """copies one table line content to individual ui fields """
        if reset:
            Screen.reset_fields(ui_content, fields, ui_prefix)
        if not index.isValid():
            return
        row = index.row()
        cur = table.rowObject(row)
        Screen.bdd2ui(cur, ui_content, fields, ui_prefix)
        return cur

    @staticmethod
    def ui2bdd(cur, ui_content, fields, klass, ui_prefix):
        valid = True
        value_errors = []
        fl_fields = first_level_fields(fields)
        for field in fl_fields:
            # search corresponding field in ui
            uiObject = getattr(ui_content, ui_prefix + field, None)
            if uiObject:
                try:
                    Screen.ui2attr(cur, field, uiObject, klass)
                except ValueError as e:
                    valid = False
                    value_errors.append((field, e.message))

            else:
                # No UIobject, this covers many case:
                # + a FK not represented by a datalookup (example with address)
                # + case where there is nothing to do (tabbar)
                # FIXME: and valid???? and errors..
                if klass.is_fk(field) or klass.is_o2m(field):
                    subklass = retreive_class(field, klass)
                    subfields = second_level_fields(fields, field)
                    subcur = getattr(cur, field, None)
                    if not subcur:
                        subcur = subklass()
                    if hasattr(subcur, cur.get_ui_prefix()[:-1]):
                        setattr(subcur, cur.get_ui_prefix()[:-1], cur)
                    for f in subfields:
                        bdd_field_name = f.split(field + '_', 1)[1]
                        uiSubobject = getattr(ui_content, ui_prefix + f, None)
                        Screen.ui2attr(subcur, bdd_field_name,
                                       uiSubobject, subklass)
                    if hasattr(cur, field):
                        setattr(cur, field, subcur)

        if not valid:
            return ValueError(*value_errors)

    @staticmethod
    def bdd2ui(cur, ui_content, fields, ui_prefix):
        Screen.reset_fields(ui_content, fields, ui_prefix)
        for field in fields:
            uiObject = getattr(ui_content, ui_prefix + field, None)
            if uiObject:
                # let's find the value from instance
                value = getattr_ex(cur, field)
                map_ex(lambda x: Screen.value2ui(value, x), uiObject)

    @staticmethod
    def value2ui(value, uiObject):
        if hasattr(value, 'code'):
            if ALTCODE in value.__class__.code.info:
                uiObject.setVal(
                    getattr(value, value.__class__.code.info[ALTCODE]))
            else:
                uiObject.setVal(value.code)
        elif isinstance(value, Base):
            uiObject.setVal(value)
        elif isinstance(value, list):
            uiObject.setVal(value)
        elif isinstance(value, bool):
            uiObject.setVal(value)
        elif value or isinstance(value, int) or \
                isinstance(value, float):
            uiObject.setVal(unicode(value))
        else:
            uiObject.setVal('')

    # FIXME use klass.toPython_type to simplify the code
    @staticmethod
    def ui2attr(cur, field, uiObject, klass):
        if klass.is_direct(field):
            value = uiObject.value()
            mapper = klass.__mapper__
            prop = mapper.get_property(field)
            bd_type = type(prop.columns[0].type)
            if bd_type == Float:
                if value and not value == '':
                    value = locale.atof(str(value))
                    setattr(cur, field, value)
            elif bd_type == Integer:
                if value and not value == '':
                    value = locale.atoi(str(value))
                    setattr(cur, field, value)
            elif bd_type == Boolean:
                setattr(cur, field, value)
            elif bd_type == Enum:
                if not value or value == '':
                    raise ValueError("%s: No set" % field)
                else:
                    setattr(cur, field, str(value))
            elif bd_type == String:
                setattr(cur, field, str(value))
            elif bd_type == Date or bd_type == DateTime:
                if isinstance(value, unicode):
                    # FIXME: To be reviewed according to localization
                    if '-' in value:
                        value = datetime.datetime.strptime(
                            value, '%Y-%m-%d').date()
                    elif '/' in value:
                        value = datetime.datetime.strptime(
                            value, '%d/%m/%Y').date()

                setattr(cur, field, value)
            else:
                setattr(cur, field, unicode(value))
        elif klass.is_fk(field):
            value = uiObject.valueObject()
            setattr(cur, field, value)
        elif klass.is_m2m(field) or klass.is_o2m(field):
            value = uiObject.valueObject()
            if not value:
                value = []
            if isinstance(value, list):
                setattr(cur, field, value)
            else:
                setattr(cur, field, [value])

    @staticmethod
    def addDataLookUpLink(f, ui_content, fields, klass, prefix):
        uiObject = getattr(ui_content, prefix + f, None)
        # add 'description' if necessary (becareful, description is not always
        # decription)
        if uiObject and isinstance(uiObject, UIDataLookup) and not f == 'code':
            info = getattr_info(f, klass)
            if 'description' in info:
                for desc in info['description']:
                    f_desc = f + '_' + desc
                    if not f_desc in fields:
                        fields.append(f_desc)
            else:
                f_desc = f + '_description'
                if not f_desc in fields:
                    fields.append(f_desc)
        # call setLink
        if uiObject:
            if not isinstance(uiObject, list):
                uiObject = [uiObject]
            for ui in uiObject:
                if (not ui.klass and
                        (isinstance(ui, SDataLookup) or
                         isinstance(ui, LinkedTable))):
                    ui.setLink(klass=retreive_class_ex(f, klass),
                               prefix=prefix)

    @staticmethod
    def setDefaults(ui_content, defaults, prefix):
        for (obj, attr, target) in defaults:
            uiObject = getattr(ui_content, prefix + obj, None)
            if uiObject:
                uiObject.addDefault((attr, target))
            else:
                if hasattr(attr, '__call__'):
                    val = attr()
                else:
                    val = attr
                # let's handle the fact that namy object have the same name
                for uiTarget in ui_content.find_children(prefix + target):
                    uiTarget.setVal(val)

    @staticmethod
    def get_current_value():
        from sysgrove.context import kernel
        screen = kernel.get_current_context().screen
        return screen.newValue['value']

    @staticmethod
    def update_on_fly(to_fly, on_fly, call=None):
        from sysgrove.context import kernel
        screen = kernel.get_current_context().screen
        cur = screen.newValue['value']
        errors = Screen.ui2bdd(cur, screen.ui_space,
                               to_fly, screen.klass,
                               screen.ui_prefix)
        if errors:
            raise errors
        getattr(cur, call)()
        Screen.bdd2ui(cur, screen.ui_space, on_fly, screen.ui_prefix)

    def setTables(self):
        # Add the default table for self.klass if it is required
        # buy the ui part
        if self.klass:
            ui_name = "table_%s" % self.klass.__name__
            ui_table = getattr(self.ui_space, ui_name, None)

            if ui_table:
                if hasattr(self.klass, 'code'):
                    query = self.klass.query.order_by('code').all
                else:
                    query = self.klass.query.all
                self.default_table = {
                    'ui_name': ui_name,
                    'query': query,
                    'ui': ui_table,
                    'klass': self.klass
                }
                self.bdd2uiTable(self.klass, ui_table, query())
                self.tables.append(self.default_table)
            else:
                self.default_table = None

        for table in self.tables:
            # compute other ui keys
            if not 'ui' in table:
                table['ui'] = getattr(self.ui_space, table['ui_name'], None)

    def showTables(self):
        for table in self.tables:
            query = table['query']()
            self.bdd2uiTable(table['klass'], table['ui'], query)

    def setLinks(self):
        multiple = getattr(self, 'multiple', None)
        if hasattr(self, 'linkTo'):
            for link in self.linkTo:
                slave = link['slave'][0]
                if not multiple:
                    uiSlave = getattr(
                        self.ui_space, self.ui_prefix + slave)
                    if hasattr(uiSlave, 'setLink'):
                        uiSlave.setLink(
                            klass=self.klass,
                            prefix=self.ui_prefix,
                            **link)
                else:
                    for uiContent in multiple:
                        uiSlave = getattr(
                            uiContent, self.ui_prefix + slave, None)
                        if uiSlave:
                            uiSlave.setLink(
                                klass=self.klass,
                                prefix=self.ui_prefix,
                                **link)

        # We also need to link other UIDataLoopUp with an
        # sqlalchemy class
        for f in self.fields:
            if not multiple:
                Screen.addDataLookUpLink(
                    f, self.ui_space, self.fields, self.klass, self.ui_prefix)
            else:
                for uiContent in multiple:
                    Screen.addDataLookUpLink(
                        f, uiContent, self.fields, self.klass, self.ui_prefix)

    def build_filter(self, filters):
        res = {}
        if self.newValue:
            instance = self.newValue['value']
        else:
            instance = None
        for (f, value) in filters.iteritems():
            if isinstance(value, str):
                attr = getattr_ex(instance, value)
                if not attr:
                    # let's try to find a value from the uiObject
                    sp = value.split('_', 1)
                    uiObject = getattr(self.ui_space,
                                       self.ui_prefix + sp[0],
                                       None)
                    if uiObject:
                        if len(sp) > 0:
                            value = uiObject.valueObject()
                            res[f] = getattr_ex(value, sp[1])
                        else:
                            res[f] = value
                else:
                    res[f] = attr
            else:
                res[f] = value
        return res

    # Only used for the default table
    def show_index(self, index):
        self.curToUpdate = Screen.uiTable2ui(self.ui_space,
                                             self.default_table['ui'],
                                             index,
                                             self.ui_prefix,
                                             self.klass,
                                             self.fields)

    def setActions(self):
        # Dynamic button
        actions = ['save', 'delete', 'cancel',
                   'display', 'create', 'linePlus', 'lineMinus', 'printX']
        for action in actions:
            if hasattr(self, action):
                if self.dyns and action in self.dyns.dynamic_buttons:
                    self.dyns.dynamic_buttons[action]\
                        .clicked.connect(getattr(self, action))
                if (hasattr(self.ui_space, action) and
                   isinstance(getattr(self.ui_space, action), UIObject)):
                    getattr(self.ui_space, action).clicked.connect(
                        getattr(self, action))
        # Default table selection
        if self.default_table:
            if self.mode in [MODE_02, MODE_03]:
                self.default_table['ui'].clicked.connect(self.show_index)

    def reset_ui_fields(self, fields):
        Screen.reset_fields(self.ui_space, fields, self.ui_prefix)
        for table in self.tables:
            if not table is self.default_table:
                table['ui'].clear()

    def delete(self):
        codeUI = getattr(self.ui_space, "%scode" % self.ui_prefix, None)
        if codeUI:
            code = codeUI.value().upper()
            if not code == '':
                self.curToUpdate = self.klass.query.filter_by(code=code).one()

        if self.curToUpdate:
            self.curToUpdate.delete()
            self.curToUpdate = None
            self.showTables()
            self.reset_ui_fields(self.fields)

    def cancel(self):
        from sysgrove.context import kernel
        current = kernel.get_current_context()
        back = current.backContext
        if back:
            postfix = back[-2:]
            current.hide(submenu=False)
            if postfix in ctm:
                kernel.set_current_context(back)


class DisplayMixin(object):

    def setReadOnly(self):
        for f in self.fields:
            if not f == 'code':
                getattr(self.ui_space, self.ui_prefix + f).setReadOnly(True)


class CreationMixin(object):

    def save(self):
        cur = self.klass()
        valid = save_or_error_popup(cur, self, self.fields)
        if valid:
            self.showTables()
            self.reset_ui_fields(self.fields)


class UpdateMixin(object):

    def save(self):
        codeUI = getattr(self.ui_space, "%scode" % self.ui_prefix, None)
        if codeUI:
            code = codeUI.value().upper()
            if code == '':
                codeUI.setInvalidStyle()
                return
            else:
                self.curToUpdate = self.klass.query.filter_by(code=code).one()
        # self.curToUpdate could be set by show_index
        if self.curToUpdate:
            valid = save_or_error_popup(self.curToUpdate, self, self.fields)
            if valid:
                self.showTables()
                self.reset_ui_fields(self.fields)
            self.curToUpdate = None


# CREATE
class Screen01(Screen, CreationMixin):
    pass


# MODIF
class Screen02(Screen, UpdateMixin):
    pass


# DISPLAY
class Screen03(Screen, DisplayMixin):
    pass


# ALLOACTION
class Screen04(Screen, UpdateMixin):

    def __init__(self, links, link, module='sysgrove.modules',
                 newValue=None, klass=None, code=''):
        self.fields = ['code', 'description', links]
        super(Screen04, self).__init__(
            module=module, newValue=newValue, klass=klass, code=code)
        self.linkTo = [{
            'slave': (links, link),
            'masters': [('code', self.klass)],
        }]
        self.setTree()

    def setTree(self):
        raise NotImplemented


class Screen04_site(Screen04):

    """ Each Screen04 specialization will have its own setTree to
        generate a tree matching the allocation table
    """

    def __init__(self, module='sysgrove.modules',
                 newValue=None, klass=None, code=''):
        super(Screen04_site, self).__init__(
            'sites', 'site',
            module, newValue, klass, code)
        self.attr_name = self.klass.get_ui_prefix()[:-1] + 's'
        self.site_fields = [
            'description', '',
            'company', 'company_description',
            'company_group', 'company_group_description',
            self.attr_name
        ]

    def setLinks(self):
        from sysgrove.modules.masterdata.models import Group, Site
        super(Screen04_site, self).setLinks()

        uigroup = getattr(self.ui_space, 'site_company_group', None)
        uicomp = getattr(self.ui_space, 'site_company', None)
        uisite = getattr(self.ui_space, 'site', None)
        uiallocated = getattr(
            self.ui_space, 'site_' + self.attr_name, None)
        uigroup.force_fields(['company_group_description'])
        uigroup.setLink(klass=Group,
                        prefix=Site.get_ui_prefix())

        uicomp.force_fields(['company_description', 'company_group'])
        comp_link = {
            'slave': ('company', 'company'),
            'masters': [('company_group', 'group')],
        }
        uicomp.setLink(klass=Site,
                       prefix=Site.get_ui_prefix(),
                       **comp_link)

        site_link = {
            'slave':
            ('site', 'site'),
            'masters':
            [('company', 'company')],
        }
        uisite.force_fields(
            ['description', 'company', 'company_group', self.attr_name])
        uisite.setLink(klass=Site,
                       prefix=Site.get_ui_prefix(),
                       **site_link)
        alloc_link = {
            'slave': (self.attr_name, self.klass),
            'masters': [('code', 'site')],
        }
        uiallocated.setLink(klass=Site,
                            prefix=Site.get_ui_prefix(),
                            **alloc_link)

    def save(self):
        codeUI = getattr(self.ui_space, "%scode" % self.ui_prefix, None)
        codeTabWidget = codeUI.parent.parent.parent
        assert(isinstance(codeTabWidget, UITabWidget))
        # We are in normal case, use the normal method
        if codeTabWidget.isVisible():
            if codeUI:
                if codeUI.value():
                    super(Screen04_site, self).save()
                else:
                    codeUI.setInvalidStyle()
        else:
            from sysgrove.modules.masterdata.models import Group, Company, Site
            uigroup = getattr(self.ui_space, 'site_company_group', None)
            uicomp = getattr(self.ui_space, 'site_company', None)
            uisite = getattr(self.ui_space, 'site', None)
            uiallocated = getattr(
                self.ui_space, 'site_' + self.attr_name, None)
            allocated = uiallocated.valueObject()
            if uisite.value():
                site = Site.query.filter_by(code=uisite.value()).one()
                setattr(site, self.attr_name, allocated)
                site.save()
                self.reset_fields(
                    self.ui_space, self.site_fields, Site.get_ui_prefix())
            elif uicomp.value():
                comp = Company.query.filter_by(code=uicomp.value()).one()
                for site in comp.sites:
                    setattr(site, self.attr_name, allocated)
                    site.save()
                    self.reset_fields(
                        self.ui_space, self.site_fields, Site.get_ui_prefix())
            elif uigroup.value():
                group = Group.query.filter_by(code=uigroup.value()).one()
                for comp in group.companies:
                    for site in comp.sites:
                        setattr(site, self.attr_name, allocated)
                        site.save()
                        self.reset_fields(
                            self.ui_space,
                            self.site_fields,
                            Site.get_ui_prefix()
                        )
            else:
                uigroup.setInvalidStyle()
                uicomp.setInvalidStyle()
                uisite.setInvalidStyle()

    # Init a Group/Company/Site tree
    def setTree(self):
        from sysgrove.modules.masterdata.models import Group
        groups = Group.query.all()
        grp, cpn = [], []
        uiTree = getattr(self.ui_space, self.klass.get_ui_prefix() + 'sites')
        for group in groups:
            for company in group.companies:
                for site in company.sites:
                    if not group in grp:
                        uiGroup = TreeWidgetItem(
                            uiTree, group.description, group.code)
                        uiGroup.setCheckBox()
                        grp.append(group)

                    if not company in cpn:
                        uiCompany = TreeWidgetItem(
                            uiGroup,
                            company.description,
                            company.code
                        )
                        uiCompany.setCheckBox()
                        cpn.append(company)

                    uiSite = TreeWidgetItem(
                        uiCompany,
                        site.description,
                        site.code
                    )
                    uiSite.setCheckBox()
