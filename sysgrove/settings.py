#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton
07 / 02 / 2013
Version 1.0

Role:
    define some keys values settings usefull as business constante.

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import datetime

# misceallaneous
EVER = datetime.date(2099, 12, 31)
ALTCODE = 'alternate_code'
UNSET = u''

# mode
MODE_01 = 'CREATE'
MODE_02 = 'MODIFY'
MODE_03 = 'DISPLAY'
MODE_04 = 'ALLOCATION'
MODE_nn = 'OTHER'


# special datalookup class.
TP_DATAKOOKUP = 'sysgrove.modules.masterdata.ui.customs.UIDataLookupThirdParty'
SIMPLE_DATALOOKUP = 'sysgrove.ui.classes.SDataLookup'

# FlowId
SALES_ID = 1
PURCHASE_ID = 2
TRANSPORT_ID = 3
PRODUCTION_ID = 4
FINANCE_ID = 5
REPORTING_ID = 6
MDATA_ID = 7


# ThirdPartyType
SOLDETO_CODE = 1
SHIPTO_CODE = 2
BILLTO_CODE = 3
AGENT_CODE = 4

# Status
STATUS_OPEN = 1
STATUS_EXEC = 2
STATUS_PARTEXEC = 3
STATUS_CANCEL = 4
STATUS_CLOSE = 5
STATUS_WASHOUT = 6
STATUS_STRING = 7
STATUS_CIRCLE = 8

STATUS_FROZEN = [
    STATUS_WASHOUT, STATUS_CIRCLE,
    STATUS_STRING, STATUS_CANCEL
]

# DocGroup
DG_QUOTATION = 1
DG_CONTRACT = 2
DG_ORDER = 3
DG_RORDER = 4
DG_DELIVERY = 5
DG_INVOICE = 6

# some css:
READONLY_COLOR = "#C5C6C8"
READONLY_CSS = "background-color: %s;" % READONLY_COLOR


# rounding stuff, minimal version....
ROUNDING_MIN = 0.000001
