#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton
15/02/2013
Version 1.0

Role :

        - Provides a class for using context in Kernel

Revisions:
revision # 1
Date of revision: 25/02/2013
reason for revision:

        - screen setup fixed. (removing the QLayout bug)


Version 5.0 -- 02/28/2013

    - Contexts support now :
        - Screen
        - Submenu (accordion)
        - DynamicButtonZone

        The fact is dynamic buttons and submenus are linked to a given context.



contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

----
"""
import logging
# from functools import partial
from PyQt4 import QtCore, QtGui
from sysgrove.config import CONFIG
from sysgrove.utils import (
    get_class, cc_prefix,
    cc_print, code_to_screen,
    code_to_alchemyClass,
    cc_prev,
    debug_trace
)
from sysgrove.ui.classes import (
    UIStaticButtonZone, UITabBar,
    UIStatusBar, UIMenuBar, WarningPopup,
    ExceptionMessagePopup, UISpace
)
from sysgrove.ui.window import GfxContentLayout
from sysgrove.user import User
from sysgrove import i18n
_ = i18n.language.ugettext
log = logging.getLogger('sysgrove')


def get_prefix(ct):
    try:
        prefix = int(cc_prefix(ct))
    except ValueError:
        prefix = ct[:1]  # FIXME ?
    return prefix


class Context(object):

    def __init__(self, uid, context_code, screen, dynamicButtonZone,
                 ui_submenu_class, old_submenu=None, context_vars={}):

        self.uid = uid
        self.context_code = context_code
        self.ui_space = UISpace("context_code")
        self.ui_submenu_class = None
        self.submenu = None
        self.screen = screen
        self.context_vars = context_vars
        self.dynamicButtonZone = dynamicButtonZone

        # to deal with context_code such as 1OR01_main
        self.context_code_print = cc_print(context_code)
        self.back = None

        if hasattr(screen, 'ui_space'):
            # Creates a container widget
            self.ui_space = self.screen.ui_space
            # Adds the container to layout
            GfxContentLayout.addWidget(self.ui_space.qtObject)

            # Hides the container (before it will be filled)
            self.ui_space.hide()
            self.ui_space.style()  # Nice alternative
        else:
            GfxContentLayout.addWidget(self.ui_space.qtObject)

        if ui_submenu_class:
            self.ui_submenu_class = ui_submenu_class
            # Configures submenu
            self.submenu = self.ui_submenu_class.setupUi()
            self.submenu.context_code = context_code
            self.checkRights()

        # Means, we gave the function the oldsubmenu to reuse it instead.
        elif old_submenu:
            self.submenu = old_submenu

        self.variables = {}

    def checkRights(self):
        rights = map(lambda x: x.code, User().get_current.rights)

        def iter_rights(tree):
            if tree.code not in rights:
                tree.setHidden(True)
            for child in tree.childs:
                iter_rights(child)

        iter_rights(self.submenu)

    # "High" level methods ...
    def display(self):
        """ Shows the widget containing the context screen """
        if (self.ui_space):
            self.ui_space.show()
        if self.submenu:
            self.submenu.show()
        if self.dynamicButtonZone:
            self.dynamicButtonZone.show()

    def hide(self, submenu=True):
        """ Hides the widget containing the context screen """
        if (self.ui_space):
            self.ui_space.hide()
        if submenu and self.submenu:
            self.submenu.hide()
        if self.dynamicButtonZone:
            self.dynamicButtonZone.hide()

    def getVars(self):
        return self.variables

    def setBackContext(self, back_context):
        self.back = back_context

    @property
    def backContext(self):
        return self.back


class Kernel(object):

    def __init__(self):

        # Contexts related variables
        self.contexts = {}
        self.current_context = None
        self.context_count = 0
        self.context_uid = 10000  # Just counter's starting value.

        # UIComponents Classes
        self.sub_menus = {}
        # self.dyns_classes = {}
        # self.popups_classes = {}

        #  User related info
        self.common_data = {
            'username': 'L',
            'session': '0x00000090'
        }
        self.popupTooContexts = QtGui.QWidget(None)

    def addLinks(self):
        UIStatusBar.setUserCode(User().get_current.code)
        # FIXME: the code does not work, but if we change it to
        # for name, button in UIStaticButtonZone.static_buttons.items():
        #     button.clicked.connect(partial(self.open_context, button.code))
        # the log is lost???
        # It seems the the code is not the right one, 'L' vs 'L01' which is
        # the right one?
        for name, button in UIStaticButtonZone.static_buttons.iteritems():
            button.setAction(
                "clicked()",
                lambda: self.open_context(button.code))

        closeButton = UIStaticButtonZone.addStaticButton("closeButton", '')
        closeButton.setAction("clicked()", lambda: self.close_context())
        closeButton.addImage("close.svg")
        closeButton.setText(_("Close"))

    @staticmethod
    def is_menu_action(code):
        return code and cc_prefix(code) != code

    def get_context_dynamicZone_by_code(self, code):
        return self.findDynByCode(code)

    def get_current_context(self):
        """ Sometimes we might need a function rather a
         simple attributes, for lambdas, etc. """
        return self.contexts[self.current_context]

    def getSubMenuClassByContextCode(self, code):
        val = None
        for sc in self.sub_menus:
             # if code.startswith ( sc ): #prefix check
            if cc_prefix(code) == cc_prefix(sc):
                val = self.sub_menus[sc]

        return val

    def open_uniq_context(self, context_code):
        """ Should be called in place of open_context for
        contexts which cannot have more than one instance at the same time """
        prefix = cc_prefix(context_code)
        for c in self.contexts:
            if cc_prefix(self.contexts[c].context_code) == prefix:
                self.display_context(c)
                return

        self.open_context(context_code)

    # no screen here because when we open a context we always use
    # default screen for a given context code
    def open_context(self, context_code):
        """ Opens in a new tab (if any available) matching
        the context_code given and call set_context to create the context """

        # Max context reached !
        if self.context_count >= CONFIG['context_limit']:
            # FIXME: we  should  call a trapped popup
            self.popupTooContexts.hide()
            self.popupTooContexts = ExceptionMessagePopup(
                None,
                "Too contexts are open. Please close one.",
                None)
            self.popupTooContexts.show()
            #raise Exception('KC_MaxContextReached %s ' % context_code)
        else:
            # This is the next context
            # don't put '+ 1' because contexts id starts at 0

            next_context_num = self.context_count

            # open_tab ( context_code )
            newtab = UITabBar.addTab(next_context_num)

            # We display the captions only for contexts which have a screen
            if self.is_menu_action(context_code):
                newtab.setText(context_code)
            else:
                newtab.setText("")

            # FIXME : Use trap ?
            newtab.setAction(
                "clicked()",
                lambda: self.display_context(newtab.getContextNum()))

            # Do it after the tab is created
            self.set_context(next_context_num, context_code)

            # Now, the context is opened, let's increase the context count
            self.context_count += 1

    def open_with_vars(self, context_code, cvars):
        """ Opens in a new tab (if any available) matching
        the context_code given and call set_context to create the context """

        # Max context reached !
        if self.context_count >= CONFIG['context_limit']:
            # FIXME: we  should  call a trapped popup
            raise Exception('KC_MaxContextReached %s ' % context_code)
        else:
            # This is the next context
            # don't put '+ 1' because contexts id starts at 0

            next_context_num = self.context_count

            # open_tab ( context_code )
            newtab = UITabBar.addTab(next_context_num)

            # We display the captions only for contexts which have a screen
            if self.is_menu_action(context_code):
                newtab.setText(context_code)
            else:
                newtab.setText("")

            # FIXME : Use trap ?
            newtab.setAction(
                "clicked()",
                lambda: self.display_context(newtab.getContextNum()))

            # Do it after the tab is created
            self.set_context(next_context_num, context_code, cvars)

            # Now, the context is opened, let's increase the context count
            self.context_count += 1

    def open_context_if_not_empty(self, code):
        for c in self.contexts:
            # NOTE : This is used by MenuButtons. Context_codes ARE preffixes !
            if code == self.contexts[c].context_code:
                self.set_context(c, code)
                return  # don't forget me :)

        # No matching context found, opening in new tab. NOTE : Doesn't check
        # if context limit is reached (actually open_context will do)
        self.open_context(code)

    def open_context_if_available(self, code):

        # We need to check is there is already a context WITH NO SCREEN, that
        # have a code which is a prefix of the context we want to open.

        for c in self.contexts:
            if cc_prefix(code) == cc_prefix(self.contexts[c].context_code) or \
                    code == self.contexts[c].context_code:
                # Exactly same context
                if code == self.contexts[c].context_code:
                    self .display_context(c)  # easy solution ...
                else:
                    # we setup the context in the screen.
                    #  (for example if '5' is opened and we type '5PC02',
                    # setup 5PC02 in 5 and move to it.
                    self.set_context(c, code)
                return  # don't forget me :)

        # No matching context found, opening in new tab. NOTE : Doesn't check
        # if context limit is reached (actually open_context will do)
        self.open_context(code)

    def set_current_context(self, code):
        # If there is already an opened context,
        # destroy it and set a new one up
        if isinstance(self.current_context, int):
            vars_backup = self.backup_vars(self.current_context)
            self.set_context(self.current_context, code, cvars=vars_backup)
        else:  # If not, open a new context
            self.open_context(code)

    def inSameSubMenu(self, a, b):
        return (cc_prefix(a) == cc_prefix(b))

    # FIXME: after the context is setup, do we need code+screen ???
    def set_context(self, context_num, context_code, cvars={}):
        """ Creates and displays the context
        (this is the setup, called once only) """

        # Backuping submenu
        if (context_num in self.contexts):
            osm = self.contexts[context_num].submenu
        else:
            osm = None

        # Destroys previous context
        # FIXME : don't destroy submenu maybe
        self.destroy_context(context_num)

        screen = None
        screen_klass = None
        dynamic = None

        if cc_prefix(context_code) != context_code:
            prefix = get_prefix(context_code)
            module_name = CONFIG['modules'][prefix]
            screenClassname = '%s.screens.Screen%s' % (
                module_name, context_code)

            try:
                screen_klass = get_class(screenClassname)
            except AttributeError:
                code = context_code[-2:]
                screen_klass = code_to_screen(code)
                aClass = code_to_alchemyClass(context_code)
                screen = screen_klass(
                    module=CONFIG['modules'][
                        get_prefix(context_code)] + '.screen',
                    code=context_code,
                    klass=aClass
                )
                widget_name = filter(
                    lambda x: 'screen' in x,
                    screen.ui_space.__items__.keys())[0]
                ui_widget = getattr(screen.ui_space, widget_name)
                if not aClass:
                    ui_widget.setStyleSheet("background-color:red;")

            if screen_klass and (screen is None):
                if 'newValue' in cvars:
                    screen = screen_klass(newValue=cvars['newValue'])
                else:
                    screen = screen_klass()

        if screen and hasattr(screen, 'dyns'):
            dynamic = screen.dyns

        # First : get previous submenu to know we need to change it.
        if (osm and self.inSameSubMenu(context_code, osm.context_code)):
            submenu_class = None  # Mean we want to keep
        else:
            submenu_class = self.getSubMenuClassByContextCode(context_code)

        if submenu_class:
            self.contexts[context_num] = Context(
                self.context_uid,
                context_code,
                screen,
                dynamic,
                submenu_class,
                cvars)
        else:
            self.contexts[context_num] = Context(
                self.context_uid,
                context_code,
                screen,
                dynamic,
                None,
                osm,
                cvars)

        self.contexts[context_num].setBackContext(cc_prev(context_code))
        self.context_uid += 1

        if screen:
            self.current_context = context_num
            if hasattr(screen, 'klass'):
                screen.setLinks()

            UITabBar.getTabByContextNum(context_num).setText(
                self.contexts[context_num].context_code_print)

        UITabBar.getTabByContextNum(context_num).setIcon(
            self.getIconByContextCode(self.contexts[context_num].context_code))

        # Now, display our context and hide the others (hide only, no
        # destruction !)
        self.display_context(context_num)

    def display_context(self, context_num):
        """ "Just" displays the context """
        # Update status bar
        try:
            # change me later
            UIStatusBar.setScreenCode(self.contexts[
                                      context_num].context_code_print)
        except Exception as e:
            log.debug("display context failed %s" % e)

        # Updates Menubar
        UIMenuBar.highlightButton(self.contexts[context_num].context_code)

        # Displays our context and hides the others
        # (hide only, no destruction !)
        for ct in self.contexts:
            # First, unmark all tabs
            try:
                x = UITabBar.getTabByContextNum(ct)
                x.unmark()
            except Exception as e:
                log.debug("display context failed (TABBAR) %s" % e)

            # Hide all screens
            self.contexts[ct].hide()

        if self.contexts[context_num]:
            self.contexts[context_num].display()
            try:
                x = UITabBar.getTabByContextNum(context_num)
                if x:
                    x.marker()
            except Exception as e:
                raise Exception('KE_DisplayContext_CannotMarkTab : %s' % e)
            # Finally sets up the current context value
            self.current_context = context_num

    def close_context(self):
        """
            Clean version, that closes the context.
            This version does not ask for data saving or so.
            Each module has to overwrite the close button link to
            what function ever they want that will finally close this
            function if necessary.
        """

        # These is nothing to do
        if self.context_count == 0:
            return

        context_num = self.current_context
        x = UITabBar.getTabByContextNum(context_num)
        if x:
            self.contexts[context_num].hide()
            x.qtObject.hide()
            x.qtObject.deleteLater()
            del x
            del UITabBar.tabs[context_num]
        del self.contexts[context_num]

        #
        # Problem : We need to do a 'shift' to update tabs (e.g. if
        # we close the 2nd tab, the 3rd will be the 2nd, the 4th the 3rd ...)
        # we can update nums but signals linked to tabs uses constant values
        # to context nums. We need to update them.
        # The easiest way to do it is to delete all tabs and create new ones
        # instead with new signals, nums ...
        #

        # Removes all tabs
        UITabBar.clear()

        u = 0
        new_contexts = {}

        for c in self.contexts:
            new_contexts[u] = self.contexts[c]

            # Solution create new tabs and delete old ones
            newtab = UITabBar.addTab(u)
            # We display the captions only for contexts which have a screen
            if self.is_menu_action(new_contexts[u].context_code):
                newtab.setText(new_contexts[u].context_code_print)
            else:
                newtab.setText("")
            newtab.setIcon(self.getIconByContextCode(
                new_contexts[u].context_code))

            # We cannot setup a connection within a loop in the same scope.
            # An intermediate function is needed.
            self.setNewTabAction(newtab)

            u += 1

        self.contexts = new_contexts
        self.current_context = min(
            self.current_context,
            len(self.contexts) - 1)
        self.context_count -= 1

        if (self.context_count <= 0):
            self.context_count = 0
            self.current_context = 0
            UIMenuBar.highlightNone()

            # change me later
            UIStatusBar.setScreenCode("")
        else:
            self.display_context(self.current_context)

    def setNewTabAction(self, newtab):
        """ Note : Python fails to add actions in a loop
        within the same execution context. We need an
        intermediate function that scopes the iterated values """
        newtab.setAction(
            "clicked()",
            lambda: self.display_context(newtab.getContextNum())
        )

    def backup_vars(self, context_num):
        # Nothing to dbackUp, because nothing has been created yet
        if self.contexts == {}:
            return

        x = {}
        if self.contexts[context_num].screen:
            if hasattr(self.contexts[context_num].screen, 'newValue'):
                x['newValue'] = self.contexts[context_num].screen.newValue
        return x

    # TODO : delete things for real
    def destroy_context(self, context_num):
        """
            Low level version, that deletes the context for real
            Note : THIS METHOD MUST RETURN THE CONTEXT_VARS
        """

        # because context_num can has 0 as a real value
        if isinstance(context_num, int) and context_num in self.contexts:
            self.contexts[context_num].hide()

    def lookupContextByCode(self, code):
        """ Wrapper used by Search bar """
        code = code.upper()
        if self.is_menu_action(code):
            self.open_context_if_available(code)
            return True
        return False

    # FIXME : in Gui/main maybe ?
    def getIconByContextCode(self, code):
        val = None
        for bt in UIMenuBar.menu_buttons:
            # prefix check
            if cc_prefix(code) == cc_prefix(UIMenuBar.menu_buttons[bt].code):
                val = UIMenuBar.menu_buttons[bt].iconName

        if val:
            return val

        for (bt, value) in UIStaticButtonZone.static_buttons.iteritems():
            if cc_prefix(code) == cc_prefix(value.code):
                val = UIStaticButtonZone.static_buttons[bt].iconName
        return val


# This is the instance of kernel that will be used almost everywhere ...
kernel = Kernel()
