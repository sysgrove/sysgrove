#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ___
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton
07/02/2013
Version 1.0

Role :

    -- ERROR MANAGER --

        Role :

            - Provides an unified way to handle exceptions and errors in the whole application
            - Overloads the standard exception management system
            - Defines classes `SGException` and `SGError`
            - Defines function `addException` that should be use to associate callbacks to exceptions and errors

revision # 1
Date of revision: 21/02/2013
reason for revision: - Adding the class SGError to differentiate real Errors from Exceptions (that are commonly used as callback trigggers)

revision # 2
Date of revision: 25/02/2013
reason for revision: - Minor changes (adding this header ...)

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

#
# This module only needs 'sys' and itself. Nice.
#

import sys

# This table will contain all exceptions types and all their matching callbacks
error_association_table = {}


class SG_Exception:  # represents exceptions. exceptions are used like internal triggers

    def __init__(self, args_dict):  # args_list is a list passed to exception when raised
        self.args_dict = args_dict


class SG_Error:  # Exactly like exception but only represents Errors (real errors)

    def __init__(self, args_dict):  # args_list is a list passed to exception when raised
        self.args_dict = args_dict


class SG_SIGError (SG_Error):

    def __init__(self, args_dict):
        pass


def callSigFunction(fun, args, kw):
    """ Calls the function, but through a ttrap signal, "borrowing" NM's thread """
    s = fun.__name__ + '(' + ','.join(args)
    if kw:
        s += "," + str(kw)
    s += ')'

    from ..NetworkManager.NetworkManager import nm  # ...
    nm.thread.ttrap.emit(s)


def callFunction(fun, args, kw):  # kw is the args_dict from the exception itself !
    fun(*args, **kw)  # well, ok.


def ErrorManager(type, value, tback):
    if 'args_dict' in value.__dict__:
        dct = value.args_dict
    else:
        dct = None  # Or maybe {}

    if (type in error_association_table):  # If the exception has an entry in exception table
        if issubclass(type, SG_SIGError):
            callSigFunction(error_association_table[type][
                            "func"], error_association_table[type]["args"], dct)
        else:
            callFunction(error_association_table[type][
                         "func"], error_association_table[type]["args"], dct)

# Replacing default's excepthook by our great ErrorManger :)
# sys.excepthook = ErrorManager # NOTE: Temporary disabled

# Just call me from anywhere to add a new exception to the list.


def addException(ex, fun, args):
    error_association_table[ex] = {"func": fun, "args": args}

# If the exception class doesn't exist, create it and give it global
# visibility <---- no, impossible here ... need to import errors in main
# ... :/
