#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton
07/02/2013
Version 1.0

Role :

        - Defines default values for all parameters
        - TODO : Reads config to override (some) parameters

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import os
import tempfile
from ConfigParser import SafeConfigParser, NoOptionError
from PyQt4.QtCore import QFile, QIODevice, QVariant
# do not remove, PyQt resources is used
# it is the main point to import PyQt resources module
import sysgrove
import sysgrove.qrc_resources
from sysgrove.settings import (
    SALES_ID, PURCHASE_ID,
    FINANCE_ID, MDATA_ID,
    REPORTING_ID
)


"""
Database configuration handle many possibilities
[database]
engine = # set sqlite or postgresql, or ... see sqlalchemy documentation
name = # database name
user = # (optional) database user
password = # (optional) password of the database user
host = # (optional) database host connection
port = # (optional) database port
"""


def read_config_file():
    parser = SafeConfigParser()
    parser.read("config.dat")

    try:
        engine = parser.get('database', 'engine')
        if engine == 'postgresql':
            engine = engine + '+psycopg2'
        name = parser.get('database', 'name')
    except NoOptionError:
        raise Exception("Cannot found database parameters")

    user = ''
    try:
        user = parser.get('database', 'user')
    except NoOptionError:
        pass

    passwd = ''
    try:
        passwd = parser.get('database', 'password')
    except NoOptionError:
        pass

    if user and passwd:
        usp = '%s:%s' % (user, passwd)
    else:
        usp = ''

    # value with a default value
    host = ''
    try:
        host = parser.get('database', 'host')
    except NoOptionError:
        pass

    port = ''
    try:
        port = parser.get('database', 'port')
    except NoOptionError:
        pass

    if host and port:
        host = '%s:%s' % (host, port)

    if usp and host:
        conn = '%s@%s' % (usp, host)
    else:
        conn = ''

    db_params = '%s://%s/%s' % (engine, conn, name)
    try:
        version = parser.get('version', 'extended')
    except:
        version = ''

    log_conf = {'agent': 'default',
                'host': '127.0.0.1',
                'port': 7000}

    try:
        log_conf['agent'] = parser.get('logging', 'agent')
    except:
        pass

    try:
        log_conf['host'] = parser.get('logging', 'host')
    except:
        pass

    try:
        log_conf['port'] = parser.getint('logging', 'port')
    except:
        pass

    return {'db_params': db_params,
            'logging': log_conf,
            'version_extended': version}


# getting css from a file handled by PyQt resouce
def get_css():
    css = QFile(':classic')
    css.open(QIODevice.ReadOnly)
    res = ""
    if css.isOpen():
        res = QVariant(css.readAll()).toString()
    css.close()
    return res


class Config (object):

    def __init__(self):
        self.c = {}
        self.locks = {}
        self.menus = {
            'menu': {},
            'submenu': {},
            'menuentry': {},
            'menuaction': {}
        }

    def __getitem__(self, attr):
        return self.c[attr]

    def __setitem__(self, attr, value):
        if attr in self.locks and self.locks[attr]:
            raise Exception('KE_Config_CannotOverrideLocked({})')
        self.c[attr] = value

    def lock(self, *args):
        for a in args:
            self.locks[a] = True

    def imp(self, d):
        self . c . update(d)


CONFIG = Config()

#
# -- Default values --#
#

config_file = read_config_file()
CONFIG . imp({
    "db": config_file["db_params"],
    "db_default": "sqlite:///sysgrove.sqlite",
    "logging": config_file["logging"],
    "appname": "SysGrove",
    "version": sysgrove.__version__,
    "version_extended": config_file["version_extended"],
    "theme": "classic",
    "user_language": "english",
    "server": "127.0.0.1",
    "port": "7709",
    "start_fullscreen": True,
    "master_server": "master.sysgrove.com",
    "master_port": "1228",
    "appid": 1332725964102984184117864131401591,
    "context_limit": 5,
    "licence": sysgrove.__licence__,
    "file_import_limit": (1048576 * 128),
    "export_path": os.path.expanduser("~"),
    "tmp_directory": os.path.join(tempfile.gettempdir(), "sysgrove-0"),
    "ui_tmp_filename": "truc2sam",
    "specs_filename": "specs.xml",
    "ui_env_filename": "ambient.xml",
    "language": "fr",
    "modules": {
        MDATA_ID: 'sysgrove.modules.masterdata',
        'H': 'sysgrove.modules.help',
        SALES_ID: 'sysgrove.modules.sales',
        PURCHASE_ID: 'sysgrove.modules.purchase',
        FINANCE_ID: 'sysgrove.modules.finance',
        'L': 'sysgrove.modules.log',
        REPORTING_ID: 'sysgrove.modules.reporting'
    },
    "menus": {
        'menu': {}
    },
    "css": get_css(),
    # All images (which are not in CSS (icons, pixmaps ...) must be
    # listed here in config['image'].
    "images": {}
    # Modules will update this dict
    # with their own ressources
    # paths. NOTE : All images must
    # be in 'gfx_dir'
})


# -- Following properties cannot be overwritten by config file. --#
#
CONFIG . lock('appname', 'version', 'license', 'appid', 'context_limit')
