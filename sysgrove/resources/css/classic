/**
   ____             ____                    
 / ___| _   _ ___ / ___|_ __ _____   _____ 
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/                              



Copyright SARL SysGrove
contributor(s) : G.Souveton 
07/02/2013
Version 1.0

    Role :

        - 
        
revision #
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the 
logistic execution, production, analysis, finance, transportation, 
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


---- **/


/**
    Special variables :
    $gfx$    refers to the directory where the gfx are.
**/

#GfxAccordion{
  border: 0px;
  background: #ffffff;
  font-size:small
}

#menubar > QWidget > QWidget > QToolButton {
  border: 2px solid #8f8f91;
  border-radius: 6px;
  font-weight: bold;
  font-size: 10px;
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #f6f7fa, stop: 1 #dadbde);
}

#menubar  > QWidget > QWidget > QToolButton[menuselected="true"] {
  border: 2px solid #F5BC40;
  border-radius: 6px;
  font-weight: bold;
  font-size: 10px;
  font-weight: bold;
  background-color: qlineargradient( x1: 0, y1: 0, x2: 0, y2: 1,
                 stop: 0.5 #F5BC40,stop: 1 #f6f7fa);
}

#menubar  > QWidget > QWidget > QToolButton[menuselected="false"] {
  border: 2px solid #8f8f91;
  border-radius: 6px;
  font-weight: bold;
  font-size: 10px;
  font-weight: bold;
  background-color: qlineargradient( x1: 0, y1: 0, x2: 0, y2: 1,
                 stop: 0 #f6f7fa, stop: 1 #dadbde);    
}

#menubar >  QWidget > QWidget > QToolButton:pressed {
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #dadbde, stop: 1 #f6f7fa);
}

#menubar >  QWidget > QWidget > QToolButton:hover{
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #dadbde, stop: 1 #f6f7fa);
  border: 2px solid #F5BC40;
}

#SysGroveMain{
  background-color: #EEEEEE;
}

#UIContent{
  background-image: -webkit-linear-gradient(bottom, rgb(170,209,193) 2%, rgb(240,240,240) 76%);
}

#UIContent{
  background-color: #aaaaaa;
}

#UIContent QWidget[class=UIWidget]{
  border: 0;
}

QComboBox {
  border: 1px solid #aaaaaa;
  border-radius: 3px;
  padding: 1px 18px 1px 3px;  
}

QComboBox:editable {
  background: white;
}

QComboBox:!editable, QComboBox::drop-down:editable {
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #FCFCFC, stop: 0.4 #F6F6F6,
                              stop: 0.5 #EBEBEB, stop: 1.0 #E7E7E7);
}

/* QComboBox gets the "on" state when the popup is open */
QComboBox:!editable:on, QComboBox::drop-down:editable:on {
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #E3E3E3, stop: 0.4 #E8E8E8,
                              stop: 0.5 #EEEEEE, stop: 1.0 #F1F1F1);
}

QComboBox:on { /* shift the text when the popup opens */
  padding-top: 3px;
  padding-left: 4px;
}

QComboBox::drop-down {
  subcontrol-origin: padding;
  subcontrol-position: top right;
  width: 15px;
  border-left-width: 1px;
  border-left-color: darkgray;
  border-left-style: solid; /* just a single line */
  border-top-right-radius: 3px; /* same radius as the QComboBox */
  border-bottom-right-radius: 3px;
}

QComboBox::down-arrow {
  image: url(:down-arrow.png);
  width: 20px;
  height: 20px;
}

QComboBox::down-arrow:on { /* shift the arrow when popup is open */
  top: 1px;
  left: 1px;
}

QComboBox QAbstractItemView {
  border: 1px solid #bbbbbb;
  selection-background-color: #666688;
  outline: none;
}

QGroupBox {
  /*background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #CCCCCC, stop: 0.2 #E0E0E0 ,stop: 0.8 #E0E0E0 stop: 1 #EEEEEE);*/
  background-color: #EEEEEE;     
  color: white;                 
  /*margin-top: 1ex; /* leave space at the top for the title */
  margin-top: 11px;
  padding: 0px;
  padding-top: 16px;
  /*border: 1px solid ;000000*/
  border: 1px solid #000000;
  border-radius: 4px;
  font-size: 10px; /* Don't ask why ... we cannot put it QGroupBox::title */
  font-weight: bold; /* Anyway, the only text in the groupbox is the title ... */
  /*padding : 0px;
  margin : 0px;*/
}

QGroupBox::title {
  subcontrol-origin: margin;
  subcontrol-position: top left; /* position at the top center */
  margin-left: 0px;
  padding: 0px 9px;
  margin-top: 11px;
  padding-left: 20px;
  padding-right: 20px;
  color: #000000;
  /*border: 1px solid #aaaaaa;*/
  border: 1px solid #000000;
  border-top-left-radius: 4px;
  border-bottom-right-radius: 4px;
  /*background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #EEEEEE, stop: 1 #CCCCCC);
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #E0E0E0, stop: 0 #EEEEEE);
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #E75232, stop: 1 #F5BC40);*/
  background-color : #3E4D58;
  color: white;
}

QLabel{
  background-color: transparent;
  border-radius: 0;
  border:none;
}

QLineEdit{
  background-color: #ffffff;
  border: 1px solid #aaaaaa;
}

/*
QPushButton{
  border: 1px solid black;
  border-radius: 6px;
}
*/

QPushButton{
border: 2px solid #8f8f91;
    border-radius: 6px;
    font-weight: bold;
    font-size: 10px;
    background-color: qlineargradient(
x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #f6f7fa, stop: 1 #dadbde);
 }
QPushButton:hover{
/*border: 2px solid #8f8f91;*/
border: 2px solid #F5BC40;
    border-radius: 6px;
    font-weight: bold;
    font-size: 10px;
    background-color: qlineargradient(
x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #dadbde, stop: 1 #f6f7fa);
}

QScrollArea{
  margin: 0;
  padding: 0;
  background-color: #eeeeee;
}

QScrollBar:vertical {
  border: none;
  background: #ececec;
  width: 16px;
  margin: 20px 0 20px 0;
}

QScrollBar::handle:vertical {
  background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                              stop: 0 #F3AA3F, stop: 1 #EE8639);
  width: 14px;
  min-height: 18px;
  background-image: url(:scrollhandlevertical.png);
  background-repeat: no-repeat;
  background-position: center center;
  border-radius: 4px;
  border: 1px solid #888888;  
}

QScrollBar::handle:vertical:hover {
  background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                              stop: 0 #EE8639, stop: 1 #F3AA3F);
  background-image: url(:scrollhandlevertical.png);
  background-repeat: no-repeat;
  background-position: center center;
  border: 1px solid #666666; 
}

QScrollBar::add-line:vertical {
  border: 1px solid #999999;
  background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                              stop: 0 #F5BC41, stop: 1 #F3AA3F);
  height: 20px;
  subcontrol-position: bottom;
  subcontrol-origin: margin;
}

QScrollBar::sub-line:vertical {
  border: 1px solid #999999;
  background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                              stop: 0 #F5BC41, stop: 1 #F3AA3F);
  height: 20px;
  subcontrol-position: top;
  subcontrol-origin: margin;
}
QScrollBar::add-line:vertical:hover {
  border: 1px solid #999999;
  background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                              stop: 0 #F3AA3F, stop: 1 #F5BC41);
  height: 20px;
  subcontrol-position: bottom;
  subcontrol-origin: margin;
}

QScrollBar::sub-line:vertical:hover {
  border: 1px solid #999999;
  background: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                              stop: 0 #F3AA3F, stop: 1 #F5BC41);
  height: 20px;
  subcontrol-position: top;
  subcontrol-origin: margin;
}
QScrollBar::up-arrow:vertical {
  width: 16px;
  height: 20px;
  background: url(:scroll-up.png); /* FIXME */
  background-repeat: no-repeat;
  background-position: center center;
}

QScrollBar::down-arrow:vertical {
  width: 16px;
  height: 20px;
  background: url(:scroll-down.png); /* FIXME */
  background-repeat: no-repeat;
  background-position: center center;
}

QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
  background: none;
}

QScrollBar:horizontal {
  border: none;
  background: #ececec;
  height: 16px;
  margin: 0 20px 0 20px;
}

QScrollBar::handle:horizontal {
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #F3AA3F, stop: 1 #EE8639);
  height: 14px;
  min-width: 18px;
  background-image: url(:scrollhandlehorizontal.png);
  background-repeat: no-repeat;
  background-position: center center;
  border-radius: 4px;
  border: 1px solid #888888;  
}

QScrollBar::handle:horizontal:hover {
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #EE8639, stop: 1 #F3AA3F);
  background-image: url(:scrollhandlehorizontal.png);
  background-repeat: no-repeat;
  background-position: center center;
  border: 1px solid #666666; 
}

QScrollBar::add-line:horizontal {
  border: 1px solid #999999;
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #F5BC41, stop: 1 #F3AA3F);
  width: 20px;
  subcontrol-position: right;
  subcontrol-origin: margin;
}

QScrollBar::sub-line:horizontal {
  border: 1px solid #999999;
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #F5BC41, stop: 1 #F3AA3F);
  width: 20px;
  subcontrol-position: left;
  subcontrol-origin: margin;
}

QScrollBar::right-arrow:horizontal {
  width: 20px;
  height: 16px;
  background: url(:scroll-right.png); /* FIXME */
  background-repeat: no-repeat;
  background-position: center center;
}

QScrollBar::left-arrow:horizontal {
  width: 20px;
  height: 16px;
  background: url(:scroll-left.png); /* FIXME */
  background-repeat: no-repeat;
  background-position: center center;
}

QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {
  background: none;
}

QSplitter::handle:horizontal {
  background-color: #424242;
  width: 10px;
}

QSplitter::handle:vertical {
  background-color: #424242;
  height: 10px;
}

/* Style the tab using the tab sub-control. Note that it reads QTabBar _not_ QTabWidget */
QTabBar::tab {
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,
                              stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);
  border: 1px solid #999999;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  min-width: 8ex;
  padding-left: 5px;
  padding-right: 5px;
  padding-top: 3px;
  padding-bottom: 1px;
}

QTabBar::tab:selected {
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 1 #E75232, stop: 0 #F5BC40, stop: 0.5 #C5C6C8, stop: 1.0 #FFFFFF);
  border: 1px solid #F5BC40;
}

QToolBox QScrollBar::down-arrow:vertical {
  width: 16px;
  height: 0px;
}

QToolBox QScrollBar::up-arrow:vertical {
  width: 16px;
  height: 0px;
}

QToolBox  QScrollBar::add-line:vertical {
  height: 0px;
  border: 0;
  subcontrol-position: bottom;
  subcontrol-origin: margin;
}

QToolBox QScrollBar::sub-line:vertical {
  height: 0px;
  border: 0;
  subcontrol-position: top;
  subcontrol-origin: margin;
}

QToolBox QScrollBar:vertical {
  border: 0;
  width: 16px;
  margin: 0;
}

QTabWidget{
  border-radius: 4px; 
  background-color: #dddddd;
  background-color: #EEEEEE;
  border: 2px solid #E75232;
  font-size: 11px;
  font-weight: bold;
}

QTabWidget::pane { /* The tab widget frame */
  border: 2px solid #F5BC40;
  border-top-right-radius: 4px;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
  /*background-color: #EEEEEE;*/
  background-color: #C5C6C8;
}

QTabWidget::tab-bar { /*     background-color: #0f0;*/
  left: 0px; /* move to the right by 5px */
  border: 0px;
}

QToolBox::tab {
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #D1D1D1, stop: 0.4 #CCCCCC,
                              stop: 0.5 #C8C8C8, stop: 1.0 #C3C3C3);
  border-radius: 5px;
  color: #000000;
  font-weight: bold;
}

QToolBox::tab:selected { /* italicize selected tabs */
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #A2A4F1, stop: 0.4 #DCDCCC,
                              stop: 0.5 #A8A8F8, stop: 1.0 #D2D2B2);
  color: #000000;
}

QToolBox::tab#sub{
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,
                              stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);
  border-radius: 5px;
  color: #000000;
  font-weight: bold;
  font-style: italic;
}

QToolBox::tab:selected#sub { /* italicize selected tabs */
  background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                              stop: 0 #F1D1D1, stop: 0.4 #EDDDDD,
                              stop: 0.5 #F8D8D8, stop: 1.0 #D2C2C2);
  color: #000000;
}

QToolBox > QWidget {
  font-weight: normal;
}

QToolButton{
  border: 2px solid #8f8f91;
  border-radius: 6px;
  font-weight: bold;
  font-size: 10px;
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #f6f7fa, stop: 1 #dadbde);
}

QToolButton:hover{
  border: 2px solid #F5BC40;
  border-radius: 6px;
  font-weight: bold;
  font-size: 10px;
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                    stop: 0 #dadbde, stop: 1 #f6f7fa);
}

QToolButton:flat {
  border: none; /* no border for a flat push button */
}

QToolButton:default {
  border-color: navy; /* make the default button prominent */
}

QToolButton#masterData{
}

QToolButton[menuselected="false"]{
  
}

QToolButton[menuselected="true"]{
  border: 2px solid #F5BC40;
  border-radius: 5px;
  font-weight: bold;
  font-size: 10px;
  font-weight: bold;
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                    stop: 0 #F5BC40, stop: 1 #f4f3c5);
}

#GfxMenuBar > QToolButton { 
  border: 2px solid #8f8f91; 
  border-radius: 6px; font-weight: bold; 
  font-size: 10px; 
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,  stop: 0 #f6f7fa, stop: 1 #dadbde);
}

QToolButton.StaticButton{
  border: 2px solid #4f4f51;
  border-radius: 5px;
  font-size: 10px;
  font-weight: bold;
  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #e4e4c8, 
                                    stop: 1 #b9b9a9);
}

QTreeWidget{
  border: 1px solid black;
}

QWidget{
  color: #000000;
}

QWidget#screen{
  /*background-color: #C5C6C8;*/
  background-color: #aaaaaa;
  padding: 0px;
  margin: 0px;
  border: none;
}



/*******Commentaire******/
#widget_4{
  /*background-color: #00e; */
}

#widget_11{
  /*background-color: #34B902; */
}

/*
.GfxPage{
  background:#ecffe5;
}
*/

/*
#menubar > QToolButton{
    border: 2px solid #8f8f91;
    border-radius: 6px;
    font-weight: bold;
    font-size: 10px;
    background-color: transparent;*/
/*qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 rgba(0,0,0,255), stop: 1 rgba(0,0,0,0));
}*/