#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

This module provides useful function for alembic migration
Alembic migration may involved data creation, update or delete, the
functions of these module aim to help these kind of needs.

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from sysgrove.utils import submenu_to_dict
from sysgrove.modules.masterdata.models import Menu
from sysgrove.modules.masterdata.models import UserProfile
from sysgrove.modules.masterdata.models import Menu_UserProfile_Allocation


log = logging.getLogger('sysgrove')


def create_menu(data, code, connection):
    """ From the code we could deduce the depth of the menu.

    """
    if len(code) == 1:
        # case of a module, there is no parent_menu in this case
        # add it only for ROOT user

        assert(code == data.flowId)
        root = connection.execute(
            UserProfile.__table__.select()
            .where(UserProfile.__table__.c.UserProfileCode == u"ROOT")
        ).first()

        menu = connection.execute(
            Menu.__table__.insert()
            .values(
                MenuDescription=data.verbose,
                MenuCode=unicode(code),
                FlowID_pk=int(data.flowId),
                CancelData=False
            )
        )

        menu_pk = menu.inserted_primary_key[0]
        log.info("Menu %s insert with pk %s" % (code, menu_pk))
        connection.execute(
            Menu_UserProfile_Allocation.insert()
            .values(UserProfile_pk=root.UserProfile_pk,
                    Menu_pk=menu_pk)
        )

    else:
        # from the code lenght we are able to know the depth of the menu
        depth = (len(code) - 1) / 2
        parent_menu = connection.execute(
            Menu.__table__.select()
            .where(Menu.__table__.c.MenuCode == unicode(code[:-2]))
        ).first()
        submenu_dict = submenu_to_dict(data.submenu)
        new_menu_details = submenu_dict
        for i in xrange(depth - 1, 0, -1):
            new_menu_details = new_menu_details[code[:-(2 * i)]]
        new_menu_details = new_menu_details[code]
        menu = connection.execute(
            Menu.__table__.insert()
            .values(
                MenuDescription=u"%s - %s" % (code,
                                              new_menu_details['description']),
                MenuCode=unicode(code),
                ParentMenu_pk=parent_menu.Menu_pk,
                Model=new_menu_details['model'],
                FlowID_pk=int(data.flowId),
                DocGroup_pk=new_menu_details['docGroup'],
                CancelData=False
            )
        )

        # we the menu pk, is there a bett
        menu_pk = menu.inserted_primary_key[0]
        log.info("Menu %s insert with pk %s" % (code, menu_pk))

        # create rights for all Users that have already the right of the
        # parent_menu
        for uma in connection.execute(
            Menu_UserProfile_Allocation.select()
                .where(Menu_UserProfile_Allocation.c.Menu_pk ==
                       parent_menu.Menu_pk)):

            connection.execute(
                Menu_UserProfile_Allocation.insert()
                .values(UserProfile_pk=uma.UserProfile_pk,
                        Menu_pk=menu_pk)
            )


def remove_menu(code, connection):
    menu = connection.execute(
        Menu.__table__.select()
        .where(Menu.__table__.c.MenuCode == unicode(code))
    ).first()
    connection.execute(
        Menu_UserProfile_Allocation.delete()
        .where(Menu_UserProfile_Allocation.c.Menu_pk == menu.Menu_pk))
    connection.execute(Menu.__table__.delete()
                       .where(Menu.__table__.c.Menu_pk == menu.Menu_pk))
