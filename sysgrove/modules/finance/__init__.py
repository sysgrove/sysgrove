#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sysgrove.utils import find_key
from sysgrove.config import CONFIG
from sysgrove.modules import ModuleData
from sysgrove import i18n
_ = i18n.language.ugettext

_menu = [
    ('5MI', _('Miscellaneous'), [
        ('5MI01', _('Creation'), []),
        ('5MI02', _('Modification'), []),
        ('5MI03', _('Display'), []),
    ]),
    ('5CL', _('Credit Limit'), [
        ('5CL01', _('Creation'), []),
        ('5CL02', _('Modification'), []),
        ('5CL03', _('Display'), []),
    ], 'creditLimit'),
    ('5PS', _('Accounts Recevable'), [
        ('5PS01', _('Payment'), [], 'payment'),
        ('5PS02', _('Lettering'), [], 'lettering'),
    ]),
    ('5PC', _('Accounts Payable'), [
        ('5PC01', _('Payment'), [], 'payment'),
        ('5PC02', _('Lettering'), [], 'lettering'),
    ]),
    ('5MV', _('Market Valuation'), [
        ('5MV01', _('Creation'), []),
        ('5MV02', _('Modification'), []),
        ('5MV03', _('Display'), []),
    ], 'marketValuation'),
    ('5RE', _('Reports'), [
        ('5RE01', _('General Balance'), []),
        ('5RE02', _('Third Parties Balance'), []),
        ('5RE03', _('Treasery Forecast'), []),
        ('5RE04', _('General Ledger'), []),
        ('5RE05',_('Third Parties Ledger'), []),
        ('5RE06', _('Third Parties Aged'), []),
        ('5RE07', _('Analytics'), []),
        ('5RE08', _('Journals'), []),
    ]),
    ('5CO', _('Closing'), [
        ('5CO01', _('Entry Validation'), []),
        ('5CO02', _('Month End'), []),
        ('5CO03', _('Year End'), []),
    ]),

]


data = ModuleData(
    find_key(__package__, CONFIG['modules']),
    u'Finance',
    'finance.svg',
    _menu
)
