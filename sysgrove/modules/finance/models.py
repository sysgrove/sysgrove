#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
__all__ = [
    "Lettering",
    "Payment",
    "MarketValuation",
]

from sqlalchemy import Column, Integer, ForeignKey, Float, Date, Unicode
from sqlalchemy.orm import relationship
from sysgrove.settings import TP_DATAKOOKUP
from sysgrove.models import Base
from sysgrove.models import InitMixin, SimplMixin
from sysgrove.modules.masterdata.mixin import SiteMixin
from sysgrove.modules.masterdata.models import Currency as _Currency
from sysgrove.modules.masterdata.models import MarketType as _Market
from sysgrove.modules.masterdata.models import ItemDetail as _ItemDetail
from sysgrove.modules.masterdata.models import ItemGroup as _ItemGroup
from sysgrove.modules.masterdata.models import Ledger as _Ledger
from sysgrove.modules.masterdata.models import ThirdParty as _ThirdParty
from sysgrove.modules.masterdata.models import ThirdPartyType as _TPT
from sysgrove.modules.masterdata.models import PaymentType as _PaymentType
from sysgrove.core.document.models import DocumentDetail as _DocumentDetail
from sysgrove import i18n
_ = i18n.language.ugettext


class MarketValuation(InitMixin, SiteMixin, Base):
    verbose_name = _('Market Valuation')
    list_selection = ['site', 'marketType',
                      'itemDetail_itemGroup', 'itemDetail']
    list_valuation = [
        'valuationDate', ('UIMonth', 'marketMonth'), 'marketPrice', 'currency']

    list_display = [
        [(('GroupBox', _('Market Selection')), list_selection)],
        [(('GroupBox', _('Market Valuation')), list_valuation)],

    ]

    # market FK
    marketType_id = Column(
        "Market",
        Integer,
        ForeignKey('MarketType.MarketType_pk')
    )
    marketType = relationship(
        "MarketType",
        info={'verbose_name': _Market.verbose_name}
    )

    # ItemDetail FK
    itemDetail_id = Column(
        "ItemDetail_pk",
        Integer,
        ForeignKey('ItemDetail.ItemDetail_pk')
    )
    itemDetail = relationship(
        "ItemDetail",
        info={'verbose_name': _ItemDetail.verbose_name}
    )

    valuationDate = Column(
        "ValuationDate", Date,
        info={'verbose_name': _('Valuation Date')}
    )

    marketMonth = Column(
        'MarketMonth', Date,
        info={'verbose_name': _('Market Month')}
    )

    marketPrice = Column(
        'MarketPrice', Float,
        info={'verbose_name': _('Market Price')}
    )

    # ItemGroup FK
    itemGroup_id = Column(
        "ItemGroup_pk",
        Integer,
        ForeignKey('ItemGroup.ItemGroup_pk')
    )
    itemGroup = relationship(
        "ItemGroup",
        info={'verbose_name': _ItemGroup.verbose_name}
    )

    # Currency FK
    currency_id = Column(
        "Currency_pk",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    currency = relationship(
        "Currency",
        info={'verbose_name': _Currency.verbose_name}
    )


class Payment(InitMixin, SiteMixin, Base):
    verbose_name = _('Payment')

    list_base = [
        'site', 'paymentDate',
        'thirdParty', 'paymentType', 'ledger',
        ['paidAmount', 'currency'],
        'paymentReference'
    ]
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],

    ]

    # 3party fk
    thirdParty_id = Column(
        "ThirdParty_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )
    thirdParty = relationship(
        "ThirdParty",
        info={'verbose_name': _ThirdParty.verbose_name,
              'datalookup': TP_DATAKOOKUP,
              }
    )
    # 3partyType fk
    thirdPartyType_id = Column(
        "ThirdPartyType_pk",
        Integer,
        ForeignKey('ThirdPartyType.ThirdPartyType_pk')
    )
    thirdPartyType = relationship(
        "ThirdPartyType",
        info={'verbose_name': _TPT.verbose_name})

    # payment type fk
    paymentType_id = Column(
        "PaymentType_pk",
        Integer,
        ForeignKey('PaymentType.PaymentType_pk')
    )
    paymentType = relationship(
        "PaymentType",
        info={'verbose_name': _PaymentType.verbose_name}
    )

    paymentDate = Column(
        "PaymentDate", Date, info={'verbose_name': _('Payment Date')}
    )

    # ledger fk
    ledger_id = Column(
        "Ledger_pk",
        Integer,
        ForeignKey('Ledger.Ledger_pk')
    )
    ledger = relationship(
        "Ledger",
        info={'verbose_name': _Ledger.verbose_name}
    )

    paidAmount = Column(
        "PaidAmount", Float,
        info={'verbose_name': _('Paid Amount')}
    )

    # Currency FK
    currency_id = Column(
        "Currency_pk",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    currency = relationship(
        "Currency",
        info={'verbose_name': _Currency.verbose_name}
    )

    paymentReference = Column(
        "PaymentReference",
        Unicode(),
        info={'verbose_name': _('Payment Reference'), "length": 80}
    )


class Lettering(SimplMixin, Base):
    verbose_name = _('Lettering')

    # Payment fk
    payment_id = Column(
        "Payment_pk",
        Integer,
        ForeignKey('Payment.Payment_pk')
    )
    payment = relationship(
        "Payment",
        info={'verbose_name': Payment.verbose_name}
    )

    # DocumentDetail fk
    documentDetail_id = Column(
        "DocumentDetail_pk",
        Integer,
        ForeignKey('DocumentDetail.DocumentDetail_pk')
    )
    documentDetail = relationship(
        "DocumentDetail",
        info={'verbose_name': _DocumentDetail.verbose_name}
    )
