#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
____             ____
/ ___| _   _ ___ / ___|_ __ _____   _____
\___ \| | | / __| |  _| '__/ _ \ \ / / _ \
        ___) | |_| \__ \ |_| | | | (_) \ V /  __/
|____/ \__, |___/\____|_|  \___/ \_/ \___|
|___/



Copyright SARL SysGrove
contributor(s) : [Claude Huchet]
[07:08/2013]
Version 0.0

Revisions:
    [revision #]
    [date of revision]
    [reason for revision]

contact: contact@sysgrove.com

Role :
    This module aims to defined general classes to build ui for
    simple alchemy classes, the classes that inherite only from SGMixin.
    I would like to use inheritance instead of specif xml files.


This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from functools import partial
from PyQt4.QtGui import (QTreeWidget, QTreeWidgetItem, QComboBox,
                         QPushButton, QVBoxLayout)
from sysgrove.models import DocumentLog
from sysgrove.modules.masterdata.models import DocType, Menu
from sysgrove.core.document.models import DocumentHeader
from sysgrove.ui.classes import *
from sysgrove import i18n
_ = i18n.language.ugettext


class MenuFilter(QComboBox):

    def __init__(self, *args, **kwargs):

        super(MenuFilter, self).__init__(*args, **kwargs)

        self.setEditable(True)
        master = Menu.query.filter_by(description="Master Data")[0]

        entries = master.children

        tpl = "%s"
        items = [tpl % (e.description, )
                 for e in entries]
        self.addItems(items)

        self.entries = [e.id for e in entries]

    def entry(self, index):
        return self.entries[index]


class SubMenuFilter(QComboBox):

    def __init__(self, menu_widget, *args, **kwargs):

        super(SubMenuFilter, self).__init__(*args, **kwargs)
        self.setEditable(True)

        menu_widget.currentIndexChanged.connect(self.refresh)
        self.menu_widget = menu_widget
        self.refresh(0)

    def refresh(self, index):

        self.clear()
        self.clearEditText()

        entry = self.menu_widget.entry(index)
        sub_menus = Menu.query.filter_by(parentMenu_id=entry)

        tpl = u"%s"
        items = [tpl % (sm.description, ) for sm in sub_menus]
        self.addItems(items)

        self.entries = [str(sm.model).capitalize() for sm in sub_menus]

    def entry(self, index):
        print self.entries[index]
        return self.entries[index]


class DocTypeFilter(QComboBox):

    def __init__(self, *args, **kwargs):

        super(DocTypeFilter, self).__init__(*args, **kwargs)
        self.setEditable(True)

        doc_types = DocType.query.all()
        tpl = "%s-%s"
        items = [tpl % (d.code, d.description)
                 for d in doc_types]
        self.addItems(items)

        self.doc_types = [d.id for d in doc_types]

    def doc_type(self, index):
        return self.doc_types[index]


class DocRefFilter(QComboBox):

    def __init__(self, types_widget, *args, **kwargs):

        super(DocRefFilter, self).__init__(*args, **kwargs)
        self.setEditable(True)

        types_widget.currentIndexChanged.connect(self.refresh)
        self.types_widget = types_widget
        self.refresh(0)

    def refresh(self, index):

        self.clear()
        self.clearEditText()

        doc_type = self.types_widget.doc_type(index)
        doc_refs = DocumentHeader.query.filter_by(docType_id=doc_type)

        tpl = "%s-%s"
        items = [tpl % (dr.documentNumber, dr.creationDate) for dr in doc_refs]
        self.addItems(items)

        self.doc_refs = [dr.id for dr in doc_refs]

    def doc_ref(self, index):
        return self.doc_refs[index]


class UIL01(object):

    def masterdata_tab(self, parent):
        self.mode_md = UITabWidget(parent, "mode_md")
        self.mode_md.setTitle("Master Data")
        self.log_md_mode = UIHLayout(self.mode_md, "log_md_mode")
        self.searchBarWidget = UIGroupBox(self.mode_md, "searchBarWidget")
        self.searchBarWidget.setTitle("Filter Log")
        self.searchBarLayout = QVBoxLayout(self.searchBarWidget.qtObject)

        self.log_md_g = UIGridLayout(None, "log_md_g")
        self.lfilter_label = UILabel(self.searchBarWidget, "lfilter_label")
        self.lfilter_label.setText(_("Data"))
        self.master_filter = MenuFilter(self.searchBarWidget.qtObject)
        self.ilfilter_label = UILabel(self.searchBarWidget, "ilfilter_label")
        self.ilfilter_label.setText(_("Code"))
        self.master_code_filter = SubMenuFilter(self.master_filter,
                                                self.searchBarWidget.qtObject)

        self.log_md_g.addWidget(self.lfilter_label, 0, 0)
        self.log_md_g.addWidget(self.master_filter, 0, 1)
        self.log_md_g.addWidget(self.ilfilter_label, 1, 0)
        self.log_md_g.addWidget(self.master_code_filter, 1, 1)
        self.searchBarLayout.addLayout(self.log_md_g.qtObject)
        self.log_md_mode.addWidget(self.searchBarWidget)

        filter_button = QPushButton()
        filter_button.setText(_("Filter logs"))
        self.searchBarLayout.addWidget(filter_button)
        results = QTreeWidget()
        self.searchBarLayout.addWidget(results)

        filter_button.clicked.connect(partial(self.update_masterdata,
                                              results))

    def update_masterdata(self, destination):

        logs = DocumentLog.query.filter_by(doc_type=None)

        idx = self.master_code_filter.currentIndex()
        if idx != -1:
            logs = logs.filter_by(doc_name=self.master_code_filter.entry(idx))

        self.fill_results_area(logs.order_by(DocumentLog.created_at),
                               destination)

    def update_documents(self, destination):

        logs = DocumentLog.query

        idx = self.doc_type_filter.currentIndex()
        if idx != "":
            doc_type = self.doc_type_filter.doc_type(idx)
            logs = logs.filter_by(doc_type=doc_type)

        idx = self.doc_ref_filter.currentIndex()
        if idx != -1:
            ref = self.doc_ref_filter.doc_ref(idx)
            logs = logs.filter_by(doc_ref=ref)

        self.fill_results_area(logs.order_by(DocumentLog.created_at),
                               destination)

    def documents_tab(self, parent):

        self.mode_doc = UITabWidget(parent, "mode_doc")
        self.mode_doc.setTitle("Document")
        self.log_doc_mode = UIHLayout(self.mode_doc, "log_doc_mode")
        self.searchBarWidgetDoc = UIGroupBox(self.mode_doc,
                                             "searchBarWidgetDoc")

        self.searchBarWidgetDoc.setTitle("Filter Log")
        self.searchBarLayoutDoc = QVBoxLayout(self.searchBarWidgetDoc.qtObject)
        self.doc_type_filter = DocTypeFilter(self.searchBarWidgetDoc.qtObject)

        self.doc_ref_filter = DocRefFilter(self.doc_type_filter,
                                           self.searchBarWidgetDoc.qtObject)
        #self.lfilter_label = UILabel(self.searchBarWidgetDoc, "lfilter_label")
        self.searchBarLayoutDoc.addWidget(self.doc_type_filter)
        self.searchBarLayoutDoc.addWidget(self.doc_ref_filter)
        #self.searchBarLayoutDoc.addWidget(self.lfilter_label.qtObject)
        self.log_doc_mode.addWidget(self.searchBarWidgetDoc)

        filter_button = QPushButton()
        filter_button.setText(_("Filter logs"))
        self.searchBarLayoutDoc.addWidget(filter_button)
        results = QTreeWidget()
        self.searchBarLayoutDoc.addWidget(results)

        filter_button.clicked.connect(partial(self.update_documents,
                                              results))

    def fill_results_area(self, logs, tree):

        tree.clear()
        template = "%s - %s user id %s, description %s\n"
        field_tpl = "field %s, old value %s, new value %s\n"
        lines = []
        for l in logs:
            if l.created_by_id == 1:
                created_by = "root"
            else:
                created_by = l.created_by

            line = QTreeWidgetItem(tree)
            line.setText(0, template % (l.doc_name, l.created_at,
                                        created_by, l.description))
            lines.append(line)

            if l.details:
                details = []
                for d in l.details:
                    detail = QTreeWidgetItem(line)
                    detail.setText(0, field_tpl % (d.field_name,
                                                   d.old_value,
                                                   d.new_value))
                    details.append(detail)

                line.addChildren(details)

            tree.addTopLevelItems(lines)

    def setupUi(self, Screen):
        self.widgeth01 = UIWidget(Screen, "widgeth01")
        self.widgeth01.setMinimumWidth(820)
        self.widgeth01.setMinimumHeight(520)
        self.contentLayouth01bis = UIVLayout(self.widgeth01,
                                             "contentLayouth01bis")
        self.contentLayouth01 = UIHLayout(None,
                                          "contentLayouth01")
        self.log_mode_selection = UITabPanel(self.widgeth01,
                                             "log_mode_selection")

        self.masterdata_tab(self.log_mode_selection)
        self.documents_tab(self.log_mode_selection)

        self.contentLayouth01bis.addLayout(self.contentLayouth01)
        self.contentLayouth01bis.addWidget(self.log_mode_selection)

uiL01 = UIL01()


class Dyn01(UIDynamicButtonZoneInst):

    def setupUi(self):
        self.flt = self.addDynamicButton("flt")
        self.flt.setText(_("Filter"))
