
from sysgrove.core.screen import Screen01
from sysgrove.ui.classes.widget import UIWidget
from sysgrove.ui.classes.button import UIButton
from sysgrove import i18n
_ = i18n.language.ugettext


class ScreenL01(Screen01):

    def __init__(self, *p, **k):
        self.default_table = None
        super(ScreenL01, self).__init__(*p, **k)

    def create_log_view(self):
        w = UIWidget(self.ui_space, "log_main_area")
        b = UIButton(w, "find_logs")
        b.setText(_("find logs"))
        return w

    def getDataFilter(self):
        # TODO / this only works for masterData (i.e. things that have a
        # header only, and a 'code'/'description' form)

        self.ui_space.logContent.setHtml(
            logTemplate_masterdata(
                self.ui_space.log_filter.value(),
                self.ui_space.idlog_filter.value()
            )
        )

# TODO
#
# - Replace second lineEdit by datalookup dynamically linked to first lineEdit value
#
# - Log doesn't records changes on fields (see models/update for a stub) : need to check
#   every attribute individually to record differences
#
# - Make it working for Docs
#
# - Make it working for update/deletes ...
#
# - Add CATS (only Insertion works for now)
