from Kernel.Kernel import kernel
import re
from Kernel.Config import config
 
"""

    > This module uses ModuleManager to load help files ... howto change it ?

    > This module uses ModuleManager to replace Kernel's doSearch()

    > 'searchWordForHelp' seems to be ok.





"""

helpDico = {}
tradDico = {}

def loadHelpWelcomeScreen ( context_num ):

#    lang = config["language"]

#    x = kernel.mainEnv["x"] FIXME

#    content = []
    
#    content.append("Help/doc/"+lang+"/HelpWelcome_"+lang) 

#    helpContent = kernel.find(context_num,"helpScreen")

#    helpContent . setHtml ( formalizeContent(content) )
    
    #helpContent.load("http://www.google.fr") <--- this is the best help ever :)
    
    
# Need to define a function that load ALL the help files.
# For ALL modules, read all files in doc/

# Then store them in a dictionary
# And then you just have to look in that dictionary when doing a doSearch

def loadHelp( context_num ):

    #lang = config["language"]

    #MM = kernel.mainEnv["x"] FIXME


    for c in  
    
    #for module in MM.modules:
    #    for helpFile in MM.listFiles(module, "doc/"+lang+"/"):
    #        if helpFile[-2:] == lang:
    #            helpDico[module+'/'+helpFile] = MM.readFile(module, helpFile)
        
    #    for tradFile in MM.listFiles(module, "lang/"):
    #        if tradFile[5:7] == lang:
    #            txt = MM.readFile(module, tradFile)
    #            words = re.split('\n', txt)
    #            imp = 3
    #            keywordsImp = {} 
    #            dico = {}
    #            for entry in words :
    #                if entry == '#':
    #                    keywordsImp[imp] = dico
    #                    dico={}
    #                    imp -= 1
    #                else:
    #                    if entry != '':
    #                        w = re.split('-', entry)
    #                        dico[w[0]] = w[1]
                
                    
    #            tradDico[tradFile[5:]] = keywordsImp
          

    loadHelpWelcomeScreen( context_num )
        
    
def searchWordForHelp( request ):
    
    lang = config["language"]+"_en"
    
    results1 = []
    results2 = []
    
    helpContent = kernel.find(kernel.current_context,"helpScreen")

    html = '<html><head></head><body><ul>'
    
    words = re.split(' ', request)

    for word in words:

        word = word.lower()

        word = trad(word, lang)

        for entry in helpDico :
            if word in entry.lower() :
                if not(entry in results2) and not(entry in results1):
                    results1.append(entry)
            if word in helpDico[entry].lower() and not( word in entry.lower() ):
                if not(entry in results2) and not(entry in results1):
                    results2.append(entry)
             
    results1 = checkImportanceOfResults(lang, words, results1)
             
    for result in results1 :
        html += '<li><a href="#'+result+'">'+result+'</a></li>'
        
    results2 = checkImportanceOfResults(lang, words, results2)     
        
    for result in results2 :
        html += '<li><a href="#'+result+'">'+result+'</a></li>'

    html += '</ul>'
    
    html += formalizeContent( results1 ) 
    
    html += formalizeContent( results2 )
    
    html += '</body></html>'

    helpContent.setHtml(html)

#    helpContent = kernel.find(kernel.current_context,"helpScreen")
#    helpContent . setHtml ( '<html><head></head><body>'+self.mainEnv["helpDico"][entry]+'</body></html>' )
    
def formalizeContent( results ):

    html = ''

    for result in results :
        html += '<h2 id="'+result+'">'+result+'</h2>'
        html += '<p>'
        helpDico[result] = helpDico[result].replace('\n', '<br/>')
        html += helpDico[result]+'</p>' 
        
    return html      

def trad( word, lang=None):

    if lang == None:
        return word
        
    for imp in tradDico[lang]:
        if word in tradDico[lang][imp]:
            return tradDico[lang][imp][word]            
        else:
            return word


def checkImportanceOfResults( lang, words, results ):
    
    newResults = []
    impOfResults = {}
    
    # Monster loops...
    
    # For all results found
    for result in results:
        impOfResults[result] = 0
        # For every word in the request
        for word in words:
            # For every word in the current result
            for r in re.split('/',result):
                # if word in result
                if word.lower() in r.lower():
                    # Check if word in lang list      
                    for imp in tradDico[lang]:
                        if word.lower() in tradDico[lang][imp].values():
                            impOfResults[result] += imp
    
    for key, value in sorted(impOfResults.iteritems(), key=lambda(k,v):(v,k)):
        newResults.append(key)
        
    newResults.reverse()
    
    return newResults
            

