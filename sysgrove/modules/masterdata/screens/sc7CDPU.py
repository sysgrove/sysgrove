
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name
 of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore
from sysgrove.utils import debug_trace
from sysgrove.core.screen import (
    Screen,
    DisplayMixin,
    CreationMixin,
    UpdateMixin
)
from sysgrove.modules.masterdata.models import UserProfile
from sysgrove.modules.masterdata.models.menu import Menu
from sysgrove.ui.classes import UIForm
from sysgrove.ui.classes.tree import TreeWidgetItem


class Screen7CDPU(Screen):

    def __init__(self, *p, **k):
        self.klass = UserProfile
        self.fields = UIForm.form2attr_list(self.klass.list_display)
        self.fields.append('rights')
        self.linkTo = [{
            'slave': ('rights', 'menu'),
            'masters': [('code', self.klass)],
        }]

        super(Screen7CDPU, self).__init__(*p, **k)
        self.setTree()

    def setTree(self):
        uiTree = getattr(self.ui_space, self.klass.get_ui_prefix() + 'rights')
        menus = Menu.query.filter_by(parentMenu_id=None).all()

        # FIXME should be recursive since only one table exist for this tree :
        # Node
        for menu in menus:
            submenus = Menu.query.filter_by(
                parentMenu_id=menu.id).order_by(Menu.code).all()
            uiMenu = TreeWidgetItem(uiTree, menu.description, menu.code)
            uiMenu.setCheckBox()
            for submenu in submenus:
                menuentries = Menu.query.filter_by(
                    parentMenu_id=submenu.id).order_by(Menu.code).all()
                uiSubMenu = TreeWidgetItem(uiMenu,
                                           submenu.description,
                                           submenu.code
                                           )
                uiSubMenu.setCheckBox()
                for menuentry in menuentries:
                    menuactions = Menu.query.filter_by(
                        parentMenu_id=menuentry.id).order_by(Menu.code).all()
                    uiMenuEntry = TreeWidgetItem(uiSubMenu,
                                                 menuentry.description,
                                                 menuentry.code
                                                 )
                    uiMenuEntry.setCheckBox()
                    for menuaction in menuactions:
                        uiMenuAction = TreeWidgetItem(uiMenuEntry,
                                                      menuaction.description,
                                                      menuaction.code
                                                      )
                        uiMenuAction.setCheckBox()


class Screen7CDPU01(Screen7CDPU, CreationMixin):
    pass


class Screen7CDPU02(Screen7CDPU, UpdateMixin):
    pass


class Screen7CDPU03(Screen7CDPU, DisplayMixin):
    pass
