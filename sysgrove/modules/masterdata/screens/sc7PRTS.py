
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.modules.masterdata.models import TypeStock
from sysgrove.core.screen import Screen04

from sysgrove.ui.classes.tree import TreeWidgetItem
from sysgrove.ui.classes import (
    UIDynamicButtonZone,
    UISpace,
    UIDataLookup,
    SDataLookup,
    UIForm, UITabWidget,
    ExceptionMessagePopup
)


class Screen7PRTS05(Screen04):

    def __init__(self, module='sysgrove.modules',
                 newValue=None, klass=None, code=''):
        self.klass = TypeStock
        super(Screen7PRTS05, self).__init__(
            'docTypes', 'docType',
            module, newValue, klass, code)
        self.attr_name = self.klass.get_ui_prefix()[:-1] + 's'
        self.docType_fields = [
            'description', '',
            'docGroup', 'docGroup_description',
            self.attr_name
        ]

    def setTree(self):
        from sysgrove.modules.masterdata.models import DocGroup
        docgroups = DocGroup.query.all()
        uiTree = getattr(
            self.ui_space, self.klass.get_ui_prefix() + "docTypes")
        grp, cpn = [], []
        for docgroup in docgroups:
            for docT in docgroup.docTypes:
                if not docgroup in grp:
                    uiDocGroup = TreeWidgetItem(
                        uiTree, docgroup.description, docgroup.code)
                    uiDocGroup.setCheckBox()
                    grp.append(docgroup)

                if not docT in cpn:
                    uiDocType = TreeWidgetItem(
                        uiDocGroup,
                        docT.code + " - " + docT.description,
                        docT.code
                    )
                    uiDocType.setCheckBox()
                    cpn.append(docT)

    def setLinks(self):
        from sysgrove.modules.masterdata.models import DocGroup, DocType

        uidocGroup = getattr(self.ui_space, 'docType_docGroup', None)
        uidocType = getattr(self.ui_space, 'docType', None)

        uidocGroup.force_fields(['docType_docGroup_description'])
        uidocGroup.setLink(klass=DocGroup,
                           prefix=DocType.get_ui_prefix())

        uidocType.force_fields(['docType_description', 'docType_docGroup'])
        comp_link = {
            'slave': ('docType', 'docType'),
            'masters': [('docGroup', 'docGroup')],
        }
        uidocType.setLink(klass=DocType,
                          prefix=DocType.get_ui_prefix(),
                          **comp_link)

        super(Screen7PRTS05, self).setLinks()

    def save(self):
        codeUI = getattr(self.ui_space, "%scode" % self.ui_prefix, None)
        codeTabWidget = codeUI.parent.parent.parent
        assert(isinstance(codeTabWidget, UITabWidget))
        # We are in normal case, use the normal method
        if codeTabWidget.isVisible():
            if codeUI:
                if codeUI.value():
                    super(Screen7PRTS05, self).save()
                else:
                    codeUI.setInvalidStyle()
        else:
            from sysgrove.modules.masterdata.models import DocGroup, DocType
            uidocGroup = getattr(self.ui_space, 'docType_docGroup', None)
            uidocType = getattr(self.ui_space, 'docType', None)
            uiallocated = getattr(
                self.ui_space, 'docType_' + self.attr_name, None)
            allocated = uiallocated.valueObject()
            if uidocType.value():
                docType = DocType.query.filter_by(code=uidocType.value()).one()
                setattr(docType, self.attr_name, allocated)
                docType.save()
                self.reset_fields(
                    self.ui_space, self.docType_fields,
                    DocType.get_ui_prefix())

            elif uidocGroup.value():
                docGroup = DocGroup.query.filter_by(
                    code=uidocGroup.value()).one()
                for docType in docGroup.docTypes:
                    setattr(docType, self.attr_name, allocated)
                    docType.save()
                    self.reset_fields(
                        self.ui_space, self.docType_fields,
                        DocType.get_ui_prefix())
            else:
                uidocGroup.setInvalidStyle()
                uidocType.setInvalidStyle()
