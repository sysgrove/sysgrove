
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.main import db_session
from sysgrove.utils import debug_trace
from sysgrove.modules.masterdata.models import ExchangeRate
from sysgrove.core.screen import (
    Screen,
    DisplayMixin,
    CreationMixin,
    UpdateMixin,
    save_or_error_popup
)


class Screen7FIER(Screen):

    def __init__(self, *p, **k):
        self.klass = ExchangeRate  # Principal Alchemy class

        # linked objects
        super(Screen7FIER, self).__init__(*p, **k)
        company_currency_ui = getattr(
            self.ui_space, self.ui_prefix + 'company_currency')
        company_currency_ui.setReadOnly(True)

    # Only used for the default table
    def show_index(self, index):
        Screen.uiTable2ui(self.ui_space,
                          self.default_table['ui'],
                          index,
                          self.ui_prefix,
                          self.klass,
                          ['company', 'exchangeRateDate'])

    def save(self):
        table = getattr(self.ui_space, self.ui_prefix +
                        self.klass.dim['cell'], None)
        for val in table.objectValues():
            if isinstance(val, dict):
                cur = self.klass()
                for k, attr in self.klass.dim.iteritems():
                    setattr(cur, attr, val[k])
                save_or_error_popup(cur, self, self.fields)

        self.showTables()
        self.reset_ui_fields(self.fields)

    def delete(self):

        uicomp = getattr(self.ui_space, self.ui_prefix + 'company')
        uidate = getattr(self.ui_space, self.ui_prefix + 'exchangeRateDate')

        company = uicomp.valueObject()
        date = uidate.value()

        values = self.klass.query.filter_by(
            exchangeRateDate=date.toPyDate(),
            company_id=company.id).all()
        for val in values:
            val.delete()
        self.showTables()
        self.reset_ui_fields(self.fields)


# CREATION
class Screen7FIER01(Screen7FIER, CreationMixin):
    pass


# MODIFICATION
class Screen7FIER02(Screen7FIER, UpdateMixin):
    pass


# DISPLAY
class Screen7FIER03(Screen7FIER, DisplayMixin):
    pass
