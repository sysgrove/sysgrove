
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore
from sysgrove.core.screen import (
    Screen,
    DisplayMixin,
    CreationMixin,
    UpdateMixin
)
from sysgrove.modules.masterdata.models import ItemFamily


class Screen7CDIF(Screen):

    def __init__(self, *p, **k):
        self.klass = ItemFamily
        super(Screen7CDIF, self).__init__(*p, **k)

        # add readonly properties for flags
        # if serviceFlag is ticked it will disable the current 3
        # (product to be kept on stock, product liable to analyses,
        # product liable to production), and vice versa,
        # if one of the 3 is selected then this service item flag is disabled.
        self.serviceUI = getattr(self.ui_space,
                                 self.ui_prefix + 'serviceFlag',
                                 None)
        self.prodUI = getattr(self.ui_space,
                              self.ui_prefix + 'productionFlag',
                              None)
        self.analUI = getattr(self.ui_space,
                              self.ui_prefix + 'analysisFlag',
                              None)
        self.stockUI = getattr(self.ui_space,
                               self.ui_prefix + 'stockFlag',
                               None)
        self.serviceUI.stateChanged.connect(self.serviceChange)
        self.prodUI.stateChanged.connect(self.productChange)
        self.analUI.stateChanged.connect(self.productChange)
        self.stockUI.stateChanged.connect(self.productChange)

    def serviceChange(self, state):
        if state == QtCore.Qt.Unchecked:
            self.prodUI.setReadOnly(False)
            self.analUI.setReadOnly(False)
            self.stockUI.setReadOnly(False)
        else:
            self.prodUI.setReadOnly(True)
            self.analUI.setReadOnly(True)
            self.stockUI.setReadOnly(True)

    def productChange(self, state):
        if state == QtCore.Qt.Unchecked:
            if self.prodUI.checkState() == self.analUI.checkState() and \
                    self.prodUI.checkState() == self.stockUI.checkState():
                self.serviceUI.setReadOnly(False)
        else:
            self.serviceUI.setReadOnly(True)


class Screen7CDIF01(Screen7CDIF, CreationMixin):
    pass


class Screen7CDIF02(Screen7CDIF, UpdateMixin):
    pass


class Screen7CDIF03(Screen7CDIF, DisplayMixin):
    pass
