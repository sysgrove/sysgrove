#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import datetime
from sysgrove.settings import EVER
from sysgrove.ui.classes import UIForm
from sysgrove.core.screen import (
    Screen,
    DisplayMixin,
    CreationMixin,
    UpdateMixin
)
from sysgrove.modules.masterdata.models import ThirdParty


class Screen7LOTP(Screen):

    def __init__(self, *p, **k):
        self.klass = ThirdParty
        self.defaults = [
            ('', EVER.strftime("%d/%m/%Y"), 'validTillDate'),
        ]
        self.linkTo = [{
            'slave': ('parentThirdParty', 'thirdParty'),
            'masters': [('thirdPartyTypes', 'thirdPartyType')],
        }]
        super(Screen7LOTP, self).__init__(*p, **k)


class Screen7LOTP01(Screen7LOTP, CreationMixin):

    def __init__(self, *p, **k):
        super(Screen7LOTP01, self).__init__(*p, **k)
        # has setDefault is called by __init__... we have to recall it
        self.defaults.append(('', ThirdParty.next_code, 'code'))
        self.setDefaults(self.ui_space, self.defaults, self.ui_prefix)

    def reset_ui_fields(self, fields):
        super(Screen7LOTP01, self).reset_ui_fields(fields)

        Screen.reset_fields(self.ui_space, fields, self.ui_prefix)
        for table in self.tables:
            if not table is self.default_table:
                table['ui'].clear()
        self.setDefaults(self.ui_space, self.defaults, self.ui_prefix)


class Screen7LOTP02(Screen7LOTP, UpdateMixin):
    pass


class Screen7LOTP03(Screen7LOTP, DisplayMixin):
    pass
