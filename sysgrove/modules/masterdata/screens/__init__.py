#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sc7CDSI import Screen7CDSI01, Screen7CDSI02, Screen7CDSI03
from sc7CDDN import Screen7CDDN01, Screen7CDDN02, Screen7CDDN03
from sc7CDPU import Screen7CDPU01, Screen7CDPU02, Screen7CDPU03
from sc7CDCR import Screen7CDCR01, Screen7CDCR02, Screen7CDCR03
from sc7CDIF import Screen7CDIF01, Screen7CDIF02, Screen7CDIF03
from sc7LOTP import Screen7LOTP01, Screen7LOTP02, Screen7LOTP03
from sc7PRIT import Screen7PRIT01, Screen7PRIT02, Screen7PRIT03
from sc7FICA import Screen7FICA01, Screen7FICA02, Screen7FICA03
from sc7FIER import Screen7FIER01, Screen7FIER02, Screen7FIER03
from sc7PRTS import Screen7PRTS05
from sc7CDST import Screen7CDST06
