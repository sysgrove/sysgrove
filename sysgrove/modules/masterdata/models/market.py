#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import datetime
from sqlalchemy import (
    Column, Integer, ForeignKey,
    Float, ColumnDefault
)
from sqlalchemy.orm import relationship
from sysgrove.settings import SIMPLE_DATALOOKUP
from sysgrove.models import Base
from sysgrove.models import SGMixin, CDMixin
from .currency import Currency
from .item import ItemGroup

from .uom import UnitOfMeasure
from sysgrove import i18n
_ = i18n.language.ugettext

_mcb = 'sysgrove.ui.classes.combobox.MonthComboBox'


class MarketType(SGMixin, Base):
    verbose_name = _('Market')

    # FIXME : this is not the right way to set this relationship
    # itemGroups = relationship(
    #     "ItemGroup",
    #     secondary='MarketCommodity')


class MarketCommodity(CDMixin, Base):
    verbose_name = _('Market Commodity Definition')

    marketType_id = Column("MarketType",
                           Integer,
                           ForeignKey('MarketType.MarketType_pk'),
                           primary_key=True)
    marketType = relationship(
        "MarketType",
        info={'verbose_name': MarketType.verbose_name})

    itemGroup_id = Column("ItemGroup_pk",
                          Integer,
                          ForeignKey('ItemGroup.ItemGroup_pk'),
                          primary_key=True)
    itemGroup = relationship(
        "ItemGroup",
        info={'verbose_name': ItemGroup.verbose_name})

    tradedQuantity = Column("TradedQuantity",
                            Float,
                            info={'verbose_name': _('Contract Quantity')})

    unitOfMeasure_id = Column(
        "UnitOfMeasure_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    unitOfMeasure = relationship(
        "UnitOfMeasure",
        info={'verbose_name': UnitOfMeasure.verbose_name}
    )

    # Currency FK
    currency_id = Column(
        "Currency_pk",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    currency = relationship(
        "Currency",
        info={'verbose_name': Currency.verbose_name}
    )

    changingDay = Column(
        'ChangingDay', Integer,
        ColumnDefault(15),
        nullable=False,
        info={'verbose_name': _('Day where the month change')})

    month1 = Column("Month1",
                    Integer,
                    info={'verbose_name': _('January')})

    month2 = Column("Month2",
                    Integer,
                    info={'verbose_name': _('February')})

    month3 = Column("Month3",
                    Integer,
                    info={'verbose_name': _('March')})

    month4 = Column("Month4",
                    Integer,
                    info={'verbose_name': _('April')})

    month5 = Column("Month5",
                    Integer,
                    info={'verbose_name': _('May')})

    month6 = Column("Month6",
                    Integer,
                    info={'verbose_name': _('June')})

    month7 = Column("Month7",
                    Integer,
                    info={'verbose_name': _('July')})

    month8 = Column("Month8",
                    Integer,
                    info={'verbose_name': _('August')})

    month9 = Column("Month9",
                    Integer,
                    info={'verbose_name': _('September')})

    month10 = Column("Month10",
                     Integer,
                     info={'verbose_name': _('October')})

    month11 = Column("Month11",
                     Integer,
                     info={'verbose_name': _('November')})

    month12 = Column("Month12",
                     Integer,
                     info={'verbose_name': _('December')})

    list_base = [
        'marketType',
        'itemGroup',
        ['tradedQuantity', (SIMPLE_DATALOOKUP, 'unitOfMeasure')],
        'currency',
        'changingDay',
        [('Vlist', [(_mcb, 'month1'), (_mcb, 'month3'),
                    (_mcb, 'month5'), (_mcb, 'month7'),
                    (_mcb, 'month9'), (_mcb, 'month11')]),
         ('Vlist', [(_mcb, 'month2'), (_mcb, 'month4'),
                    (_mcb, 'month6'), (_mcb, 'month8'),
                    (_mcb, 'month10'), (_mcb, 'month12')]),
         ]
    ]

    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    def months(self):
        res = set([])
        for m in range(1, 13):
            res.add(getattr(self, 'month%s' % m))
        return list(res)
    # forbiden months :
    # [m for m in range(1, 13) if not m in months]

    # select a month according to a date and a spread month
    # the spread month is also a date (we should not forgetten the year)
    def select_month(self, date, spread_month=None):
        if spread_month:
            return spread_month

        month = date.month
        year = date.year
        market_month = getattr(self, 'month%s' % month)
        if month == market_month and date.day >= self.changingDay:
            month = month + 1
            if month == 13:
                month = 1
                year = year + 1
        return datetime.date(year, getattr(self, 'month%s' % month), 1)

    # It is the next market month after the the spread_month
    # If spread_month does not exist, then return the market month
    # after the market month selected from the date
    def select_spread_month(self, date, spread=None):
        if not spread:
            spread = self.select_month(date)

        month = spread.month + 1
        year = spread.year
        if month == 13:
            month = 1
            year = year + 1
        return datetime.date(year, getattr(self, 'month%s' % month), 1)
