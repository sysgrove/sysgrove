#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from sysgrove.utils import prefix
from sysgrove.models import Base
from sysgrove.models import SGMixin
from sysgrove.modules.masterdata.mixin import AddrMixin
from .tax import TaxType
from .company import Company
from .address import Address
from sysgrove import i18n
_ = i18n.language.ugettext


class Site(SGMixin, AddrMixin, Base):
    verbose_name = _('Site')
    list_base = ['company_group', 'company', 'code', 'description', 'taxType']
    list_base.extend(prefix(Address))

    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    list_columns = [
        'company_group_code',
        'company_group_description',
        'company_code',
        'company_description',
        'code',
        'description',
        'taxType_code',
        'taxType_description'
    ]

    company_id = Column(
        "Company_pk",
        Integer,
        ForeignKey('Company.Company_pk')
    )
    company = relationship(
        "Company",
        info={'verbose_name': Company.verbose_name}
    )

    taxType_id = Column(
        "TaxType_pk",
        Integer,
        ForeignKey('TaxType.TaxType_pk')
    )
    taxType = relationship(
        "TaxType", info={'verbose_name': TaxType.verbose_name})

    # Allocation tables.
    itemDetails = relationship(
        "ItemDetail",
        secondary="ItemDetail_Site_Allocation"
    )

    itemGroups = relationship(
        "ItemGroup",
        secondary="ItemGroup_Site_Allocation"
    )

    itemFamilys = relationship(
        "ItemFamily",
        secondary="ItemFamily_Site_Allocation"
    )

    docTypes = relationship("DocType", secondary="DocType_Site_Allocation")
    docGroups = relationship("DocGroup", secondary="DocGroup_Site_Allocation")

    # Well naming convention is not allways very smart !!!
    thirdPartys = relationship(
        "ThirdParty",
        secondary="ThirdParty_Site_Allocation")

    userProfiles = relationship(
        "UserProfile",
        secondary="UserProfile_Site_Allocation")

    flowIDs = relationship(
        "FlowID",
        secondary="FlowID_Site_Allocation")

    docNumberings = relationship(
        "DocNumbering",
        secondary="DocNumbering_Site_Allocation")

    warehouses = relationship(
        "Warehouse",
        secondary="Warehouse_Site_Allocation")

    typeStocks = relationship(
        "TypeStock",
        secondary="TypeStock_Site_Allocation")

    incoterms = relationship(
        "Incoterm",
        secondary="Incoterm_Site_Allocation")

    salesReps = relationship(
        "SalesRep",
        secondary="SalesRep_Site_Allocation")

    priceSchemaHeaders = relationship(
        "PriceSchemaHeader",
        secondary="PriceSchemaHeader_Site_Allocation")

    paymentTerms = relationship(
        "PaymentTerm",
        secondary="PaymentTerm_Site_Allocation")

    banks = relationship(
        "Bank",
        secondary="Bank_Site_Allocation")

    analysiss = relationship(
        "Analysis",
        secondary="Analysis_Site_Allocation")
