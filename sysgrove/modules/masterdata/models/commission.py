#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship
from sysgrove.settings import TP_DATAKOOKUP
from sysgrove.models import Base
from sysgrove.models import InitMixin
from .price import PriceCondition
from sysgrove import i18n
_ = i18n.language.ugettext


class CommissionDefinition(InitMixin, Base):
    verbose_name = _('Commission Definition')
    list_base = ['thirdPartyCodeAgent', 'thirdPartyCodeClient']
    list_columns = list_base
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    priceCondition_id = Column(
        "PriceCondition_pk",
        Integer,
        ForeignKey('PriceCondition.PriceCondition_pk')
    )
    priceCondition = relationship(
        "PriceCondition",
        info={'verbose_name': PriceCondition.verbose_name})

    thirdPartyCodeAgent_id = Column(
        "ThirdPartyCodeAgent_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )
    thirdPartyCodeAgent = relationship(
        "ThirdParty",
        primaryjoin=
        "CommissionDefinition.thirdPartyCodeAgent_id==ThirdParty.id",
        info={'verbose_name': _('Agent'),
              'datalookup': TP_DATAKOOKUP,
              }
    )

    thirdPartyCodeClient_id = Column(
        "ThirdPartyCodeClient_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )
    thirdPartyCodeClient = relationship(
        "ThirdParty",
        primaryjoin=
        "CommissionDefinition.thirdPartyCodeClient_id==ThirdParty.id",
        info={'verbose_name': _('Counter Party'),
              'datalookup': TP_DATAKOOKUP,
              }

    )

    itemDetail_id = Column("ItemDetail_pk", Integer, ForeignKey(
        'ItemDetail.ItemDetail_pk'))
    itemDetail = relationship("ItemDetail")

    fromSign = Column("FromSign", String())
    fromQuantity = Column("FromQuantity", String())
    toSign = Column("ToSign", String())
    ToQuantity = Column("ToQuantity", String())
    CommissionValue = Column("CommissionValue", String())

    # UnitOfMeasure FK
    unitOfMeasure_id = Column("UnitOfMeasure_pk", Integer, ForeignKey(
        'UnitOfMeasure.UnitOfMeasure_pk'))
    unitOfMeasure = relationship("UnitOfMeasure")

    currency_id = Column("Currency_pk", Integer, ForeignKey(
        'Currency.Currency_pk'))
    currency = relationship("Currency")
