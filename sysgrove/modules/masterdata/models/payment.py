#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sqlalchemy import Column, Text, Boolean, Integer, ForeignKey
from sqlalchemy.orm import relationship
from sysgrove.models import Base
from sysgrove.models import SGMixin
from .date import DateType
from sysgrove import i18n
_ = i18n.language.ugettext


class PaymentTerm(SGMixin, Base):
    verbose_name = _('Payment Term')
    list_base = [
        'code', 'description', 'numberDays', 'dateType',
        (('ClickShowS', 'selectDayCheckBox',
          u'Payment Term on a specific day'),
         ['selectDayValue'])
    ]
    list_columns = [
        'code', 'description', 'numberDays', 'dateType_code',
        'dateType_description', 'selectDayValue_description'
    ]

    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    numberDays = Column(
        "NumberDays",
        Integer, info={'verbose_name': _('Number of Days')}
    )
    dateType_id = Column(
        "DateType_pk",
        Integer,
        ForeignKey('DateType.DateType_pk')
    )
    dateType = relationship(
        "DateType",
        info={'verbose_name': DateType.verbose_name}
    )
    # FIXME should be replace by a Choices Field (same as in Django)
    selectDayValue_id = Column(
        "SelectDayValue",
        Integer,
        ForeignKey('WeekDays.WeekDays_pk')
    )
    selectDayValue = relationship(
        "WeekDays",
        info={'verbose_name': _('Select day')}
    )
    selectDayCheckBox = Column(
        "SelectDayCheckBox",
        Boolean,
        info={'verbose_name': _('Payment Term on a specific day')}
    )
    sites = relationship("Site",
                         secondary="PaymentTerm_Site_Allocation",
                         info={'verbose_name': _("Sites")})


class PaymentTermText(SGMixin, Base):
    verbose_name = _('Payment Term Text')
    list_base = ['code', 'description', 'paymentTerm', 'text']
    list_columns = ['code', 'description', 'paymentTerm_code']
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    paymentTerm_id = Column(
        "PaymentTerm_pk",
        Integer,
        ForeignKey('PaymentTerm.PaymentTerm_pk'))
    paymentTerm = relationship(
        "PaymentTerm", info={'verbose_name': PaymentTerm.verbose_name})
    text = Column("Text", Text, info={'verbose_name': _('Text')})


class PaymentType(SGMixin, Base):
    verbose_name = _('Payment Type')
