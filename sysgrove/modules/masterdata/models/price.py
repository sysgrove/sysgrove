#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.utils import memoize
from sqlalchemy import (
    Column,
    Unicode,
    Integer,
    ForeignKey,
    Boolean,
    Enum, ColumnDefault,
)
from sqlalchemy.orm import relationship
from sysgrove.models import Base
from sysgrove.models import SGMixin, InitMixin
from sysgrove import i18n
_ = i18n.language.ugettext


class PriceCondition(SGMixin, Base):

    verbose_name = _('Price Condition')
    list_base = []
    list_display = [[(('GroupBox', 0), [
        'code', 'description', 'serviceFlag',
        'typeCondition',
        'calculationType',
        'futureType'
    ])]]

    typeCondition = Column(
        "TypeCondition",
        Enum("Price", "VAT", "Commission", name='type_condition'),
        info={'verbose_name': _('This Condition Type is :')}
    )

    serviceFlag = Column(
        "ServiceFlag",
        Boolean,
        info={'verbose_name': _('Service')}
    )

    calculationType = Column(
        "CalculationType",
        Enum("Absolute", "Percentage", "Sum", name='calculation_type'),
        info={'verbose_name': _('Calculation Type is :')}
    )

    futureType = Column(
        'FutureType',
        Enum("Flat", "Premium", "Forex", "Spread", "Future",
             name='future_type'),
        ColumnDefault("Flat"),
        info={'verbose_name': _("Future Price Condition")}
    )

    def is_flat(self):
        return self.futureType == 'Flat'

    @staticmethod
    @memoize
    def vat_condition():
        try:
            return PriceCondition.query.filter_by(typeCondition="VAT").first()
        except:
            return None

    @staticmethod
    @memoize
    def future_condition():
        try:
            return PriceCondition.query.filter_by(futureType="Future").first()
        except:
            return None

    @staticmethod
    @memoize
    def prm_condition():
        try:
            return PriceCondition.query.filter_by(futureType="Premium").first()
        except:
            return None

    @staticmethod
    @memoize
    def spread_condition():
        try:
            return PriceCondition.query.filter_by(futureType="Spread").first()
        except:
            return None

    @staticmethod
    @memoize
    def forex_condition():
        try:
            return PriceCondition.query.filter_by(futureType="Forex").first()
        except:
            return None


class PriceSchemaHeader(SGMixin, Base):
    verbose_name = _('Price Schema')

    list_display = [[(('GroupBox', 0), [
        'code', 'description',
        (('Inline', 'priceSchemaDetail'), 'details')])]]

    list_columns = ['code', 'description']

    details = relationship("PriceSchemaDetail",
                           order_by="PriceSchemaDetail.step",
                           info={'verbose_name': _('Details')})

    sites = relationship("Site",
                         secondary="PriceSchemaHeader_Site_Allocation",
                         info={'verbose_name': _('Sites')})


class PriceSchemaDetail(InitMixin, Base):
    verbose_name = _('Price Schema Detail')
    list_display = [
        'step',
        'priceCondition', 'priceCondition_description',
        'conditionValue', 'action',
    ]

    # step = Column("Step", Unicode(), info={'verbose_name': u'Step'})
    step = Column("Step", Integer, info={'verbose_name': _('Step')})
    action = Column("Action", Unicode(), info={'verbose_name': _('Action')})
    conditionValue = Column("ConditionValue", Unicode(),
                            info={'verbose_name': _(' Condition Value')})

    priceSchemaHeader_id = Column(
        "PriceSchemaHeader_pk",
        Integer,
        ForeignKey('PriceSchemaHeader.PriceSchemaHeader_pk')
    )
    priceSchemaHeader = relationship("PriceSchemaHeader")

    priceCondition_id = Column(
        "PriceCondition_pk",
        Integer,
        ForeignKey('PriceCondition.PriceCondition_pk')
    )
    priceCondition = relationship(
        "PriceCondition",
        info={'verbose_name': PriceCondition.verbose_name})
