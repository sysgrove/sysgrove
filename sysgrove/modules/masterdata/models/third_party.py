#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sqlalchemy import (
    Column, Integer, ForeignKey,
    Float, Text, ColumnDefault
)
from sqlalchemy import Boolean, Date, Unicode
from sqlalchemy.orm import relationship, backref
from sysgrove.settings import EVER, TP_DATAKOOKUP
from sysgrove.utils import prefix, get_class, debug_trace
from sysgrove.utils import classproperty
from sysgrove.models import Base
from sysgrove.models import SGMixin, InitMixin
from sysgrove.modules.masterdata.mixin import AddrMixin
from .address import Address
from .incoterm import Incoterm
from .tax import TaxType
from .payment import PaymentTerm
from .credit import CreditLimit
from .currency import Currency
from .uom import UnitOfMeasure
from .item import ItemDetail
from flowId import FlowID
from sysgrove import i18n
_ = i18n.language.ugettext

tplink_datalookup = 'sysgrove.modules.masterdata.ui.customs.UIDataLookupTPLink'

# find the prem no used index


def find_prem_valid_int(l):
    l = sorted(l, int.__cmp__)
    nb = len(l)
    if nb == 0:
        return 1
    else:
        # skip consecutive numbers
        i = 0
        while l[i] + 1 == l[i + 1] and i < nb - 2:
            i = i + 1
        if i == nb - 2:
            # the two element have to be compare to
            if l[i] + 1 == l[i + 1]:
                return l[i + 1] + 1
            else:
                return l[i] + 1
        else:
            return l[i] + 1


class ThirdPartyType(SGMixin, Base):
    verbose_name = _('Third Party Type')
    sites = relationship(
        "Site",
        secondary="ThirdPartyType_Site_Allocation",
        info={'verbose_name': _("Sites")})

    thirdPartys = relationship(
        "ThirdParty",
        secondary="ThirdPartyType_ThirdParty_Allocation",
        info={'verbose_name': _('Third Parties')}
    )


class ThirdParty(SGMixin, AddrMixin, Base):
    verbose_name = _('Third Party')
    datalookup = TP_DATAKOOKUP

    list_base = [
        'code', 'description',
        ('sysgrove.modules.masterdata.ui.customs.UITreeTPT',
         'thirdPartyTypes'),
        'parentThirdParty'
    ]

    addr_list = prefix(Address)
    addr_list.append(['validFromDate', 'validTillDate'])

    logistic_list = [
        'intragroup', 'serviceProviderFlag', 'incoterm']
    contact_list = [(('Inline', 'contactItem'), 'contact_items')]

    finance_list = [
        ['registrationNumber', 'vATRegNumber'],
        'taxType',
        'paymentTerm',
        'creditLimit',
        ['creditLimitAmount', 'creditLimitUseValue'],
        ['creditLimitUseCurrency', 'currency'],
        ['blockFlag', 'blockReasonText']

    ]
    list_columns = ['code', 'description', 'parentThirdParty', 'taxType']
    list_tab = [('TabPanel', [
        (u"Existing Entries", [('DefaultTable', 'table')]),
        (u'Address', addr_list),
        (u'Contacts', contact_list),
        (u'Logistic', logistic_list),
        (u'Finance', finance_list)
    ])]

    # Take care it is mandatory to disting GroupBoxes
    # because of UIObject access...If it is a number it is not uset in the
    # GroupBox title
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', 1), list_tab)]
    ]

    validFromDate = Column(
        "ValidFromDate",
        Date,
        nullable=False,
        info={'verbose_name': _('Valid from')}
    )
    validTillDate = Column(
        "ValidTillDate",
        Date,
        ColumnDefault(EVER),
        nullable=False,
        info={'verbose_name': _('Till')}
    )

    intragroup = Column(
        "Intragroup",
        Boolean,
        info={'verbose_name': _('Counter Party is an intra-group party')}
    )

    incoterm_id = Column(
        "Incoterm_pk",
        Integer,
        ForeignKey('Incoterm.Incoterm_pk'),
    )
    incoterm = relationship(
        "Incoterm",
        info={'verbose_name': Incoterm.verbose_name}
    )

    vATRegNumber = Column(
        "VATRegNumber",
        Unicode(),
        info={'verbose_name': _('VAT Registration Number')}
    )
    registrationNumber = Column(
        "RegistrationNumber",
        Unicode(),
        info={'verbose_name': _('Local Registration Number')}
    )

    # TaxType FK
    taxType_id = Column(
        "TaxType_pk",
        Integer,
        ForeignKey('TaxType.TaxType_pk'),
        nullable=False,
    )
    taxType = relationship(
        "TaxType", info={'verbose_name': TaxType.verbose_name})

    # PaymentTerm FK
    paymentTerm_id = Column(
        "PaymentTerm_pk",
        Integer,
        ForeignKey('PaymentTerm.PaymentTerm_pk')
    )
    paymentTerm = relationship(
        "PaymentTerm",
        info={'verbose_name': PaymentTerm.verbose_name})

    # Currency FK
    currency_id = Column(
        "Currency_pk",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    currency = relationship(
        "Currency",
        primaryjoin="ThirdParty.currency_id == Currency.id",
        info={'verbose_name': Currency.verbose_name}
    )

    # CreditLimit
    creditLimit_id = Column(
        "CreditLimit_pk",
        Integer,
        ForeignKey('CreditLimit.CreditLimit_pk')
    )
    creditLimit = relationship(
        "CreditLimit",
        info={'verbose_name': CreditLimit.verbose_name}
    )

    creditLimitAmount = Column(
        "CreditLimitAmount",
        Float,
        info={'verbose_name': _('Credit Limit Amount')}
    )

    blockFlag = Column(
        "BlockFlag",
        Boolean,
        info={'verbose_name': _('Block Counter Party')}
    )
    blockReasonText = Column("BlockReasonText", Text)
    creditLimitUseValue = Column(
        "CreditLimitUseValue",
        Float,
        info={'verbose_name': _('Credit limit amount in use :')}
    )

    creditLimitUseCurrency_id = Column(
        "CreditLimitUseCurrency",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    creditLimitUseCurrency = relationship(
        "Currency",
        primaryjoin="ThirdParty.creditLimitUseCurrency_id == Currency.id",
        info={'verbose_name': _('Credit Limit Currency')}
    )

    contact_id = Column(
        "Contact_pk",
        Integer,
        ForeignKey('Contact.Contact_pk')
    )
    contact = relationship("Contact")

    serviceProviderFlag = Column(
        "ServiceProviderFlag",
        Boolean,
        info={'verbose_name': _('Counter Party is a service provider')}
    )

    # FK to the parent ThirdParty
    parentThirdParty_id = Column(
        "ParentThirdParty_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )

    childrenThirdParty = relationship(
        'ThirdParty',
        backref=backref("parentThirdParty",
                        remote_side='ThirdParty.id',
                        uselist=False,
                        single_parent=True,
                        info={'verbose_name': u'Parent ThirdParty',
                              'datalookup': tplink_datalookup}),
        info={'verbose_name': _('Children ThirdParty')},
    )

    # backward relation from allocation tables
    sites = relationship(
        "Site",
        secondary="ThirdParty_Site_Allocation",
        info={'verbose_name': _("Sites")})

    thirdPartyTypes = relationship(
        "ThirdPartyType",
        secondary="ThirdPartyType_ThirdParty_Allocation",
        info={'verbose_name': ThirdPartyType.verbose_name})

    # def pre_save(self):
    #     if self.thirdPartyTypes == []:
    #         raise ValueError('thirdPartyType: cannot be empty')

    @classmethod
    def next_code(cls):
        l = cls.query.all()
        l = map(lambda x: int(x.code), l)
        return str(find_prem_valid_int(l))

    # For thidParty code has to be a number
    @classmethod
    def validate_code(cls, target, value, oldvalue, initiator):
        try:
            int(value)
        except ValueError:
            raise ValueError(u'code: ThidParty code has to be a number')
        return super(ThirdParty, cls).validate_code(
            target, value, oldvalue, initiator)

    def copy(self, origin):
        fields = self._fields().keys()
        fields.remove('thirdPartyType')
        for f in fields:
            setattr(self, f, getattr(origin, f))


class ThirdPartyItemList(InitMixin, Base):
    verbose_name = _('ThirdParty Item List')
    itemDetail_pk_id = Column(
        "ItemDetail_pk",
        Integer,
        ForeignKey('ItemDetail.ItemDetail_pk')
    )
    itemDetail_pk = relationship(
        "ItemDetail",
        info={'verbose_name': ItemDetail.verbose_name}
    )

    flowID_id = Column(
        "FlowID_pk",
        Integer,
        ForeignKey('FlowID.FlowID_pk')
    )
    flowID = relationship(
        "FlowID",
        info={'verbose_name': FlowID.verbose_name}
    )

    thirdPartyType_id = Column(
        "ThirdPartyType_pk",
        Integer,
        ForeignKey('ThirdPartyType.ThirdPartyType_pk')
    )
    thirdPartyType = relationship(
        "ThirdPartyType",
        info={'verbose_name': ThirdPartyType.verbose_name}
    )

    thirdParty_id = Column(
        "ThirdParty_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )
    thirdParty = relationship(
        "ThirdParty",
        info={'verbose_name': ThirdParty.verbose_name,
              'datalookup': TP_DATAKOOKUP,
              }
    )


class ThirdPartyPriceList(InitMixin, Base):

    itemDetail_pk_id = Column(
        "ItemDetail_pk",
        Integer,
        ForeignKey('ItemDetail.ItemDetail_pk')
    )
    itemDetail_pk = relationship(
        "ItemDetail",
        info={'verbose_name': ItemDetail.verbose_name}
    )

    thirdPartyType_id = Column(
        "ThirdPartyType_pk",
        Integer,
        ForeignKey('ThirdPartyType.ThirdPartyType_pk')
    )
    thirdPartyType = relationship(
        "ThirdPartyType",
        info={'verbose_name': ThirdPartyType.verbose_name}
    )

    thirdParty_id = Column(
        "ThirdParty_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )
    thirdParty = relationship(
        "ThirdParty",
        info={'verbose_name': ThirdParty.verbose_name,
              'datalookup': TP_DATAKOOKUP,
              }
    )

    flowID_id = Column(
        "FlowID_pk",
        Integer,
        ForeignKey('FlowID.FlowID_pk')
    )
    flowID = relationship(
        "FlowID",
        info={'verbose_name': FlowID.verbose_name}
    )

    price = Column("Price", Float)

    currency_id = Column(
        "Currency_pk",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    currency = relationship(
        "Currency",
        info={'verbose_name': Currency.verbose_name}
    )

    # UnitOfMeasure FK
    unitOfMeasure_id = Column(
        "UnitOfMeasure_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    unitOfMeasure = relationship(
        "UnitOfMeasure",
        info={'verbose_name': UnitOfMeasure.verbose_name}
    )

    differential = Column("Differential", Boolean)
