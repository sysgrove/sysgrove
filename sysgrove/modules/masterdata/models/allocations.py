#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    Table,
    Boolean,
    ColumnDefault
)
from sysgrove.models import Base


ItemDetail_Site_Allocation = Table(
    'ItemDetail_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("ItemDetail_pk", Integer,
           ForeignKey('ItemDetail.ItemDetail_pk'),
           primary_key=True),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True)
)

ItemGroup_Site_Allocation = Table(
    'ItemGroup_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("ItemGroup_pk", Integer,
           ForeignKey('ItemGroup.ItemGroup_pk'),
           primary_key=True)
)


ItemFamily_Site_Allocation = Table(
    'ItemFamily_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("ItemFamily_pk", Integer,
           ForeignKey('ItemFamily.ItemFamily_pk'),
           primary_key=True)
)

Analysis_ItemDetail_Allocation = Table(
    'Analysis_ItemDetail_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("ItemDetail_pk", Integer,
           ForeignKey('ItemDetail.ItemDetail_pk'),
           primary_key=True),
    Column("Analysis_pk", Integer,
           ForeignKey('Analysis.Analysis_pk'),
           primary_key=True)
)


ItemDetail_Packing_Allocation = Table(
    'ItemDetail_Packing_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("ItemDetail_pk", Integer,
           ForeignKey('ItemDetail.ItemDetail_pk'),
           primary_key=True),
    Column("Packing_pk", Integer,
           ForeignKey('Packing.Packing_pk'),
           primary_key=True)
)

# ok
DocGroup_Site_Allocation = Table(
    'DocGroup_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("DocGroup_pk", Integer,
           ForeignKey('DocGroup.DocGroup_pk'),
           primary_key=True)
)


# ok
FlowID_Site_Allocation = Table(
    'FlowID_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("FlowID_pk", Integer,
           ForeignKey('FlowID.FlowID_pk'),
           primary_key=True)
)


DocType_Site_Allocation = Table(
    'DocType_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("DocType_pk", Integer,
           ForeignKey('DocType.DocType_pk'),
           primary_key=True)
)


ChartOfAccount_Group_Allocation = Table(
    'ChartOfAccount_Group_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("ChartOfAccount_pk", Integer,
           ForeignKey('ChartOfAccount.ChartOfAccount_pk'),
           primary_key=True),
    Column("Group_pk", Integer,
           ForeignKey('Group.Group_pk'),
           primary_key=True)
)


ChartOfAccount_Company_Allocation = Table(
    'ChartOfAccount_Company_Allocation', Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column(
        'ChartOfAccount_pk',
        Integer,
        ForeignKey('ChartOfAccount.ChartOfAccount_pk'),
        primary_key=True),
    Column('Company_pk', Integer,
           ForeignKey('Company.Company_pk'),
           primary_key=True)
)


UserProfile_Site_Allocation = Table(
    'UserProfile_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column(
        "UserProfile_pk",
        Integer,
        ForeignKey('UserProfile.UserProfile_pk'),
        primary_key=True)
)


Menu_UserProfile_Allocation = Table(
    'Menu_UserProfile_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("UserProfile_pk",
           Integer,
           ForeignKey('UserProfile.UserProfile_pk'),
           primary_key=True),
    Column("Menu_pk", Integer,
           ForeignKey('Menu.Menu_pk'),
           primary_key=True)
)


ThirdPartyType_Site_Allocation = Table(
    'ThirdPartyType_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("ThirdPartyType_pk", Integer,
           ForeignKey('ThirdPartyType.ThirdPartyType_pk'),
           primary_key=True)
)


DocNumbering_Site_Allocation = Table(
    'DocNumbering_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("DocNumbering_pk", Integer,
           ForeignKey('DocNumbering.DocNumbering_pk'),
           primary_key=True)
)

# ok
ThirdParty_Site_Allocation = Table(
    'ThirdParty_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("ThirdParty_pk", Integer,
           ForeignKey('ThirdParty.ThirdParty_pk'),
           primary_key=True)
)


# FIXME : needed ?
Bank_Site_Allocation = Table(
    'Bank_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("Bank_pk", Integer,
           ForeignKey('Bank.Bank_pk'),
           primary_key=True)
)

SalesRep_Site_Allocation = Table(
    'SalesRep_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("SalesRep_pk", Integer,
           ForeignKey('SalesRep.SalesRep_pk'),
           primary_key=True)
)

Incoterm_Site_Allocation = Table(
    'Incoterm_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("Incoterm_pk", Integer,
           ForeignKey('Incoterm.Incoterm_pk'),
           primary_key=True)
)

# ok
PaymentTerm_Site_Allocation = Table(
    'PaymentTerm_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("PaymentTerm_pk", Integer,
           ForeignKey('PaymentTerm.PaymentTerm_pk'),
           primary_key=True)
)


Warehouse_Site_Allocation = Table(
    'Warehouse_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("Warehouse_pk", Integer,
           ForeignKey('Warehouse.Warehouse_pk'),
           primary_key=True)
)


TypeStock_Document_Allocation = Table(
    'TypeStock_Document_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("TypeStock_pk", Integer,
           ForeignKey('TypeStock.TypeStock_pk'),
           primary_key=True),
    Column("DocType_pk", Integer,
           ForeignKey('DocType.DocType_pk'),
           primary_key=True)
)


Packing_Site_Allocation = Table(
    'Packing_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("Packing_pk", Integer,
           ForeignKey('Packing.Packing_pk'),
           primary_key=True)
)


TypeStock_Site_Allocation = Table(
    'TypeStock_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("TypeStock_pk", Integer,
           ForeignKey('TypeStock.TypeStock_pk'),
           primary_key=True)
)

PriceSchemaHeader_Site_Allocation = Table(
    'PriceSchemaHeader_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("PriceSchemaHeader_pk", Integer,
           ForeignKey('PriceSchemaHeader.PriceSchemaHeader_pk'),
           primary_key=True)
)


Analysis_Site_Allocation = Table(
    'Analysis_Site_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("Site_pk", Integer,
           ForeignKey('Site.Site_pk'),
           primary_key=True),
    Column("Analysis_pk", Integer,
           ForeignKey('Analysis.Analysis_pk'),
           primary_key=True)
)


ThirdPartyType_ThirdParty_Allocation = Table(
    'ThirdPartyType_ThirdParty_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("ThirdParty_pk", Integer,
           ForeignKey('ThirdParty.ThirdParty_pk'),
           primary_key=True),
    Column("ThirdPartyType_pk", Integer,
           ForeignKey('ThirdPartyType.ThirdPartyType_pk'),
           primary_key=True)
)


ItemDetail_ItemDetail_Allocation = Table(
    'ItemDetail_ItemDetail_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("RawMaterial_pk", Integer,
           ForeignKey('ItemDetail.ItemDetail_pk'),
           primary_key=True),
    Column("FinishedProduct_pk", Integer,
           ForeignKey('ItemDetail.ItemDetail_pk'),
           primary_key=True)
)

DocumentHeader_ExchangeRate_Allocation = Table(
    'DocumentHeader_ExchangeRate_Allocation',
    Base.metadata,
    Column("CancelData", Boolean, ColumnDefault(False)),
    Column("DocumentHeader_pk", Integer,
           ForeignKey('DocumentHeader.DocumentHeader_pk'),
           primary_key=True),
    Column("ExchangeRate_pk", Integer,
           ForeignKey('ExchangeRate.ExchangeRate_pk'),
           primary_key=True)
)
