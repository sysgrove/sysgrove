#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sqlalchemy import Column, Integer, ForeignKey, Float
from sqlalchemy import String, Unicode
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declared_attr
from sysgrove.settings import TP_DATAKOOKUP
from sysgrove.models import Base
from sysgrove.models import SGMixin, InitMixin
from .third_party import ThirdParty
from .tax import TaxType
from .item import ItemDetail
from .price import PriceCondition
from .company import Company
from sysgrove import i18n
_ = i18n.language.ugettext


class Ledger(SGMixin, Base):
    verbose_name = _("Ledger")

    code_content = ['lclass', 'code1', 'code2', 'code3', 'code4', 'code5']
    list_display = ['description'] + code_content + ['code']
    list_columns = ['code', 'description']

    @declared_attr
    def description(cls):
        return Column(cls.__tablename__ + "Description", Unicode(),
                      info={'verbose_name': _('description'),
                            "length": 30})

    @declared_attr
    def lclass(cls):
        return Column(cls.__tablename__ + "Class", String(),
                      info={'verbose_name': _('class')})

    @declared_attr
    def code1(cls):
        return Column(cls.__tablename__ + "CodeLevel1", String(),
                      info={'verbose_name': _('code 1')})

    @declared_attr
    def code2(cls):
        return Column(cls.__tablename__ + "CodeLevel2", String(),
                      info={'verbose_name': _('code 2')})

    @declared_attr
    def code3(cls):
        return Column(cls.__tablename__ + "CodeLevel3", String(),
                      info={'verbose_name': _('code 3')})

    @declared_attr
    def code4(cls):
        return Column(cls.__tablename__ + "CodeLevel4", String(),
                      info={'verbose_name': _('code 4')})

    @declared_attr
    def code5(cls):
        return Column(cls.__tablename__ + "CodeLevel5", String(),
                      info={'verbose_name': _('code 5')})

    @declared_attr
    def code(cls):
        return Column(cls.__tablename__ + "CodeComplete", String(),
                      info={'verbose_name': _('Ledger Code')})

    chartOfAccount_id = Column(
        "ChartOfAccount_pk",
        Integer,
        ForeignKey('ChartOfAccount.ChartOfAccount_pk'))
    chartOfAccount = relationship("ChartOfAccount")


class ThirdPartyLedgerDetermination(InitMixin, Base):
    verbose_name = _("Third Party Ledger Determination")
    list_base = ['company', 'thirdParty',
                 'country',
                 'ledgerCodeCompleteSales',
                 'ledgerCodeCompletePurchase',
                 ]
    list_columns = list_base
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    # Company FK
    company_id = Column(
        "Company_pk",
        Integer,
        ForeignKey('Company.Company_pk'))
    company = relationship(
        "Company",
        info={'verbose_name': Company.verbose_name})

    country_id = Column(
        "Country_pk",
        Integer,
        ForeignKey('Country.Country_pk')
    )
    country = relationship("Country",
                           info={'verbose_name': _('BillTo Country')})

    ledgerCodeCompleteSales_id = Column(
        "LedgerCodeCompleteSales",
        Integer,
        ForeignKey('Ledger.Ledger_pk')
    )
    ledgerCodeCompleteSales = relationship(
        "Ledger",
        primaryjoin=
        "ThirdPartyLedgerDetermination.ledgerCodeCompleteSales_id==Ledger.id",
        info={'verbose_name': _('Customer Ledger')}
    )

    ledgerCodeCompletePurchase_id = Column(
        "LedgerCodeCompletePurchase",
        Integer,
        ForeignKey('Ledger.Ledger_pk')
    )

    ledgerCodeCompletePurchase = relationship(
        "Ledger",
        primaryjoin=
        "ThirdPartyLedgerDetermination.ledgerCodeCompletePurchase_id"
        "==Ledger.id",
        info={'verbose_name': _('Supplier Ledger')})

    thirdParty_id = Column(
        "ThirdParty_pk",
        Integer,
        ForeignKey('ThirdParty.ThirdParty_pk')
    )
    thirdParty = relationship(
        "ThirdParty",
        primaryjoin=
        "ThirdPartyLedgerDetermination.thirdParty_id == ThirdParty.id",
        info={'verbose_name': ThirdParty.verbose_name,
              'datalookup': TP_DATAKOOKUP,
              }

    )


class TaxLedgerDetermination(InitMixin, Base):
    verbose_name = _("Tax Type Ledger Determination")
    list_base = ['company',
                 'taxType',
                 'taxValue',
                 'ledgerCodeCompleteSales', 'ledgerCodeCompletePurchase'
                 ]
    list_columns = list_base
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    # Company FK
    company_id = Column(
        "Company_pk",
        Integer,
        ForeignKey('Company.Company_pk'))
    company = relationship(
        "Company",
        info={'verbose_name': Company.verbose_name})

    taxType_id = Column(
        "TaxType_pk",
        Integer,
        ForeignKey('TaxType.TaxType_pk')
    )
    taxType = relationship(
        "TaxType",
        info={'verbose_name': _("Tax Type")})

    taxValue = Column("TaxValue", Float,
                      info={'verbose_name': _("Tax Value")})

    ledgerCodeCompleteSales_id = Column(
        "LedgerCodeCompleteSales",
        Integer,
        ForeignKey('Ledger.Ledger_pk')
    )
    ledgerCodeCompleteSales = relationship(
        "Ledger",
        primaryjoin=
        "TaxLedgerDetermination.ledgerCodeCompleteSales_id==Ledger.id",
        info={'verbose_name': _('Customer Ledger')}
    )

    ledgerCodeCompletePurchase_id = Column(
        "LedgerCodeCompletePurchase",
        Integer,
        ForeignKey('Ledger.Ledger_pk')
    )

    ledgerCodeCompletePurchase = relationship(
        "Ledger",
        primaryjoin=
        "TaxLedgerDetermination.ledgerCodeCompletePurchase_id==Ledger.id",
        info={'verbose_name': _('Supplier Ledger')})


class ItemDetailLedgerDetermination(InitMixin, Base):
    verbose_name = _("ItemDetail Ledger Determination")
    list_base = ['company',
                 'itemDetail_itemGroup_itemFamily',
                 'itemDetail_itemGroup',
                 'itemDetail',
                 'ledgerCodeCompleteSales',
                 'ledgerCodeCompletePurchase',
                 ]
    list_columns = list_base
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    # Company FK
    company_id = Column(
        "Company_pk",
        Integer,
        ForeignKey('Company.Company_pk'))
    company = relationship(
        "Company",
        info={'verbose_name': Company.verbose_name})

    itemDetail_id = Column(
        "ItemDetail_pk",
        Integer,
        ForeignKey('ItemDetail.ItemDetail_pk'))
    itemDetail = relationship("ItemDetail",
                              info={'verbose_name': ItemDetail.verbose_name})

    ledgerCodeCompleteSales_id = Column(
        "LedgerCodeCompleteSales",
        Integer,
        ForeignKey('Ledger.Ledger_pk'))
    ledgerCodeCompleteSales = relationship(
        "Ledger",
        primaryjoin=
        "ItemDetailLedgerDetermination.ledgerCodeCompleteSales_id"
        "==Ledger.id",
        info={'verbose_name': _("Sales Ledger")})

    ledgerCodeCompletePurchase_id = Column(
        "LedgerCodeCompletePurchase",
        Integer, ForeignKey('Ledger.Ledger_pk'))
    ledgerCodeCompletePurchase = relationship(
        "Ledger",
        primaryjoin=
        "ItemDetailLedgerDetermination.ledgerCodeCompletePurchase_id"
        "==Ledger.id",
        info={'verbose_name': _("Purchase Ledger")})


class PriceConditionLedgerDetermination(InitMixin, Base):
    verbose_name = _("Price Condition Ledger Determination")
    list_base = ['company', 'priceCondition', 'taxType',
                 'ledgerCodeCompleteSales',
                 'ledgerCodeCompletePurchase',
                 ]
    list_columns = list_base
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    # Company FK
    company_id = Column(
        "Company_pk",
        Integer,
        ForeignKey('Company.Company_pk'))
    company = relationship(
        "Company",
        info={'verbose_name': Company.verbose_name})

    priceCondition_id = Column(
        "PriceCondition_pk",
        Integer,
        ForeignKey('PriceCondition.PriceCondition_pk')
    )
    priceCondition = relationship(
        "PriceCondition",
        info={'verbose_name': PriceCondition.verbose_name})

    taxType_id = Column(
        "TaxType_pk",
        Integer,
        ForeignKey('TaxType.TaxType_pk')
    )
    taxType = relationship("TaxType",
                           info={'verbose_name': TaxType.verbose_name})

    ledgerCodeCompleteSales_id = Column(
        "LedgerCodeCompleteSales",
        Integer,
        ForeignKey('Ledger.Ledger_pk')
    )
    ledgerCodeCompleteSales = relationship(
        "Ledger",
        primaryjoin=
        "PriceConditionLedgerDetermination.ledgerCodeCompleteSales_id"
        "==Ledger.id",
        info={'verbose_name': _("Sales Ledger")}
    )

    ledgerCodeCompletePurchase_id = Column(
        "LedgerCodeCompletePurchase",
        Integer,
        ForeignKey('Ledger.Ledger_pk')
    )
    ledgerCodeCompletePurchase = relationship(
        "Ledger",
        primaryjoin=
        "PriceConditionLedgerDetermination.ledgerCodeCompletePurchase_id"
        "==Ledger.id",
        info={'verbose_name': _("Purchase Ledger")}
    )
