#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
import logging
from sqlalchemy import String, Column, Boolean, ColumnDefault
from sqlalchemy import Integer, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from sysgrove.models import Base
from sysgrove.models import SGMixin, CDMixin
from sysgrove import i18n
_ = i18n.language.ugettext


log = logging.getLogger('sysgrove')


# FIXME : replace by a Choices
class Status(SGMixin, Base):
    verbose_name = _('Document Status')
    list_base = ['code', 'description',
                 'internalFlag', 'headerOnlyFlag',
                 'cancelLikeFlag']

    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    internalFlag = Column('InternalFlag', Boolean,
                          ColumnDefault(False),
                          nullable=False,
                          info={'verbose_name': _("Internal Flag")})
    headerOnlyFlag = Column('HeaderOnlyFlag',
                            Boolean,
                            ColumnDefault(False),
                            nullable=False,
                            info={'verbose_name': _("Header Only Flag")})
    cancelLikeFlag = Column('CancelLikeFlag',
                            Boolean,
                            ColumnDefault(False),
                            nullable=False,
                            info={'verbose_name': _("Cancel Like Flag")})

    # @declared_attr
    # def code(cls):
    #     return Column(
    #         cls.__tablename__ + "Code",
    #         String(),
    #         info={ALTCODE: 'description',
    #               'verbose_name': u'%s Code' % cls.verbose_name}
    #     )


class HeaderStatusComputation(CDMixin, Base):
    verbose_name = _('Document Header Status Compation')
    list_base = ['detailsStatus', 'headerStatus']
    list_display = [[(('GroupBox', 0), list_base)]]

    detailsStatus = Column(
        "DetailsStatus", String(), primary_key=True,
        info={'verbose_name': _('Document Details Status representation')})

    headerStatus_id = Column('HeaderStatus_pk',
                             Integer,
                             ForeignKey('Status.Status_pk'))

    headerStatus = relationship(
        "Status",
        info={'verbose_name': Status.verbose_name}
    )

    def to_status_combinaison(self):
        res = []
        details_status = Status.query\
            .filter_by(headerOnlyFlag=False).order_by('id').all()
        for i in xrange(len(self.detailsStatus)):
            if self.detailsStatus[i] == '1':
                    res.append(details_status[i])
        return res

    @classmethod
    def from_status_combinaison(cls, status_list):
        """ return the instance corresponding this list of status
        """
        details_status = Status.query\
            .filter_by(headerOnlyFlag=False)\
            .order_by(Status.id)\
            .all()
        index = ""
        for status in details_status:
            if status in status_list:
                index = index + "1"
            else:
                index = index + "0"
        try:
            res = cls.query.filter_by(detailsStatus=index).one()
        except NoResultFound:
            return None
        except MultipleResultsFound:
            log.error('Many result found in HeaderStatusComputation for %s ' %
                      status_list)
            return None
        return res.headerStatus

    def __repr__(self):
        return u"<%s(%s)>" % (self.__tablename__, self.detailsStatus)

    def __str__(self):
        return self.detailsStatus

    def __unicode__(self):
        return unicode(self)
