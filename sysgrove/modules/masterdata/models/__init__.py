#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sqlalchemy.orm import relationship
from sysgrove.models import Base, SGMixin
# the follwing line is mandatory to create Allocation tables
from .allocations import *
from .user_profile import UserProfile
from .tax import TaxType, TaxTypeDetermination
from .company import Company
from .address import Address
from .site import Site
from .group import Group
from .document import DocType, DocGroup, DocCopyRule, DocNumbering
from .price import PriceSchemaHeader
from .price import PriceCondition
from .price import PriceSchemaDetail
from .ledger import Ledger
from .ledger import ThirdPartyLedgerDetermination
from .ledger import TaxLedgerDetermination
from .ledger import ItemDetailLedgerDetermination
from .ledger import PriceConditionLedgerDetermination
from .item import ItemFamily, ItemGroup, ItemDetail
from .chart_of_account import ChartOfAccount, ChartOfAccountMapping
from .menu import Menu
from .third_party import ThirdPartyType
from .third_party import ThirdParty
from .third_party import ThirdPartyItemList
from .third_party import ThirdPartyPriceList
from .payment import PaymentTerm, PaymentTermText, PaymentType
from .contact import Contact, ContactItem
from .warehouse import Warehouse
from .stock import TypeStock
from .commission import CommissionDefinition
from .bank import Bank
from .exchange import ExchangeRate
from .cost import RepartitionCosts
from .country import Country
from .incoterm import Incoterm, IncotermText
from .uom import UnitOfMeasure, UnitOfMeasureConversion
from .date import DateType
from .currency import Currency
from .flowId import FlowID
from .language import Language
from .credit import CreditLimit
from .transport import TransportType
from .market import MarketType, MarketCommodity
from .production import Lot, Packing
from .status import Status, HeaderStatusComputation
from .sales_rep import SalesRep
from .vessel import Vessel


# FIXME : replace by a Choices
class WeekDays(SGMixin, Base):
    pass


class Analysis(SGMixin, Base):
    verbose_name = u'Type of Analysis'
    sites = relationship("Site",
                         secondary="Analysis_Site_Allocation",
                         info={'verbose_name': u"Sites"})
