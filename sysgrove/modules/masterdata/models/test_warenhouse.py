#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.main import init_db
from .warehouse import Warehouse
from .third_party import find_prem_valid_int

# We use currency to test the code_validator
init_db()


def test_warenhouse():
    top = Warehouse(code='TOP', description=u'top')
    top.save()
    aa = Warehouse(code='A', description=u'a', referenceWarehouse=top)
    aa.save()
    bb = Warehouse(code='B', description=u'b', referenceWarehouse=top)
    bb.save()

    aa.save()
    bb.save()
    aaref = aa.referenceWarehouse
    bbref = bb.referenceWarehouse
    topref = top.referenceWarehouse
    top.delete()
    aa.delete()
    bb.delete()

    assert(aaref is not None)
    assert(bbref is not None)
    assert(topref is None)


def test_find_prem_valid_int():

    assert(find_prem_valid_int([]) == 1)
    assert(find_prem_valid_int(range(12)) == 12)

    l = range(45)
    l.remove(27)
    assert(find_prem_valid_int(l) == 27)
    l.remove(28)
    assert(find_prem_valid_int(l) == 27)
    l.remove(26)
    assert(find_prem_valid_int(l) == 26)

    # take care of the two last elements in the list
    l = range(8)
    l.remove(6)
    assert(find_prem_valid_int(l) == 6)
