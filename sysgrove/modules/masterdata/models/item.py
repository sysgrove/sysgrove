#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    Unicode, Text,
    Float,
    Boolean
)
from sqlalchemy.orm import relationship
from sysgrove.models import Base
from sysgrove.models import SGMixin
from .uom import UnitOfMeasure
from .currency import Currency
from .tax import TaxType
from sysgrove import i18n
_ = i18n.language.ugettext


class ItemFamily(SGMixin, Base):
    verbose_name = _('Item Family')
    list_base = [
        'code', 'description',
        'serviceFlag',
        ['stockFlag', 'analysisFlag', 'productionFlag']
    ]
    list_display = [[
        (('GroupBox', 0),
            list_base
         )],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    serviceFlag = Column(
        "ServiceFlag",
        Boolean,
        info={'verbose_name': _('Service')}
    )
    productionFlag = Column(
        "ProductionFlag",
        Boolean,
        info={'verbose_name': _('Product liable to production')}
    )
    analysisFlag = Column(
        "AnalysisFlag",
        Boolean,
        info={'verbose_name': _('Product liable to analysis')}
    )
    stockFlag = Column(
        "StockFlag",
        Boolean,
        info={'verbose_name': _('Product to be kept on stock')}
    )
    sites = relationship("Site",
                         secondary="ItemFamily_Site_Allocation",
                         info={'verbose_name': _("Sites")})


class ItemGroup(SGMixin, Base):
    verbose_name = _('Item Group')
    list_display = [
        [(('GroupBox', 0), ['itemFamily', 'code', 'description'])],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]
    list_columns = [
        'itemFamily_code', 'itemFamily_description', 'code', 'description']

    itemFamily_id = Column(
        "ItemFamily_pk",
        Integer,
        ForeignKey('ItemFamily.ItemFamily_pk')
    )
    itemFamily = relationship(
        "ItemFamily",
        info={'verbose_name': ItemFamily.verbose_name}
    )
    sites = relationship("Site",
                         secondary="ItemGroup_Site_Allocation",
                         info={'verbose_name': _("Sites")})

    # FIXME: is possibly false.
    # as an ItelDetail may be link to two marketType
    # marketCommodity may return a list and not a single value
    marketCommodity = relationship("MarketCommodity",
                                   single_parent=True,
                                   uselist=False)

    # markets = relationship(
    #     "Market",
    #     info={'verbose_name': "Market"},
    #     primaryjoin="Market_ItemGroup_Allocation.itemGroup_id==ItemGroup.id",
    #     foreign_keys="[ItemGroup.id]",
    #     backref=backref("market",
    #                     remote_side='Market_ItemGroup_Allocation.market_id'))


class ItemDetail(SGMixin, Base):
    verbose_name = _('Item')
    datalookup = 'sysgrove.modules.masterdata.ui.customs.DataLookupItemDetail'
    list_base = ['itemGroup_itemFamily', 'itemGroup', 'code', 'description']

    logistic_list = [
        [('Vlist', ['uoMCodeDocument', 'country',

                    'itemPrintedDescription', 'externalReference']),
         'printedComment']
    ]
    prod_list = [
        ['qtyProdMin', 'uoMCodeQtyProdMin'],
        ['qtyProdMax', 'uoMCodeQtyProdMax'],
        ['timeProduction', 'uoMCodeTimeProduction'],
        ['timeAnalysis', 'uoMCodeTimeAnalysis'],

        'productionComments'
    ]

    finance_list = [
        'productionPrice', 'currency', 'uoMCodePrice',
        'taxType', 'marketType',
    ]

    list_tab = [('TabPanel', [
        (u"Existing Entries", [('DefaultTable', 'table')]),
        (u'Logistics', logistic_list),
        (u'Production', prod_list),
        (u'Finance', finance_list)
    ])]

    # Take care it is mandatory to disting GroupBoxes
    # because of UIObject access...If it is a number it is not uset in the
    # GroupBox title
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', 1), list_tab)],

    ]

    itemPrintedDescription = Column(
        "ItemPrintedDescription",
        Unicode(),
        info={'verbose_name': _('Printing Description'), 'length': 40}
    )
    externalReference = Column(
        "ExternalReference",
        Unicode(),
        info={'verbose_name': _('External Reference'), 'length': 40}
    )
    printedComment = Column(
        "PrintedComment",
        Text,
        info={'verbose_name': _('Item Printed Comments')}
    )

    # Market FK
    marketType_id = Column("Market",
                           Integer,
                           ForeignKey('MarketType.MarketType_pk'))
    marketType = relationship(
        "MarketType",
        info={'verbose_name': _('Trading Market Place')}
    )

    # UnitOfMeasure FK
    uoMCodePrice_id = Column(
        "UoMCodePrice",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    uoMCodePrice = relationship(
        "UnitOfMeasure",
        primaryjoin="ItemDetail.uoMCodePrice_id == UnitOfMeasure.id",
        info={'verbose_name': u'Pricing %s' % UnitOfMeasure.verbose_name}
    )

    uoMCodeDocument_id = Column(
        "UoMCodeDocument",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    uoMCodeDocument = relationship(
        "UnitOfMeasure",
        primaryjoin="ItemDetail.uoMCodeDocument_id == UnitOfMeasure.id",
        info={'verbose_name': u'Document %s' % UnitOfMeasure.verbose_name}
    )

    uoMCodeQtyProdMin_id = Column(
        "UoMCodeQtyProdMin",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    uoMCodeQtyProdMin = relationship(
        "UnitOfMeasure",
        primaryjoin="ItemDetail.uoMCodeQtyProdMin_id == UnitOfMeasure.id",
        info={'verbose_name': UnitOfMeasure.verbose_name}
    )

    uoMCodeQtyProdMax_id = Column(
        "UoMCodeQtyProdMax",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    uoMCodeQtyProdMax = relationship(
        "UnitOfMeasure",
        primaryjoin="ItemDetail.uoMCodeQtyProdMax_id == UnitOfMeasure.id",
        info={'verbose_name': UnitOfMeasure.verbose_name}
    )

    uoMCodeTimeProduction_id = Column(
        "UoMCodeTimeProduction",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    uoMCodeTimeProduction = relationship(
        "UnitOfMeasure",
        primaryjoin="ItemDetail.uoMCodeTimeProduction_id == UnitOfMeasure.id",
        info={'verbose_name': UnitOfMeasure.verbose_name}

    )

    uoMCodeTimeAnalysis_id = Column(
        "UoMCodeTimeAnalysis",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    uoMCodeTimeAnalysis = relationship(
        "UnitOfMeasure",
        primaryjoin="ItemDetail.uoMCodeTimeAnalysis_id == UnitOfMeasure.id",
        info={'verbose_name': UnitOfMeasure.verbose_name}

    )

    uoMCodeAnalysis_id = Column(
        "UoMCodeAnalysis",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    uoMCodeAnalysis = relationship(
        "UnitOfMeasure",
        primaryjoin="ItemDetail.uoMCodeAnalysis_id == UnitOfMeasure.id"
    )

    # Currency FK
    currency_id = Column(
        "Currency_pk",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    currency = relationship(
        "Currency",
        info={'verbose_name': Currency.verbose_name}
    )

    # TaxType FK
    taxType_id = Column(
        "TaxType_pk",
        Integer,
        ForeignKey('TaxType.TaxType_pk')
    )
    taxType = relationship(
        "TaxType",
        info={'verbose_name': TaxType.verbose_name}
    )

    # ItemGroup FK
    itemGroup_id = Column(
        "ItemGroup_pk",
        Integer,
        ForeignKey('ItemGroup.ItemGroup_pk')
    )
    itemGroup = relationship(
        "ItemGroup",
        info={'verbose_name': ItemGroup.verbose_name}
    )

    # Country FK
    country_id = Column(
        "Country_pk",
        Integer,
        ForeignKey('Country.Country_pk')
    )
    country = relationship(
        "Country",
        info={'verbose_name': _('Origin Of Goods')}
    )

    qtyProdMin = Column(
        "QtyProdMin",
        Float,
        info={'verbose_name': _('Min Production Quantity')}
    )
    qtyProdMax = Column(
        "QtyProdMax",
        Float,
        info={'verbose_name': _('Max Production Quantity')}
    )
    timeProduction = Column(
        "TimeProduction",
        Float,
        info={'verbose_name': _('Production Time')}
    )
    timeAnalysis = Column(
        "TimeAnalysis",
        Float,
        info={'verbose_name': _('Analysis Time')}
    )
    analysisMinValue = Column("AnalysisMinValue", Unicode())
    analysisMaxValue = Column("AnalysisMaxValue", Unicode())
    analysisResult = Column("AnalysisResult", Unicode())
    productionComments = Column(
        "ProductionComments",
        Text,
        info={'verbose_name': _('Item Specific Production Comments')}
    )
    productionPrice = Column(
        "ProductionPrice",
        Float,
        info={'verbose_name': _('Production Price')}
    )

    sites = relationship("Site",
                         secondary="ItemDetail_Site_Allocation",
                         info={'verbose_name': _("Sites")})
    packings = relationship(
        "Packing", secondary="ItemDetail_Packing_Allocation")
    analysis = relationship(
        "Analysis",
        secondary="Analysis_ItemDetail_Allocation"
    )

    # For detail see:
    # http://docs.sqlalchemy.org/en/latest/orm/relationships.html#self-referential-many-to-many-relationship
    rawMaterials = relationship(
        "ItemDetail",
        secondary="ItemDetail_ItemDetail_Allocation",
        primaryjoin=
        "ItemDetail.id==ItemDetail_ItemDetail_Allocation.c.FinishedProduct_pk",
        secondaryjoin=
        "ItemDetail.id==ItemDetail_ItemDetail_Allocation.c.RawMaterial_pk",
        backref="finishedProducts",
        info={'verbose_name': _("Raw Materials")}
    )

    @property
    def marketCommodity(self):
        from sysgrove.modules.masterdata.models.market import MarketCommodity
        mt = self.marketType
        ig = self.itemGroup
        if mt:
            return MarketCommodity.query\
                .filter_by(marketType_id=mt.id, itemGroup_id=ig.id)\
                .one()
        else:
            # FIXME: rather rude!!! Business rule?
            return MarketCommodity.query\
                .filter_by(itemGroup_id=ig.id)\
                .first()
