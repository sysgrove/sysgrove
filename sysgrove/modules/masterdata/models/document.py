#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sqlalchemy import (Column, Integer, ForeignKey, Boolean,
                        ColumnDefault, String, Text)
from sqlalchemy.orm import relationship
from sysgrove.models import Base
from sysgrove.models import SGMixin, InitMixin
from .flowId import FlowID
from sysgrove import i18n
_ = i18n.language.ugettext


class DocGroup(SGMixin, Base):
    verbose_name = _('Document Group')
    sites = relationship(
        "Site",
        secondary="DocGroup_Site_Allocation",
        info={'verbose_name': _("Sites")}
    )
    docTypes = relationship("DocType")

    def initial(self):
        return u"%s" % self.description[0].upper()


class DocNumbering(SGMixin, Base):
    verbose_name = _('Numbering')
    list_base = ['code', 'description', 'documentNumber']
    list_columns = list_base[:]
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    documentNumber = Column(
        "DocumentNumber",
        Integer,  ColumnDefault(0),
        info={'verbose_name': _('Start Number')}
    )

    sites = relationship(
        "Site",
        secondary="DocNumbering_Site_Allocation",
        info={'verbose_name': _("Sites")}
    )

    def still_updatable(self):
        from sysgrove.core.document.models import DocumentHeader
        docTypes = DocType.query.filter_by(docNumbering_id=self.id).all()
        for docType in docTypes:
            docs = DocumentHeader.query.filter_by(docType_id=docType.id).all()
            if len(docs) > 0:
                return False
        return True


class DocType(SGMixin, Base):
    verbose_name = _('Document Type')
    list_color = ('UIColor', 'color')

    list_tabPanel1 = [
        'priceHeader',  'priceDetail',
        (('ClickShowS', 'stockFlag', u'Document linked to stock'),
            ['lotFlag', 'increaseStockFlag', 'decreaseStockFlag']),
        'intraGroupFlag', (list_color)
    ]

    list_display = [
        [(('GroupBox', 0),
            ['docGroup', 'flowID', 'docNumbering',
             'code', 'description'])],
        [('TabPanel',
            [(u"Existing Entries", [('DefaultTable', 'table')]),
             (u'Additional Information', list_tabPanel1),
             (u'Text', ['text'])])]
    ]

    list_columns = [
        'docGroup_code',
        'docGroup_description',
        'flowID_code',
        'flowID_description',
        'docNumbering_code',
        'docNumbering_description',
        'priceHeader_code',
        'priceDetail_code', 'code',
        'description', 'stockFlag', 'intraGroupFlag'
    ]

    color = Column(
        "Color",
        String(20),
        ColumnDefault('0, 0, 0'),
        info={'verbose_name': _('Color')}
    )

    stockFlag = Column(
        "StockFlag",
        Boolean,
        info={'verbose_name': _('Document linked to stock')}
    )
    lotFlag = Column(
        "LotFlag",
        Boolean,
        info={'verbose_name': _('Mandatory Lot')}
    )

    intraGroupFlag = Column(
        "IntraGroupFlag",
        Boolean,
        info={'verbose_name': _('Intra Group')})

    flowID_id = Column(
        "FlowID_pk",
        Integer,
        ForeignKey('FlowID.FlowID_pk')
    )
    flowID = relationship(
        "FlowID",
        info={'verbose_name': FlowID.verbose_name}
    )

    docGroup_id = Column(
        "DocGroup_pk",
        Integer,
        ForeignKey('DocGroup.DocGroup_pk')
    )
    docGroup = relationship(
        "DocGroup",
        info={'verbose_name': DocGroup.verbose_name}
    )

    docNumbering_id = Column(
        "DocNumbering_pk",
        Integer,
        ForeignKey('DocNumbering.DocNumbering_pk')
    )
    docNumbering = relationship(
        "DocNumbering",
        info={'verbose_name': DocNumbering.verbose_name}
    )

    priceHeader_id = Column(
        "PriceSchemaHeader_pk",
        Integer,
        ForeignKey('PriceSchemaHeader.PriceSchemaHeader_pk'),
        nullable=False,
    )
    priceHeader = relationship(
        "PriceSchemaHeader",
        primaryjoin="DocType.priceHeader_id==PriceSchemaHeader.id",
        info={'verbose_name': _("Default Price Schema for Document Header")}
    )

    priceDetail_id = Column(
        "PriceSchemaDetail_pk",
        Integer,
        ForeignKey('PriceSchemaHeader.PriceSchemaHeader_pk'),
        nullable=False,
    )
    priceDetail = relationship(
        "PriceSchemaHeader",
        primaryjoin="DocType.priceDetail_id==PriceSchemaHeader.id",
        info={'verbose_name': _("Default Price Schema for Document Detail")}
    )
    increaseStockFlag = Column(
        "IncreaseStockFlag",
        Boolean,
        info={'verbose_name': _('Increasing Stock')})
    decreaseStockFlag = Column(
        "DecreaseStockFlag", Boolean,
        info={'verbose_name': _('Decreasing Stock')})

    text = Column(
        "text",
        Text,
        info={'verbose_name': _('Text')})

    sites = relationship("Site", secondary="DocType_Site_Allocation",
                         info={'verbose_name': _("Sites")})

    typeStocks = relationship(
        "TypeStock",
        secondary="TypeStock_Document_Allocation",
        info={'verbose_name': _("TypeStocks")})


class DocCopyRule(InitMixin, Base):
    verbose_name = _('Document Copy Rule')
    list_origin = ['flowIDOrigin', 'docGroupOrigin',
                   'docTypeCodeOrigin', 'siteOrigin']
    list_target = ['flowIDTarget', 'docGroupTarget',
                   'docTypeCodeTarget', 'siteTarget']

    list_display = [
        [(('GroupBox', u'%s From' % verbose_name), list_origin),
         (('GroupBox', u'%s To' % verbose_name), list_target)],
        [(('GroupBox', u"Existing Entries"), [('DefaultTable', 'table')])],
    ]

    list_columns = ['id']
    list_columns.extend(list_origin)
    list_columns.extend(list_target)

    docTypeCodeOrigin_id = Column(
        "DocTypeCodeOrigin",
        Integer,
        ForeignKey('DocType.DocType_pk')
    )
    docTypeCodeOrigin = relationship(
        "DocType",
        primaryjoin="DocCopyRule.docTypeCodeOrigin_id==DocType.id",
        info={'verbose_name': DocType.verbose_name}
    )

    docTypeCodeTarget_id = Column(
        "DocTypeCodeTarget",
        Integer,
        ForeignKey('DocType.DocType_pk')
    )
    docTypeCodeTarget = relationship(
        "DocType",
        primaryjoin="DocCopyRule.docTypeCodeTarget_id==DocType.id",
        info={'verbose_name': DocType.verbose_name}
    )

    docGroupOrigin_id = Column(
        "DocGroupOrigin_pk",
        Integer, ForeignKey('DocGroup.DocGroup_pk')
    )
    docGroupOrigin = relationship(
        "DocGroup",
        primaryjoin="DocCopyRule.docGroupOrigin_id==DocGroup.id",
        info={'verbose_name': DocGroup.verbose_name}
    )

    docGroupTarget_id = Column(
        "DocGroupTarget_pk",
        Integer,
        ForeignKey('DocGroup.DocGroup_pk')
    )
    docGroupTarget = relationship(
        "DocGroup",
        primaryjoin="DocCopyRule.docGroupTarget_id==DocGroup.id",
        info={'verbose_name': DocGroup.verbose_name}

    )

    flowIDOrigin_id = Column(
        "FlowIDOrigin_pk",
        Integer,
        ForeignKey('FlowID.FlowID_pk')
    )
    flowIDOrigin = relationship(
        "FlowID",
        primaryjoin="DocCopyRule.flowIDOrigin_id==FlowID.id",
        info={'verbose_name': FlowID.verbose_name}
    )

    flowIDTarget_id = Column(
        "FlowIDTarget_pk",
        Integer,
        ForeignKey('FlowID.FlowID_pk')
    )
    flowIDTarget = relationship(
        "FlowID",
        primaryjoin="DocCopyRule.flowIDTarget_id==FlowID.id",
        info={'verbose_name': FlowID.verbose_name}
    )

    siteOrigin_id = Column(
        "SiteOrigin_pk",
        Integer,
        ForeignKey('Site.Site_pk')
    )
    siteOrigin = relationship(
        "Site",
        primaryjoin="DocCopyRule.siteOrigin_id==Site.id",
        info={'verbose_name': _('Site')}
    )

    siteTarget_id = Column(
        "SiteTarget_pk",
        Integer,
        ForeignKey('Site.Site_pk')
    )
    siteTarget = relationship(
        "Site",
        primaryjoin="DocCopyRule.siteTarget_id==Site.id",
        info={'verbose_name': _('Site')}
    )
