#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sqlalchemy import Date, Column, Integer, ForeignKey, Float
from sqlalchemy.orm import relationship
from sysgrove.models import Base
from sysgrove.models import InitMixin
from sysgrove import i18n
_ = i18n.language.ugettext

ex_uidate = 'sysgrove.modules.masterdata.ui.customs.UIExchangeRateDate'
lineedit = 'sysgrove.ui.classes.lineedit.UISLineEditWL'


class ExchangeRate(InitMixin, Base):
    verbose_name = _('Exchange Rate')

    dim = {'horz': 'targetCurrency',
           'cell': 'exchangeRateValue'}

    list_base = ['company', (lineedit, 'company_currency'),
                 (ex_uidate, 'exchangeRateDate'),
                 (('DTable', dim),
                     # useless : kept ot be comptable with other tuple in
                     # form.py
                     'exchangeRateValue')
                 ]

    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]
    list_columns = ['company', 'company_currency',
                    'exchangeRateDate', 'exchangeRateValue', 'targetCurrency']

    exchangeRateDate = Column("ExchangeRateDate",
                              Date,
                              nullable=False,
                              info={'verbose_name': _('Date')})
    exchangeRateValue = Column(
        "ExchangeRateValue", Float,
        info={'verbose_name': _('Exchange Rate Value')})

    # Currency FK
    company_id = Column(
        "Company_pk",
        Integer,
        ForeignKey('Company.Company_pk'))
    company = relationship(
        "Company",
        info={'verbose_name': _('Company')})

    targetCurrency_id = Column(
        "TargetCurrency",
        Integer,
        ForeignKey('Currency.Currency_pk')
    )
    targetCurrency = relationship(
        "Currency",
        primaryjoin="ExchangeRate.targetCurrency_id == Currency.id",
        info={'verbose_name': _('Target Currency')}

    )
