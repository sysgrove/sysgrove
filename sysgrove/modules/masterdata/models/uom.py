#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.utils import memoize
from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    Float,
    Enum, ColumnDefault
)
from sqlalchemy.orm import relationship
from sysgrove.models import Base, Boolean
from sysgrove.models import SGMixin, InitMixin
from sysgrove import i18n
_ = i18n.language.ugettext


class UnitOfMeasure(SGMixin, Base):
    verbose_name = _('Unit Of Measure')
    list_base = [
        'code', 'description',
        ['masterUoM', 'standAlone']
    ]

    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    masterUoM = Column(
        "MasterUoM", Boolean,
        info={'verbose_name': _('Master Unit Of Measure')}
    )
    standAlone = Column(
        "StandAlone", Boolean,
        info={'verbose_name': _('Stand Alone Unit Of Measure')}
    )

    @staticmethod
    @memoize
    def master():
        try:
            return UnitOfMeasure.query.filter_by(masterUoM=True).one()
        except:
            return None


class UnitOfMeasureConversion(InitMixin, Base):
    verbose_name = _('Unit Of Measure Conversion')

    list_base = [
        'unitOfMeasureOrigin', 'quantityOrigin',
        'sign',
        'unitOfMeasureTarget', 'quantityTarget'
    ]
    list_columns = list_base[:]
    list_display = [
        [(('GroupBox', 0), list_base)],
        [(('GroupBox', _("Existing Entries")), [('DefaultTable', 'table')])],
    ]

    unitOfMeasureOrigin_id = Column(
        "UnitOfMeasureOrigin_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    unitOfMeasureOrigin = relationship(
        "UnitOfMeasure",
        primaryjoin=
        "UnitOfMeasureConversion.unitOfMeasureOrigin_id==UnitOfMeasure.id",
        info={'verbose_name': _('Unit Of Measure - Origin')}
    )

    unitOfMeasureTarget_id = Column(
        "UnitOfMeasureTarget_pk",
        Integer,
        ForeignKey('UnitOfMeasure.UnitOfMeasure_pk')
    )
    unitOfMeasureTarget = relationship(
        "UnitOfMeasure",
        primaryjoin=
        "UnitOfMeasureConversion.unitOfMeasureTarget_id==UnitOfMeasure.id",
        info={'verbose_name': _('Unit Of Measure - Target')}

    )

    sign = Column(
        "Sign",
        Enum("=", name='sign'),
        info={'verbose_name': _('Sign')}
    )

    quantityOrigin = Column(
        "QuantityOrigin",
        Float,
        ColumnDefault(0.0),
        info={'verbose_name': _('Quantity Origin')}
    )
    quantityTarget = Column(
        "QuantityTarget",
        Float,
        ColumnDefault(0.0),
        info={'verbose_name': _('Quantity Target')}
    )
