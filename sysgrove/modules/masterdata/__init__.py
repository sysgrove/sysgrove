#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.utils import find_key
from sysgrove.config import CONFIG
from sysgrove.modules import ModuleData
from sysgrove import i18n
_ = i18n.language.ugettext

_menu = [
    # Core Data
    ('7CD', _('Core Data'), [
        ('7CDGR', _('Group'), [
            ('7CDGR01', _('Creation'), []),
            ('7CDGR02', _('Modification'), []),
            ('7CDGR03', _('Display'), []),
        ], 'group'),
        ('7CDCM', _('Company'), [
            ('7CDCM01', _('Creation'), []),
            ('7CDCM02', _('Modification'), []),
            ('7CDCM03', _('Display'), []),
        ], 'company'),
        ('7CDSI', _('Site'), [
            ('7CDSI01', _('Creation'), []),
            ('7CDSI02', _('Modification'), []),
            ('7CDSI03', _('Display'), []),
        ], 'site'),
        ('7CDPU', _('User Profiles'), [
            ('7CDPU01', _('Creation'), []),
            ('7CDPU02', _('Modification'), []),
            ('7CDPU03', _('Display'), []),
            ('7CDPU04', _('Allocation'), []),
        ], 'userProfile'),

        ('7CDFT', _('ID Flow'), [
            ('7CDFT01', _('Creation'), []),
            ('7CDFT02', _('Modification'), []),
            ('7CDFT03', _('Display'), []),
            ('7CDFT04', _('Allocation'), []),
        ], 'flowID'),

        ('7CDDG', _('Document Group'), [
            ('7CDDG01', _('Creation'), []),
            ('7CDDG02', _('Modification'), []),
            ('7CDDG03', _('Display'), []),
            ('7CDDG04', _('Allocation'), []),
        ], 'docGroup'),
        ('7CDDT', _('Document Type'), [
            ('7CDDT01', _('Creation'), []),
            ('7CDDT02', _('Modification'), []),
            ('7CDDT03', _('Display'), []),
            ('7CDDT04', _('Allocation'), []),
        ], 'docType'),
        ('7CDCR', _('Document Copy Rules'), [
            ('7CDCR01', _('Creation'), []),
            ('7CDCR02', _('Modification'), []),
            ('7CDCR03', _('Display'), []),
        ]),
        ('7CDDN', _('Document Numbering'), [
            ('7CDDN01', _('Creation'), []),
            ('7CDDN02', _('Modification'), []),
            ('7CDDN03', _('Display'), []),
            ('7CDDN04', _('Allocation'), []),
        ], 'docNumbering'),
        ('7CDTD', _('Type of date'), [
            ('7CDTD01', _('Creation'), []),
            ('7CDTD02', _('Modification'), []),
            ('7CDTD03', _('Display'), []),
        ], 'dateType'),
        ('7CDIF', _('Item Family'), [
            ('7CDIF01', _('Creation'), []),
            ('7CDIF02', _('Modification'), []),
            ('7CDIF03', _('Display'), []),
            ('7CDIF04', _('Allocation'), []),
        ], 'itemFamily'),
        ('7CDIG', _('Item Group'), [
            ('7CDIG01', _('Creation'), []),
            ('7CDIG02', _('Modification'), []),
            ('7CDIG03', _('Display'), []),
            ('7CDIG04', _('Allocation'), []),
        ], 'itemGroup'),
        ('7CDTT', _('Third Party Type'), [
            ('7CDTT01', _('Creation'), []),
            ('7CDTT02', _('Modification'), []),
            ('7CDTT03', _('Display'), []),
        ], 'thirdPartyType'),
        ('7CDST', _('Document Status'), [
            ('7CDST01', _('Creation'), []),
            ('7CDST02', _('Modification'), []),
            ('7CDST03', _('Display'), []),
            ('7CDST06', u"Document Status Computation", [],
             'headerStatusComputation')
        ], 'status'),


    ]),

    # Logistics
    ('7LO', _('Logistics'), [
        ('7LOTP', _('Third Parties'), [
            ('7LOTP01', _('Creation'), []),
            ('7LOTP02', _('Modification'), []),
            ('7LOTP03', _('Display'), []),
            ('7LOTP04', _('Allocation'), []),
        ], 'thirdParty'),
        ('7LOSR', _('Sales Rep'), [
            ('7LOSR01', _('Creation'), []),
            ('7LOSR02', _('Modification'), []),
            ('7LOSR03', _('Display'), []),
            ('7LOSR04', _('Allocation'), []),
        ], 'salesRep'),
        ('7LOSL', _('Supplier Item List'), [
            ('7LOSL01', _('Creation'), []),
            ('7LOSL02', _('Modification'), []),
            ('7LOSL03', _('Display'), []),
            ('7LOSL04', _('Allocation'), []),
        ]),
        ('7LOPL', _('Purchase Price List'), [
            ('7LOPL01', _('Creation'), []),
            ('7LOPL02', _('Modification'), []),
            ('7LOPL03', _('Display'), []),
            ('7LOPL04', _('Allocation'), []),
        ]),
        ('7LOCL', _('Customer Item List'), [
            ('7LOCL01', _('Creation'), []),
            ('7LOCL02', _('Modification'), []),
            ('7LOCL03', _('Display'), []),
            ('7LOCL04', _('Allocation'), []),
        ]),
        ('7LOSP', _('Sales Price List'), [
            ('7LOSP01', _('Creation'), []),
            ('7LOSP02', _('Modification'), []),
            ('7LOSP03', _('Display'), []),
            ('7LOSP04', _('Allocation'), []),
        ]),
        ('7LOIN', _('Incoterms'), [
            ('7LOIN01', _('Creation'), []),
            ('7LOIN02', _('Modification'), []),
            ('7LOIN03', _('Display'), []),
            ('7LOIN04', _('Allocation'), []),
        ], 'incoterm'),
        ('7LOIT', _('Incoterms Text'), [
            ('7LOIT01', _('Creation'), []),
            ('7LOIT02', _('Modification'), []),
            ('7LOIT03', _('Display'), []),
        ], 'incotermText'),
        ('7LOTR', _('Transport Types'), [
            ('7LOTR01', _('Creation'), []),
            ('7LOTR02', _('Modification'), []),
            ('7LOTR03', _('Display'), []),
        ], 'transportType'),
    ]),

    # Production
    ('7PR', _('Production'), [
        ('7PRIT', _('Items'), [
            ('7PRIT01', _('Creation'), []),
            ('7PRIT02', _('Modification'), []),
            ('7PRIT03', _('Display'), []),
            ('7PRIT04', _('Allocation'), []),
        ], 'itemDetail'),
        ('7PRUM', _('Units of Measure'), [
            ('7PRUM01', _('Creation'), []),
            ('7PRUM02', _('Modification'), []),
            ('7PRUM03', _('Display'), []),
        ], 'unitOfMeasure'),
        ('7PRUC', _('Units of Measure Conversion'), [
            ('7PRUC01', _('Creation'), []),
            ('7PRUC02', _('Modification'), []),
            ('7PRUC03', _('Display'), []),
        ], 'unitOfMeasureConversion'),
        ('7PRWH', _('Warehouse'), [
            ('7PRWH01', _('Creation'), []),
            ('7PRWH02', _('Modification'), []),
            ('7PRWH03', _('Display'), []),
            ('7PRWH04', _('Allocation'), []),
        ], 'warehouse'),
        ('7PRTS', _('Type of Stock'), [
            ('7PRTS01', _('Creation'), []),
            ('7PRTS02', _('Modification'), []),
            ('7PRTS03', _('Display'), []),
            ('7PRTS04', _('Allocation Site'), []),
            ('7PRTS05', _('Allocation DocType'), []),
        ], 'typeStock'),
        ('7PRAN', _('Type of Analyses'), [
            ('7PRAN01', _('Creation'), []),
            ('7PRAN02', _('Modification'), []),
            ('7PRAN03', _('Display'), []),
            ('7PRAN04', _('Allocation'), []),
            ('7PRAN05', _('Item Allocation'), []),
        ], 'analysis'),
        ('7PRPA', _('Packing'), [
            ('7PRPA01', _('Creation'), []),
            ('7PRPA02', _('Modification'), []),
            ('7PRPA03', _('Display'), []),
        ], 'packing'),

    ]),
    # Finance
    ('7FI', _('Finance'), [
        ('7FIMT', _('Market Type'), [
            ('7FIMT01', _('Creation'), []),
            ('7FIMT02', _('Modification'), []),
            ('7FIMT03', _('Display'), []),
        ], 'marketType'),
        ('7FIMC', _('Market Commodity'), [
            ('7FIMC01', _('Creation'), []),
            ('7FIMC02', _('Modification'), []),
            ('7FIMC03', _('Display'), []),
        ], 'marketCommodity'),

        ('7FICP', _('Price Condition'), [
            ('7FICP01', _('Creation'), []),
            ('7FICP02', _('Modification'), []),
            ('7FICP03', _('Display'), []),
        ], 'priceCondition'),
        ('7FIPS', _('Price Schema'), [
            ('7FIPS01', _('Creation'), []),
            ('7FIPS02', _('Modification'), []),
            ('7FIPS03', _('Display'), []),
            ('7FIPS04', _('Allocation'), []),
        ], 'priceSchemaHeader'),
        ('7FITX', _('Tax Types'), [
            ('7FITX01', _('Creation'), []),
            ('7FITX02', _('Modification'), []),
            ('7FITX03', _('Display'), []),
        ], 'taxType'),
        ('7FITD', _('Tax Types Determination'), [
            ('7FITD01', _('Creation'), []),
            ('7FITD02', _('Modification'), []),
            ('7FITD03', _('Display'), []),
        ], 'taxTypeDetermination'),



        # ledger determinations
        ('7FITL', _('Third Party Ledger Determination'), [
            ('7FITL01', _('Creation'), []),
            ('7FITL02', _('Modification'), []),
            ('7FITL03', _('Display'), []),
        ], 'thirdPartyLedgerDetermination'),
        ('7FIXL', _('Tax Type Ledger Determination'), [
            ('7FIXL01', _('Creation'), []),
            ('7FIXL02', _('Modification'), []),
            ('7FIXL03', _('Display'), []),
        ], 'taxLedgerDetermination'),
        ('7FIIL', _('ItemDetail Ledger Determination'), [
            ('7FIIL01', _('Creation'), []),
            ('7FIIL02', _('Modification'), []),
            ('7FIIL03', _('Display'), []),
        ], 'itemDetailLedgerDetermination'),
        ('7FIPL', _('PriceCondition Ledger Determination'), [
            ('7FIPL01', _('Creation'), []),
            ('7FIPL02', _('Modification'), []),
            ('7FIPL03', _('Display'), []),
        ], 'priceConditionLedgerDetermination'),


        ('7FICO', _('Commission'), [
            ('7FICO01', _('Creation'), []),
            ('7FICO02', _('Modification'), []),
            ('7FICO03', _('Display'), []),
        ], 'commissionDefinition'),
        ('7FICC', _('Commission Calculation'), [
            ('7FICC01', _('Creation'), []),
            ('7FICC02', _('Modification'), []),
            ('7FICC03', _('Display'), []),
        ]),

        ('7FIRC', _('Repartition Costs'), [
            ('7FIRC01', _('Creation'), []),
            ('7FIRC02', _('Modification'), []),
            ('7FIRC03', _('Display'), []),
        ], 'repartitionCosts'),

        ('7FIPT', _('Payment Terms'), [
            ('7FIPT01', _('Creation'), []),
            ('7FIPT02', _('Modification'), []),
            ('7FIPT03', _('Display'), []),
            ('7FIPT04', _('Allocation'), []),
        ], 'paymentTerm'),
        ('7FITT', _('Payment Terms Text'), [
            ('7FITT01', _('Creation'), []),
            ('7FITT02', _('Modification'), []),
            ('7FITT03', _('Display'), []),
        ], 'paymentTermText'),
        ('7FICA', _('Chart Of Account'), [
            ('7FICA01', _('Creation'), []),
            ('7FICA02', _('Modification'), []),
            ('7FICA03', _('Display'), []),
        ]),
        ('7FIBA', _('Banks'), [
            ('7FIBA01', _('Creation'), []),
            ('7FIBA02', _('Modification'), []),
            ('7FIBA03', _('Display'), []),
            ('7FIBA04', _('Allocation'), []),
        ], 'bank'),
        ('7FIER', _('Exchange Rate'), [
            ('7FIER01', _('Creation'), []),
            ('7FIER02', _('Modification'), []),
            ('7FIER03', _('Display'), []),
        ], 'exchangeRate'),
        ('7FICU', _('Currencies'), [
            ('7FICU01', _('Creation'), []),
            ('7FICU02', _('Modification'), []),
            ('7FICU03', _('Display'), []),
        ], 'currency'),
        ('7FICN', _('Countries'), [
            ('7FICN01', _('Creation'), []),
            ('7FICN02', _('Modification'), []),
            ('7FICN03', _('Display'), []),
        ], 'country'),
    ]),
]


data = ModuleData(
    find_key(__package__, CONFIG['modules']),
    u'Master Data',
    'master-data.svg',
    _menu
)
