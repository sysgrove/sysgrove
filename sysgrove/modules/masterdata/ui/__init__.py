#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

# FIXME UI2 has to be replace by UI
from .gen import UI, Dyn01, Dyn02, Dyn03
from .gen import Dyn04, Dyn05, Alloc, AllocDocType, UI7CDPU
from .gen import UI7CDST06

# Core Data

# Group 7CDGR
ui7CDGR01 = UI(klass="group", code='01')
ui7CDGR02 = UI(klass="group", code='02')
ui7CDGR03 = UI(klass="group", code='03')
# Company 7CDCM
ui7CDCM01 = UI(klass="company", code='01')
ui7CDCM02 = UI(klass="company", code='02')
ui7CDCM03 = UI(klass="company", code='03')
# Site 7CDSI
ui7CDSI01 = UI(klass="site", code='01')
ui7CDSI02 = UI(klass="site", code='02')
ui7CDSI03 = UI(klass="site", code='03')
# UserProfil
ui7CDPU01 = UI7CDPU(code="01")
ui7CDPU02 = UI7CDPU(code="02")
ui7CDPU03 = UI7CDPU(code="03")
ui7CDPU04 = Alloc(klass="userProfile", code='04')
# flowID 7CDFT
ui7CDFT01 = UI(klass="flowID", code='01')
ui7CDFT02 = UI(klass="flowID", code='02')
ui7CDFT03 = UI(klass="flowID", code='03')
ui7CDFT04 = Alloc(klass="flowID", code='04')
# docGroup 7CDDG
ui7CDDG01 = UI(klass="docGroup", code='01')
ui7CDDG02 = UI(klass="docGroup", code='02')
ui7CDDG03 = UI(klass="docGroup", code='03')
ui7CDDG04 = Alloc(klass="docGroup", code='04')
# docType 7CDDT
ui7CDDT01 = UI(klass="docType", code='01')
ui7CDDT02 = UI(klass="docType", code='02')
ui7CDDT03 = UI(klass="docType", code='03')
ui7CDDT04 = Alloc(klass='docType', code='04')
# DocCopyRule 7CDCR
ui7CDCR01 = UI(klass="docCopyRule", code='01')
ui7CDCR02 = UI(klass="docCopyRule", code='02')
ui7CDCR03 = UI(klass="docCopyRule", code='03')
# DocNumbering 7CDDN
ui7CDDN01 = UI(klass="DocNumbering", code='01')
ui7CDDN02 = UI(klass="DocNumbering", code='02')
ui7CDDN03 = UI(klass="DocNumbering", code='03')
ui7CDDN04 = Alloc(klass="DocNumbering", code='04')
# dateType 7CDTD
ui7CDTD01 = UI(klass="dateType", code='01')
ui7CDTD02 = UI(klass="dateType", code='02')
ui7CDTD03 = UI(klass="dateType", code='03')
# itemFamily 7CDIF
ui7CDIF01 = UI(klass="itemFamily", code='01')
ui7CDIF02 = UI(klass="itemFamily", code='02')
ui7CDIF03 = UI(klass="itemFamily", code='03')
ui7CDIF04 = Alloc(klass='itemFamily', code='04')
# itemGroup 7CDIG
ui7CDIG01 = UI(klass="itemGroup", code='01')
ui7CDIG02 = UI(klass="itemGroup", code='02')
ui7CDIG03 = UI(klass="itemGroup", code='03')
ui7CDIG04 = Alloc(klass="itemGroup", code='04')
# thirdPartyType 7CDTT
ui7CDTT01 = UI(klass="thirdPartyType", code='01')
ui7CDTT02 = UI(klass="thirdPartyType", code='02')
ui7CDTT03 = UI(klass="thirdPartyType", code='03')
ui7CDTT04 = Alloc(klass="thirdPartyType", code='04')
# status 7CDST
ui7CDST01 = UI(klass="status", code='01')
ui7CDST02 = UI(klass="status", code='02')
ui7CDST03 = UI(klass="status", code='03')
ui7CDST06 = UI7CDST06(klass="headerStatusComputation", code='06')


#
#  LOGISTIC
#
# thirdParty 7LOTP
ui7LOTP01 = UI(klass="thirdParty", code='01', existsTable=False)
ui7LOTP02 = UI(klass="thirdParty", code='02', existsTable=False)
ui7LOTP03 = UI(klass="thirdParty", code='03', existsTable=False)
ui7LOTP04 = Alloc(klass="thirdParty", code='04')
# thirdParty 7LOSR
ui7LOSR01 = UI(klass="salesRep", code='01')
ui7LOSR02 = UI(klass="salesRep", code='02')
ui7LOSR03 = UI(klass="salesRep", code='03')
ui7LOSR04 = Alloc(klass="salesRep", code='04')
# incoterm 7LOIN
ui7LOIN01 = UI(klass="incoterm", code='01')
ui7LOIN02 = UI(klass="incoterm", code='02')
ui7LOIN03 = UI(klass="incoterm", code='03')
ui7LOIN04 = Alloc(klass="incoterm", code='04')
# incotermText 7LOIT
ui7LOIT01 = UI(klass="incotermText", code='01')
ui7LOIT02 = UI(klass="incotermText", code='02')
ui7LOIT03 = UI(klass="incotermText", code='03')
# transportType 7LOTR
ui7LOTR01 = UI(klass="transportType", code='01')
ui7LOTR02 = UI(klass="transportType", code='02')
ui7LOTR03 = UI(klass="transportType", code='03')


#
# Production
#
# ItemDetail 7PRIT
ui7PRIT01 = UI(klass='itemDetail', code='01', existsTable=False)
ui7PRIT02 = UI(klass='itemDetail', code='02', existsTable=False)
ui7PRIT03 = UI(klass='itemDetail', code='03', existsTable=False)
ui7PRIT04 = Alloc(klass='itemDetail', code='04')
# unitOfMeasure 7PRUM
ui7PRUM01 = UI(klass="unitOfMeasure", code='01')
ui7PRUM02 = UI(klass="unitOfMeasure", code='02')
ui7PRUM03 = UI(klass="unitOfMeasure", code='03')
# unitOfMeasure 7PRUC
ui7PRUC01 = UI(klass="unitOfMeasureConversion", code='01')
ui7PRUC02 = UI(klass="unitOfMeasureConversion", code='02')
ui7PRUC03 = UI(klass="unitOfMeasureConversion", code='03')
# warehouse 7PRWH
ui7PRWH01 = UI(klass="warehouse", code='01')
ui7PRWH02 = UI(klass="warehouse", code='02')
ui7PRWH03 = UI(klass="warehouse", code='03')
ui7PRWH04 = Alloc(klass="warehouse", code='04')

# typeStock 7PRTS
ui7PRTS01 = UI(klass="typeStock", code='01')
ui7PRTS02 = UI(klass="typeStock", code='02')
ui7PRTS03 = UI(klass="typeStock", code='03')
ui7PRTS04 = Alloc(klass="typeStock", code='04')
ui7PRTS05 = AllocDocType(klass="typeStock", code='04')
# analysis 7PRAN
ui7PRAN01 = UI(klass="analysis", code='01')
ui7PRAN02 = UI(klass="analysis", code='02')
ui7PRAN03 = UI(klass="analysis", code='03')
ui7PRAN04 = Alloc(klass="analysis", code='04')
# packing 7PRPA
ui7PRPA01 = UI(klass="packing", code='01')
ui7PRPA02 = UI(klass="packing", code='02')
ui7PRPA03 = UI(klass="packing", code='03')


#
# Finance
#

# marketType 7FIMT
ui7FIMT01 = UI(klass="marketType", code='01')
ui7FIMT02 = UI(klass="marketType", code='02')
ui7FIMT03 = UI(klass="marketType", code='03')

# marketCommodity 7FIMC
ui7FIMC01 = UI(klass="marketCommodity", code='01',
               keys=['marketType_id', 'itemGroup_id'])
ui7FIMC02 = UI(klass="marketCommodity", code='02',
               keys=['marketType_id', 'itemGroup_id'])
ui7FIMC03 = UI(klass="marketCommodity", code='03',
               keys=['marketType_id', 'itemGroup_id'])

# priceCondition 7FICP
ui7FICP01 = UI(klass="priceCondition", code='01')
ui7FICP02 = UI(klass="priceCondition", code='02')
ui7FICP03 = UI(klass="priceCondition", code='03')

# PriceSchemaHeader 7FIPS
ui7FIPS01 = UI(klass="priceSchemaHeader", code='01')
ui7FIPS02 = UI(klass="priceSchemaHeader", code='02')
ui7FIPS03 = UI(klass="priceSchemaHeader", code='03')
ui7FIPS04 = Alloc(klass="priceSchemaHeader", code='04')

# commissionDefinition 7FICC
ui7FICO01 = UI(klass="commissionDefinition", code='01')
ui7FICO02 = UI(klass="commissionDefinition", code='02')
ui7FICO03 = UI(klass="commissionDefinition", code='03')
# repartitionCosts 7FIRC
ui7FIRC01 = UI(klass="repartitionCosts", code='01')
ui7FIRC02 = UI(klass="repartitionCosts", code='02')
ui7FIRC03 = UI(klass="repartitionCosts", code='03')
# paymentTerm 7FIPT
ui7FIPT01 = UI(klass="paymentTerm", code='01')
ui7FIPT02 = UI(klass="paymentTerm", code='02')
ui7FIPT03 = UI(klass="paymentTerm", code='03')
ui7FIPT04 = Alloc(klass="paymentTerm", code='04')
# paymentTermText 7FIPT
ui7FITT01 = UI(klass="paymentTermText", code='01')
ui7FITT02 = UI(klass="paymentTermText", code='02')
ui7FITT03 = UI(klass="paymentTermText", code='03')
# bank 7FIBA
ui7FIBA01 = UI(klass="bank", code='01')
ui7FIBA02 = UI(klass="bank", code='02')
ui7FIBA03 = UI(klass="bank", code='03')
ui7FIBA04 = Alloc(klass="bank", code='04')
# exchange Rate 7FIER
ui7FIER01 = UI(klass="exchangeRate", code='01')
ui7FIER02 = UI(klass="exchangeRate", code='02')
ui7FIER03 = UI(klass="exchangeRate", code='03')

# Currency 7FICU
ui7FICU01 = UI(klass="currency", code='01')
ui7FICU02 = UI(klass="currency", code='02')
ui7FICU03 = UI(klass="currency", code='03')
# Country 7FICN
ui7FICN01 = UI(klass="country", code='01')
ui7FICN02 = UI(klass="country", code='02')
ui7FICN03 = UI(klass="country", code='03')
# TaxType 7FITX
ui7FITX01 = UI(klass="taxType", code='01')
ui7FITX02 = UI(klass="taxType", code='02')
ui7FITX03 = UI(klass="taxType", code='03')
# taxTypeDetermination 7FITD
ui7FITD01 = UI(klass="taxTypeDetermination", code='01')
ui7FITD02 = UI(klass="taxTypeDetermination", code='02')
ui7FITD03 = UI(klass="taxTypeDetermination", code='03')


# ChartOfAccount 7FICA
ui7FICA01 = UI(klass="chartOfAccount", code='01')
ui7FICA02 = UI(klass="chartOfAccount", code='02')
ui7FICA03 = UI(klass="chartOfAccount", code='03')

# ledger determinations
ui7FITL01 = UI(klass="thirdPartyLedgerDetermination", code='01')
ui7FITL02 = UI(klass="thirdPartyLedgerDetermination", code='02')
ui7FITL03 = UI(klass="thirdPartyLedgerDetermination", code='03')

ui7FIXL01 = UI(klass="taxLedgerDetermination", code='01')
ui7FIXL02 = UI(klass="taxLedgerDetermination", code='02')
ui7FIXL03 = UI(klass="taxLedgerDetermination", code='03')

ui7FIIL01 = UI(klass="itemDetailLedgerDetermination", code='01')
ui7FIIL02 = UI(klass="itemDetailLedgerDetermination", code='02')
ui7FIIL03 = UI(klass="itemDetailLedgerDetermination", code='03')

ui7FIPL01 = UI(klass="priceConditionLedgerDetermination", code='01')
ui7FIPL02 = UI(klass="priceConditionLedgerDetermination", code='02')
ui7FIPL03 = UI(klass="priceConditionLedgerDetermination", code='03')
