#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : S. Collins, A. Mokhtari, G. Souveton
20/02/2013
Version 1.0


    Role :


revision # 1
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

import logging
from functools import partial
from sqlalchemy.orm.exc import NoResultFound
from PyQt4 import QtCore, QtGui
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

from sysgrove.settings import (
    SOLDETO_CODE, SHIPTO_CODE, BILLTO_CODE, AGENT_CODE
)
from sysgrove.utils import compute_all_unique_combinaison
from sysgrove.ui.classes import (
    UITree, UIDataLookupPopup, UIDataLookup, UIObject
)
from sysgrove.ui.classes.inline import UIInline
from sysgrove.ui.classes.date import UIDate
from sysgrove.ui.classes.table import CheckBoxTableWidgetItem
from sysgrove.ui.classes.table import ErrorTableWidget
from sysgrove.ui.classes.widgets.exclusive_tree import ExclusiveTreeWidget

log = logging.getLogger('sysgrove')


class UITreeRights(UITree):

    # checked and partitialy checked node must be retrune
    def valueObject(self):

        def values(tree, klass):
            res = []
            for child in tree.childs:
                if child.checkState(0) == QtCore.Qt.Checked or \
                        child.checkState(0) == QtCore.Qt.PartiallyChecked:
                    code = child.code
                    try:
                        val = klass.query.filter_by(code=code).one()
                        res.append(val)
                    except NoResultFound:
                        pass
                res.extend(values(child, klass))
            return res

        return values(self, self.klass)


# Special wigget for ThirdPartyType
# the method 'valueObject' has behave like in UITreeRights
class UITreeTPT(UIObject):

    def __init__(self, parent, name):
        from sysgrove.modules.masterdata.models import ThirdPartyType
        super(UITreeTPT, self).__init__(parent, name)
        self.qtObject = ExclusiveTreeWidget(parent.qtObject)
        header = self.headerItem()
        header.setHidden(True)

        self.klass = ThirdPartyType

        # build the tree
        soldTo = self.treeWidgetItem(SOLDETO_CODE, self.qtObject)
        self.treeWidgetItem(SHIPTO_CODE, soldTo)
        self.treeWidgetItem(BILLTO_CODE, soldTo)
        self.treeWidgetItem(AGENT_CODE, self.qtObject)

        self.setMaximumWidth(300)

    def treeWidgetItem(self, tpt_id, parent):
        item = QtGui.QTreeWidgetItem(parent)
        tpt = self.klass.query.filter_by(id=tpt_id).one()
        item.setText(0, tpt.description)
        item.setText(1, str(tpt_id))
        item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
        item.setCheckState(0, QtCore.Qt.Unchecked)
        return item

    # Only the ckecked value have to be return
    def valueObject(self):
        res = []
        it = QtGui.QTreeWidgetItemIterator(self.qtObject)
        while it.value():
            if it.value().checkState(0) == QtCore.Qt.Checked:
                val = self.klass.query\
                    .filter_by(id=int(it.value().text(1))).one()
                res.append(val)
            it += 1
        return res

    def setVal(self, values):
        val_ids = map(lambda x: x.id, values)
        it = QtGui.QTreeWidgetItemIterator(self.qtObject)
        while it.value():
            if int(it.value().text(1)) in val_ids:
                it.value().setCheckState(0, QtCore.Qt.Checked)
            it += 1

    def clear(self):
        it = QtGui.QTreeWidgetItemIterator(self.qtObject)
        while it.value():
            it.value().setCheckState(0, QtCore.Qt.Unchecked)
            it += 1


class UIDataLookupBank(UIDataLookup):
    def __init__(self, parent, name, popup_class=UIDataLookupPopup):
        self.popup_class = popup_class
        super(UIDataLookupBank, self).__init__(parent, name)

    def create_popup(self):
        self.popup = self.popup_class(
            self,
            pk_field='code',
            fields=['code', 'description',
                    'currency',
                    'iBANCode']
        )


class UIDataLookupThirdParty(UIDataLookup):

    def __init__(self, parent, name, popup_class=UIDataLookupPopup):
        self.popup_class = popup_class
        super(UIDataLookupThirdParty, self).__init__(parent, name)

    def create_popup(self):
        self.popup = self.popup_class(
            self,
            pk_field='code',
            fields=['code', 'description',
                    'address_addressLine1',
                    'address_zIPCode',
                    'address_city', 'address_country']
        )


class UIDataLookupTPLink(UIDataLookupThirdParty):

    def __init__(self, parent, name):
        super(UIDataLookupTPLink, self).__init__(parent, name)

    # a simplified version for ThirParty Link
    def values(self, linkedUI, pk_field='code'):
        from sysgrove.context import kernel
        from sysgrove.modules.masterdata.models import ThirdPartyType
        screen = kernel.get_current_context().screen

        # Only one master in this case and no filters
        master = linkedUI.linkMasters[0]
        uiMaster = getattr(screen.ui_space,
                           linkedUI.linkPrefix + master[0],
                           None)
        tptypes = map(lambda x: int(x.code), uiMaster.valueObject())

        # filters have to be build according to the link rules
        # if SoldTo in tptypes, them only agents are possible
        # if tptypes = [ShipTo] or if tpypes=[billTo] only SoldTo are possible
        # if tptypes = [Agent] only soldTo are possible
        if SOLDETO_CODE in tptypes:
            agent = ThirdPartyType.query.filter_by(id=AGENT_CODE).one()
            return agent.thirdPartys
        elif tptypes == [SHIPTO_CODE] or \
            tptypes == [BILLTO_CODE] or \
                tptypes == [AGENT_CODE]:
            soldTo = ThirdPartyType.query.filter_by(id=SOLDETO_CODE).one()
            return soldTo.thirdPartys
        else:
            log.debug('undhandle case for thirParties link')
            log.debug('with types %s' % tptypes)
            return []


class UIDataLookupTPDocument(UIDataLookupThirdParty):

    def __init__(self, parent, name):
        super(UIDataLookupTPDocument, self).__init__(parent, name)

    # the filters is for ThirdPartyType and not for the
    # linkedUI.klass
    def values(self, linkedUI, pk_field='code'):
        if 'id' in linkedUI.linkFilters:
            type_filter = linkedUI.linkFilters['id']
            del linkedUI.linkFilters['id']
        else:
            type_filter = None

        first_selection = super(UIDataLookupTPDocument,
                                self).values(linkedUI, pk_field)

        # if we are selecting a ship/bill to 1/3Party then
        # the linked 1/3Party have to be in the selection.
        # As this is not done by filter... let's do it by hand.
        if 'parentThirdParty_id' in linkedUI.linkFilters:
            from sysgrove.context import kernel
            parent_id = kernel.get_current_context()\
                .screen\
                .build_filter(linkedUI.linkFilters)['parentThirdParty_id']
            parent = self.klass.query.filter_by(id=parent_id).one()
            first_selection.append(parent)

        # The filter have to be handled in a diffrent way
        from sysgrove.modules.masterdata.models import ThirdPartyType
        if type_filter:
            tptype = ThirdPartyType.query.filter_by(id=type_filter).one()
            linkedUI.linkFilters['id'] = type_filter
            second_selection = tptype.thirdPartys
        else:
            second_selection = []

        res = set(first_selection).intersection(set(second_selection))
        return list(res)


class DataLookupItemDetail(UIDataLookup):

    def create_popup(self):
        self.popup = UIDataLookupPopup(
            self,
            pk_field='code',
            fields=['code',
                    'description',
                    'itemGroup_itemFamily_serviceFlag'
                    ]
        )


# Special widget for Leger
class UIInlineLedger(UIInline):

    def __init__(self, parent, name):
        from sysgrove.modules.masterdata.models import Ledger
        super(UIInlineLedger, self).__init__(parent, name, klass=Ledger)
        # from sysgrove.utils import debug_trace
        # debug_trace()
        # print self.li()
        self._code.setReadOnly(True)
        for c in self.klass.code_content:
            getattr(self, '_%s' % c).returnPressed.connect(self.update_code)
            getattr(self, '_%s' % c).editingFinished.connect(self.update_code)
            getattr(self, '_%s' % c).selectionChanged.connect(self.update_code)

    def update_code(self):
        text = ''
        for c in self.klass.code_content:
            text = text + getattr(self, '_%s' % c).text()
        self._code.setText(text)


class UIExchangeRateDate(UIDate):

    def __init__(self, parent, name):
        super(UIExchangeRateDate, self).__init__(parent, name)
        self.dateFormat.textChanged.connect(self.set_date)

    def set_date(self, text):
        if text == '//':
            return
        date = self.value()
        from sysgrove.modules.masterdata.models import ExchangeRate
        table = getattr(self.parent, ExchangeRate.get_ui_prefix()
                        + 'exchangeRateValue')
        uicomp = getattr(self.parent, ExchangeRate.get_ui_prefix() + 'company')
        company = uicomp.valueObject()
        values = ExchangeRate.query.filter_by(
            exchangeRateDate=date,
            company_id=company.id).all()
        table.setVal(values)


class UIHeaderStatusComputation(UIObject):

    """ Special table to handle HeaderStatusComputation class instances
        The table horital header is filled with the Status
    (except the specific one for the header)
    """

    def __init__(self, parent, name):
        super(UIHeaderStatusComputation, self).__init__(parent, name)
        from sysgrove.modules.masterdata.models import Status
        if hasattr(parent, 'qtObject'):
            parent = parent.qtObject
        self.qtObject = ErrorTableWidget(parent)
        self.setObjectName(name)

        # some request
        self.header_status = Status.query.order_by(Status.id).all()
        self.details_status = Status.query\
            .filter_by(headerOnlyFlag=False)\
            .order_by(Status.id)\
            .all()
        self.nb_status = len(self.details_status)
        self.setColumnCount(self.nb_status + 1)
        header = [status.description for status in self.details_status]
        header.append(u"Document Header Result")
        self.setHorizontalHeaderLabels(header)
        self.verticalHeader().hide()
        self.details_status_dict = {}
        for i in xrange(len(self.details_status)):
            self.details_status_dict[self.details_status[i]] = i
        # style
        self.horizontalHeader()\
            .setResizeMode(QtGui.QHeaderView.ResizeToContents)

        # init the table
        standart_flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.combinaisons = compute_all_unique_combinaison(self.details_status)

        self.setRowCount(len(self.combinaisons))
        for i in xrange(len(self.combinaisons)):
            for status in self.details_status:
                item = CheckBoxTableWidgetItem("")
                item.setFlags(standart_flags)
                if status in self.combinaisons[i]:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)

                self.setItem(i, self.details_status_dict[status], item)

            item = QtGui.QTableWidgetItem()
            item.setFlags(standart_flags | QtCore.Qt.ItemIsEditable)
            combobox = self.hs_combobox()
            self.setCellWidget(i, self.nb_status, combobox)
            combobox.currentIndexChanged.connect(
                partial(self.comboBoxSelectionChanged,
                        item,
                        combobox))

            self.setItem(i, self.nb_status, item)

        # self.setSortingEnabled(True)

    def hs_combobox(self):
        combobox = QtGui.QComboBox()
        combobox.insertItem(0, "", userData=QtCore.QVariant(None))
        for status in self.header_status:
            combobox.insertItem(status.id,
                                status.description,
                                userData=QtCore.QVariant(status))

        combobox.setCurrentIndex(0)
        return combobox

    def comboBoxSelectionChanged(self, item, combobox, index):
        # self.setSortingEnabled(False)
        item.setData(QtCore.Qt.DisplayRole,
                     combobox.itemText(index))
        item.setData(QtCore.Qt.UserRole,
                     combobox.itemData(index))
        # self.setSortingEnabled(True)

    def clear(self):
        for i in xrange(self.rowCount()):
            combobox = self.cellWidget(i, self.nb_status)
            combobox.setCurrentIndex(0)

    def setVal(self, values):
        # self.setSortingEnabled(False)
        for value in values:
            combinaison = value.to_status_combinaison()
            # assert(combinaison in self.combinaisons)

            if combinaison in self.combinaisons:
                row = self.combinaisons.index(combinaison)
                item = self.item(row, self.nb_status)
                combobox = self.cellWidget(row, self.nb_status)
                index = combobox.findText(value.headerStatus.description)
                combobox.setCurrentIndex(index)
                item.setData(QtCore.Qt.DisplayRole,
                             value.headerStatus.description)
                item.setData(QtCore.Qt.UserRole,
                             QtCore.QVariant(value.headerStatus))
            else:
                log.error(u'Unknown combinaison %s' % combinaison)
        # self.setSortingEnabled(True)

    def valuesObject(self):
        from sysgrove.modules.masterdata.models import HeaderStatusComputation
        res = []
        for row in xrange(self.rowCount()):
            headerStatus = self.item(row, self.nb_status)\
                .data(QtCore.Qt.UserRole).toPyObject()
            if headerStatus:
                index = ""
                for col in xrange(self.nb_status):
                    state = self.item(row, col).checkState()
                    if state == QtCore.Qt.Checked:
                        index = index + "1"
                    else:
                        index = index + "0"
                try:
                    value = HeaderStatusComputation.query\
                        .filter_by(detailsStatus=index).one()
                except:
                    value = HeaderStatusComputation(detailsStatus=index)
                value.headerStatus = headerStatus
                res.append(value)

        return res


if __name__ == '__main__':
    import sys
    from PyQt4.QtGui import QApplication

    app = QApplication(sys.argv)
    print 'APP is set'
    w = QtGui.QWidget()
    l = QtGui.QVBoxLayout(w)

    table = UIHeaderStatusComputation(w, 'table')

    l.addWidget(table.qtObject)
    w.resize(400, 300)
    w.show()
    sys.exit(app.exec_())
