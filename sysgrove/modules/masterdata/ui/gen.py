#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [Claude Huchet]
[07:08/2013]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact: contact@sysgrove.com

Role :
    This module aims to defined general classes to build ui for
simple alchemy classes, the classes that inherite only from SGMixin.
I would like to use inheritance instead of specif xml files.


This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from PyQt4 import QtCore, QtGui
from sysgrove.utils import (
    code_to_mode,
    retreive_class
)
from sysgrove.ui.classes import (
    UIDynamicButtonZoneInst,
    UIWidget, UIGroupBox, UIForm, UIGroupBoxWM,
    UIVLayout, UIHLayout, UIGridLayout,
    UIVSpacer, UIHSpacer,
    UILabel, UIDataLookup, UITree, UITabPanel, UITabWidget,
    CheckedTable,
    UIButton
)
from sysgrove.modules.masterdata.models import UserProfile
from .customs import UITreeRights
from .customs import UIHeaderStatusComputation
from sysgrove import i18n
_ = i18n.language.ugettext


class UIAllocTable(CheckedTable):

    def __init__(self, parent, name, klass):
        self.columns = [
            ("", bool, True, False, ""),
            (u"Code", str, False, False, 'code'),
            (u"Description", str, False, False, 'description'),
            ('id', int, False, True, '')
        ]
        super(UIAllocTable, self).__init__(parent,
                                           name,
                                           klass=klass,
                                           columns=self.columns,
                                           selectAll=0)

        for e in self.klass.query.all():
            row = [False, e.code, e.description, e.id]
            self.addRow(row)
        self.setMinimumWidth(self.width())

    def setVal(self, values):
        self.clear()
        values_id = map(lambda x: x.id, values)
        nb_rows = self.rowCount()
        for row in range(nb_rows):
            value, ok = self.item(row, 3).data(QtCore.Qt.UserRole).toInt()
            if ok and value in values_id:
                item = self.item(row, 0)
                item.setCheckState(QtCore.Qt.Checked)

    def valueObject(self):
        # retreive checked lines
        res = []
        nb_rows = self.rowCount()
        for row in range(nb_rows):
            check_cel = self.item(row, 0)
            if check_cel.checkState() == QtCore.Qt.Checked:
                row_id, ok = self.item(row, 3).data(QtCore.Qt.UserRole).toInt()
                if ok:
                    obj = self.klass.query.filter_by(id=row_id).one()
                    res.append(obj)
        return res

    def clear(self):
        nb_rows = self.rowCount()
        for row in range(nb_rows):
            item = self.item(row, 0)
            item.setCheckState(QtCore.Qt.Unchecked)


class UIMain(object):

    def __init__(self, klass, code, existsTable=True, keys=None):
        self.klass = retreive_class(klass)
        self.code = code
        self.existsTable = existsTable
        self.keys = keys

    @property
    def title(self):
        mode = code_to_mode(self.code)
        mode = mode.lower().capitalize()
        return u"%s %s" % (mode, self.klass.verbose_name)


class UI(UIMain):

    def setupUi(self, screen):
        self.screen = UIWidget(screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)
        self.container = UIWidget(self.screen, "container")
        self.LayoutV = UIVLayout(self.container, "LayoutV")
        # self.spacer1 = UIVSpacer(self.container, "spacer1")

        self.form = UIForm(self.container, self.klass.__name__)
        self.form.setAlchemyClass(self.klass)
        self.form.attributes(self.code)
        self.LayoutV.addWidget(self.form)

        # self.spacer2 = UIVSpacer(self.container, "spacer2")

        # self.tableLayout = UIHLayout(None, "tableLayout")
        # self.tableLeftSpacer = UIHSpacer(self.container, "tableLeftSpacer")
        # self.LayoutV.addWidget(self.form)
        self.mainLayout.addWidget(self.container)
        screen.layout.addWidget(self.screen)


# FIXME: this class should reuse more of the UI Class...
class UI7CDPU(UI):

    def __init__(self, code, existsTable=True):
        super(UI7CDPU, self).__init__(UserProfile, code, existsTable)

    def setupUi(self, Screen):
        self.screen = UIWidget(Screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)
        self.container = UIWidget(self.screen, "container")
        self.LayoutVPU = UIVLayout(self.container, "LayoutVPU")
        self.generalSpacer1 = UIVSpacer(self.container, "generalSpacer1")
        self.LayyHPU = UIHLayout(None, "LayyHPU")

        self.form = UIForm(self.container, self.klass.__name__)
        self.form.setAlchemyClass(self.klass)
        self.form.attributes(self.code)

        self.userProfilesCreationUpperSpacer2 = UIHSpacer(
            self.container, "userProfilesCreationUpperSpacer2")

        if self.code == "01":
            self.GroupBox12 = UIGroupBox(self.container, "GroupBox12")
            self.GroupBox12.setTitle("With Reference To")
            self.userProfilesCreationRightGridLayout = UIGridLayout(
                self.GroupBox12, "userProfilesCreationRightGridLayout")
            self.siteCodeReferenceLabel = UILabel(
                self.GroupBox12, "siteCodeReferenceLabel")
            self.siteCodeReferenceLabel.setText(_("Site Code"))
            self.siteCodeReferenceLayout = UIHLayout(
                None, "siteCodeReferenceLayout")
            self.siteCodeReferenceDataLookup = UIDataLookup(
                self.GroupBox12, "siteCodeReferenceDataLookup")
            self.siteCodeReferenceDataLookup.setCharSize(10)
            self.siteCodeReferenceDescription = UILabel(
                self.GroupBox12, "siteCodeReferenceDescription")
            self.siteCodeReferenceDescription.setCharSize(40)
            self.spacer1 = UIHSpacer(self.GroupBox12, "spacer1")
            self.siteCodeReferenceLayout.addWidget(
                self.siteCodeReferenceDataLookup)
            self.siteCodeReferenceLayout.addWidget(
                self.siteCodeReferenceDescription)
            self.siteCodeReferenceLayout.addSpacerItem(self.spacer1)
            self.userNameReferenceLabel = UILabel(
                self.GroupBox12, "userNameReferenceLabel")
            self.userNameReferenceLabel.setText(_("User Name Code"))
            self.userNameReferenceLayout = UIHLayout(
                None, "userNameReferenceLayout")
            self.userNameReferenceDataLookup = UIDataLookup(
                self.GroupBox12, "userNameReferenceDataLookup")
            self.userNameReferenceDataLookup.setCharSize(10)
            self.userNameReferenceDescription = UILabel(
                self.GroupBox12, "userNameReferenceDescription")
            self.userNameReferenceDescription.setCharSize(40)
            self.spacer1 = UIHSpacer(self.GroupBox12, "spacer1")
            self.userNameReferenceLayout.addWidget(
                self.userNameReferenceDataLookup)
            self.userNameReferenceLayout.addWidget(
                self.userNameReferenceDescription)
            self.userNameReferenceLayout.addSpacerItem(self.spacer1)
            self.userProfilesCreationRightGridLayout.addWidget(
                self.siteCodeReferenceLabel, 0, 0)
            self.userProfilesCreationRightGridLayout.addLayout(
                self.siteCodeReferenceLayout, 0, 1)
            self.userProfilesCreationRightGridLayout.addWidget(
                self.userNameReferenceLabel, 1, 0)
            self.userProfilesCreationRightGridLayout.addLayout(
                self.userNameReferenceLayout, 1, 1)

            self.userProfilesCreationUpperSpacer3 = UIHSpacer(
                self.container, "userProfilesCreationUpperSpacer3")

        self.LayyHPU.addWidget(self.form)
        self.LayyHPU.addSpacerItem(self.userProfilesCreationUpperSpacer2)
        # if self.code == "01":
        #     self.LayyHPU.addWidget(self.GroupBox12)
        #     self.LayyHPU.addSpacerItem(self.userProfilesCreationUpperSpacer3)
        self.userProfilesGeneralSpacer2 = UIVSpacer(
            self.container, "userProfilesGeneralSpacer2")
        self.userProfilesTreeLayout = UIHLayout(None, "userProfilesTreeLayout")
        self.GroupBox2 = UIGroupBox(self.container, "GroupBox2")
        self.GroupBox2.setMinimumWidth(400)
        self.GroupBox2.setTitle("Access Granted To")
        self.tableHLayout = UIHLayout(self.GroupBox2, "tableHLayout")
        self.userProfile_rights = UITreeRights(
            self.GroupBox2, "userProfile_rights")
        self.userProfile_rights.setTitle("Modules")
        self.tableHLayout.addWidget(self.userProfile_rights)
        self.userProfilesTreeSpacer2 = UIHSpacer(
            self.container, "userProfilesTreeSpacer2")

        # self.setupTable()

        self.userProfilesTreeLayout.addWidget(self.GroupBox2)
        self.userProfilesTreeLayout.addSpacerItem(self.userProfilesTreeSpacer2)
        if self.code == "01":
            self.userProfilesTreeLayout.addWidget(self.GroupBox12)
            self.userProfilesTreeLayout.addSpacerItem(
                self.userProfilesCreationUpperSpacer3)
        # self.userProfilesTreeLayout.addWidget(self.tableGB)
        self.userProfilesGeneralSpacer3 = UIVSpacer(
            self.container, "userProfilesGeneralSpacer3")
        self.LayoutVPU.addSpacerItem(self.generalSpacer1)
        self.LayoutVPU.addLayout(self.LayyHPU)
        self.LayoutVPU.addSpacerItem(self.userProfilesGeneralSpacer2)
        self.LayoutVPU.addLayout(self.userProfilesTreeLayout)
        self.LayoutVPU.addSpacerItem(self.userProfilesGeneralSpacer3)
        self.mainLayout.addWidget(self.container)
        Screen.layout.addWidget(self.screen)


class UI7CDST06(UIMain):

    def setupUi(self, screen):
        main = UIWidget(screen, "screen")
        mainLayout = UIVLayout(main, "mainLayout")
        mainLayout.setMargin(0)
        mainLayout.setSpacing(0)

        container = UIWidget(main, "container")
        layoutV = UIVLayout(container, "layoutV")

        buttonLayout = UIHLayout(None, "layoutV")
        saveButton = UIButton(container, 'save')
        # saveButton.setText('save')
        saveButton.setIcon(QtGui.QIcon(':document-save.svg'))
        saveButton.setIconSize(QtCore.QSize(40, 40))
        buttonLayout.addWidget(saveButton)
        buttonLayout.addStretch(1)

        tableGB = UIGroupBox(container, "TtableGB")
        tableGB.setTitle("Existing Entries")
        tableHLayout = UIHLayout(tableGB, "tableHLayout")
        # Table
        table = UIHeaderStatusComputation(
            tableGB,
            "table_%s" % self.klass.__name__)

        tableHLayout.addWidget(table)
        layoutV.addLayout(buttonLayout)
        layoutV.addWidget(tableGB)
        mainLayout.addWidget(container)
        screen.layout.addWidget(main)


class Alloc(UIMain):

    def setupUi(self, Screen):
        self.prefix = self.klass.get_ui_prefix()
        self.screen = UIWidget(Screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)
        self.container = UIWidget(self.screen, "container")
        self.generalHLayout = UIHLayout(self.container, "generalHLayout")
        self.generalVLayout = UIVLayout(None, "generalVLayout")
        self.spacer1 = UIVSpacer(self.container, "spacer1")
        self.upperLayout = UIHLayout(None, "upperLayout")
        self.leftSpacer = UIHSpacer(self.container, "leftSpacer")
        self.TabPanel = UITabPanel(self.container, "TabPanel")

        # first Tab
        self.TabWidget = UITabWidget(self.TabPanel, "TabWidget")
        self.TabWidget.setTitle("Filter by " + self.klass.verbose_name)
        self.TabWidget.setMinimumWidth(300)

        self.LayoutOfWidget = UIVLayout(self.TabWidget, "LayoutOfWidget")
        self.WidgetOfTabWidget = UIWidget(self.TabWidget, "WidgetOfTabWidget")

        self.gBAlloc = UIGroupBox(self.WidgetOfTabWidget, "gBAlloc")
        self.gBAlloc.setTitle(self.klass.verbose_name + " Allocation")
        self.allocationLayout = UIGridLayout(self.gBAlloc, "allocationLayout")
        self.codeLabel = UILabel(self.gBAlloc, "codeLabel")
        self.codeLabel.setText(
            self.klass.code.info['verbose_name'] + "          ")
        self.HLayout1 = UIHLayout(None, "HLayout1")
        self.dataloopup = UIDataLookup(self.gBAlloc, self.prefix + "code")
        self.dataloopup.setCharSize(10)
        self.description = UILabel(
            self.gBAlloc, self.prefix + "description")
        self.description.setCharSize(40)
        self.HLayout1.addWidget(self.dataloopup)
        self.HLayout1.addWidget(self.description)
        self.allocationLayout.addWidget(self.codeLabel, 0, 0)
        self.allocationLayout.addLayout(self.HLayout1, 0, 1)
        self.upperRightSpacer = UIHSpacer(self.container, "upperRightSpacer")

        self.upperLayout.addSpacerItem(self.leftSpacer)
        self.upperLayout.addWidget(self.gBAlloc)
        self.upperLayout.addSpacerItem(self.upperRightSpacer)
        self.LayoutOfWidget.addLayout(self.upperLayout)
        self.spacer6 = UIVSpacer(self.container, "spacer6")
        self.LayoutOfWidget.addSpacerItem(self.spacer6)
        self.generalVLayout.addSpacerItem(self.spacer1)
        self.generalVLayout.addWidget(self.TabPanel)
        self.spacer2 = UIVSpacer(self.container, "spacer2")
        self.treeLayout = UIHLayout(None, "treeLayout")
        self.treeLeftspacer = UIHSpacer(self.container, "treeLeftspacer")

        self.gBsites = UIGroupBoxWM(self.WidgetOfTabWidget, "gBsites")
        self.gBsites.setTitle("Existing Entries")
        self.gBsitesLayout = UIHLayout(self.gBsites, "gBsitesLayout")
        self.sites = UITree(self.gBsites, self.prefix + "sites")
        self.sites.setTitle("Allocation Site")
        self.gBsitesLayout.addWidget(self.sites)
        self.treeRightSpacer = UIHSpacer(self.container, "treeRightSpacer")
        self.treeLayout.addSpacerItem(self.treeLeftspacer)
        self.treeLayout.addWidget(self.gBsites)
        self.treeLayout.addSpacerItem(self.treeRightSpacer)
        self.LayoutOfWidget.addLayout(self.treeLayout)
        self.generalVLayout.addSpacerItem(self.spacer2)

        self.spacer3 = UIHSpacer(self.container, "spacer3")
        self.spacer4 = UIHSpacer(self.container, "spacer4")
        self.generalHLayout.addSpacerItem(self.spacer3)
        self.generalHLayout.addLayout(self.generalVLayout)
        self.generalHLayout.addSpacerItem(self.spacer4)

        # second Tab
        self.TabWidget2 = UITabWidget(self.TabPanel, "TabWidget2")
        self.TabWidget2.setTitle("Filter by Site")
        self.LayoutOfWidget2 = UIVLayout(self.TabWidget2, "LayoutOfWidget2")
        self.WidgetOfTabWidget2 = UIWidget(
            self.TabWidget2, "WidgetOfTabWidget2")

        self.gBAlloc2 = UIGroupBox(self.WidgetOfTabWidget2, "gBAlloc2")
        self.gBAlloc2.setTitle(self.klass.verbose_name + " Allocation")
        self.allocationLayout = UIGridLayout(self.gBAlloc2, "allocationLayout")
        self.codeLabelGroup = UILabel(self.gBAlloc2, "codeLabelGroup")
        self.codeLabelGroup.setText(_("Group Code"))
        self.HLayout1 = UIHLayout(None, "HLayout1")
        self.dataloopup = UIDataLookup(self.gBAlloc2, "site_company_group")
        self.dataloopup.setCharSize(10)
        self.description = UILabel(
            self.gBAlloc2, "site_company_group_description")
        self.description.setCharSize(40)
        self.HLayout1.addWidget(self.dataloopup)
        self.HLayout1.addWidget(self.description)
        self.allocationLayout.addWidget(self.codeLabelGroup, 0, 0)
        self.allocationLayout.addLayout(self.HLayout1, 0, 1)

        self.codeLabelCompany = UILabel(self.gBAlloc2, "codeLabelCompany")
        self.codeLabelCompany.setText(_("Company Code"))
        self.HLayout1 = UIHLayout(None, "HLayout1")
        self.dataloopup = UIDataLookup(self.gBAlloc2, "site_company")
        self.dataloopup.setCharSize(10)
        self.description = UILabel(self.gBAlloc2, "site_company_description")
        self.description.setCharSize(40)
        self.HLayout1.addWidget(self.dataloopup)
        self.HLayout1.addWidget(self.description)
        self.allocationLayout.addWidget(self.codeLabelCompany, 1, 0)
        self.allocationLayout.addLayout(self.HLayout1, 1, 1)

        self.codeLabelSite = UILabel(self.gBAlloc2, "codeLabelSite")
        self.codeLabelSite.setText(_("Site Code"))
        self.HLayout1 = UIHLayout(None, "HLayout1")
        self.dataloopup = UIDataLookup(self.gBAlloc2, "site")
        self.dataloopup.setCharSize(10)
        self.description = UILabel(self.gBAlloc2, "site_description")
        self.description.setCharSize(40)
        self.HLayout1.addWidget(self.dataloopup)
        self.HLayout1.addWidget(self.description)
        self.allocationLayout.addWidget(self.codeLabelSite, 2, 0)
        self.allocationLayout.addLayout(self.HLayout1, 2, 1)
        self.upperLayout2 = UIHLayout(None, "upperLayout2")
        self.upperLayout2.addSpacerItem(self.leftSpacer)
        self.upperLayout2.addWidget(self.gBAlloc2)
        self.upperLayout2.addSpacerItem(self.upperRightSpacer)
        self.LayoutOfWidget2.addLayout(self.upperLayout2)

        self.gBsites2 = UIGroupBoxWM(self.WidgetOfTabWidget2, "gBsites2")
        self.gBsites2.setTitle("Existing Entries")
        # Table
        self.tableHLayout = UIHLayout(self.gBsites2, "tableHLayout")
        self.table = UIAllocTable(
            self.gBsites2,
            "site_" + self.klass.get_ui_prefix()[:-1] + "s",
            self.klass)

        self.tableHLayout.addWidget(self.table)
        self.tableRightSpacer = UIHSpacer(self.container, "tableRightSpacer")
        self.tableLeftSpacer = UIHSpacer(self.container, "tableLeftSpacer")
        self.tableLayout = UIHLayout(None, "tableLayout")
        self.tableLayout.addSpacerItem(self.tableLeftSpacer)
        self.tableLayout.addWidget(self.gBsites2)
        self.tableLayout.addSpacerItem(self.tableRightSpacer)
        # self.LayoutOfWidget2.addWidget(self.gBsites2)
        self.LayoutOfWidget2.addLayout(self.tableLayout)
        self.mainLayout.addWidget(self.container)
        Screen.layout.addWidget(self.screen)


class AllocDocType(UIMain):

    def setupUi(self, Screen):
        self.prefix = self.klass.get_ui_prefix()
        self.screen = UIWidget(Screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)
        self.container = UIWidget(self.screen, "container")
        self.generalHLayout = UIHLayout(self.container, "generalHLayout")
        self.generalVLayout = UIVLayout(None, "generalVLayout")
        self.spacer1 = UIVSpacer(self.container, "spacer1")
        self.upperLayout = UIHLayout(None, "upperLayout")
        self.leftSpacer = UIHSpacer(self.container, "leftSpacer")
        self.TabPanel = UITabPanel(self.container, "TabPanel")

        # first Tab
        self.TabWidget = UITabWidget(self.TabPanel, "TabWidget")
        self.TabWidget.setTitle("Filter by " + self.klass.verbose_name)
        self.TabWidget.setMinimumWidth(300)

        self.LayoutOfWidget = UIVLayout(self.TabWidget, "LayoutOfWidget")
        self.WidgetOfTabWidget = UIWidget(self.TabWidget, "WidgetOfTabWidget")

        self.gBAlloc = UIGroupBox(self.WidgetOfTabWidget, "gBAlloc")
        self.gBAlloc.setTitle(self.klass.verbose_name + " Allocation")
        self.allocationLayout = UIGridLayout(self.gBAlloc, "allocationLayout")
        self.codeLabel = UILabel(self.gBAlloc, "codeLabel")
        self.codeLabel.setText(
            self.klass.code.info['verbose_name'] + "          ")
        self.HLayout1 = UIHLayout(None, "HLayout1")
        self.dataloopup = UIDataLookup(self.gBAlloc, self.prefix + "code")
        self.dataloopup.setCharSize(10)
        self.description = UILabel(
            self.gBAlloc, self.prefix + "description")
        self.description.setCharSize(40)
        self.HLayout1.addWidget(self.dataloopup)
        self.HLayout1.addWidget(self.description)
        self.allocationLayout.addWidget(self.codeLabel, 0, 0)
        self.allocationLayout.addLayout(self.HLayout1, 0, 1)
        self.upperRightSpacer = UIHSpacer(self.container, "upperRightSpacer")

        self.upperLayout.addSpacerItem(self.leftSpacer)
        self.upperLayout.addWidget(self.gBAlloc)
        self.upperLayout.addSpacerItem(self.upperRightSpacer)
        self.LayoutOfWidget.addLayout(self.upperLayout)
        self.spacer6 = UIVSpacer(self.container, "spacer6")
        self.LayoutOfWidget.addSpacerItem(self.spacer6)
        self.generalVLayout.addSpacerItem(self.spacer1)
        self.generalVLayout.addWidget(self.TabPanel)
        self.spacer2 = UIVSpacer(self.container, "spacer2")
        self.treeLayout = UIHLayout(None, "treeLayout")
        self.treeLeftspacer = UIHSpacer(self.container, "treeLeftspacer")

        self.gBsites = UIGroupBoxWM(self.WidgetOfTabWidget, "gBsites")
        self.gBsites.setTitle("Existing Entries")
        self.gBsitesLayout = UIHLayout(
            self.gBsites, "gBsitesLayout")
        self.sites = UITree(self.gBsites, self.prefix + "docTypes")
        self.sites.setTitle("Allocation linkedStock")
        self.gBsitesLayout.addWidget(self.sites)
        self.treeRightSpacer = UIHSpacer(
            self.container, "treeRightSpacer")
        self.treeLayout = UIHLayout(None, "treeLayout")
        self.treeLayout.addSpacerItem(self.treeLeftspacer)
        self.treeLayout.addWidget(self.gBsites)
        self.treeLayout.addSpacerItem(self.treeRightSpacer)
        self.LayoutOfWidget.addLayout(self.treeLayout)

        self.generalVLayout.addSpacerItem(self.spacer2)
        self.spacer3 = UIHSpacer(self.container, "spacer3")
        self.spacer4 = UIHSpacer(self.container, "spacer4")
        self.generalHLayout.addSpacerItem(self.spacer3)
        self.generalHLayout.addLayout(self.generalVLayout)
        self.generalHLayout.addSpacerItem(self.spacer4)

        # Tab 2
        self.TabWidget2 = UITabWidget(self.TabPanel, "TabWidget2")
        self.TabWidget2.setTitle("Filter by DocType")
        self.LayoutOfWidget2 = UIVLayout(
            self.TabWidget2, "LayoutOfWidget2")
        self.WidgetOfTabWidget2 = UIWidget(
            self.TabWidget2, "WidgetOfTabWidget2")
        self.gBAlloc2 = UIGroupBox(self.WidgetOfTabWidget2, "gBAlloc2")
        self.gBAlloc2.setTitle(self.klass.verbose_name + " Allocation")
        self.allocationLayout2 = UIGridLayout(
            self.gBAlloc2, "allocationLayout2")
        self.codeLabelGroup2 = UILabel(
            self.gBAlloc2, "codeLabelGroup2")
        self.codeLabelGroup2.setText(_("Document Group"))
        self.HLayout2 = UIHLayout(None, "HLayout2")
        self.dataloopup2 = UIDataLookup(
            self.gBAlloc2, "docType_docGroup")
        self.dataloopup2.setCharSize(10)
        self.description = UILabel(
            self.gBAlloc2, "docGroup_description")
        self.description.setCharSize(40)
        self.HLayout2.addWidget(self.dataloopup2)
        self.HLayout2.addWidget(self.description)
        self.allocationLayout2.addWidget(self.codeLabelGroup2, 0, 0)
        self.allocationLayout2.addLayout(self.HLayout2, 0, 1)

        self.labelDocType = UILabel(self.gBAlloc2, "labelDocType")
        self.labelDocType.setText(_("Document Type"))
        self.HLayout2 = UIHLayout(None, "HLayout2")
        self.dataloopup2 = UIDataLookup(self.gBAlloc2, "docType")
        self.dataloopup2.setCharSize(10)
        self.description = UILabel(self.gBAlloc2, "description")
        self.description.setCharSize(40)
        self.HLayout2.addWidget(self.dataloopup2)
        self.HLayout2.addWidget(self.description)
        self.allocationLayout2.addWidget(self.labelDocType, 1, 0)
        self.allocationLayout2.addLayout(self.HLayout2, 1, 1)

        self.upperLayout2 = UIHLayout(None, "upperLayout2")
        self.upperLayout2.addSpacerItem(self.leftSpacer)
        self.upperLayout2.addWidget(self.gBAlloc2)
        self.upperLayout2.addSpacerItem(self.upperRightSpacer)
        self.LayoutOfWidget2.addLayout(self.upperLayout2)

        self.gBsites2 = UIGroupBoxWM(self.WidgetOfTabWidget2, "gBsites2")
        self.gBsites2.setTitle("Existing Entries")
        # Table
        self.tableHLayout = UIHLayout(self.gBsites2, "tableHLayout")
        self.table = UIAllocTable(
            self.gBsites2,
            "docType_" + self.klass.get_ui_prefix()[:-1] + "s",
            self.klass)

        self.tableHLayout.addWidget(self.table)
        self.tableLayout2 = UIHLayout(None, "tableLayout2")
        self.tableRightSpacer = UIHSpacer(self.container, "tableRightSpacer")
        self.tableLeftSpacer = UIHSpacer(self.container, "tableLeftSpacer")
        self.tableLayout2.addSpacerItem(self.tableLeftSpacer)
        self.tableLayout2.addWidget(self.gBsites2)
        self.tableLayout2.addSpacerItem(self.tableRightSpacer)
        self.LayoutOfWidget2.addLayout(self.tableLayout2)

        self.mainLayout.addWidget(self.container)
        Screen.layout.addWidget(self.screen)


class Dyn(UIDynamicButtonZoneInst):

    def setupUi(self):
        self.cancel = self.addDynamicButton("cancel")
        self.cancel.addImage("edit-undo.svg")
        self.cancel.setText(_("Cancel"))


class Dyn01(Dyn):

    def setupUi(self):
        super(Dyn01, self).setupUi()
        self.save = self.addDynamicButton("save")
        self.save.addImage("document-save.svg")
        self.save.setText(_("Save"))


class Dyn02(Dyn01):

    def setupUi(self):
        super(Dyn02, self).setupUi()
        self.delete = self.addDynamicButton("delete")
        self.delete.addImage("edit-clear.svg")
        self.delete.setText(_("Delete"))


class Dyn03(Dyn):
    pass

Dyn04 = Dyn01
Dyn05 = Dyn04
