#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [Claude Huchet]
[07:08/2013]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact: contact@sysgrove.com

Role :
    This module aims to defined general classes to build ui for
simple alchemy classes, the classes that inherite only from SGMixin.
I would like to use inheritance instead of specif xml files.


This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sqlalchemy import or_, and_, distinct
from sysgrove.ui.classes import UIDynamicButtonZoneInst
from sysgrove.core.document.ui import Dyn01, Dyn02, Dyn03, Dyn04
from sysgrove.core.document.ui import (
    Dyn_main01, Dyn_main02, Dyn_main03, Dyn_main04
)
from sysgrove.modules.masterdata.ui.gen import UIMain
from sysgrove.modules.sales.models import SalesDocumentHeader
from sysgrove.modules.purchase.models import PurchaseDocumentHeader
from sysgrove.ui.classes import (
    UIDynamicButtonZoneInst,
    UIWidget, UIGroupBox, UIForm, UIGroupBoxWM,
    UIVLayout, UIHLayout, UIGridLayout,
    UIVSpacer, UIHSpacer, UIDate,
    UILabel, UIDataLookup, UITree, UITabPanel, UITabWidget,
    LinkedTable, UIButton,
)
from sysgrove.ui.classes.table import build_columns
from sysgrove.ui.classes.widgets.multiSelectPopup import MultiSelect
from PyQt4 import QtCore, QtGui
from sysgrove.core.document.models import DocumentDetail
from sysgrove.modules.masterdata.models.document import (DocType,
                                                         DocNumbering,
                                                         DocGroup)
from sysgrove.modules.masterdata.models.status import Status
from sysgrove.core.document.models import DocumentHeader
from sysgrove.modules.masterdata.models.third_party import (ThirdParty,
                                                            ThirdPartyType)
from sysgrove.modules.masterdata.models.warehouse import Warehouse
from sysgrove.modules.masterdata.models.item import (ItemGroup,
                                                     ItemDetail,
                                                     ItemFamily)
from sysgrove.modules.masterdata.models.production import Lot
from sysgrove.utils import retreive_class
from sysgrove import i18n
_ = i18n.language.ugettext


class UI(UIMain):

    def setupUi(self, screen):
        self.screen = UIWidget(screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")

        self.spacer = UIVSpacer(self.screen, 'spacer')
        self.mainLayout.addSpacerItem(self.spacer)
        self.spacer = UIHSpacer(self.screen, 'spacer')
        self.mainLayout.addSpacerItem(self.spacer)

        self.tableGB = UIGroupBoxWM(self.screen, "TtableGB")
        self.tableGB.setTitle("Report")
        self.tableHLayout = UIHLayout(self.tableGB, "tableHLayout")

        # etablish Table
        from sysgrove.modules.reporting.models import Filter
        self.table = LinkedTable(self.tableGB,
                                 "table_%s" % Filter,
                                 columns=build_columns(Filter),
                                 klass=Filter)

        self.table.setVal(Filter.query.filter_by(code=self.code).all())
        self.table.doubleClicked.connect(self.doubleClickOnTableLine)
        self.tableHLayout.addWidget(self.table)

        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)

        self.mainLayout.addWidget(self.tableGB)

        self.spacer = UIVSpacer(self.screen, 'spacer')
        self.mainLayout.addSpacerItem(self.spacer)
        self.spacer = UIHSpacer(self.screen, 'spacer')
        self.mainLayout.addSpacerItem(self.spacer)

        screen.layout.addWidget(self.screen)

    def doubleClickOnTableLine(self, index):
        if self.code == "01":
            newContext = "6SP_main01"
            ui6SP_main01.idFilter = self.table.rowObject(index.row())
        elif self.code == "02":
            newContext = "6TP_main01"
            ui6TP_main01.idFilter = self.table.rowObject(index.row())
        elif self.code == "03":
            newContext = "6FI_main01"
            ui6FI_main01.idFilter = self.table.rowObject(index.row())
        from sysgrove.context import kernel
        kernel.set_current_context(newContext)


class UIFavoris(UIMain):

    def setupUi(self, screen):
        self.screen = UIWidget(screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        self.groupBox = UIGroupBox(self.screen, "FilterGB")
        self.groupBox.setTitle("Favoris")
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)
        screen.layout.addWidget(self.screen)


class UI_main(UIMain):

    def __init__(self, klass, code):
        self.idFilter = None
        self.fields = None
        self.list = None
        self.qtObject = None
        self.dateFrom = None

    def setupUi(self, screen):
        self.screen = UIWidget(screen, "screen")
        self.mainLayout = UIVLayout(self.screen, "mainLayout")
        spacer = UIVSpacer(self.screen, 'spacer')
        self.mainLayout.addSpacerItem(spacer)
        self.widgetGB = UIWidget(self.screen, "widgetGB")
        self.FieldsGB = UIGroupBox(self.widgetGB, "FieldsGB")
        self.FieldsGB.setTitle("Filters selection")

        self.HLayout = UIHLayout(self.widgetGB, "HLayout")
        spacer = UIHSpacer(self.widgetGB, 'spacer')
        self.HLayout.addSpacerItem(spacer)
        self.HLayout.addWidget(self.FieldsGB)

        spacer = UIHSpacer(self.widgetGB, 'spacer')
        self.HLayout.addSpacerItem(spacer)

        self.tableVLayout = UIVLayout(self.FieldsGB, "tableVLayout")

        label = UILabel(self.FieldsGB, "")
        label.setVal(self.idFilter.name)
        self.tableVLayout.addWidget(label)

        widgetTableField = UIWidget(self.FieldsGB, "gridWidget")
        self.gridLayout = UIGridLayout(widgetTableField, "gridLayout")
        self.tableVLayout.addWidget(widgetTableField)

        widgetTableDate = UIWidget(self.FieldsGB, "gridWidgetDate")
        self.gridLayoutDate = UIGridLayout(widgetTableDate, "gridLayout")
        self.tableVLayout.addWidget(widgetTableDate)
        # Create field of Filter
        # work, but for test, this is in comment
        #self.list = self.row.request.split(";")
        from sysgrove.modules.reporting.models import Filter
        self.list = getattr(Filter, 'list_fields', [])

        number = 0
        for l in self.list:
            self.items = []
            self.codeInDescription = None
            if l == "documentValueDate":
                label = UILabel(self.FieldsGB, "")
                label.setVal(_("Document Value Date"))
                self.dateFrom = UIDate(widgetTableDate,
                                       l + "Start",
                                       typeDouble="From")
                self.dateTill = UIDate(widgetTableDate,
                                       l + "End",
                                       typeDouble="Till")
                self.dateFrom.addLink(self.dateTill)
                self.dateTill.addLink(self.dateFrom)
                self.gridLayoutDate.addWidget(label, 0, 0)
                self.gridLayoutDate.addWidget(self.dateFrom, 0, 1)
                self.gridLayoutDate.addWidget(self.dateTill, 0, 2)
                spacer = UIHSpacer(widgetTableDate, "spacer")
                self.gridLayoutDate.addItem(spacer.qtObject, 0, 3)
            elif l == "reqDate":
                label = UILabel(self.FieldsGB, l + "Label")
                label.setVal(_("Required Date"))
                self.dateReqFrom = UIDate(widgetTableDate,
                                          l + "Start",
                                          typeDouble="From")
                self.dateReqTill = UIDate(widgetTableDate,
                                          l + "End",
                                          typeDouble="Till")
                self.dateReqFrom.addLink(self.dateReqTill)
                self.dateReqTill.addLink(self.dateReqFrom)
                self.gridLayoutDate.addWidget(label, 1, 0)
                self.gridLayoutDate.addWidget(self.dateReqFrom, 1, 1)
                self.gridLayoutDate.addWidget(self.dateReqTill, 1, 2)
                spacer = UIHSpacer(widgetTableDate, "spacer")
                self.gridLayoutDate.addItem(spacer.qtObject, 0, 3)
            else:
                # General case
                if l == "DocNumber":
                    self.fields = DocumentHeader.query.distinct(
                        DocumentHeader.documentNumber)
                    verbose_name = _("Document Number")
                    for field in self.fields:
                            self.items.append([field.documentNumber,
                                              field.documentNumber])
                else:
                    lClass = retreive_class(l)
                    self.fields = lClass.query.all()
                    if l == "DocGroup" or l == "Status":
                        for field in self.fields:
                            self.items.append([field.id, field.description])
                    else:
                        self.codeInDescription = True
                        for field in self.fields:
                            self.items.append([field.id,
                                               field.code + " - " +
                                               field.description])
                    verbose_name = lClass.verbose_name

                label = UILabel(self.FieldsGB, l + "Label")
                label.setVal(verbose_name)
                self.MultiSelect = MultiSelect(self.FieldsGB,
                                               l, self.items,
                                               self.codeInDescription)
                self.gridLayout.addWidget(label, number / 3, number % 3 * 2)
                self.gridLayout.addWidget(self.MultiSelect,
                                          number / 3, (number % 3) * 2 + 1)
            number += 1

        self.iconOk = QtGui.QIcon()
        self.iconOk.addPixmap(QtGui.QPixmap(':dialog-ok.svg'))
        self.iconSave = QtGui.QIcon()
        self.iconSave.addPixmap(QtGui.QPixmap(':document-save.svg'))
        self.iconPdf = QtGui.QIcon()
        self.iconPdf.addPixmap(QtGui.QPixmap(':pdf.png'))

        self.widgetHButton = UIWidget(self.FieldsGB, "widgetButton")
        self.layoutButtonH = UIHLayout(self.widgetHButton, "layoutHbutton")

        self.button = UIButton(self.widgetHButton, "OK")
        self.button.qtObject.setIcon(self.iconOk)
        self.button.qtObject.setIconSize(QtCore.QSize(30, 30))
        self.button.clicked.connect(self.display_table)

        self.buttonSaveXLS = UIButton(self.widgetHButton, "SaveXLS")
        self.buttonSaveXLS.qtObject.setIcon(self.iconSave)
        self.buttonSaveXLS.qtObject.setIconSize(QtCore.QSize(30, 30))

        self.buttonSavePDF = UIButton(self.widgetHButton, "SavePDF")
        self.buttonSavePDF.qtObject.setIcon(self.iconPdf)
        self.buttonSavePDF.qtObject.setIconSize(QtCore.QSize(30, 30))

        spacerButton = UIHSpacer(self.screen, 'spacerButton')
        self.layoutButtonH.addSpacerItem(spacerButton)
        self.layoutButtonH.addWidget(self.button)
        self.layoutButtonH.addWidget(self.buttonSaveXLS)
        self.layoutButtonH.addWidget(self.buttonSavePDF)
        self.layoutButtonH.addSpacerItem(spacerButton)
        self.tableVLayout.addWidget(self.widgetHButton)

        self.resultReportGB = UIGroupBoxWM(self.screen, "resultReport")
        self.resultReportGB.setTitle("Result Report")
        self.tableHLayout = UIHLayout(self.resultReportGB, "tableHLayout")

        self.mainLayout.addWidget(self.widgetGB)
        spacer = UIVSpacer(self.screen, 'spacer')
        self.mainLayout.addSpacerItem(spacer)
        self.mainLayout.addWidget(self.resultReportGB)
        spacer = UIVSpacer(self.screen, 'spacer')
        self.mainLayout.addSpacerItem(spacer)
        self.mainLayout.setMargin(0)
        self.mainLayout.setSpacing(0)
        screen.layout.addWidget(self.screen)

        # Create the table
        self.lc = ["documentHeader_docType_code",
                   "documentHeader_docType_description",
                   "documentHeader_documentNumber", "lineNumber",
                   "itemDetail_itemGroup_code",
                   "itemDetail_itemGroup_description", "itemDetail_code",
                   "itemDetail_description",
                   "lineQuantity", "lineQuantityOpen",
                   "lineUoM_code", "linePrice",
                   "documentHeader_currency_code",
                   "reqDeliveryStart", "reqDeliveryEnd",
                   "documentHeader_documentValueDate", "warehouse_code",
                   "warehouse_description",
                   "lot_code", "lot_description",
                   "documentHeader_incoterm_code",
                   "documentHeader_incoterm_description",
                   "documentHeader_paymentTerm_code",
                   "documentHeader_paymentTerm_description",
                   "documentHeader_status_code",
                   "documentHeader_status_description",
                   "documentHeader_id"]

        self.table = LinkedTable(
            self.resultReportGB,
            "table_DocumentDetail",
            columns=build_columns(DocumentDetail, self.lc),
            klass=DocumentDetail)
        self.tableHLayout.addWidget(self.table)

    def display_table(self, screen):
        try:
            self.tableHLayout.removeWidget(self.table)
        except:
            pass
        self.tableHLayout.addWidget(self.table)


class DynSP(UIDynamicButtonZoneInst):

    def setupUi(self):
        self.cancel = self.addDynamicButton("cancel")
        self.cancel.setText(_("Cancel"))
        self.cancel.setToolTip(u"Cancel")


class DynTP(DynSP):
    pass


class DynFI(DynSP):
    pass


class DynFA(DynSP):
    pass

ui6SP = UI(klass="SalesDocumentHeader", code='01')
ui6SP_main01 = UI_main(klass="SalesDocumentHeader", code='01')

ui6TP = UI(klass="transportType", code='02')
ui6TP_main01 = UI_main(klass="transportType", code='02')

ui6FI = UI(klass="SalesDocumentHeader", code='03')
ui6FI_main01 = UI_main(klass="SalesDocumentHeader", code='03')

ui6FA = UIFavoris(klass="SalesDocumentHeader", code='04')
ui6FA_main01 = UI_main(klass="SalesDocumentHeader", code='04')
