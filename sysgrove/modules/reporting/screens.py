#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sqlalchemy import or_, and_
from sysgrove.core.document.screens import ScreenDocument
from sysgrove.modules.sales.models import SalesDocumentHeader
from sysgrove.modules.reporting.models import Filter
from sysgrove.core.screen import (
    Screen,
    DisplayMixin,
    CreationMixin,
    UpdateMixin
)
from PyQt4 import QtCore, QtGui
import xlwt, os
from datetime import datetime
from sysgrove.ui.classes import (UISpace)
from sysgrove.core.document.models import DocumentDetail
from sysgrove.modules.masterdata.models.document import (DocType,
                                                         DocNumbering,
                                                         DocGroup)
from sysgrove.modules.masterdata.models.status import Status
from sysgrove.core.document.models import DocumentHeader
from sysgrove.modules.masterdata.models.third_party import (ThirdParty,
                                                            ThirdPartyType)
from sysgrove.modules.masterdata.models.warehouse import Warehouse
from sysgrove.modules.masterdata.models.item import (ItemGroup,
                                                     ItemDetail,
                                                     ItemFamily)
from sysgrove.modules.masterdata.models.production import Lot


class ScreenReporting(Screen):
    def __init__(self, *p, **k):
        self.klass = Filter
        super(ScreenReporting, self).__init__(*p, **k)

    def setActions(self):
        super(ScreenReporting, self).setActions()
        buttonOk = getattr(self.ui_space,
                           "OK",
                           None)
        if buttonOk:
            buttonOk.clicked.connect(self.display_table)

        multiSelectDocGroup = getattr(self.ui_space, "DocGroup", None)
        if multiSelectDocGroup:
            multiSelectDocGroup.lineEdit.textChanged.connect(
                self.link_DocGroup_DocType)

        multiSelectDocType = getattr(self.ui_space, "DocType", None)
        if multiSelectDocType:
            multiSelectDocType.lineEdit.textChanged.connect(
                self.link_Doctype_DocNumber)

        multiSelectItemFamily = getattr(self.ui_space, "ItemFamily", None)
        if multiSelectItemFamily:
            multiSelectItemFamily.lineEdit.textChanged.connect(
                self.link_ItemFamily_ItemGroup)

        multiSelectItemGroup = getattr(self.ui_space, "ItemGroup", None)
        if multiSelectItemGroup:
            multiSelectItemGroup.lineEdit.textChanged.connect(
                self.link_ItemGroup_ItemDetail)

        buttonXLS = getattr(self.ui_space, "SaveXLS", None)
        if buttonXLS:
            buttonXLS.clicked.connect(self.saveXLS)

        buttonPDF = getattr(self.ui_space, "SavePDF", None)
        if buttonPDF:
            buttonPDF.clicked.connect(self.savePDF)

    def savePDF(self):
        table_DocumentDetail = getattr(self.ui_space,
                                       "table_DocumentDetail",
                                       None)
        item = table_DocumentDetail.qtObject.selectedItems()
        line = table_DocumentDetail.getRow(item[0].row())
        docHeaderid = line[-1]
        if(docHeaderid):
            DocumentHeader.query.filter(
                DocumentHeader.id == docHeaderid).one().generatePdf()

    def saveXLS(self):
        from xlwt import Workbook
        table_DocumentDetail = getattr(self.ui_space,
                                       "table_DocumentDetail",
                                       None)
        doc = Workbook()
        # create page 1
        feuil1 = doc.add_sheet('feuille 1')
        # header for table
        i = 0
        for colum in table_DocumentDetail.columns:
            feuil1.write(0, i, colum[0])
            i += 1
        # values of table
        for row in range(table_DocumentDetail.rowCount()):
            for column in range(table_DocumentDetail.columnCount()):
                item = table_DocumentDetail.item(row, column)
                feuil1.write(row+1, column, unicode(
                    item.text()).encode('utf8'))
        path = QtGui.QFileDialog.getSaveFileName(
            None, 'Save File', '', 'excel(*.xls)')
        if path != '':
            doc.save(path)
            os.startfile(path)

    def link_DocGroup_DocType(self):
        multiSelectDocGroup = getattr(self.ui_space, "DocGroup")
        multiSelectDocType = getattr(self.ui_space, "DocType")
        multiSelectDocNumber = getattr(self.ui_space, "DocNumber")
        idList = []
        idList2 = []
        if multiSelectDocGroup.lcChoose == []:
            self.items = DocType.query.all()
            self.items2 = DocumentHeader.query.distinct(
                DocumentHeader.documentNumber)
        else:
            for i in multiSelectDocGroup.lcChoose:
                idList.append(i[0])
            self.items = DocType.query.filter(
                DocType.docGroup_id.in_(idList)).all()

            for i in self.items:
                idList2.append(i.id)
            self.items2 = DocumentHeader.query.filter(
                DocumentHeader.docType_id.in_(idList2)).distinct(
                    DocumentHeader.documentNumber)
        lc = []
        for i in self.items:
            lc.append([i.id, i.code+" - "+i.description])
        multiSelectDocType.popup.changeLc(lc)
        lc = []
        for i in self.items2:
            lc.append([i.documentNumber, i.documentNumber])
        multiSelectDocNumber.popup.changeLc(lc)

    def link_Doctype_DocNumber(self):
        multiSelectDocGroup = getattr(self.ui_space, "DocGroup")
        multiSelectDocType = getattr(self.ui_space, "DocType")
        multiSelectDocNumber = getattr(self.ui_space, "DocNumber")
        idList = []
        if multiSelectDocType.lcChoose == []:
            if multiSelectDocGroup.lcChoose == []:
                self.items = DocumentHeader.query.distinct(
                    DocumentHeader.documentNumber)
            else:
                self.link_DocGroup_DocType()
                return
        else:
            for i in multiSelectDocType.lcChoose:
                idList.append(i[0])
            self.items = DocumentHeader.query.filter(
                DocumentHeader.docType_id.in_(idList)).distinct(
                    DocumentHeader.documentNumber)
        lc = []
        for i in self.items:
            lc.append([i.documentNumber, i.documentNumber])
        multiSelectDocNumber.popup.changeLc(lc)

    def link_ItemFamily_ItemGroup(self):
        multiSelectItemFamily = getattr(self.ui_space, "ItemFamily")
        multiSelectItemGroup = getattr(self.ui_space, "ItemGroup")
        multiSelectItemDetail = getattr(self.ui_space, "ItemDetail")
        idList = []
        idList2 = []
        if multiSelectItemFamily.lcChoose == []:
            self.items = ItemGroup.query.all()
            self.items2 = ItemDetail.query.all()
        else:
            #value for itemgroup
            for i in multiSelectItemFamily.lcChoose:
                idList.append(i[0])
            self.items = ItemGroup.query.filter(
                ItemGroup.itemFamily_id.in_(idList)).all()
            #value for itemdetail
            for i in self.items:
                idList2.append(i.id)
            self.items2 = ItemDetail.query.filter(
                ItemDetail.itemGroup_id.in_(idList2)).all()
        lc = []
        for i in self.items:
            lc.append([i.id, i.code+" - "+i.description])
        multiSelectItemGroup.popup.changeLc(lc)
        lc = []
        for i in self.items2:
            lc.append([i.id, i.code+" - "+i.description])
        multiSelectItemDetail.popup.changeLc(lc)

    def link_ItemGroup_ItemDetail(self):
        multiSelectItemFamily = getattr(self.ui_space, "ItemFamily")
        multiSelectItemGroup = getattr(self.ui_space, "ItemGroup")
        multiSelectItemDetail = getattr(self.ui_space, "ItemDetail")

        if multiSelectItemGroup.lcChoose == []:
            if multiSelectItemFamily.lcChoose == []:
                self.items = ItemDetail.query.all()
            else:
                self.link_ItemFamily_ItemGroup()
                return
        else:
            idList = []
            for i in multiSelectItemGroup.lcChoose:
                idList.append(i[0])
            self.items = ItemDetail.query.filter(
                ItemDetail.itemGroup_id.in_(idList)).all()
        lc = []
        for i in self.items:
            lc.append([i.id, i.code+" - "+i.description])
        multiSelectItemDetail.popup.changeLc(lc)

    def display_table(self):
        v = DocumentDetail.query
        v = v.join(DocumentHeader)
        v = v.join(ItemDetail)
        v = v.join(DocType)
        v = v.join(ThirdParty, and_(
            DocumentHeader.soldToCode_id == ThirdParty.id))
        # boucle for fields
        #self.row = Filter.query.filter_by(id=self.idFilter).first()
        #self.list = self.row.request.split(";")
        from sysgrove.modules.reporting.models import Filter
        self.list = getattr(Filter, 'list_fields', [])
        for l in self.list:
            idList = []
            if l == "documentValueDate":
                self.dateFrom = getattr(self.ui_space,
                                        "documentValueDateStart",
                                        None)
                self.dateTill = getattr(self.ui_space,
                                        "documentValueDateEnd",
                                        None)
                if(self.dateFrom.dateFormat.text() != "//" and
                   self.dateTill.dateFormat.text() != "//"):

                    dateFromRef = datetime.strptime(
                        str(self.dateFrom.dateFormat.text()),
                        '%d/%m/%Y').date()
                    dateTillRef = datetime.strptime(
                        str(self.dateTill.dateFormat.text()),
                        '%d/%m/%Y').date()
                    v = v.filter(
                        DocumentHeader.documentValueDate >= dateFromRef,
                        DocumentHeader.documentValueDate <= dateTillRef)
            elif l == "reqDate":
                self.dateReqFrom = getattr(self.ui_space, "reqDateStart", None)
                self.dateReqTill = getattr(self.ui_space, "reqDateEnd", None)

                if(self.dateReqFrom.dateFormat.text() != "//"
                   and self.dateReqTill.dateFormat.text() != "//"):
                    dateFromRef = datetime.strptime(
                        str(self.dateReqFrom.dateFormat.text()),
                        '%d/%m/%Y').date()
                    dateTillRef = datetime.strptime(
                        str(self.dateReqTill.dateFormat.text()),
                        '%d/%m/%Y').date()
                    v = v.filter(
                        or_(and_(
                            DocumentDetail.reqDeliveryStart >= dateFromRef,
                            DocumentDetail.reqDeliveryStart <= dateTillRef),
                            and_(
                            DocumentDetail.reqDeliveryEnd >= dateFromRef,
                            DocumentDetail.reqDeliveryEnd <= dateTillRef)))
            elif getattr(self.ui_space.screen, l).lcChoose != []:
                if l == "DocType":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(DocumentHeader.docType_id.in_(idList))

                elif l == "DocNumber":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(str(i[0]))
                    v = v.filter(DocumentHeader.documentNumber.in_(idList))

                elif l == "Status":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(DocumentHeader.status_id.in_(idList))
                elif l == "ItemFamily":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(ItemGroup.itemFamily_id.in_(idList))

                elif l == "ItemGroup":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(ItemDetail.itemGroup_id.in_(idList))

                elif l == "ItemDetail":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(DocumentDetail.itemDetail_id.in_(idList))

                elif l == "ThirdParty":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(DocumentHeader.soldToCode_id.in_(idList))
                elif l == "ThirdPartyType":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(
                        ThirdParty.thirdPartyTypes.any(
                            ThirdPartyType.id.in_(idList)))
                elif l == "Warehouse":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(DocumentDetail.warehouse_id.in_(idList))

                elif l == "Lot":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(DocumentDetail.lot_id.in_(idList))

                elif l == "DocGroup":
                    for i in getattr(self.ui_space.screen, l).lcChoose:
                        idList.append(i[0])
                    v = v.filter(DocType.docGroup_id.in_(idList))

        table_DocumentDetail = getattr(self.ui_space,
                                       "table_DocumentDetail",
                                       None)
        self.ui_space.table_DocumentDetail.setVal(v.all())


class Screen6SP(ScreenReporting):
    pass


class Screen6TP(ScreenReporting):
    pass


class Screen6FI(ScreenReporting):
    pass


class Screen6FA(ScreenReporting):
    pass


class Screen6SP_main01(ScreenReporting):
    pass


class Screen6TP_main01(ScreenReporting):
    pass


class Screen6FI_main01(ScreenReporting):
    pass


class Screen6FA_main01(ScreenReporting):
    pass
