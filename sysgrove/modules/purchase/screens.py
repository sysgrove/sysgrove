#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.modules.purchase.models import PurchaseDocumentHeader
from sysgrove.core.document.screens import ScreenDocument
from sysgrove.core.document.screens import ScreenDocument01
from sysgrove.core.document.screens import ScreenDocument_main
from sysgrove.core.document.screens import ScreenDocument_main04


class ScreenPurchase(ScreenDocument):

    def __init__(self, *p, **k):
        self.klass = PurchaseDocumentHeader
        super(ScreenPurchase, self).__init__(*p, **k)


class ScreenPurchase01(ScreenDocument01):

    def __init__(self, *p, **k):
        self.klass = PurchaseDocumentHeader
        super(ScreenPurchase01, self).__init__(*p, **k)


class ScreenPurchase_main(ScreenDocument_main):

    def __init__(self, *p, **k):
        self.klass = PurchaseDocumentHeader
        super(ScreenPurchase_main, self).__init__(*p, **k)


class ScreenPurchase_main04(ScreenDocument_main04):

    def __init__(self, *p, **k):
        self.klass = PurchaseDocumentHeader
        super(ScreenPurchase_main04, self).__init__(*p, **k)


class ScreenPurchase02(ScreenPurchase):
    pass


class ScreenPurchase03(ScreenPurchase):
    pass


class ScreenPurchase04(ScreenPurchase):
    pass


#
# Purchase Bids
#
class Screen2BI01(ScreenPurchase01):
    pass


class Screen2BI02(ScreenPurchase02):
    pass


class Screen2BI03(ScreenPurchase03):
    pass


class Screen2BI04(ScreenPurchase04):
    pass


class Screen2BI_main01(ScreenPurchase_main):
    pass


class Screen2BI_main02(ScreenPurchase_main):
    pass


class Screen2BI_main03(ScreenPurchase_main):
    pass


class Screen2BI_main04(ScreenPurchase_main04):
    pass


#
# Purchase Quotations
#
class Screen2CT01(ScreenPurchase01):
    pass


class Screen2CT02(ScreenPurchase02):
    pass


class Screen2CT03(ScreenPurchase03):
    pass


class Screen2CT04(ScreenPurchase04):
    pass


class Screen2CT_main01(ScreenPurchase_main):
    pass


class Screen2CT_main02(ScreenPurchase_main):
    pass


class Screen2CT_main03(ScreenPurchase_main):
    pass


class Screen2CT_main04(ScreenPurchase_main04):
    pass


#
# Purchase Order
#
class Screen2OR01(ScreenPurchase01):
    pass


class Screen2OR02(ScreenPurchase02):
    pass


class Screen2OR03(ScreenPurchase03):
    pass


class Screen2OR04(ScreenPurchase04):
    pass


class Screen2OR_main01(ScreenPurchase_main):
    pass


class Screen2OR_main02(ScreenPurchase_main):
    pass


class Screen2OR_main03(ScreenPurchase_main):
    pass


class Screen2OR_main04(ScreenPurchase_main04):
    pass


#
# Purchase Release Orders
#
class Screen2RO01(ScreenPurchase01):
    pass


class Screen2RO02(ScreenPurchase02):
    pass


class Screen2RO03(ScreenPurchase03):
    pass


class Screen2RO04(ScreenPurchase04):
    pass


class Screen2RO_main01(ScreenPurchase_main):
    pass


class Screen2RO_main02(ScreenPurchase_main):
    pass


class Screen2RO_main03(ScreenPurchase_main):
    pass


class Screen2RO_main04(ScreenPurchase_main04):
    pass


#
# Purchase Inbound Delivery
#
class Screen2GR01(ScreenPurchase01):
    pass


class Screen2GR02(ScreenPurchase02):
    pass


class Screen2GR03(ScreenPurchase03):
    pass


class Screen2GR04(ScreenPurchase04):
    pass


class Screen2GR_main01(ScreenPurchase_main):
    pass


class Screen2GR_main02(ScreenPurchase_main):
    pass


class Screen2GR_main03(ScreenPurchase_main):
    pass


class Screen2GR_main04(ScreenPurchase_main04):
    pass


#
# Purchase Invoices
#
class Screen2IN01(ScreenPurchase01):
    pass


class Screen2IN02(ScreenPurchase02):
    pass


class Screen2IN03(ScreenPurchase03):
    pass


class Screen2IN04(ScreenPurchase04):
    pass


class Screen2IN_main01(ScreenPurchase_main):
    pass


class Screen2IN_main02(ScreenPurchase_main):
    pass


class Screen2IN_main03(ScreenPurchase_main):
    pass


class Screen2IN_main04(ScreenPurchase_main04):
    pass
