#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [Claude Huchet]
[07:08/2013]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact: contact@sysgrove.com

Role :
    This module aims to defined general classes to build ui for
simple alchemy classes, the classes that inherite only from SGMixin.
I would like to use inheritance instead of specif xml files.


This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sysgrove.core.document.ui import Dyn01, Dyn02, Dyn03, Dyn04
from sysgrove.core.document.ui import (
    Dyn_main01, Dyn_main02, Dyn_main03, Dyn_main04
)
from sysgrove.core.document.ui import UI01, UI, UI_main, UI_main04
from sysgrove.modules.purchase.models import PurchaseDocumentHeader


# 2BI Bids
ui2BI01 = UI01(klass=PurchaseDocumentHeader)
ui2BI02 = UI(klass=PurchaseDocumentHeader, code='02')
ui2BI03 = UI(klass=PurchaseDocumentHeader, code='03')
ui2BI04 = UI(klass=PurchaseDocumentHeader, code='04')
ui2BI_main01 = UI_main(klass=PurchaseDocumentHeader, code='01')
ui2BI_main02 = UI_main(klass=PurchaseDocumentHeader, code='02')
ui2BI_main03 = UI_main(klass=PurchaseDocumentHeader, code='03')
ui2BI_main04 = UI_main04(klass=PurchaseDocumentHeader, code='04')

# 2CT Contracts
ui2CT01 = UI01(klass=PurchaseDocumentHeader)
ui2CT02 = UI(klass=PurchaseDocumentHeader, code='02')
ui2CT03 = UI(klass=PurchaseDocumentHeader, code='03')
ui2CT04 = UI(klass=PurchaseDocumentHeader, code='04')
ui2CT_main01 = UI_main(klass=PurchaseDocumentHeader, code='01')
ui2CT_main02 = UI_main(klass=PurchaseDocumentHeader, code='02')
ui2CT_main03 = UI_main(klass=PurchaseDocumentHeader, code='03')
ui2CT_main04 = UI_main04(klass=PurchaseDocumentHeader, code='04')

# 2OR Orders
ui2OR01 = UI01(klass=PurchaseDocumentHeader, price=True)
ui2OR02 = UI(klass=PurchaseDocumentHeader, code='02')
ui2OR03 = UI(klass=PurchaseDocumentHeader, code='03')
ui2OR04 = UI(klass=PurchaseDocumentHeader, code='04')
ui2OR_main01 = UI_main(klass=PurchaseDocumentHeader, code='01')
ui2OR_main02 = UI_main(klass=PurchaseDocumentHeader, code='02')
ui2OR_main03 = UI_main(klass=PurchaseDocumentHeader, code='03')
ui2OR_main04 = UI_main04(klass=PurchaseDocumentHeader, code='04')

# 2RO Release Orders
ui2RO01 = UI01(klass=PurchaseDocumentHeader)
ui2RO02 = UI(klass=PurchaseDocumentHeader, code='02')
ui2RO03 = UI(klass=PurchaseDocumentHeader, code='03')
ui2RO04 = UI(klass=PurchaseDocumentHeader, code='04')
ui2RO_main01 = UI_main(klass=PurchaseDocumentHeader, code='01')
ui2RO_main02 = UI_main(klass=PurchaseDocumentHeader, code='02')
ui2RO_main03 = UI_main(klass=PurchaseDocumentHeader, code='03')
ui2RO_main04 = UI_main04(klass=PurchaseDocumentHeader, code='04')


# 2GR Inbound Delivery
ui2GR01 = UI01(klass=PurchaseDocumentHeader)
ui2G202 = UI(klass=PurchaseDocumentHeader, code='02')
ui2GR03 = UI(klass=PurchaseDocumentHeader, code='03')
ui2GR04 = UI(klass=PurchaseDocumentHeader, code='04')
ui2GR_main01 = UI_main(klass=PurchaseDocumentHeader, code='01')
ui2GR_main02 = UI_main(klass=PurchaseDocumentHeader, code='02')
ui2GR_main03 = UI_main(klass=PurchaseDocumentHeader, code='03')
ui2GR_main04 = UI_main04(klass=PurchaseDocumentHeader, code='04')


# Invoice 2IN
ui2IN01 = UI01(klass=PurchaseDocumentHeader)
ui2IN02 = UI(klass=PurchaseDocumentHeader, code='02')
ui2IN03 = UI(klass=PurchaseDocumentHeader, code='03')
ui2IN04 = UI(klass=PurchaseDocumentHeader, code='04')
ui2IN_main01 = UI_main(klass=PurchaseDocumentHeader, code='01')
ui2IN_main02 = UI_main(klass=PurchaseDocumentHeader, code='02')
ui2IN_main03 = UI_main(klass=PurchaseDocumentHeader, code='03')
ui2IN_main04 = UI_main04(klass=PurchaseDocumentHeader, code='04')
