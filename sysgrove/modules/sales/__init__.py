#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sysgrove.utils import find_key
from sysgrove.config import CONFIG
from sysgrove.modules import ModuleData
from sysgrove import i18n
_ = i18n.language.ugettext
# This correspond to a "standard" customization of the system
# This init file is used to initiate table such as Menu one

_menu = [

    ('1QT', _('Quotations'), [
        ('1QT01', _('Creation'), []),
        ('1QT02', _('Modification'), []),
        ('1QT03', _('Display'), []),
        ('1QT04', _('Document Flow'), []),
    ], 'sysgrove.modules.sales.models.SalesDocumentHeader', 1),
    ('1CT', _('Contracts'), [
        ('1CT01', _('Creation'), []),
        ('1CT02', _('Modification'), []),
        ('1CT03', _('Display'), []),
        ('1CT04', _('Document Flow'), []),
    ], 'sysgrove.modules.sales.models.SalesDocumentHeader', 2),
    ('1OR', _('Orders'), [
        ('1OR01', _('Creation'), []),
        ('1OR02', _('Modification'), []),
        ('1OR03', _('Display'), []),
        ('1OR04', _('Document Flow'), []),
    ], 'sysgrove.modules.sales.models.SalesDocumentHeader', 3),
    ('1RO', _('Release Orders'), [
        ('1RO01', _('Creation'), []),
        ('1RO02', _('Modification'), []),
        ('1RO03', _('Display'), []),
        ('1RO04', _('Document Flow'), []),
    ], 'sysgrove.modules.sales.models.SalesDocumentHeader', 4),
    ('1GI', _('Outbound Delivery'), [
        ('1GI01', _('Creation'), []),
        ('1GI02', _('Modification'), []),
        ('1GI03', _('Display'), []),
        ('1GI04', _('Document Flow'), []),
    ], 'sysgrove.modules.sales.models.SalesDocumentHeader', 5),
    ('1IN', _('Invoices'), [
        ('1IN01', _('Creation'), []),
        ('1IN02', _('Modification'), []),
        ('1IN03', _('Display'), []),
        ('1IN04', _('Document Flow'), []),
    ], 'sysgrove.modules.sales.models.SalesDocumentHeader', 6),
]


data = ModuleData(
    find_key(__package__, CONFIG['modules']),
    u'Sales',
    'sales.svg',
    _menu
)
