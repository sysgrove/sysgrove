#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [Claude Huchet]
[07:08/2013]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact: contact@sysgrove.com

Role :
    This module aims to defined general classes to build ui for
simple alchemy classes, the classes that inherite only from SGMixin.
I would like to use inheritance instead of specif xml files.


This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""

from sysgrove.core.document.ui import Dyn01, Dyn02, Dyn03, Dyn04
from sysgrove.core.document.ui import (
    Dyn_main01, Dyn_main02, Dyn_main03, Dyn_main04
)
from sysgrove.core.document.ui import UI01, UI, UI_main, UI_main04
from sysgrove.modules.sales.models import SalesDocumentHeader


# 1QT Quotations
ui1QT01 = UI01(klass=SalesDocumentHeader)
ui1QT02 = UI(klass=SalesDocumentHeader, code='02')
ui1QT03 = UI(klass=SalesDocumentHeader, code='03')
ui1QT04 = UI(klass=SalesDocumentHeader, code='04')
ui1QT_main01 = UI_main(klass=SalesDocumentHeader, code='01')
ui1QT_main02 = UI_main(klass=SalesDocumentHeader, code='02')
ui1QT_main03 = UI_main(klass=SalesDocumentHeader, code='03')
ui1QT_main04 = UI_main04(klass=SalesDocumentHeader, code='04')

# 1CT Contracts
ui1CT01 = UI01(klass=SalesDocumentHeader)
ui1CT02 = UI(klass=SalesDocumentHeader, code='02')
ui1CT03 = UI(klass=SalesDocumentHeader, code='03')
ui1CT04 = UI(klass=SalesDocumentHeader, code='04')
ui1CT_main01 = UI_main(klass=SalesDocumentHeader, code='01')
ui1CT_main02 = UI_main(klass=SalesDocumentHeader, code='02')
ui1CT_main03 = UI_main(klass=SalesDocumentHeader, code='03')
ui1CT_main04 = UI_main04(klass=SalesDocumentHeader, code='04')

# 1OR Orders
ui1OR01 = UI01(klass=SalesDocumentHeader, price=True)
ui1OR02 = UI(klass=SalesDocumentHeader, code='02')
ui1OR03 = UI(klass=SalesDocumentHeader, code='03')
ui1OR04 = UI(klass=SalesDocumentHeader, code='04')
ui1OR_main01 = UI_main(klass=SalesDocumentHeader, code='01')
ui1OR_main02 = UI_main(klass=SalesDocumentHeader, code='02')
ui1OR_main03 = UI_main(klass=SalesDocumentHeader, code='03')
ui1OR_main04 = UI_main04(klass=SalesDocumentHeader, code='04')

# 1RO Release Orders
ui1RO01 = UI01(klass=SalesDocumentHeader)
ui1RO02 = UI(klass=SalesDocumentHeader, code='02')
ui1RO03 = UI(klass=SalesDocumentHeader, code='03')
ui1RO04 = UI(klass=SalesDocumentHeader, code='04')
ui1RO_main01 = UI_main(klass=SalesDocumentHeader, code='01')
ui1RO_main02 = UI_main(klass=SalesDocumentHeader, code='02')
ui1RO_main03 = UI_main(klass=SalesDocumentHeader, code='03')
ui1RO_main04 = UI_main04(klass=SalesDocumentHeader, code='04')


# 1GI Outbound Delivery
ui1GI01 = UI01(klass=SalesDocumentHeader)
ui1GI02 = UI(klass=SalesDocumentHeader, code='02')
ui1GI03 = UI(klass=SalesDocumentHeader, code='03')
ui1GI04 = UI(klass=SalesDocumentHeader, code='04')
ui1GI_main01 = UI_main(klass=SalesDocumentHeader, code='01')
ui1GI_main02 = UI_main(klass=SalesDocumentHeader, code='02')
ui1GI_main03 = UI_main(klass=SalesDocumentHeader, code='03')
ui1GI_main04 = UI_main04(klass=SalesDocumentHeader, code='04')


# Invoice 1IN
ui1IN01 = UI01(klass=SalesDocumentHeader)
ui1IN02 = UI(klass=SalesDocumentHeader, code='02')
ui1IN03 = UI(klass=SalesDocumentHeader, code='03')
ui1IN04 = UI(klass=SalesDocumentHeader, code='04')
ui1IN_main01 = UI_main(klass=SalesDocumentHeader, code='01')
ui1IN_main02 = UI_main(klass=SalesDocumentHeader, code='02')
ui1IN_main03 = UI_main(klass=SalesDocumentHeader, code='03')
ui1IN_main04 = UI_main04(klass=SalesDocumentHeader, code='04')
