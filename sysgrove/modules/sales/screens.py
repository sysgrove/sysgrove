#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : [name of the individuals]
[date of creation]
Version 0.0

Revisions:
[revision #]
[date of revision]
[reason for revision]

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.


----
"""
from sysgrove.modules.sales.models import SalesDocumentHeader
from sysgrove.core.document.screens import ScreenDocument
from sysgrove.core.document.screens import ScreenDocument01
from sysgrove.core.document.screens import ScreenDocument_main
from sysgrove.core.document.screens import ScreenDocument_main04


class ScreenSales(ScreenDocument):

    def __init__(self, *p, **k):

        self.klass = SalesDocumentHeader
        super(ScreenSales, self).__init__(*p, **k)


class ScreenSales01(ScreenDocument01):

    def __init__(self, *p, **k):
        self.klass = SalesDocumentHeader
        super(ScreenSales01, self).__init__(*p, **k)


class ScreenSales_main(ScreenDocument_main):

    def __init__(self, *p, **k):
        self.klass = SalesDocumentHeader
        super(ScreenSales_main, self).__init__(*p, **k)


class ScreenSales_main04(ScreenDocument_main04):

    def __init__(self, *p, **k):
        self.klass = SalesDocumentHeader
        super(ScreenSales_main04, self).__init__(*p, **k)


class ScreenSales02(ScreenSales):
    pass


class ScreenSales03(ScreenSales):
    pass


class ScreenSales04(ScreenSales):
    pass


#
# Sales Quotations
#
class Screen1QT01(ScreenSales01):
    pass


class Screen1QT02(ScreenSales02):
    pass


class Screen1QT03(ScreenSales03):
    pass


class Screen1QT04(ScreenSales04):
    pass


class Screen1QT_main01(ScreenSales_main):
    pass


class Screen1QT_main02(ScreenSales_main):
    pass


class Screen1QT_main03(ScreenSales_main):
    pass


class Screen1QT_main04(ScreenSales_main04):
    pass


#
# Sales Contracts
#
class Screen1CT01(ScreenSales01):
    pass


class Screen1CT02(ScreenSales02):
    pass


class Screen1CT03(ScreenSales03):
    pass


class Screen1CT04(ScreenSales04):
    pass


class Screen1CT_main01(ScreenSales_main):
    pass


class Screen1CT_main02(ScreenSales_main):
    pass


class Screen1CT_main03(ScreenSales_main):
    pass


class Screen1CT_main04(ScreenSales_main04):
    pass


#
# Sales Order
#
class Screen1OR01(ScreenSales01):
    pass


class Screen1OR02(ScreenSales02):
    pass


class Screen1OR03(ScreenSales03):
    pass


class Screen1OR04(ScreenSales04):
    pass


class Screen1OR_main01(ScreenSales_main):
    pass


class Screen1OR_main02(ScreenSales_main):
    pass


class Screen1OR_main03(ScreenSales_main):
    pass


class Screen1OR_main04(ScreenSales_main04):
    pass


#
# Sales Release Orders
#
class Screen1RO01(ScreenSales01):
    pass


class Screen1RO02(ScreenSales02):
    pass


class Screen1RO03(ScreenSales03):
    pass


class Screen1RO04(ScreenSales04):
    pass


class Screen1RO_main01(ScreenSales_main):
    pass


class Screen1RO_main02(ScreenSales_main):
    pass


class Screen1RO_main03(ScreenSales_main):
    pass


class Screen1RO_main04(ScreenSales_main04):
    pass


#
# Sales Outbound Delivery
#
class Screen1GI01(ScreenSales01):
    pass


class Screen1GI02(ScreenSales02):
    pass


class Screen1GI03(ScreenSales03):
    pass


class Screen1GI04(ScreenSales04):
    pass


class Screen1GI_main01(ScreenSales_main):
    pass


class Screen1GI_main02(ScreenSales_main):
    pass


class Screen1GI_main03(ScreenSales_main):
    pass


class Screen1GI_main04(ScreenSales_main04):
    pass


#
# Sales Invoices
#
class Screen1IN01(ScreenSales01):
    pass


class Screen1IN02(ScreenSales02):
    pass


class Screen1IN03(ScreenSales03):
    pass


class Screen1IN04(ScreenSales04):
    pass


class Screen1IN_main01(ScreenSales_main):
    pass


class Screen1IN_main02(ScreenSales_main):
    pass


class Screen1IN_main03(ScreenSales_main):
    pass


class Screen1IN_main04(ScreenSales_main04):
    pass
