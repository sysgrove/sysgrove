# Welcome to SysGrove® ERP system

More detail about SysGrove can be found here [SysGrove](http://sysgrove.com)

This tool is a still under development, the master branch is reflecting the daily development steps.
In the `test` branch (to be created soon), you will found a freeze of the project made every two weeks.
You can run the application from a python environment or as an excutable for windows 7 os. The Windows 7 build (or version) is not yet available, but you simply have to copy the `dist` folder and configure the `config.dat` file.

To run the application in a python environment, clone the repo and launch `ipython sysgrove/main.py`

SysGrove® runs with postgreSQL and sqlite, configuration of the database is done in the config.dat file. It needs a minimum set of data to be able to start. To initialize such a database, you will have to setup the `config.dat` file and run at least the `test/inidb.py` file, or `test/poplatedb2.py` to have some fake data. You can enter in the application with the `GUEST` user, its password is simply `guest`.