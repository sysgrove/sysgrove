#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton, S. Collins
07/02/2013
Version 1.0

    Role :

        - This file is the main file for the main program

revision #
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

"""
import StringIO
from sysgrove.main import init_db
init_db()
from sysgrove.models import Base


# BEGIN;
# SELECT setval(pg_get_serial_sequence('"DateType"','DateType_pk'), coalesce(max("DateType_pk"), 1), max("DateType_pk") IS NOT null) FROM "DateType";
# SELECT setval(pg_get_serial_sequence('"Country"','Country_pk'),
# coalesce(max("Country_pk"), 1), max("Country_pk") IS NOT null) FROM
# "Country";
# COMMIT;
frame = """SELECT setval(pg_get_serial_sequence('"%s"','%s'), coalesce(max("%s"), 1), max("%s") IS NOT null) FROM "%s";
"""


frame_id = """SELECT setval(pg_get_serial_sequence('"%s"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "%s";
"""

sg_tables = filter(lambda x: x.startswith('SG_'),
                   map(lambda x: x.name, Base.metadata.sorted_tables))


def select(name):
    if name.startswith('SG_'):
        return False
    if 'Allocation' in name:
        return False
    if name == 'PurchaseAndSalesDocumentPricingLink':
        return False
    if name == 'PurchaseAndSalesDocumentLogisticLink':
        return False
    return True

output = StringIO.StringIO()
output.write('BEGIN;\n')

for table_name in sg_tables:
    output.write(frame_id % (table_name, table_name))

for table in Base.metadata.sorted_tables:
    table_name = table.name
    if select(table_name):
        pk_name = "%s_pk" % table_name
        output.write(frame %
                     (table_name, pk_name, pk_name, pk_name, table_name))


output.write('COMMIT;\n')

contents = output.getvalue()
output.close()

print contents
