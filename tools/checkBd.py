#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  ____             ____
 / ___| _   _ ___ / ___|_ __ _____   _____
 \___ \| | | / __| |  _| '__/ _ \ \ / / _ \
  ___) | |_| \__ \ |_| | | | (_) \ V /  __/
 |____/ \__, |___/\____|_|  \___/ \_/ \___|
        |___/



Copyright SARL SysGrove
contributor(s) : G. Souveton, S. Collins
07/02/2013
Version 1.0

    Role :

        - This file is the main file for the main program

revision #
Date of revision:
reason for revision:

contact:
contact@sysgrove.com

This software is a computer program whose purpose is to manage the
logistic execution, production, analysis, finance, transportation,
master data, reporting and communication of end user companies.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

"""

"""
Very simple tools to check compliance between database
The 'ref' database must be build from the SQL Power Architect tool
and the 'sysgrove' is the database build by sqlalchemy create_all
methode from the models package of the sysgrove application.
The SQL Power Architect script is run as it is, thus table and
columns name loose their capitalized caractere

To call it, just call the 'compare' function, the parameters are obvious

TODO : deeper comparaison on columns

25/06/2013 : first simple version : we compare only columns and table name.


"""
import logging
import sqlsoup
from sqlalchemy import create_engine
from sysgrove.config import CONFIG
from sysgrove.utils import get_class
from sysgrove.models import Base, SG_Classes
from sysgrove.main import init_db

# init the logger
log = logging.getLogger('checkBd')
log.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('checkBd.log')
fh.setLevel(logging.DEBUG)
# create a consol handler
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
log.addHandler(fh)
log.addHandler(ch)


def set_sysgrove_engine():
    try:
        engine = create_engine(CONFIG['db'], echo=False)
    except ImportError as e:
        log.wwarning('Cannot used database configurations: %s' % e)
        engine = create_engine(CONFIG['db_default'], echo=False)
    return engine


def retreive(classname):
    for klass in Base.metadata.sorted_tables:
        if klass.name.lower() == classname:
            return klass.fullname


def set_ref_engine(name='ref', user='admin', passwd='123',
                   host='localhost', port='5432'):
    "The reference databse must be a postgresql database"
    conn = "postgresql+psycopg2://%s:%s@%s:%s/%s" % (
        user, passwd, host, port, name)
    return create_engine(conn, echo=False)


def compare_tables(sysTable, refTable):
    ref_columns = set(refTable.columns.keys())
    sys_columns = set(map(lambda x: x.lower(), sysTable.columns.keys()))
    extra = sys_columns.difference(ref_columns)
    lacking = ref_columns.difference(sys_columns)

    idem = extra == set([]) and lacking == set([])
    if not idem:
        log.info("+++ Table %s" % sysTable)
        if not extra == set([]):
            log.info('Extra columns %s' % list(extra))
        if not lacking == set([]):
            log.info('Forgotten columns %s' % list(lacking))
        log.info("+++ End %s" % sysTable)
        log.info("   ")


def compare(user='admin', passwd='123', detail=False):
    e_ref = set_ref_engine(user=user, passwd=passwd)
    # e_sysgrove = set_sysgrove_engine()

    db = sqlsoup.SQLSoup(e_ref)
    db._metadata.reflect()
    init_db()

    log.info('Classes number in SysGrove %s :' % len(Base.metadata.tables))
    log.info('Classes number in Ref %s : ' % len(db._metadata.tables))
    # log.info('Classes number in Ref %s : ' % len(db._metadata.sorted_tables))
    log.info("   ")

    # get the diff of classes, build from name
    ref_classes_name = set([])
    sysgrove_classes_name = set([])

    for klass in Base.metadata.sorted_tables:
        sysgrove_classes_name.add(klass.name.lower())

    for klass in db._metadata.tables.keys():
        ref_classes_name.add(klass)
    # for klass in db._metadata.sorted_tables:
    #     ref_classes_name.add(klass.fullname)

    nyi = ref_classes_name.difference(sysgrove_classes_name)
    removed = sysgrove_classes_name.difference(ref_classes_name)
    if not nyi == set([]):
        log.info('Not yet implemented classes %s ' % list(nyi))
    if not removed == set([]):
        log.info('Removed classes %s ' % list(removed))
    log.info("   ")

    if detail:
        common = sysgrove_classes_name.intersection(ref_classes_name)
        for name in common:
            # FIXME same name in differente package???? not possible
            rname = retreive(name)
            sg_klass = SG_Classes.query.filter_by(name=rname).first()
            if sg_klass:
                klass = get_class('%s.%s' % (sg_klass.package, sg_klass.name))
                compare_tables(klass.__table__, getattr(db, name)._table)
            else:
                compare_tables(Base.metadata.tables[
                               rname], getattr(db, name)._table)
